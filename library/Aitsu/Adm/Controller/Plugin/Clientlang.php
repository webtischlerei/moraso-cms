<?php


/**
 * @author Andreas Kummer, w3concepts AG
 * @copyright Copyright &copy; 2010, w3concepts AG
 */

/**
* edited by CK
*/
class Aitsu_Adm_Controller_Plugin_Clientlang extends Zend_Controller_Plugin_Abstract
{
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
		// Sprache
		$currentLanguage = \Moraso\Session::get('currentLanguage');

		if (empty($currentLanguage)) {
			$currentLanguage = 1;
		}

		\Moraso\Session::set('currentLanguage', $currentLanguage);
		Aitsu_Registry::get()->env->idlang = $currentLanguage;

		// Mandant (System wird bald abgeschafft)
		$currentClient = 1;

		\Moraso\Session::set('currentClient', $currentClient);
		Aitsu_Registry::get()->env->idclient = $currentClient;
	}
}