<?php


/**
 * @author Andreas Kummer, w3concepts AG
 * @copyright Copyright &copy; 2010, w3concepts AG
 */

class Aitsu_Cron_Bootstrap {

	protected function _initDefaultTimezone()
	{
		date_default_timezone_set('Europe/Berlin');
	}
	
	protected function _initBackendConfig() {

		Aitsu_Registry :: get()->config = Moraso_Config_Json :: getInstance();
	}

	protected function _initDatabaseConnection()
	{
		\Moraso\Database\Connection::init();
	}

	protected function _initAppStatus() {

		Aitsu_Application_Status :: isEdit(false);
		Aitsu_Application_Status :: isPreview(false);
		Aitsu_Application_Status :: setEnv('backend');
		// Aitsu_Application_Status :: lock();
	}

	protected function _initCrons() {

		$jobs = Aitsu_Util_Dir :: scan(APPLICATION_PATH . '/cron/jobs');

		foreach ($jobs as $job) {
			$jobName = basename($job, '.php');
			$className = 'CronJob_' . $jobName;
			include_once $job;
			$job = new $className ();
			$job->execute($jobName);
		}
	}

	public static function run() {

		static $running = false;

		if ($running) {
			throw new Exception('The bootstrap may only run once for each request.');
		}

		$instance = new self();

		try {
			$counter = 0;
			foreach (get_class_methods($instance) as $phase) {
				if (substr($phase, 0, strlen('_')) == '_') {
					call_user_func(array (
						$instance,
						$phase
					));
				}
				$counter++;
			}
		} catch (Exception $e) {
			trigger_error('Exception in ' . __FILE__ . ' on line ' . __LINE__ . ': ' . $e->getMessage());
			trigger_error("Stack trace: \n" . $e->getTraceAsString());
			exit ();
		}

		return $instance;
	}
}