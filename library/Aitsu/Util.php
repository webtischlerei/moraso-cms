<?php


/**
 * Aitsu utilities.
 * 
 * @author Anreas Kummer, w3concepts AG
 * @author Christian Kehres, webtischlerei
 * 
 * @copyright Copyright &copy; 2010, w3concepts AG
 * @copyright Copyright &copy; 2011 - 2014, webtischlerei
 */

class Aitsu_Util
{
	protected function __construct()
	{
	}

	public static function getInstance()
	{
		static $instance;

		if (!isset($instance)) {
			$instance = new self();
		}

		return $instance;
	}

	public static function getCategories($client, $lang)
	{
		return Aitsu_Db::fetchAll('' .
		'select ' .
		'	catlang.idcat as idcat, ' .
		'	catlang.name as catname, ' .
		'	count(parent.idcat) as level ' .
		'from ' .
		'	_cat as node ' .
		'	join _cat as parent ' .
		'	join _cat_lang as catlang ' .
		'where ' .
		'	node.lft between parent.lft and parent.rgt ' .
		'	and node.idcat = catlang.idcat ' .
		'	and parent.idclient = ? ' .
		'	and catlang.idlang = ? ' .
		'group by node.idcat ' .
		'order by node.lft ', array (
			$client,
			$lang
		));
	}

	public static function getCurrentUrl()
	{
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
			return 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		}

		return 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	}

	public static function parseSimpleIni($text, $base = null)
	{
		if (preg_match_all("/([^\\s*=]*)\\s*=\\s*([\"']?)(.*)\\2/", $text, $matches) == 0) {
			return (object) array ();
		}

		$return = $base == null ? (object) array () : $base;

		for ($i = 0; $i < count($matches[0]); $i++) {
			$current = & $return;
			$parts = explode('.', $matches[1][$i]);
			for ($j = 0; $j < count($parts); $j++) {
				if (!isset ($current-> {
					$parts[$j] })) {
					$current-> {
						$parts[$j] }
					= (object) array ();
				}
				$current = & $current-> {
					$parts[$j] };
				if ($j == count($parts) - 1) {
					if (strtolower($matches[3][$i]) == 'true') {
						$current = true;
					}
					elseif (strtolower($matches[3][$i]) == 'false') {
						$current = false;
					} else {
						$current = (strlen($matches[2][$i]) > 0) ? $matches[3][$i] : trim($matches[3][$i]);
					}
				}
			}
		}

		return $return;
	}

	public static function getIdArtLang($idart = null, $idlang = null)
	{
		if (empty($idart)) {
			$idart = Aitsu_Registry::get()->env->idart;
		}

		if (empty($idlang)) {
			$idlang = Aitsu_Registry::get()->env->idlang;
		}

		return Aitsu_Db::fetchOne("SELECT `idartlang` FROM `_art_lang` WHERE `idart` =:idart AND `idlang` =:idlang", array (
			':idart' => $idart,
			':idlang' => $idlang
		));
	}

	public static function getIdArt($idartlang = null)
	{
		if (empty($idartlang)) {
			$idartlang = Aitsu_Registry::get()->env->idartlang;
		}

		return Aitsu_Db::fetchOne("SELECT `idart` FROM `_art_lang` WHERE `idartlang` =:idartlang", array (
			':idartlang' => $idartlang
		));
	}

	public static function getIdCatByIdArt($idart = null)
	{
		if (empty($idart)) {
			$idart = Aitsu_Registry::get()->env->idart;
		}

		return Aitsu_Db::fetchOne("SELECT `idcat` FROM `_cat_art` WHERE `idart` =:idart", array (
			':idart' => $idart
		));
	}

	public static function getIdCatByIdArtlang($idartlang = null)
	{
		if (empty($idartlang)) {
			$idartlang = Aitsu_Registry::get()->env->idartlang;
		}

		$idart = self::getIdArt($idartlang);

		return self::getIdCatByIdArt($idart);
	}
}