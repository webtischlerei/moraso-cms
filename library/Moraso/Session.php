<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */

namespace Moraso;

use Zend\Session\Config\StandardConfig;
use Zend\Session\SessionManager;

use Zend\Session\Container;

use Zend\Session\Validator\HttpUserAgent;
use Zend\Session\Validator\RemoteAddr;

class Session
{
	/**
	* TODO
	*/
	public static function init()
	{
		$config = new StandardConfig();
		$config->setOptions(array(
			'name'                => 'moraso',
			'remember_me_seconds' => 1800
		));

		$manager = new SessionManager($config);

		$manager->getValidatorChain()->attach('session.validate', array(new HttpUserAgent(), 'isValid'));
		$manager->getValidatorChain()->attach('session.validate', array(new RemoteAddr(), 'isValid'));

		Container::setDefaultManager($manager);

		$container = self::createContainer();

		$container->sessionPeriod = 1800;
	}

	/**
	* TODO
	*/
	public static function createContainer($container = 'moraso')
	{
		$container = new Container($container);

		if (!isset($container->init)) {
			 $container->getManager()->regenerateId(true);
			 $container->init = 1;
		}

		return $container;
	}

	/**
	* TODO
	*/
	public static function set($item, $value, $container = 'moraso')
	{
		$container = new Container($container);

		$container->$item = $value;
	}

	/**
	* TODO
	*/
	public static function get($item, $container = 'moraso')
	{
		$container = new Container($container);

		return !empty($container->$item) ? $container->$item : '';
	}

	/**
	* TODO
	*/
	public static function destroy($container = 'moraso')
	{
		$container = new Container($container);

		$container->getManager()->destroy();
	}
}