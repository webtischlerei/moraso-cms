<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */
class Moraso_Bootstrap_EvalRequest
{
	protected $_redirectStep;

	public static function run()
	{
		$instance = new self();

		$obj = call_user_func(array (
			Aitsu_Registry::get()->config->rewrite->controller,
			'getInstance'
		));

		try {
			$obj->registerParams();
		} catch (Zend_Db_Statement_Exception $e) {
			echo $e->getMessage();
			exit ();
		}

		if (isset(Aitsu_Registry::get()->env->idart)) {
			self::setIdartlang(Aitsu_Registry::get()->env->idart, null);
		} elseif (isset(Aitsu_Registry::get()->env->idcat)) {
			self::setIdartlang(null, Aitsu_Registry::get()->env->idcat);
		}

		if (!isset(Aitsu_Registry::get()->env->idartlang)) {
			$altUrl = Aitsu_Rewrite_History::getInstance()->getAlternative();

			if ($altUrl !== false) {
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: " . $altUrl);
				exit;
			}

			if (!isset(Aitsu_Registry::get()->env->idlang)) {
				Aitsu_Registry::get()->env->idlang = Aitsu_Registry::get()->config->sys->language;
			}

			header("HTTP/1.0 404 Not Found");

			Aitsu_Registry::get()->env->idart = Aitsu_Registry::get()->config->sys->errorpage;

			self::setIdartlang(Aitsu_Registry::get()->env->idart);
		}

		$instance->_resolveRedirects();

		Aitsu_Rewrite_History::getInstance()->saveUrl();

		Aitsu_Registry::get()->env->locale = new Zend_Locale('de');
	}

	public static function setIdartlang($idart = null, $idcat = null)
	{
		$reg = Aitsu_Registry::get();

		if ($idart != null) {
			$results = Moraso_Db::fetchAllC('eternal', '' .
			'select ' .
			'	artlang.idartlang as idartlang, ' .
			'	catart.idcat as idcat, ' .
			'	catlang.public ' .
			'from _art_lang as artlang ' .
			'left join _cat_art as catart on artlang.idart = catart.idart ' .
			'left join _cat_lang as catlang on catart.idcat = catlang.idcat and catlang.idlang = artlang.idlang ' .
			'where ' .
			'	artlang.idart = :idart ' .
			'	and artlang.idlang = :idlang ' .
			'	and artlang.online = 1 ' .
			'', array (
				':idart' => $idart,
				':idlang' => $reg->env->idlang
			));

			if ($results) {
				$reg->env->idartlang 	= $results[0]['idartlang'];
				$reg->env->idcat 		= $results[0]['idcat'];
				$reg->env->ispublic 	= $results[0]['public'];
			} else {
				$reg->env->idartlang 	= null;
			}
			return;
		}

		$results = Moraso_Db::fetchAllC(60 * 60, '' .
		'select ' .
		'	catlang.startidartlang as idartlang, ' .
		'	artlang.idart as idart, ' .
		'	catlang.public ' .
		'from _cat_lang as catlang ' .
		'left join _art_lang as artlang on catlang.startidartlang = artlang.idartlang ' .
		'where ' .
		'	catlang.idcat = :idcat ' .
		'	and catlang.idlang = :idlang ' .
		'	and artlang.online = 1 ' .
		'', array (
			':idcat' => $idcat,
			':idlang' => $reg->env->idlang
		));

		if ($results) {
			$reg->env->idartlang 	= $results[0]['idartlang'];
			$reg->env->idart 		= $results[0]['idart'];
			$reg->env->ispublic 	= $results[0]['public'];
		} else {
			$reg->env->idartlang 	= null;
		}
	}

	protected function _resolveRedirects()
	{
		if ($this->_redirectStep > 5) {
			header("HTTP/1.0 404 Not Found");
			Aitsu_Registry::get()->env->idart = Aitsu_Registry::get()->config->errordoc;
			self::setIdartlang(Aitsu_Registry::get()->env->idart);
			return;
		}

		$this->_redirectStep++;

		$url = Moraso_Db::fetchOneC(60 * 60, "" .
		"select " .
		"	redirect_url " .
		"from _art_lang " .
		"where " .
		"	idartlang = :idlang " .
		"	and redirect = 1 " .
		"", array (
			':idlang' => Aitsu_Registry::get()->env->idartlang
		));

		if (!$url) {
			return;
		}

		if (!isset(Aitsu_Registry::get()->meta)) {
			Aitsu_Registry::get()->meta = array (
				'robots' => 'noindex'
			);
		} else {
			Aitsu_Registry::get()->meta['robots'] = 'noindex';
		}

		if (!preg_match('/^(idart|idcat){1}\\s*(\\d*)\\s*$/', $url, $match)) {
			header("Location: $url");
			exit ();
		}

		if (strtolower($match[1]) == 'idart') {
			Aitsu_Registry::get()->env->idart = $match[2];
			Aitsu_Registry::get()->env->idcat = null;
			self::setIdartlang($match[2], null);
		} else {
			Aitsu_Registry::get()->env->idart = null;
			Aitsu_Registry::get()->env->idcat = $match[2];
			self::setIdartlang(null, $match[2]);
		}

		if (!isset(Aitsu_Registry::get()->env->idartlang)) {
			header("HTTP/1.0 404 Not Found");
			Aitsu_Registry::get()->env->idart = Aitsu_Registry::get()->config->sys->errorpage;
			self::setIdartlang(Aitsu_Registry::get()->env->idart);
			return;
		}

		$this->_resolveRedirects();
	}
}