<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Skin
 */
class Moraso_Skin_Heredity
{
	public static function build()
	{
		$id = 'skinHeredity_Client_' . Moraso_Config::get('sys.client');
		
		if (($data = Moraso_Cache::load($id)) !== false) {
			return $data;
		}

		$data = self::_build();

		Moraso_Cache::save($id, $data, 31536000, array('skin'));

		return $data;
	}

	public static function get($get, $reverse = true)
	{
		$data = self::buildSkinBasedJsonFile($reverse);

		$parts = explode('.', $get);

		for ($i = 0; $i < count($parts); $i++) {
			if (!isset($data->$parts[$i])) {
				return false;
			}

			$data = $data->$parts[$i];
		}

		return $data;
	}

	public static function buildSkinBasedJsonFile($reverse = true)
	{
		$id = 'skinHeredityJson_Client_' . Moraso_Config::get('sys.client');

		if ($reverse) {
			$id .= '_reverse';
		}
		
		if (($json = Moraso_Cache::load($id)) !== false) {
			return $json;
		}

		$heredity = self::build();

		if ($reverse) {
			$heredity = array_reverse($heredity);
		}

		$json = '';
		foreach ($heredity as $skin) {
			$json_config = new Zend_Config_Json(APPLICATION_PATH . '/skins/' . $skin . '/skin.json', null, true);

			$json = !empty($json) ? $json->merge($json_config) : $json_config;
		}

		Moraso_Cache::save($id, $json, 31536000, array('skin'));

		return $json;
	}

	private static function _build(& $heredity = null, $skin = null)
	{
		if (empty($heredity)) {
			$heredity = array();
		}

		if (empty($skin)) {
			$skin = Moraso_Config::get('skin');
		}

		$heredity[] = $skin;

		$json_file_dest = APPLICATION_PATH . '/skins/' . $skin . '/skin.json';

		if (file_exists($json_file_dest) && is_readable($json_file_dest)) {
			$json_file_content = json_decode(file_get_contents($json_file_dest));

			if (isset($json_file_content->parent) && !empty($json_file_content->parent)) {
				self::_build($heredity, $json_file_content->parent);
			}
		}

		return $heredity;
	}
}