<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Db extends Aitsu_Db
{
    public static function filter($baseQuery, $limit = null, $offset = null, $filters = null, $orders = null, $groups = null)
    {
        $limit = is_null($limit) || !is_numeric($limit) ? 100 : $limit;
        $offset = is_null($offset) || !is_numeric($offset) ? 0 : $offset;
        $filters = is_array($filters) ? $filters : array();
        $orders = is_array($orders) ? $orders : array();
        $groups = is_array($groups) ? $groups : array();

        $filterClause = array();
        $filterValues = array();
        for ($i = 0; $i < count($filters); $i++) {
            $filterClause[] = $filters[$i]->clause . ' :value' . $i;
            $filterValues[':value' . $i] = $filters[$i]->value;
        }
        $where = count($filterClause) == 0 ? '' : 'where ' . implode(' and ', $filterClause);

        $orderBy = count($orders) == 0 ? '' : 'order by ' . implode(', ', $orders);
        $groupBy = count($groups) == 0 ? '' : 'group by ' . implode(', ', $groups);

        $results = Moraso_Db::fetchAll('' .
            $baseQuery .
            ' ' . $where .
            ' ' . $groupBy .
            ' ' . $orderBy .
            'limit ' . $offset . ', ' . $limit, $filterValues);

        $return = array();

        if ($results) {
            foreach ($results as $result) {
                $return[] = (object) $result;
            }
        }

        return $return;
    }

    public static function delete($from, array $where)
    {
        Moraso_Db_Simple::delete($from, $where);
    }

    public static function simpleFetch($select, $from, array $where = array(), $limit = 1, $caching = 0, array $orderBy = array())
    {
        return Moraso_Db_Simple::fetch($select, $from, $where, $limit, $caching, $orderBy);
    }
}