<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Log
 */
class Moraso_Log
{
	protected $_logger;

	protected function __construct()
	{
		$writer = new Moraso_Log_Writer_Db();
 
		$this->_logger = new Zend_Log($writer);
	}

	protected static function _getInstance()
	{
		static $instance;

		if (!isset ($instance)) {
			$instance = new self();
		}

		return $instance;
	}

	public static function errorHandler($errno, $errstr, $errfile, $errline, $errcontext)
	{	
		$instance = self::_getInstance();

		$errfile = substr($errfile, strlen(realpath(APPLICATION_PATH . '/../')));
		
		$errorHandlerMap = array(
			E_COMPILE_ERROR	    => Zend_Log::CRIT,
			E_CORE_ERROR        => Zend_Log::CRIT,
			E_RECOVERABLE_ERROR => Zend_Log::CRIT,			
			E_WARNING           => Zend_Log::WARN,
			E_CORE_WARNING      => Zend_Log::WARN,
			E_COMPILE_WARNING   => Zend_Log::WARN,
			E_USER_WARNING      => Zend_Log::WARN,
			E_ERROR             => Zend_Log::ERR,
			E_USER_ERROR        => Zend_Log::ERR,
			E_STRICT            => Zend_Log::DEBUG,
			E_NOTICE            => Zend_Log::NOTICE,
			E_USER_NOTICE       => Zend_Log::NOTICE,			
		);
      
		if (defined('E_DEPRECATED')) {
			$errorHandlerMap['E_DEPRECATED'] = Zend_Log::DEBUG;
		}
		if (defined('E_USER_DEPRECATED')) {
			$errorHandlerMap['E_USER_DEPRECATED'] = Zend_Log::DEBUG;
		}	
        
		$level = (isset($errorHandlerMap[$errno])) ? $errorHandlerMap[$errno] : Zend_Log::INFO;
		
		$instance->_logger->log($errstr . '[Thrown @:' . $errfile . ':' . $errline . ']', $level);
	}
}