<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Database
 */

namespace Moraso\Database;

use Zend\Config\Reader\Json as JsonReader;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;

class Connection
{
	public static function init()
	{
		$reader 		= new JsonReader();

		$data 			= $reader->fromFile(ROOT_PATH . '/config.json');

		$databaseParams = (object) $data['default']['database']['params'];

		$adapter 		= new DbAdapter(array(
			'driver' 	=> 'Pdo_Mysql',
			'database' 	=> $databaseParams->dbname,
			'username' 	=> $databaseParams->username,
			'password' 	=> $databaseParams->password,
			'prefix' 	=> 'mor_'
		));

		GlobalAdapterFeature::setStaticAdapter($adapter);
	}

	public static function getAdapter()
	{
		return GlobalAdapterFeature::getStaticAdapter();
	}

	public static function getDriver()
	{
		$adapter = self::getAdapter();

		return $adapter->getDriver();
	}

	public static function getConnection()
	{
		$driver = self::getDriver();

		return $driver->getConnection();
	}

	public static function getConnectionParameters($parameter)
	{
		$connection = self::getConnection();

		$parameters = $connection->getConnectionParameters();

		return $parameters[$parameter];
	}
}