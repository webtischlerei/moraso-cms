<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */

namespace Moraso\Acl;

use Moraso\CRUD;

class Privilege extends CRUD
{
	static $mapper 	= 'Moraso\Model\Acl\Privilege\Mapper';
}