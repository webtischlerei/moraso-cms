<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model
 */

namespace Moraso\Util;

use \DateTimeZone;
use \DateTime;
use \DateInterval;

class Date
{
	const TIMEZONE = "Europe/Berlin";

	public static function getDateTime($time = "now")
	{
		return new DateTime($time, self::getDateTimeZone());
	}

	public static function getDateTimeZone()
	{
		return new DateTimeZone(self::TIMEZONE);
	}

	public static function getCurrentDateTimeForDatabaseTableEntry()
	{
		return self::getCurrentDateTime("Y-m-d H:i:s");
	}

	public static function secondsUntilEndOfThis($type)
	{
		$now = self::getDateTime();

		switch ($type) {
			case "minute":
				$rule = "today " . $now->format("H") . ":" . $now->format("i") . ":59";
				break;
			case "hour":
				$rule = "today " . $now->format("H") . ":59:59";
				break;
			case "day":
				$rule = "today 23:59:59";
				break;
			case "week":
				$rule = "sunday this week 23:59:59";
				break;
			case "month":
				$rule = "last day of this month 23:59:59";
				break;
			case "quarter":
				$rule = "previous day 01." . (ceil($now->format('n')/3)*3)+1 . "." . $now->format('Y') . " 23:59:59";
				break;
			case "half-year":
				$rule = "previous day 01." . (ceil($now->format('n')/6)*6)+1 . "." . $now->format('Y') . " 23:59:59";
				break;
			case "year":
				$rule = "last day of december 23:59:59";
				break;
		}

		$target = self::getDateTime($rule);
		
		return $target->getTimestamp() - $now->getTimestamp();
	}

	/*
	* kleine Helfer
	*/
	public static function getCurrentDateTime($format = null)
	{
		$dateTime = self::getDateTime();

		if (is_null($format)) {
			return $dateTime;
		} else {
			return $dateTime->format($format);
		}
	}
}