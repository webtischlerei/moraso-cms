<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Service
 */

namespace Moraso\Service;

use Zend\ServiceManager\ServiceManager;

class Manager
{
	static $serviceManager;

	public static function init()
	{
		$serviceManager = new ServiceManager();

		self::$serviceManager = $serviceManager;
	}
	
	public static function get()
	{
		return self::$serviceManager;
	}
}