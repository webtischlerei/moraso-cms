<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Lang
 */

namespace Moraso\Model;

use Moraso\Model\AbstractModel;

class Lang extends AbstractModel
{
	protected $idlang, $lngsort, $idclient, $name, $longname, $active;
}