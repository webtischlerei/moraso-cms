<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Author
 */

namespace Moraso\Model;

class Author extends AbstractModel
{
	protected $name, $google_plus_id = null, $initials = null, $email = null;
}