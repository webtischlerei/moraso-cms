<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/User
 */

namespace Moraso\Model;

class User extends AbstractModel
{
	protected $salutation, $firstname, $lastname, $email, $id_eav_entity;
}