<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/ListUniversal
 */

namespace Moraso\Model\ListUniversal\ColumnValues;

use Moraso\Model\AbstractTable;

class Table extends AbstractTable
{
	protected $_name = 'list_universal_column_values';
}