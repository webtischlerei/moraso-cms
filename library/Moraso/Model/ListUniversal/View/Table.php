<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/List/Universal
 */

namespace Moraso\Model\ListUniversal\View;

use Moraso\Model\AbstractTable;
use Moraso\ListUniversal;

class Table extends AbstractTable
{
	protected $_name = 'list_universal_view_list_';

	public function __construct($id = null)
	{
		$this->_name = $this->_name . $id;

		parent::__construct();

		$columnNames = $this->getColumnNames();

		if (empty($columnNames)) {
			ListUniversal::createView($id);
		}
	}
}