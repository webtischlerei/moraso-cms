<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/ListUniversal
 */

namespace Moraso\Model\ListUniversal;

use Moraso\Model\AbstractModel;

class Cell extends AbstractModel
{
	protected $id_row, $id_column, $value_varchar, $value_int, $value_float, $value_text, $value_date, $value_datetime, $value_time, $value_bool;
}