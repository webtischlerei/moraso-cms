<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/ListUniversal
 */

namespace Moraso\Model\ListUniversal\Cell;

use Moraso\Model\AbstractMapper;

class Mapper extends AbstractMapper
{
	
}