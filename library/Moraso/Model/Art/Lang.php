<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Article
 */

namespace Moraso\Model\Art;

use Moraso\Model\AbstractModel;

class Lang extends AbstractModel
{
	protected 	$idartlang, 	$idart, 		$idlang, 		$title,
				$urlname, 		$pagetitle, 	$teasertitle, 	$summary,
				$online, 		$pubfrom, 		$pubuntil, 		$published,
				$redirect, 		$redirect_url, 	$artsort, 		$locked, 
				$configsetid, 	$config, 		$mainimage;
}