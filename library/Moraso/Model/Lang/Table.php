<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Article
 */

namespace Moraso\Model\Lang;

use Moraso\Model\AbstractTable;

class Table extends AbstractTable
{
	protected 	$_name 			= 'lang',
				$_primaryKey 	= 'idlang';
}