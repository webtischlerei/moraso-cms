<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Lang
 */

namespace Moraso\Model\Lang;

use Moraso\Model\AbstractMapper;

class Mapper extends AbstractMapper
{
	
}