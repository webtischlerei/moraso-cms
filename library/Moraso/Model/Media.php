<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Media
 */

namespace Moraso\Model;

class Media extends AbstractModel
{
	protected $mediaid, $idart, $filename, $size, $extension, $xtl, $ytl, $xbr, $ybr, $uploaded, $deleted;
}