<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model
 */

namespace Moraso\Model;

use Moraso\Util\Date;

use Moraso\Database\Connection as DatabaseConnection;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\TableGateway\Feature;
use Zend\Db\Metadata\Metadata;

abstract class AbstractTable extends AbstractTableGateway
{
	protected $table, $primaryKey, $featureSet;

	public function __construct()
	{
		$this->table 		= DatabaseConnection::getConnectionParameters('prefix') . $this->_name;
		$this->primaryKey 	= isset($this->_primaryKey) ? $this->_primaryKey : 'id';

		$this->featureSet 	= new Feature\FeatureSet();
		$this->featureSet->addFeature(new Feature\GlobalAdapterFeature());

		$this->initialize();
	}
 
	public function update($set, $where = null)
	{
		if ($this->tableHasColumn('last_modified')) {
			if (empty($set['last_modified'])) {
				$set['last_modified'] = Date::getCurrentDateTimeForDatabaseTableEntry();
			}
		}

		return parent::update($set, $where);
	}

	public function tableHasColumn($column)
	{
		$metadata 	= new Metadata($this->getAdapter());

		$table 		= $metadata->getTable($this->getTable());

		foreach ($table->getColumns() as $col) {
			if ($col->getName() == $column) {
				return true;
			}
		}

		return false;
	}

	public function getColumnNames()
	{
		$metadata 	= new Metadata($this->getAdapter());

		return $metadata->getColumnNames($this->table);
	}

	public function getPrimaryKey()
	{
		return $this->primaryKey;
	}
}