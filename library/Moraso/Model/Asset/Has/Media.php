<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/_Asset
 */

namespace Moraso\Model\Asset\Has;

use Moraso\Model\AbstractModel;

class Media extends AbstractModel
{
	protected $idasset, $mediaid;
}