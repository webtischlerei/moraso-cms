<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Asset
 */

namespace Moraso\Model\Asset\Has\Media;

use Moraso\Model\AbstractTable;

class Table extends AbstractTable
{
	protected $_name = 'assets_have_media';
}