<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Asset
 */

namespace Moraso\Model\Asset;

use Moraso\Model\AbstractMapper;

class Mapper extends AbstractMapper
{
	
}