<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/User
 */

namespace Moraso\Model\User;

use Moraso\Model\AbstractModel;

class Auth extends AbstractModel
{
	protected $user_id, $status, $username, $password, $access_from = null, $access_until = null, $last_login = null;
}