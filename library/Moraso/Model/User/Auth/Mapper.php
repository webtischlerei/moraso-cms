<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/User
 */

namespace Moraso\Model\User\Auth;

use Moraso\Model\AbstractMapper;

class Mapper extends AbstractMapper
{
	public function getSalt($securityLevel = 1)
	{
		if ($securityLevel == 1) {
			$salt 	= \Moraso_Config::get('sys.salt');
		} elseif ($securityLevel == 2) {
			$bytes 	= openssl_random_pseudo_bytes(20);
			$salt 	= bin2hex($bytes);
		}

		return $salt;
	}

	public function save(& $model, $salt = null, $updateUserAuthPass = true)
	{
		if ($updateUserAuthPass) {
			if (!empty($model->password) && (empty($salt) || is_null($salt))) {
				$salt = $this->getSalt();
			}

			if (!empty($model->password)) {
				$model->password = new \Zend\Db\Sql\Expression("SHA1('" . $salt . $model->password . $salt . "')");
			}
		}

		parent::save($model);
	}

	public function find($id = null, $col = 'user_id', $forceArray = false, $limit = null, $order = null)
	{
		return parent::find($id, $col, $forceArray, $limit, $order);
	}
}