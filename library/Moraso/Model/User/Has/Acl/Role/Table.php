<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/User
 */

namespace Moraso\Model\User\Has\Acl\Role;

use Moraso\Model\AbstractTable;

class Table extends AbstractTable
{
	protected $_name = 'user_has_acl_role';
}