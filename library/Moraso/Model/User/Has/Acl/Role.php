<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/User
 */

namespace Moraso\Model\User\Has\Acl;

use Moraso\Model\AbstractModel;

class Role extends AbstractModel
{
	protected $id_user, $id_acl_role;
}