<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Log
 */

namespace Moraso\Model;

class Log extends AbstractModel
{
	protected 	$timestamp, 	$priority, 		$message,
				$file, 			$line, 			$library,
				$skin, 			$module, 		$plugin,
				$plugin_area;
}