<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Privilege
 */

namespace Moraso\Model\Acl;

use Moraso\Model\AbstractModel;

class Privilege extends AbstractModel
{
	protected $name;
}