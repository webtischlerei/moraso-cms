<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Resource
 */

namespace Moraso\Model\Acl;

use Moraso\Model\AbstractModel;

class Resource extends AbstractModel
{
	protected $name, $parent;
}