<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Resource
 */

namespace Moraso\Model\Acl\Resource;

use Moraso\Model\AbstractTable;

class Table extends AbstractTable
{
	protected $_name = 'acl_resource';
}