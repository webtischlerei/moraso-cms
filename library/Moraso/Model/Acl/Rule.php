<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Rule
 */

namespace Moraso\Model\Acl;

use Moraso\Model\AbstractModel;

class Rule extends AbstractModel
{
	protected $id_acl_role, $id_acl_resource, $id_acl_privilege, $access;
}