<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Configset
 */

namespace Moraso\Model;

class Configset extends AbstractModel
{
	protected $identifier, $config;
}