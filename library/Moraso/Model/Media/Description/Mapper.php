<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Media
 */

namespace Moraso\Model\Media\Description;

use Moraso\Model\AbstractMapper;

class Mapper extends AbstractMapper
{
	
}