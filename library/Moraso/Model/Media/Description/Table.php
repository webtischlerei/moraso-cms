<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Media
 */

namespace Moraso\Model\Media\Description;

use Moraso\Model\AbstractTable;

class Table extends AbstractTable
{
	protected 	$_name 		= 'media_description',
				$_primary 	= array('mediaid', 'idlang');
}