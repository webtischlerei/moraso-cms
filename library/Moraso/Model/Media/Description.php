<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Media
 */

namespace Moraso\Model\Media;

use Moraso\Model\AbstractModel;

class Description extends AbstractModel
{
	protected $mediaid, $idlang, $name, $subline, $description;
}