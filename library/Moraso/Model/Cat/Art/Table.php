<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Article
 */

namespace Moraso\Model\Cat\Art;

use Moraso\Model\AbstractTable;

class Table extends AbstractTable
{
	protected 	$_name 			= 'cat_art',
				$_primaryKey 	= array('idcat', 'idart');
}