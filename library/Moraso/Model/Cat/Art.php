<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/Category
 */

namespace Moraso\Model\Cat;

use Moraso\Model\AbstractModel;

class Art extends AbstractModel
{
	protected $idcat, $idart;
}