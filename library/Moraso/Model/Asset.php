<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model/_Asset
 */

namespace Moraso\Model;

use Moraso\Model\AbstractModel;

class Asset extends AbstractModel
{
	protected $headline, $subheadline, $text_1, $text_2, $active;
}