<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model
 */

namespace Moraso\Model;

abstract class AbstractModel
{
	protected $id = 0, $created, $last_modified;

	public function __construct($row = null)
	{
		if (!is_null($row)) {
			foreach ($row as $var_key => $var_value) {
				$this->$var_key = $row->$var_key;
			}
		}
	}

	public function __set($name, $value)
	{
		if ($name === 'id' && !empty($this->id)) {
			throw new \Exception("Eine eindeutige ID kann nicht verändert werden!");
		}

		$this->$name = is_numeric($value) ? (int) $value : (is_bool($value) ? (bool) $value : $value);
	}
	
	public function __get($name)
	{	
		return $this->$name;
	}

	public function __isset($name)
	{	
		return isset($this->$name);
	}

	public function __unset($name)
	{	
		unset($this->$name);
	}
}