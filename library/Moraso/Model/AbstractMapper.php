<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Model
 */

namespace Moraso\Model;

use \ReflectionClass;
use \ReflectionProperty;

use Zend\Db\Sql\Select;

abstract class AbstractMapper
{
	protected $table, $reflection, $model, $primaryKey;
	
	/**
	* TODO
	*/
	public function __construct($suffix = null)
	{
		$this->reflection 		= new ReflectionClass($this);

		$reflectionNamespace 	= $this->reflection->getNamespaceName();

		$this->model 			= '\\' . $reflectionNamespace;

		$tableAsString 			= $this->model . '\\Table';

		$this->table 			= new $tableAsString($suffix);

		$this->primaryKey 		= $this->table->getPrimaryKey();
	}

	/**
	* TODO
	*/
	public function find($id, $col = null, $forceArray = false, $limit = null, $order = null)
	{
		$where = array();

		if (!is_null($id) && $id !== 'all') {
			if (is_null($col)) {
				$col = $this->primaryKey;
			}

			if (!is_array($id) && !is_array($col)) {
				$where = array($col => $id);
			} else {
				$conditions = array();
				$where 		= array();
				
				foreach ($id as $key => $cond) {
					if (!empty($cond)) {
						$where[$col[$key]] = $cond;
					}
				}
			}
		}

		$rowset = $this->table->select(function(Select $select) use ($where, $limit, $order) {
			if (!empty($where)) {
				$select->where($where);
			}
			
			if (!is_null($limit)) {
				$select->limit(intval($limit));
			}

			if (!is_null($order)) {
				$select->order(is_array($order) ? $order : array($order));
			}
		});

		if (!$rowset) {
			return;
		}

		$result = array();

		if ($rowset->count() === 1) {
			if (!$forceArray) {
				return new $this->model($rowset->current());
			}

			$result[] = new $this->model($rowset->current());
		} else {
			foreach ($rowset as $row) {
				$result[] = new $this->model($row);
			}
		}
		
		return $result;
	}

	/**
	* TODO
	*/
	public function fetchAll($limit = null, $order = null, $forceArray = true)
	{
		return self::find('all', null, $forceArray, $limit, $order);
	}

	/**
	* TODO
	*/
	public function create()
	{
		return new $this->model();
	}

	/**
	* TODO
	*/
	public function save(& $model)
	{
		$reflect 	= new ReflectionClass($model);

		$data 		= array();

		$properties 	= $reflect->getProperties(ReflectionProperty::IS_PROTECTED);
		$namespaceName 	= $this->reflection->getNamespaceName();

		foreach ($properties as $property) {
			if ($property->class === $namespaceName) {
				$data[$property->name] = $model->{$property->name};
			}
		}

		if (empty($model->{$this->primaryKey})) {
			if ($this->table->insert($data)) {
				$model->{$this->primaryKey} = $this->table->getLastInsertValue();
			}
		} else {			
			$this->table->update($data, array($this->primaryKey . ' =?' => $model->{$this->primaryKey}));
		}
	}

	/**
	* TODO
	*/
	public function delete($id, $col = null)
	{
		if (is_null($col)) {
			$col = $this->primaryKey;
		}

		$where = array($col => $id);

		return $this->table->delete($where);
	}
}