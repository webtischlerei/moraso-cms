<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Cart_Payment_Strategy_Cash implements Moraso_Cart_Payment_Strategy
{
    public function getCheckoutUrl()
    {
        return Moraso_Config::get('sys.webpath') . '?confirmPayment';
    }

    public function getHiddenFormFields()
    {
        $cart = Moraso_Cart::getInstance();

        return array(
            'order_id' => $cart->getOrderId()
        );
    }

    public function doConfirmPayment($data)
    {        
        $return = array(
            'status' => 'WAITING'
        );
        
        return $return;
    }

    public function actionAfterConfirm($order_id)
    {
        $successUrl = Moraso_Config::get('shop.structure.article.checkout.success');

        $location   = Moraso_Rewrite_Standard::getInstance()->rewriteOutput('{ref:idart-' . $successUrl . '}');

        header('Location: ' . $location);
    }

}