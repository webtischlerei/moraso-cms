<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Cart_Payment_Strategy_Paypal implements Moraso_Cart_Payment_Strategy
{
	public function getCheckoutUrl()
	{
		return Moraso_Config::get('moraso.shop.payment.paypal.sandbox') ? 'https://www.sandbox.paypal.com/cgi-bin/webscr' : 'https://www.paypal.com/cgi-bin/webscr';
	}

	public function getHiddenFormFields()
	{
		$cart = Moraso_Cart::getInstance();

		$amount_with_shipping = $cart->getAmount();
		$amount_without_shipping = $cart->getAmount(false);

		$shippingCosts = $amount_with_shipping - $amount_without_shipping;

		$hiddenFormFields = array(
			'cmd'           => '_xclick',
			'business'      => Moraso_Config::get('moraso.shop.payment.paypal.business'),
			'item_name'     => sprintf(Moraso_Config::get('moraso.shop.checkout.orderDescription'), (int) $cart->getOrderId()),
			'item_number'   => (int) $cart->getOrderId(),
			'amount'        => str_replace(',', '.', $amount_without_shipping),
			'no_shipping' 	=> '1',
			'no_note' 		=> '1',
			'currency_code' => Moraso_Config::get('moraso.shop.currency'),
			'lc' 			=> Moraso_Config::get('moraso.shop.language'),
			'bn' 			=> 'PP-BuyNowBF',
			'notify_url' 	=> Moraso_Config::get('sys.webpath') . '?confirmPayment',
			'return' 		=> Moraso_Rewrite_Standard::getInstance()->rewriteOutput('{ref:idart-' . Moraso_Config::get('shop.structure.article.checkout.success') . '}'),
			'cancel_return' => Moraso_Rewrite_Standard::getInstance()->rewriteOutput('{ref:idart-' . Moraso_Config::get('shop.structure.article.checkout.cancel') . '}'),
			'invoice' 		=> $cart->getOrderId(),
			'shipping' 		=> str_replace(',', '.', $shippingCosts)
		);

		return $hiddenFormFields;
	}

	public function doConfirmPayment($data)
	{
		$sandbox = true;

		$raw_post_data = file_get_contents('php://input');
		$raw_post_array = explode('&', $raw_post_data);

		$myPost = array();
		foreach ($raw_post_array as $keyval) {
			$keyval = explode ('=', $keyval);

			if (count($keyval) == 2) {
				$myPost[$keyval[0]] = urldecode($keyval[1]);
			}
		}

		$req = 'cmd=_notify-validate';
		foreach ($myPost as $key => $value) {
			$value = urlencode($value);

			$req .= "&" . $key . "=" . $value;
		}

		$ch = curl_init(Moraso_Config::get('moraso.shop.payment.paypal.sandbox') ? 'https://www.sandbox.paypal.com/cgi-bin/webscr' : 'https://www.paypal.com/cgi-bin/webscr');
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

		if (file_exists(ROOT_PATH . '/cacert.pem') && is_readable(ROOT_PATH . '/cacert.pem')) {
			curl_setopt($ch, CURLOPT_CAINFO, ROOT_PATH . '/cacert.pem');
		}

		$myPost['status'] = 'ERROR';

		$res = curl_exec($ch);

		curl_close($ch);

		if (strcmp($res, "VERIFIED") == 0) {
			$myPost['status'] = 'SUCCESS';
		}
		
		return (object) $myPost;
	}

	public function actionAfterConfirm($order_id)
	{
		// nichts machen, es handelt sich um eine server-to-server communication, paypal leitet selber zur passenden Seite weiter
	}
}