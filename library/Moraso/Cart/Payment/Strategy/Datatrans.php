<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Cart_Payment_Strategy_Datatrans implements Moraso_Cart_Payment_Strategy
{
	public function getCheckoutUrl()
	{
		return 'https://payment.datatrans.biz/upp/jsp/upStart.jsp';
	}

	public function getHiddenFormFields()
	{
		$cart = Moraso_Cart::getInstance();

		$merchantId = Moraso_Config::get('moraso.shop.payment.datatrans.merchantId');
		$amount     = str_replace(',', '.', $cart->getAmount());
		$currency   = Moraso_Config::get('moraso.shop.currency');
		$successURL = Moraso_Rewrite_Standard::getInstance()->rewriteOutput('{ref:idart-' . Moraso_Config::get('shop.structure.article.checkout.success') . '}');
		$errorUrl   = Moraso_Rewrite_Standard::getInstance()->rewriteOutput('{ref:idart-' . Moraso_Config::get('shop.structure.article.checkout.error') . '}');
		$cancelURL  = Moraso_Rewrite_Standard::getInstance()->rewriteOutput('{ref:idart-' . Moraso_Config::get('shop.structure.article.checkout.cancel') . '}');
		$language   = Moraso_Config::get('moraso.shop.language');
		$order_id   = $cart->getOrderId();
	   
		return array(
			'merchantId'    => $merchantId,
			'amount'        => $amount,
			'currency'      => $currency,
			'refno'         => $order_id,
			'successURL'    => $successURL,
			'errorUrl'      => $errorUrl,
			'cancelUrl'     => $cancelURL,
			'language'      => $language,
			'order_id'      => $order_id
		);
	}

	public function doConfirmPayment($data)
	{
		if (isset($data['errorCode']) && !empty($data['errorCode'])) {
			$data['status'] = 'ERROR';
		} else {
			$data['status'] = 'SUCCESS';
		}

		return (object) $data;
	}

	public function actionAfterConfirm($order_id)
	{
		// nichts machen, es handelt sich um eine server-to-server communication, datatrans leitet selber zur passenden Seite weiter
	}
}