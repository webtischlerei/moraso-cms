<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Cart_Payment_Strategy_Wirecard implements Moraso_Cart_Payment_Strategy
{
    public function getCheckoutUrl()
    {
        return 'https://secure.wirecard-cee.com/qpay/init.php';
    }

    public function getHiddenFormFields()
    {
        $cart = Moraso_Cart::getInstance();

        $currency           = Moraso_Config::get('moraso.shop.currency');
        $language           = Moraso_Config::get('moraso.shop.language');
        $imageURL           = Moraso_Config::get('moraso.shop.imageURL');
        $cancelURL          = Moraso_Rewrite_Standard::getInstance()->rewriteOutput('{ref:idart-' . Moraso_Config::get('shop.structure.article.checkout.cancel') . '}');
        $failureURL         = Moraso_Rewrite_Standard::getInstance()->rewriteOutput('{ref:idart-' . Moraso_Config::get('shop.structure.article.checkout.error') . '}');
        $pendingURL         = Moraso_Rewrite_Standard::getInstance()->rewriteOutput('{ref:idart-' . Moraso_Config::get('shop.structure.article.checkout.pending') . '}');
        $successURL         = Moraso_Rewrite_Standard::getInstance()->rewriteOutput('{ref:idart-' . Moraso_Config::get('shop.structure.article.checkout.success') . '}');
        $confirmURL         = Moraso_Config::get('sys.webpath') . '?confirmPayment';
        $serviceURL         = Moraso_Rewrite_Standard::getInstance()->rewriteOutput('{ref:idart-' . Moraso_Config::get('shop.structure.article.checkout.service') . '}');
        $orderDescription   = sprintf(Moraso_Config::get('moraso.shop.checkout.orderDescription'), (int) $cart->getOrderId());
        $displayText        = sprintf(Moraso_Config::get('moraso.shop.checkout.displayText'), (int) $cart->getOrderId());
        $customerId         = Moraso_Config::get('moraso.shop.payment.wirecard.customerId');

        $hiddenFormFields   = array(
            'customerId'        => $customerId,
            'amount'            => str_replace(',', '.', $cart->getAmount()),
            'currency'          => $currency,
            'language'          => $language,
            'orderDescription'  => $orderDescription,
            'successURL'        => $successURL,
            'confirmURL'        => $confirmURL,
            'order_id'          => $cart->getOrderId()
        );

        $requestFingerprint = $this->_generateRequestFingerprint($hiddenFormFields);

        $hiddenFormFields['paymenttype'] = 'SELECT';
        $hiddenFormFields['displayText'] = $displayText;
        $hiddenFormFields['cancelURL'] = $cancelURL;
        $hiddenFormFields['failureURL'] = $failureURL;
        $hiddenFormFields['pendingURL'] = $pendingURL;
        $hiddenFormFields['imageURL'] = $imageURL;
        $hiddenFormFields['serviceURL'] = $serviceURL;
        $hiddenFormFields['requestFingerprintOrder'] = $requestFingerprint['requestFingerprintOrder'];
        $hiddenFormFields['requestfingerprint'] = $requestFingerprint['requestFingerprint'];

        return $hiddenFormFields;
    }

    private function _generateRequestFingerprint(array $data)
    {
        $requestFingerprintOrder = array();
        $requestFingerprintSeed = array();

        $requestFingerprintOrder[] = 'secret';
        $requestFingerprintSeed[] = Moraso_Config::get('moraso.shop.payment.wirecard.secret');

        foreach ($data as $key => $value) {
            $requestFingerprintOrder[] = $key;
            $requestFingerprintSeed[] = $value;
        }

        $requestFingerprintOrder[] = "requestFingerprintOrder";
        $requestFingerprintSeed[] = implode(',', $requestFingerprintOrder);

        return array(
            'requestFingerprintOrder' => implode(',', $requestFingerprintOrder),
            'requestFingerprint' => md5(implode('', $requestFingerprintSeed))
        );
    }

    public function doConfirmPayment($data)
    {
        $data['status'] = $data['paymentState'];

        return (object) $data;
    }

    public function actionAfterConfirm($order_id)
    {
        // nichts machen, es handelt sich um eine server-to-server communication, wirecard leitet selber zur passenden Seite weiter
    }
}