<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Shortcode extends Aitsu_Shortcode
{
	public static function getInstance()
	{
		static $instance = null;

		if (!isset($instance)) {
			$instance = new self();
		}

		return $instance;
	}

	public function evalModule($method, $params, $client, $index, $current = true)
	{
		$index = preg_replace('/[^a-zA-Z_0-9]/', '', $index);

		$profileDetails = new stdClass();

		$returnValue = '';
		Aitsu_Content_Edit::isBlock(true);

		$files = array();

		$heredity = Moraso_Skin_Heredity::build();

		$methodExploded = explode('.', $method);
		
		foreach ($heredity as $skin) {
			if (file_exists(APPLICATION_PATH . "/skins/" . $skin . "/module/" . implode('/', $methodExploded) . "/" . end($methodExploded) . ".php")) {
				$files['Skin\\Module'][] = APPLICATION_PATH . "/skins/" . $skin . "/module/" . implode('/', $methodExploded) . "/" . end($methodExploded) . ".php";
			} else {
				$files['Skin_Module'][] = APPLICATION_PATH . "/skins/" . $skin . "/module/" . implode('/', $methodExploded) . "/Class.php";
			}
		}

		$additionalLibraries = Moraso_Library_Tree::getJson();

		if (!empty($additionalLibraries)) {
			foreach ($additionalLibraries->libraries as $additionalLibrary) {
				if (file_exists(realpath(APPLICATION_PATH . '/../library/') . '/' . $additionalLibrary . '/Module/' . implode('/', $methodExploded) . '/' . end($methodExploded) . '.php')) {
					$files[$additionalLibrary . '\Module'][] = realpath(APPLICATION_PATH . '/../library/') . '/' . $additionalLibrary . '/Module/' . implode('/', $methodExploded) . '/' . end($methodExploded) . '.php';
				} else {
					$files[$additionalLibrary . '_Module'][] = realpath(APPLICATION_PATH . '/../library/') . '/' . $additionalLibrary . '/Module/' . implode('/', $methodExploded) . '/Class.php';
				}
				
			}
		}

		if (file_exists(realpath(APPLICATION_PATH . '/../library/') . '/Moraso/Module/' . implode('/', $methodExploded) . '/' . end($methodExploded) . '.php')) {
			$files['Moraso\Module'][] = realpath(APPLICATION_PATH . '/../library/') . '/Moraso/Module/' . implode('/', $methodExploded) . '/' . end($methodExploded) . '.php';
		} else {
			$files['Moraso_Module'][] = realpath(APPLICATION_PATH . '/../library/') . '/Moraso/Module/' . implode('/', $methodExploded) . '/Class.php';
		}

		$allMorasoModules = Aitsu_Util_Dir::scan(realpath(APPLICATION_PATH . '/../library/') . '/Moraso', 'Class.php');

		$secondLevelModules = array();
		foreach ($allMorasoModules as $singleModule) {
			$singleModuleInfo = explode('/', str_replace(realpath(APPLICATION_PATH . '/../library/') . '/', '', $singleModule));

			if ($singleModuleInfo[2] === 'Module' && $singleModuleInfo[1] !== 'Module') {
				if (strpos(implode('/', $singleModuleInfo), '/' . implode('/', $methodExploded) . '/') !== false) {
					$files['Moraso_' . $singleModuleInfo[1] . '_Module'][] = realpath(APPLICATION_PATH . '/../library/') . '/' . $singleModuleInfo[0] . '/' . $singleModuleInfo[1] . '/Module/' . implode('/', $methodExploded) . '/Class.php';
				}
			}
		}
		
		$exists = false;

		foreach ($files as $prefix => $file) {
			if (is_array($file)) {
				foreach ($file as $path) {
					if (file_exists($path)) {
						$exists = true;
						
						include_once $path;

						if (strpos($path, 'Class.php') !== false) {
							$profileDetails->source = $prefix . '_' . implode('_', $methodExploded) . '_Class';
																											  
							$returnValue = call_user_func(array(
									$profileDetails->source,
									'init'
								), array(
									'index' => $index,
									'params' => $params,
									'className' => $profileDetails->source
								)
							);
						} else {
							$profileDetails->source = $prefix . '\\' . implode('\\', $methodExploded) . '\\' . end($methodExploded);

							$module 				= new $profileDetails->source($index, $params);

							$returnValue 			= $module->getOutput();
							$moduleConfig 			= $module->getModuleConfig();
						}

						break;
					}
				}

				if ($exists) {
					break;
				}
			} else {
				if (file_exists($file)) {
					$exists = true;
					
					if (strpos($file, 'Class.php') !== false) {
						$profileDetails->source = $prefix . '_' . str_replace('.', '_', $method) . '_Class';
					
						include_once $file;
						
						$returnValue = call_user_func(array(
							$profileDetails->source,
							'init'
								), array(
							'index' => $index,
							'params' => $params,
							'className' => $profileDetails->source
						));
					} else {
						$profileDetails->source = $prefix . '_' . str_replace('.', '_', $method);

						$module = new $profileDetails->source($index, $params);

						$returnValue 	= $module->getOutput();
						$moduleConfig 	= $module->getModuleConfig();
					}

					break;
				}
			}
		}

		if (!$exists) {
			if (Aitsu_Registry::isEdit()) {
				return '<strong>' . sprintf('// Das Modul "%s" existiert leider nicht! //', $method) . '</strong>';
			}

			return '';
		}

		if (is_object($returnValue)) {
			$index = $returnValue->index;
			$returnValue = $returnValue->out;
		}

		if (Aitsu_Registry::isEdit()) {
			if (isset($moduleConfig)) {
				$isBlock 	= $moduleConfig->parameters['isBlock'];
				$allowEdit 	= $moduleConfig->parameters['allowEdit'];
				$cntConfigs = count($moduleConfig->configurable);

				if ($allowEdit) {
					$allowEdit = \Moraso\Acl::isAllowed('module.' . strtolower($method), 'edit');
				}
			} else {
				$isBlock 	= Aitsu_Content_Edit::isBlock();
				$allowEdit 	= Aitsu_Content_Edit::noEdit($method) ? false : true;
				$cntConfigs = 1;
			}

			if ($cntConfigs && $allowEdit) {
				if (!($method === 'Template' && $index === 'Root')) {
					if ($isBlock === true) {
						$returnValue = '<div id="' . $method . '-' . $index . '-' . Aitsu_Registry::get()->env->idartlang . '" class="aitsu_editable"><div class="aitsu_hover">' . $returnValue . '</div></div>';
					} else {
						$returnValue = '<span id="' . $method . '-' . $index . '-' . Aitsu_Registry::get()->env->idartlang . '" class="aitsu_editable" style="display:inline;"><span class="aitsu_hover">' . $returnValue . '</span></span>';
					}
				}
			}
		}

		return $returnValue;
	}
}