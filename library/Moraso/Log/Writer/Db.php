<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Log
 */
class Moraso_Log_Writer_Db extends Zend_Log_Writer_Abstract
{
	public function __construct()
	{
	}

	static public function factory($config)
	{
	}
 
	protected function _write($event)
	{
		$log_mapper = new \Moraso\Model\Log\Mapper();
		$log 		= new \Moraso\Model\Log();

		$matchesMessage = array();
		preg_match_all("/((?:\\/[\\w\\.\\-]+)+):(\\d+)/is", $event['message'], $matchesMessage);

		$log->timestamp = date('Y-m-d H:i:s', strtotime($event['timestamp']));
		$log->priority 	= $event['priority'];
		$log->message 	= str_replace('[Thrown @:' . $matchesMessage[0][0] . ']', '', $event['message']);
		
		if (isset($matchesMessage[1][0]) && !empty($matchesMessage[1][0])) {
			$log->file = $matchesMessage[1][0];
			$log->line = $matchesMessage[2][0];

			$matchesSkin = array();
			if (preg_match_all("/\\/application\\/skins\\/((?:[a-z][a-z]+))\\//is", $matchesMessage[1][0], $matchesSkin)) {
				$log->skin = $matchesSkin[1][0];
			}

			$matchesLibrary = array();
			if (preg_match_all("/\\/library\\/((?:[a-z][a-z]+))\\//is", $matchesMessage[1][0], $matchesLibrary)) {
				$log->library = $matchesLibrary[1][0];
			}

			$matchesModule = array();
			if (preg_match_all("/\\/(library|application\\/skins)\\/((?:[a-z][a-z]+))\\/module((?:\\/[\\w\\.\\-]+)+)\\//is", $matchesMessage[1][0], $matchesModule)) {
				$log->module = substr($matchesModule[3][0], 1);
			}

			$matchesPlugin = array();
			if (preg_match_all("/\\/library\\/((?:[a-z][a-z]+))\\/Plugin((?:\\/[\\w\\.\\-]+)+)\\//is", $matchesMessage[1][0], $matchesPlugin)) {
				$matchesPluginArea = array();
				preg_match_all("/((?:[a-z][a-z]+))((?:\\/[\\w\\.\\-]+)+)/is", substr($matchesPlugin[2][0], 1), $matchesPluginArea);

				$log->plugin 		= $matchesPluginArea[1][0];
				$log->pluginArea 	= substr($matchesPluginArea[2][0], 1);
			}
		}
				
		$log_mapper->save($log);
	}
}