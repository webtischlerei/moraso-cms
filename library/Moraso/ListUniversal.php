<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/ListUniversal
 */

namespace Moraso;

use Moraso\Model\ListUniversal\Listing;
use Moraso\Model\ListUniversal\Row;
use Moraso\Model\ListUniversal\Cell;
use Moraso\Model\ListUniversal\Column;
use Moraso\Model\ListUniversal\ColumnValues;
use Moraso\Model\ListUniversal\View;

class ListUniversal
{
	/**
	* TODO
	*/
	public static function getLists()
	{
		// 
		$listingMapper = new Listing\Mapper();

		// 
		return $listingMapper->fetchAll();
	}

	/**
	* TODO
	*/
	public static function getListColumns($id)
	{
		// 
		$columnMapper 	= new Column\Mapper();

		// 
		return $columnMapper->find($id, 'id_list', true);
	}

	/**
	* TODO
	*/
	public static function getColumnValues($id)
	{
		// 
		$columnValuesMapper 	= new ColumnValues\Mapper();

		// 
		return $columnValuesMapper->find($id, 'id_column', true);
	}

	/**
	* TODO
	*/
	public static function addColumnValue($id_column, $value)
	{
		// 
		$columnValuesMapper 	= new ColumnValues\Mapper();

		// 
		$columnValue 			= $columnValuesMapper->create();
		$columnValue->id_column = $id_column;
		$columnValue->value 	= $value;

		// 
		$columnValuesMapper->save($columnValue);

		// 
		return $columnValue;
	}

	/**
	* TODO
	*/
	public static function editColumnValue($id, $value)
	{
		// 
		$columnValuesMapper 	= new ColumnValues\Mapper();

		// 
		$columnValue 			= $columnValuesMapper->find($id);
		$columnValue->value 	= $value;

		// 
		$columnValuesMapper->save($columnValue);

		// 
		return $columnValue;
	}

	/**
	* TODO
	*/
	public static function getColumnValue($id)
	{
		// 
		$columnValuesMapper = new ColumnValues\Mapper();

		// 
		return $columnValuesMapper->find($id);
	}

	/**
	* TODO
	*/
	public static function getCellsByColumnId($id)
	{
		// 
		$cellMapper 	= new Cell\Mapper();

		// 
		return $cellMapper->find($id, 'id_column', true);
	}

	/**
	* TODO
	*/
	public static function getRows($id_list)
	{
		// 
		$rowMapper 	= new Row\Mapper();

		// 
		return $rowMapper->find($id_list, 'id_list', true);
	}

	/**
	* TODO
	*/
	public static function getColumn($id)
	{
		// 
		$columnMapper 	= new Column\Mapper();

		// 
		return $columnMapper->find($id);
	}

	/**
	* TODO
	*/
	public static function getColumnName($id)
	{
		// 
		$column = self::getColumn($id);

		// 
		return $column->name;
	}

	/**
	* TODO
	*/
	public static function getList($id)
	{
		// 
		$listMapper = new Listing\Mapper();

		// 
		return $listMapper->find($id);
	}

	/**
	* TODO
	*/
	public static function getListName($id)
	{
		// 
		$list = self::getList($id);

		// 
		return $list->name;
	}

	/**
	* TODO
	*/
	public static function createColumn($id_list, $name, $type)
	{
		// 
		$columnMapper 		= new Column\Mapper();

		// 
		$column 			= $columnMapper->create();
		$column->id_list 	= $id_list;
		$column->name 		= $name;
		$column->type 		= $type;
		$column->pos 		= 1;

		//
		$columnMapper->save($column);

		// 
		return $column;
	}

	/**
	* TODO
	*/
	public static function updateColumn($id, $name, $type)
	{
		// 
		$columnMapper 	= new Column\Mapper();

		// 
		$column 		= $columnMapper->find($id);
		$column->name 	= $name;
		$column->type 	= $type;

		//
		$columnMapper->save($column);

		// 
		return $column;
	}

	/**
	* TODO
	*/
	public static function setListName($id, $name)
	{
		// 
		$listMapper = new Listing\Mapper();

		// 
		$list 		= $listMapper->find($id);
		$list->name = $name;

		// 
		$listMapper->save($list);

		// 
		return $list;
	}

	/**
	* TODO
	*/
	public static function get($id)
	{
		return \Moraso_Db_Simple::fetch('*', '_list_universal_view_list_' . $id, array(), 100);
	}

	/**
	* TODO
	*/
	public static function createView($id)
	{
		// 
		$columnMapper 	= new Column\Mapper();

		// 
		$columns 		= $columnMapper->find($id, 'id_list', true);

		// 
		$mediaHolder 	= \Moraso_Config::get('article.mediaHolder');

		// 
		if ($columns) {
			// 
			$selects 		= array();

			// 
			$innerJoins 	= array();

			// 
			$leftJoins 		= array();

			// 
			$selects[] 		= 'row.id AS id';

			// 
			$innerJoins[] 	= '_list_universal_row AS row ON row.id_list = list.id ';

			// 
			foreach ($columns as $column) {
				// 
				$leftJoins[] 	= '_list_universal_cell AS cell_' . $column->id . ' ON (cell_' . $column->id . '.id_row = row.id AND cell_' . $column->id . '.id_column = ' . $column->id . ') ';

				// 
				$coalesce = array(
					'cell_' . $column->id . '.value_varchar',
					'cell_' . $column->id . '.value_int',
					'cell_' . $column->id . '.value_float',
					'cell_' . $column->id . '.value_text',
					'cell_' . $column->id . '.value_date',
					'cell_' . $column->id . '.value_datetime',
					'cell_' . $column->id . '.value_time',
					'cell_' . $column->id . '.value_bool'
				);

				// 
				if ($column->type === 'combo' || $column->type === 'radiogroup') {
					// 
					$leftJoins[] = '_list_universal_column_values AS columnValue_' . $column->id . ' ON columnValue_' . $column->id . '.id = COALESCE(cell_' . $column->id . '.value_int, cell_' . $column->id . '.value_bool) ';

					// 
					array_unshift($coalesce, 'columnValue_' . $column->id . '.value');
				}

				// 
				if ($column->type === 'fileuploadfield') {
					// 
					$leftJoins[] = '_media AS columnValue_' . $column->id . ' ON columnValue_' . $column->id . '.mediaid = COALESCE(cell_' . $column->id . '.value_int, cell_' . $column->id . '.value_bool) ';

					// 
					array_unshift($coalesce, 'CONCAT("/' . $mediaHolder . '/", columnValue_' . $column->id . '.filename)');
				}

				// 
				$selects[] = 'COALESCE(' . implode(', ', $coalesce) . ') AS ' . str_replace('-', '_', \Moraso_Util_String::slugify($column->name));
			}

			// 
			$selects[] = 'row.created AS created';
			$selects[] = 'row.last_modified AS last_modified';

			// 
			$select  = 'SELECT ' . implode(', ', $selects) . ' ';
			$select .= 'FROM _list_universal_list AS list ';
			$select .= 'INNER JOIN ' . implode('INNER JOIN ', $innerJoins);
			$select .= 'LEFT JOIN ' . implode('LEFT JOIN ', $leftJoins);
			$select .= 'WHERE list.id = ' . $id . ' ';
			$select .= 'GROUP BY row.id';

			// 
			\Moraso_Db::query('DROP VIEW IF EXISTS _list_universal_view_list_' . $id . ';');
			\Moraso_Db::query('CREATE VIEW _list_universal_view_list_' . $id . ' AS (' . $select . ');');
		}
	}

	/**
	* TODO
	*/
	public static function createList($name)
	{
		// 
		$listMapper = new Listing\Mapper();

		// 
		$list 		= $listMapper->create(); 
		$list->name = $name;

		// 
		$listMapper->save($list);

		// 
		return $list;
	}

	/**
	* TODO
	*/
	public static function getRowCells($id_row)
	{
		// 
		$cellMapper = new Cell\Mapper();

		// 
		return $cellMapper->find($id_row, 'id_row', true);
	}

	/**
	* TODO
	*/
	public static function createRow($id_list, $data)
	{
		// 
		$rowMapper 		= new Row\Mapper();

		// 
		$cellMapper 	= new Cell\Mapper();

		// 
		$row 			= $rowMapper->create();
		$row->id_list 	= $id_list;

		// 
		$rowMapper->save($row);

		// 
		foreach ($data as $id_column => $value) {
			// 
			if (is_scalar($value)) {
				// 
				$valueType 			= self::getValueType($value);

				// Sonderregelung wenn es sich um einen Float Wert handelt
				if ($valueType === 'float' && substr($value, -1, 1) === '0') {
					$valueType = 'string';
				}

				// 
				$value 				= self::transformValue($value, $valueType);

				// 
				$valueType 			= strlen($value) > 255 ? 'text' : ($valueType === 'string' ? 'varchar' : $valueType);

				// 
				$valueColumn 		= 'value_' . $valueType;

				// 
				$cell 				= $cellMapper->create();
				$cell->id_row 		= $row->id;
				$cell->id_column 	= $id_column;
				$cell->$valueColumn = $value;

				// 
				$cellMapper->save($cell);
			}
		}

		// 
		return $row;
	}

	/**
	* TODO
	*/
	public static function updateRow($id, $data)
	{
		// 
		$rowMapper 		= new Row\Mapper();

		// 
		$cellMapper 	= new Cell\Mapper();

		// 
		$row 			= $rowMapper->find($id);

		// 
		foreach ($data as $id_column => $value) {
			// 
			if (is_scalar($value)) {
				// 
				$valueType 		= self::getValueType($value);

				// Sonderregelung wenn es sich um einen Float Wert handelt
				if ($valueType === 'float' && substr($value, -1, 1) === '0') {
					$valueType = 'string';
				}

				// 
				$value 			= self::transformValue($value, $valueType);

				// 
				$valueType 		= strlen($value) > 255 ? 'text' : ($valueType === 'string' ? 'varchar' : $valueType);

				// 
				$valueColumn 	= 'value_' . $valueType;

				// 
				$cell 					= $cellMapper->find(array($row->id, $id_column), array('id_row', 'id_column'));
				$cell->value_varchar 	= null;
				$cell->value_int 		= null;
				$cell->value_float 		= null;
				$cell->value_text 		= null;
				$cell->value_date 		= null;
				$cell->value_datetime 	= null;
				$cell->value_time 		= null;
				$cell->value_bool 		= null;

				if (!$cell) {
					$cell 				= $cellMapper->create();
					$cell->id_row 		= $row->id;
					$cell->id_column 	= $id_column;
				}

				$cell->$valueColumn = $value;
				
				// 
				$cellMapper->save($cell);
			}
		}

		// 
		return $row;
	}

	/**
	* TODO
	*/
	public static function deleteRow($id)
	{
		// 
		$rowMapper = new Row\Mapper();

		// 
		$rowMapper->delete($id);
	}

	/**
	* TODO
	*/
	public static function deleteColumn($id)
	{
		// 
		$columnMapper = new Column\Mapper();

		// 
		$columnMapper->delete($id);
	}

	/**
	* TODO
	*/
	public static function deleteList($id)
	{
		// 
		$listingMapper = new Listing\Mapper();

		// 
		$listingMapper->delete($id);
	}

	/**
	* TODO
	*/
	public static function deleteColumnValue($id)
	{
		// 
		$columnValuesMapper = new ColumnValues\Mapper();

		// 
		$columnValuesMapper->delete($id);
	}

	/**
	* TODO
	*/
	public static function transformValue($value, $type)
	{
		if ($type === 'datetime') {
			return date('Y-m-d H:i:s', strtotime($value));
		}

		if ($type === 'date') {
			return date('Y-m-d', strtotime($value));
		}

		if ($type === 'time') {
			return date('H:i:s', strtotime($value));
		}

		settype($value, $type);

		return $value;
	}

	/**
	* TODO
	*/
	public static function getValueType($value)
	{
		if (is_string($value) && $value === '') {
			return 'string';
		}

		// 
		if (!is_null(filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE))) {
			return 'bool';
		}

		// 
		if (filter_var($value, FILTER_VALIDATE_INT)) {
			return 'int';
		}

		// 
		if (filter_var($value, FILTER_VALIDATE_FLOAT)) {
			return 'float';
		}

		// 
		if (self::is_date($value) && (strpos($value, '.') !== false || strpos($value, ':') !== false || strpos($value, '-') !== false)) {
			// 
			if (date('H:i:s', strtotime($value)) == '00:00:00') {
				return 'date';
			}
			
			// 
			if (preg_match('/^\d{2}:\d{2}$/', $value) || preg_match('/^\d{2}:\d{2}:\d{2}$/', $value)) {
				return 'time';
			}

			// 
			return 'datetime';
		} 

		// 
		return 'string';
	}

	public static function is_date($str)
	{ 
		$timestamp = strtotime($str); 
		
		if (!is_numeric($timestamp)) {
			return false; 
		}

		$month = date('m', $timestamp); 
		$day   = date('d', $timestamp); 
		$year  = date('Y', $timestamp); 

		if (checkdate($month, $day, $year)) {
			return true; 
		}

		return false; 
	}
}