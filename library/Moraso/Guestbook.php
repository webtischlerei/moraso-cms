<?php

/**
 * @author Christian Kehres <christian.kehres@gmail.com>
 * @copyright (c) 2014, webtischlerei <http://www.christian-kehres.de>
 */
class Moraso_Guestbook
{
    /**
     * @return Array
     */
    public static function getEntries()
    {
        $currentClient = \Moraso\Session::get('currentClient');
        $currentClientOld = Aitsu_Registry:: get()->session->currentClient;

        return Moraso_Db::fetchAll(
            '' .
            'select ' .
            '   * ' .
            'from ' .
            '   _guestbook ' .
            'where ' .
            '   idclient =:idclient ' .
            'order by ' .
            '   created desc',
            array(
                ':idclient' => $currentClient
            )
        );
    }

    /**
     * @param $limit
     * @return Array
     */
    public static function getActiveEntries($limit)
    {
        return Moraso_Db::fetchAll(
            '' .
            'select ' .
            '   * ' .
            'from ' .
            '   _guestbook ' .
            'where ' .
            '   active =:active ' .
            'and ' .
            '   idclient =:idclient ' .
            'order by ' .
            '   created desc ' .
            'limit ' .
            '   0, ' . $limit,
            array(
                ':active' => 1,
                ':idclient' => Aitsu_Registry::get()->env->idclient
            )
        );
    }

    /**
     * @param $id
     * @return Array
     */
    public static function getEntry($id)
    {
        return Moraso_Db::fetchRow(
            '' .
            'select ' .
            '   * ' .
            'from ' .
            '   _guestbook ' .
            'where ' .
            '   id =:id',
            array(
                ':id' => $id
            )
        );
    }

    /**
     * @param $data
     */
    public static function setEntry($data)
    {
        Moraso_Db::put('_guestbook', 'id', $data);
    }

    /**
     * @param $id
     * @param $active
     */
    public static function setActive($id, $active)
    {
        Moraso_Db::put(
            '_guestbook',
            'id',
            array(
                'id' => $id,
                'active' => $active
            )
        );
    }

    /**
     * @param $id
     */
    public static function deleteEntry($id)
    {
        Moraso_Db::query(
            '' .
            'delete from ' .
            '   _guestbook ' .
            'where ' .
            '   id =:id',
            array(
                ':id' => $id
            )
        );
    }
}