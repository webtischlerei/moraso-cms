<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Asset
 */

namespace Moraso;

use Moraso\CRUD;

class Asset extends CRUD
{
	protected static $mapper = 'Moraso\Model\Asset\Mapper';

	/**
	* TODO
	*/
	public static function getList($id = null, $orderBy = null, $orderType = DESC)
	{
		if (isset($orderBy)) {
			return self::read($id, null, null, array($orderBy . ' ' . $orderType));
		}

		return self::read($id);
	}

	/**
	* TODO
	*/
	public function setActive($id, $active)
	{
		// 
		$model = parent::update($id, array('active' => $active));

		// 
        $this->getEventManager()->trigger('setAssetActive', null, array('model' => $model));
	}
}