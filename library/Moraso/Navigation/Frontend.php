<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */

namespace Moraso\Navigation;

use Moraso\Acl;

class Frontend
{
	public static function getTree($idcat = null, $level = 1)
	{
		$idlang 	= \Aitsu_Registry::get()->env->idlang;
		$currentCat = \Aitsu_Registry::get()->env->idcat;

		return self::getCategorieChilds($idcat, $level, $idlang, $currentCat);
	}

	private static function getCategorieChilds($idcat, $level, $idlang, $currentCat)
	{
		$categories = \Moraso_Db::fetchAll('' .
						'SELECT ' .
						'   o.idcat, ' .
						'   catlng.name, ' .
						'   catlng.claim, ' .
						'   catlng.public AS isPublic, ' .
						'   IF (o.lft + 1 = o.rgt, false, true) AS hasChildren, ' .
						'   IF (catlng.public, true, false) AS isAccessible, ' .
						'   IF (child.idcat = o.idcat, true, false) AS isCurrent, ' .
						'   IF (child.idcat IS NULL, false, IF(child.idcat = o.idcat, false, true)) AS isParent, ' .
						'   COUNT(p.idcat)-1 AS level ' .
						'FROM ' .
						'   _cat AS n, ' .
						'   _cat AS p, ' .
						'   _cat AS o ' .
						'LEFT JOIN ' .
						'   _cat_lang AS catlng ON (catlng.idcat = o.idcat AND catlng.idlang =:idlang) ' .
						'LEFT JOIN ' .
						'   _art_lang AS artlng ON artlng.idartlang = catlng.startidartlang ' .
						'LEFT JOIN ' .
						'   _cat AS child ON (child.idcat =:currentCat AND child.lft BETWEEN o.lft AND o.rgt) ' .
						'WHERE ' .
						'   o.lft BETWEEN p.lft AND p.rgt ' .
						'AND ' .
						'   o.lft BETWEEN n.lft AND n.rgt ' .
						'AND ' .
						'   n.idcat =:id ' .
						'AND ' .
						'   artlng.online =:online ' .
						'AND ' .
						'   catlng.visible =:visible ' .
						'GROUP BY ' .
						'   o.lft ' .
						'HAVING ' .
						'   level =:level ' .
						'ORDER BY ' .
						'   o.lft ASC', array(
					':id' 			=> $idcat,
					':level' 		=> $level,
					':idlang' 		=> $idlang,
					':online' 		=> 1,
					':visible' 		=> 1,
					':currentCat' 	=> $currentCat
		));

		foreach ($categories as $key => &$category) {
			$category['isPublic'] 		= (bool) $category['isPublic'];
			$category['hasChildren'] 	= (bool) $category['hasChildren'];
			$category['isAccessible'] 	= (bool) $category['isAccessible'];
			$category['isCurrent'] 		= (bool) $category['isCurrent'];
			$category['isParent'] 		= (bool) $category['isParent'];

			if (!$category['isPublic']) {
				$category['isAccessible'] = Acl::isAllowed('cat.' . $category['idcat'], 'view');
			}

			if (!$category['isAccessible']) {
				unset($categories[$key]);
				continue;
			}
			
			if ($category['hasChildren']) {
				$category['children'] = self::getCategorieChilds($category['idcat'], $category['level'] + 1, $idlang, $currentCat);

				if (empty($category['children'])) {
					$category['hasChildren'] = false;
				} 
			}

			unset($category['isPublic']);
			unset($category['isAccessible']);
			unset($category['level']);
		}

		return $categories;
	}
}