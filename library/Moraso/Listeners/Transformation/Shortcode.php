<?php

/**
 * @author Andreas Kummer, w3concepts AG
 * @copyright Copyright &copy; 2012, w3concepts AG
 * 
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Listeners_Transformation_Shortcode implements Aitsu_Event_Listener_Interface
{
	protected function __construct()
	{
	}

	public static function getInstance()
	{
		static $instance = null;

		if (!isset($instance)) {
			$instance = new self();
		}

		return $instance;
	}

	public static function notify(Aitsu_Event_Abstract $event)
	{
		if (!isset($event->bootstrap->pageContent)) {
			return;
		}

		$event->bootstrap->pageContent = self::getInstance()->getContent($event->bootstrap->pageContent);
	}

	protected function _rewrite(& $content)
	{
		$matches = array();

		if (preg_match_all("/(\\{\{)(Template)(:)((?:[a-z][a-z0-9_]*))(\\}\})/is", $content, $matches) > 0) {
			$content = $this->_rewriteShortmodules($content, $matches);
		}

		unset($matches);

		if (preg_match_all('/(<p(>|\s+[^>]*>)_\\[(.*?)\\:(.*?)(:?\\:(\\d*))?\\]<\/p>)/', $content, $matches) > 0) {
			unset($matches[0]);
			unset($matches[2]);
			$content = $this->_rewriteShortcodes($content, array_values($matches));
		}

		if (preg_match_all('/(<p(>|\s+[^>]*>)\s\t_\\[(.*?)\\:(.*?)(:?\\:(\\d*))?\\]<\/p>)/', $content, $matches) > 0) {
			unset($matches[0]);
			unset($matches[2]);
			$content = $this->_rewriteShortcodes($content, array_values($matches));
		}
		
		unset($matches);
		
		if (preg_match_all('/_\\[(.*?)\\:(.*?)(:?\\:(\\d*))?\\]/', $content, $matches) > 0) {
			$content = $this->_rewriteShortcodes($content, $matches);
		}
		
		unset($matches);

		if (preg_match_all('@<script\\s+type=\"application/x-(moraso)\"\\s+src=\"([^:\"]+):?([^\"]*)\"[^/>]*(?:(?:/>)|(?:>(.*?)</script>))@s', $content, $matches) > 0) {
			unset($matches[1]);
			$content = $this->_rewriteScriptCodes($content, array_values($matches));
		}

		unset($matches);
	}

	public function getContent($content)
	{

		$this->_rewrite($content);

		return $content;
	}

	protected function _switchTo($idartlang, $back = false)
	{
		static $old = array();
		static $regClone;

		if ($back) {
			Aitsu_Registry::get()->env 				= $regClone->env;
			Aitsu_Registry::get()->env->idartlang 	= $old['idartlang'];
			Aitsu_Registry::get()->env->idart 		= $old['idart'];
			Aitsu_Registry::get()->env->idlang 		= $old['idlang'];
			Aitsu_Registry::get()->env->idcat 		= $old['idcat'];
			Aitsu_Registry::get()->env->client 		= $old['client'];
			return;
		}

		$context 			= Aitsu_Core_Module_Context::get($idartlang);
		$regClone 			= clone Aitsu_Registry::get();
		$old['idartlang'] 	= Aitsu_Registry::get()->env->idartlang;
		$old['idart'] 		= Aitsu_Registry::get()->env->idart;
		$old['idlang'] 		= Aitsu_Registry::get()->env->idlang;
		$old['idcat'] 		= Aitsu_Registry::get()->env->idcat;
		$old['client'] 		= Aitsu_Registry::get()->env->client;

		Aitsu_Registry::get()->env->idart 		= $context['idart'];
		Aitsu_Registry::get()->env->idartlang 	= $context['idartlang'];
		Aitsu_Registry::get()->env->idlang 		= $context['idlang'];
		Aitsu_Registry::get()->env->idcat		= $context['idcat'];
		Aitsu_Registry::get()->env->client 		= $context['client'];
	}

	protected function _rewriteShortcodes($content, $matches)
	{
		$client = Moraso_Config::get('sys.client');

		$sc = Moraso_Shortcode::getInstance();

		for ($i = 0; $i < count($matches[0]); $i++) {
			$method = $matches[1][$i];

			if (!empty($matches[3][$i])) {
				$this->_switchTo(substr($matches[3][$i], 1));
			}

			try {
				$replacement = $sc->evalModule($method, null, $client, $matches[2][$i], empty($matches[3][$i]));
			} catch (Exception $e) {
				$replacement = $e->getMessage();
			}

			if (!empty($matches[3][$i])) {
				$this->_switchTo(0, true);
			}

			$content = str_replace($matches[0][$i], $replacement, $content);
		}

		$this->_rewrite($content);

		return $content;
	}

	protected function _rewriteShortmodules($content, $matches)
	{
		foreach ($matches[0] as $i => $shortmodule) {
			$module = $matches[2][$i];
			$index = str_replace('_', '.', $matches[4][$i]);

			if ($module === 'Template') {
				$heredity = Moraso_Skin_Heredity::build();

				$availableTemplateFiles = array();

				foreach (array_reverse($heredity) as $skin) {
					$templateFiles = glob(APPLICATION_PATH . '/skins/' . $skin . '/templates/' . str_replace('.', '/', strtolower($index)) . '/' . '*.phtml');

					foreach ($templateFiles as $templateFile) {
						$templateFileContent = file_get_contents($templateFile);

						$templateFileContentMatch = array('hidden' => array(), 'visible' => array());
						preg_match('/^<\\!\\-{3}\\s*(.*?)\\s*\\-{2}>/', $templateFileContent, $templateFileContentMatch['hidden']);

						$hidden = false;

						if (!empty($templateFileContentMatch['hidden'][1])) {
							$hidden = true;
							$name = $templateFileContentMatch['hidden'][1];
						} else {
							preg_match('/^<\\!\\-{2}\\s*(.*?)\\s*\\-{2}>/', $templateFileContent, $templateFileContentMatch['visible']);

							if (!empty($templateFileContentMatch['visible'][1])) {
								$name = $templateFileContentMatch['visible'][1];
							} else {
								$hidden = true;
								$name = substr($templateFile, strlen(APPLICATION_PATH . '/skins/' . $skin . '/templates/'));
							}
						}

						$availableTemplateFiles[substr(basename($templateFile), 0, -6)] = array(
							'name' => $name,
							'hidden' => $hidden,
							'file' => 'templates/' . str_replace('.', '/', strtolower($index)) . '/' . substr(basename($templateFile), 0, -6) . '.phtml'
						);
					}
				}

				$templateList = '';
				foreach ($availableTemplateFiles as $key => $availableTemplateFile) {
					if ($availableTemplateFile['hidden']) {
						$templateList .= "template_hidden." . $key . ".name = \"" . $availableTemplateFile['name'] . "\"\ntemplate_hidden." . $key . ".file = \"" . $availableTemplateFile['file'] . "\"n\n";
					} else {
						$templateList .= "template." . $key . ".name = \"" . $availableTemplateFile['name'] . "\"\ntemplate." . $key . ".file = \"" . $availableTemplateFile['file'] . "\"\n\n";
					}
				}

				$replace = "<script type=\"application/x-moraso\" src=\"Template:" . str_replace('.', '', $index) . "\">\ndefaultTemplate = default\n\n" . $templateList . "</script>";

				$content = str_replace($shortmodule, $replace, $content);
			}
		}

		$this->_rewrite($content);

		return $content;
	}

	protected function _rewriteScriptCodes($content, $matches)
	{
		$client = Moraso_Config::get('sys.client');

		$sc = Moraso_Shortcode::getInstance();

		for ($i = 0; $i < count($matches[0]); $i++) {
			$method = $matches[1][$i];
			$index 	= isset($matches[2][$i]) ? $matches[2][$i] : '';
			$params = isset($matches[3][$i]) ? $matches[3][$i] : null;

			$switched = false;
			if (preg_match('/execcontext\\s*=\\s*(\\d*)/', $params, $match)) {
				$this->_switchTo($match[1]);
				$switched = true;
			}
			
			try {
				$replacement = $sc->evalModule($method, $params, $client, $index, true);
			} catch (Exception $e) {
				$replacement = $e->getMessage();
			}

			if ($switched) {
				$this->_switchTo(0, true);
			}

			$content = str_replace($matches[0][$i], $replacement, $content);
		}

		$this->_rewrite($content);

		return $content;
	}
}