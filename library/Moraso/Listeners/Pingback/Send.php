<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Listeners_Pingback_Send implements Aitsu_Event_Listener_Interface
{
    public static function notify(Aitsu_Event_Abstract $event)
    {
        if (Moraso_Config::get('pingback.enabled')) {
        	$idartlang = $event->idartlang;
        	$idart = Moraso_Util::getIdart($idartlang);
        	$article_url = Moraso_Rewrite_Standard::getInstance()->rewriteOutput('{ref:idart-' . $idart . '}');

            $articleContent = Moraso_Db_Simple::fetch('value', '_article_content', array('idartlang' => $event->idartlang), 9999);
            
            foreach ($articleContent as $content) {
                $article_link_matches = array();
                preg_match_all('^href="(.*)"^iU', $content, $article_link_matches);

                foreach ($article_link_matches[1] as $targetUrl) {
                    $pingbackUrl = null;

                    $httpClient = new Zend_Http_Client($targetUrl);
                    $httpResponse = $httpClient->request();

                    if (!is_null($httpResponse->getHeader('x-pingback'))) {
                        $pingbackUrl = $httpResponse->getHeader('x-pingback');
                    } else {
                        preg_match_all('^<link rel="pingback" href="(.*)"^iU', $httpResponse->getBody(), $pingbackMatches);
                    
                        if (isset($pingbackMatches[1][0])) {
                            $pingbackUrl = $pingbackMatches[1][0];
                        }
                    }

                    if ($pingbackUrl) {
                        try {
                            $xmlrpcClient = new Zend_XmlRpc_Client($pingbackUrl);
                            $xmlrpcClient->getHttpClient()->setConfig(array('timeout' => 1));
                            $xmlrpcResult = $xmlrpcClient->call('pingback.ping', array($article_url, $targetUrl));
                        } catch (Exception $e) {
                            trigger_error($e->getMessage());
                        }
                    }
                }
            }
        }
    }
}