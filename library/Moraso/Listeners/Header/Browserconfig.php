<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Listeners\Header
 */
class Moraso_Listeners_Header_Browserconfig implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		if (!isset ($event->bootstrap->pageContent)) {
			return;
		}

		$heredity = array_reverse(Moraso_Skin_Heredity::build());

		foreach ($heredity as $skin) {
			if (file_exists(APPLICATION_PATH . '/skins/' . $skin . '/browserconfig.xml')) {
				Aitsu_Registry::get()->header->browserconfig = (object) array(
					"name" 	=> "Browserconfig",
					"tag" 	=> Moraso_Util::buildTag(array('name' => 'msapplication-config', 'content' => '/skins/' . $skin . '/browserconfig.xml'))
				);
				break;
			}
		}
	}
}