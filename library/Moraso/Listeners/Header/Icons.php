<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Listeners\Header
 */
class Moraso_Listeners_Header_Icons implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		if (!isset ($event->bootstrap->pageContent)) {
			return;
		}

		$heredity = array_reverse(Moraso_Skin_Heredity::build());

		$iconTags = array();
		foreach ($heredity as $skin) {
			if (file_exists(APPLICATION_PATH . '/skins/' . $skin . '/favicons/favicon.ico')) {
				$iconTags['shortcut_icon'] = Moraso_Util::buildTag(array(
					'rel' 	=> 'shortcut icon',
					'href' 	=> '/skins/' . $skin . '/favicons/favicon.ico',
				), 	'link');
			}

			$icons = array(16, 32, 96, 160, 196);
			foreach($icons as $icon) {
				if (file_exists(APPLICATION_PATH . '/skins/' . $skin . '/favicons/favicon-' . $icon . 'x' . $icon . '.png')) {
					$iconTags['favicon-' . $icon . 'x' . $icon] = Moraso_Util::buildTag(array(
						'rel' 	=> 'icon',
						'type' 	=> 'image/png',
						'href' 	=> '/skins/' . $skin . '/favicons/favicon-' . $icon . 'x' . $icon . '.png',
						'sizes' => $icon . 'x' . $icon
					), 	'link');
				}
			}

			if (file_exists(APPLICATION_PATH . '/skins/' . $skin . '/favicons/apple-touch-icon-precomposed.png')) {
				$iconTags['apple-touch-icon'] = Moraso_Util::buildTag(array(
					'rel' 	=> 'apple-touch-icon-precomposed',
					'href' 	=> '/skins/' . $skin . '/favicons/apple-touch-icon-precomposed.png'
				), 	'link');
			} elseif (file_exists(APPLICATION_PATH . '/skins/' . $skin . '/favicons/apple-touch-icon.png')) {
				$iconTags['apple-touch-icon'] = Moraso_Util::buildTag(array(
					'rel' 	=> 'apple-touch-icon',
					'href' 	=> '/skins/' . $skin . '/favicons/apple-touch-icon.png'
				), 	'link');
			}

			$appleTouchIcons = array(57, 60, 72, 76, 114, 120, 144, 152);
			foreach($appleTouchIcons as $appleTouchIcon) {
				if (file_exists(APPLICATION_PATH . '/skins/' . $skin . '/favicons/apple-touch-icon-' . $appleTouchIcon . 'x' . $appleTouchIcon . '-precomposed.png')) {
					$iconTags['apple-touch-icon-' . $appleTouchIcon . 'x' . $appleTouchIcon . '-precomposed'] = Moraso_Util::buildTag(array(
						'rel' 	=> 'apple-touch-icon-precomposed',
						'href' 	=> '/skins/' . $skin . '/favicons/apple-touch-icon-' . $appleTouchIcon . 'x' . $appleTouchIcon . '-precomposed.png',
						'sizes' => $appleTouchIcon . 'x' . $appleTouchIcon
					), 	'link');
				} elseif (file_exists(APPLICATION_PATH . '/skins/' . $skin . '/favicons/apple-touch-icon-' . $appleTouchIcon . 'x' . $appleTouchIcon . '.png')) {
					$iconTags['apple-touch-icon-' . $appleTouchIcon . 'x' . $appleTouchIcon] = Moraso_Util::buildTag(array(
						'rel' 	=> 'apple-touch-icon',
						'href' 	=> '/skins/' . $skin . '/favicons/apple-touch-icon-' . $appleTouchIcon . 'x' . $appleTouchIcon . '.png',
						'sizes' => $appleTouchIcon . 'x' . $appleTouchIcon
					), 	'link');
				}
			}

			if (file_exists(APPLICATION_PATH . '/skins/' . $skin . '/browserconfig.xml')) {
				$xml = simplexml_load_file(APPLICATION_PATH . '/skins/' . $skin . '/browserconfig.xml');

				if (!empty($xml->msapplication->tile)) {
					foreach ($xml->msapplication->tile as $tile) {
						foreach ($tile as $key => $row) {
							$iconTags['mstile-' . $key] = Moraso_Util::buildTag(array(
								'name' 		=> 'msapplication-' . $key,
								'content' 	=> !empty($row->attributes()->src) ? $row->attributes()->src[0] : $row
							));
						}
					}
				}
			}
		}
		
		if (!empty($iconTags)) {
			Aitsu_Registry::get()->header->icons = (object) array(
				"name" 	=> "Icons",
				"tags" 	=> $iconTags
			);
		}
	}
}