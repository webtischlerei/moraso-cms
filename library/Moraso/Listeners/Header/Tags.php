<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Listeners\Header
 */
class Moraso_Listeners_Header_Tags implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		if (!isset ($event->bootstrap->pageContent)) {
			return;
		}	

		$metaTags = self::getMetaTags();
		$linkTags = self::getLinkTags();

		$skinJsonMetaTags = Moraso_Skin_Heredity::get('meta_tags');

		if (!empty($skinJsonMetaTags)) {
			foreach ($skinJsonMetaTags as $name => $content) {
				$metaTags[$name] = Moraso_Util::buildTag(array('name' => $name, 'content' => $content));
			}
		}
		
		$metas = Moraso_Db::simpleFetch('all', '_art_meta', array('idartlang' => Aitsu_Registry::get()->env->idartlang));

		unset($metas['idartlang']);

		if (!empty($metas)) {
			foreach ($metas as $name => $content) {
				if (!empty($content)) {
					if ($name === 'author') {
						$author_mapper = new Moraso\Model\Author\Mapper();

						$author 	= $author_mapper->find($content);
						$content 	= $author->name;

						if (!empty($author->google_plus_id)) {
							$linkTags['author_google_plus'] = Moraso_Util::buildTag(array('rel' => 'author', 'href' => 'https://plus.google.com/' . $author->google_plus_id), 'link');
						}
					}

					$metaTags[$name] = Moraso_Util::buildTag(array('name' => $name, 'content' => $content));
				}
			}
		}

		if (!empty($metaTags)) {
			Aitsu_Registry::get()->header->meta_tags = (object) array('name' => 'Meta Tags', 'tags' => $metaTags);
		}

		if (!empty($linkTags)) {
			Aitsu_Registry::get()->header->link_tags = (object) array('name' => 'Link Tags', 'tags' => $linkTags);
		}
	}

	private static function getMetaTags()
	{
		if (!isset(Aitsu_Registry::get()->header->meta_tags)) {
			return array();
		}

		$metaTagsObject = Aitsu_Registry::get()->header->meta_tags;

		return !isset($metaTagsObject) || empty($metaTagsObject->tags) ? array() : (array) $metaTagsObject->tags;
	}

	private static function getLinkTags()
	{
		if (!isset(Aitsu_Registry::get()->header->link_tags)) {
			return array();
		}

		$linkTagsObject = Aitsu_Registry::get()->header->link_tags;

		return !isset($linkTagsObject) || empty($linkTagsObject->tags) ? array() : (array) $linkTagsObject->tags;
	}
}