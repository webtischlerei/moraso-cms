<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Listeners\Header
 */
class Moraso_Listeners_Header_OpenGraph implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		if (!isset ($event->bootstrap->pageContent)) {
			return;
		}

		$prefix 		= Moraso_Skin_Heredity::get('pagetitle.prefix');
		$suffix 		= Moraso_Skin_Heredity::get('pagetitle.suffix');
		$type 			= Moraso_Skin_Heredity::get('open_graph.type');
		$locale 		= Moraso_Skin_Heredity::get('open_graph.locale');
		$country_name 	= Moraso_Skin_Heredity::get('open_graph.country_name');
		$image 			= Moraso_Skin_Heredity::get('open_graph.image');

		$article 			= Aitsu_Persistence_Article::factory(Aitsu_Registry::get()->env->idart);
		$articleProperty 	= Aitsu_Persistence_ArticleProperty::factory(Aitsu_Registry::get()->env->idartlang)->load();

		if (isset($articleProperty->open_graph) && !empty($articleProperty->open_graph)) {
			$open_graph = (object) $articleProperty->open_graph;
		}

		$rewrite = $article->isIndex() ? '{ref:idcat-' . Aitsu_Registry::get()->env->idcat . '}' : '{ref:idart-' . Aitsu_Registry::get()->env->idart . '}';

		$url = Moraso_Rewrite_Standard::getInstance()->rewriteOutput($rewrite);
		
		$open_graphs = array(
			'og:title' 	=> $prefix . ' ' . $article->pagetitle . ' ' . $suffix,
			'og:url' 	=> $url
		);

		if (!empty($type)) {
			$open_graphs['og:type'] 		= $type;
		}

		if (!empty($locale)) {
			$open_graphs['og:locale'] 		= $locale;
		}

		if (!empty($country_name)) {
			$open_graphs['og:country-name'] = $country_name;
		}

		if (!empty($image)) {
			$open_graphs['og:image'] 		= $image;
		}

		if (!empty($article->mainimage)) {
			$open_graphs['og:image'] 		= \Moraso\Html\Helper\Image::getPath(Aitsu_Registry::get()->env->idart, $article->mainimage, 500, 500, 2);
			$open_graphs['og:image:width'] 	= 500;
			$open_graphs['og:image:height'] = 500;
		}

		$openGraphs = array();
		foreach ($open_graphs as $name => $content) {
			if (!empty($content)) {
				$openGraphs[] = Moraso_Util::buildTag(array('name' => $name, 'content' => $content));
			}
		}

		Aitsu_Registry::get()->header->open_graph = (object) array(
			"name" => "Open Graph",
			"tags" => $openGraphs
		);
	}
}