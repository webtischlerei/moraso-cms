<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Listeners\Header
 */
class Moraso_Listeners_Header_BaseHref implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		if (!isset ($event->bootstrap->pageContent)) {
			return;
		}

		Aitsu_Registry::get()->header->base_href = (object) array(
			"name" 	=> "BaseHref",
			"tag" 	=> Moraso_Util::buildTag(array('href' => Moraso_Config::get('sys.webpath')), 'base')
		);
	}
}