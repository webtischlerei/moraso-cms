<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Listeners\Header
 */
class Moraso_Listeners_Header_Css implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		if (!isset ($event->bootstrap->pageContent)) {
			return;
		}

		$css_collection = Moraso_Skin_Heredity::get('css');

		$css = array();
		if (count($css_collection) > 0) {		
			foreach ($css_collection as $key => $value) {
				if (strpos($value, '//') !== 0 && strpos($value, '<') !== 0 && strpos($value, 'http') !== 0) {
					$css[$key] = Moraso_Util::buildTag(array('rel' => 'stylesheet', 'href' => '/skin/' . $value), 'link');
				} else {
					if (strpos($value, 'http') === 0 || strpos($value, '//') === 0) {
						$css[$key] = Moraso_Util::buildTag(array('rel' => 'stylesheet', 'href' => $value), 'link');
					} else {
						$css[$key] = $value;
					}
				}
			}

			if (!empty($css)) {
				Aitsu_Registry::get()->header->css = (object) array(
					"name" => "CSS",
					"tags" => $css
				);
			}
		}
	}
}