<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Listeners\Header
 */
class Moraso_Listeners_Header_Generator implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		if (!isset ($event->bootstrap->pageContent)) {
			return;
		}

		$status = json_decode(file_get_contents(APPLICATION_PATH . '/status.json'));

		Aitsu_Registry::get()->header->generator = (object) array(
			"name" 	=> "Generator",
			"tag" 	=> Moraso_Util::buildTag(array('name' => 'generator', 'content' => 'moraso cms - v' . $status->version->major . '.' . $status->version->minor . '.' . $status->version->revision . '-' . $status->version->build))
		);
	}
}