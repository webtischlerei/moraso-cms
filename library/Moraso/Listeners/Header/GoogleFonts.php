<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Listeners\Header
 */
class Moraso_Listeners_Header_GoogleFonts implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		if (!isset ($event->bootstrap->pageContent)) {
			return;
		}

		$google_fonts = Moraso_Skin_Heredity::get('google.fonts');

		if (count($google_fonts) > 0) {		
			$fonts = array();
			foreach ($google_fonts as $font => $width) {
				if (!empty($width)) {
					$fonts[] = Moraso_Util::buildTag(array(
						'href' 	=> 'http://fonts.googleapis.com/css?family=' . $font . ':' . $width,
						'rel' 	=> 'stylesheet',
						'type' 	=> 'text/css'
					), 'link');
				}
			}

			if (!empty($fonts)) {
				Aitsu_Registry::get()->header->google_fonts = (object) array(
					"name" => "GoogleFonts",
					"tags" => $fonts
				);
			}
		}
	}
}