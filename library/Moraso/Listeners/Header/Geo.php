<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Listeners\Header
 */
class Moraso_Listeners_Header_Geo implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		if (!isset ($event->bootstrap->pageContent)) {
			return;
		}

		$geoTags = Moraso_Skin_Heredity::get('geo');

		$geo = array();
		if (count($geoTags) > 0) {		
			foreach ($geoTags as $key => $value) {
				$geo[$key] = Moraso_Util::buildTag(array('name' => $key, 'content' => $value));
			}

			Aitsu_Registry::get()->header->geo = (object) array(
				"name" => "GEO",
				"tags" => $geo
			);
		}	
	}
}