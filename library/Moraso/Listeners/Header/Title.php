<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Listeners\Header
 */
class Moraso_Listeners_Header_Title implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		if (!isset ($event->bootstrap->pageContent)) {
			return;
		}

		$prefix = Moraso_Skin_Heredity::get('pagetitle.prefix');
		$suffix = Moraso_Skin_Heredity::get('pagetitle.suffix');

		if (!empty($prefix) || !empty($suffix)) {
			$pageTitle = Aitsu_Core_Article::factory()->pagetitle;

			Aitsu_Registry::get()->header->title = (object) array(
				"name" => "Title",
				"tag" => '<title>' . $prefix . $pageTitle . $suffix . '</title>'
			);
		}
	}
}