<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Listeners\Header
 */
class Moraso_Listeners_Header_Charset implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		if (!isset ($event->bootstrap->pageContent)) {
			return;
		}

		$charset = Moraso_Skin_Heredity::get('charset');
		
		if (!empty($charset)) {
			Aitsu_Registry::get()->header->charset = (object) array(
				"name" 	=> "Charset",
				"tag" 	=> Moraso_Util::buildTag(array('charset' => $charset), 'meta')
			);
		}
	}
}