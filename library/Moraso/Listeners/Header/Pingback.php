<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Listeners\Header
 */
class Moraso_Listeners_Header_Pingback implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		if (Moraso_Config::get('pingback.enabled')) {
			if (!isset ($event->bootstrap->pageContent)) {
				return;
			}

			$url = Moraso_Rewrite_Standard::getInstance()->rewriteOutput('{ref:idart-' . Aitsu_Registry::get()->env->idart . '}');

			Aitsu_Registry::get()->header->pingback = (object) array(
				"name" 	=> "Pingback",
				"tag" 	=> Moraso_Util::buildTag(array('rel' => 'pingback', 'href' => Moraso_Config::get('sys.webpath') . '?renderOnly=Pingback'), 'link')
			);
		}
	}
}