<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Listeners\Header
 */
class Moraso_Listeners_Header_JavaScript implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		if (!isset ($event->bootstrap->pageContent)) {
			return;
		}

		$javaScriptTop 		= Moraso_Skin_Heredity::get('js.top');
		$javaScriptBottom 	= Moraso_Skin_Heredity::get('js.bottom');

		$topJS = array();
		if (!empty($javaScriptTop)) {
			foreach ($javaScriptTop as $key => $value) {
				if (strpos($value, '//') !== 0 && strpos($value, '<') !== 0 && strpos($value, 'http') !== 0) {
					$topJS[$key] = '<script src="/skin/' . $value . '"></script>';
				} else {
					if (strpos($value, 'http') === 0 || strpos($value, '//') === 0) {
						$topJS[$key] = '<script src="' . $value . '"></script>';
					} else {
						$topJS[$key] = $value;
					}
				}
			}

			if (!empty($topJS)) {	
				Aitsu_Registry::get()->header->javascript_top = (object) array(
					"name" 		=> "JavaScript (Top)",
					"tags" 		=> $topJS,
					"position" 	=> "top"
				);
			}
		}

		$bottomJS = array();
		if (!empty($javaScriptBottom)) {
			foreach ($javaScriptBottom as $key => $value) {
				if (strpos($value, '//') !== 0 && strpos($value, '<') !== 0 && strpos($value, 'http') !== 0) {
					$bottomJS[$key] = '<script src="/skin/' . $value . '"></script>';
				} else {
					if (strpos($value, 'http') === 0 || strpos($value, '//') === 0) {
						$bottomJS[$key] = '<script src="' . $value . '"></script>';
					} else {
						$bottomJS[$key] = $value;
					}
				}
			}
			if (!empty($bottomJS)) {	
				Aitsu_Registry::get()->header->javascript_bottom = (object) array(
					"name" 		=> "JavaScript (Bottom)",
					"tags" 		=> $bottomJS,
					"position" 	=> "bottom"
				);
			}
		}
	}
}