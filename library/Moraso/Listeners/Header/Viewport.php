<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Listeners\Header
 */
class Moraso_Listeners_Header_Viewport implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		if (!isset ($event->bootstrap->pageContent)) {
			return;
		}

		$viewport = Moraso_Skin_Heredity::get('viewport');
		
		if (!empty($viewport)) {
			Aitsu_Registry::get()->header->viewport = (object) array(
				"name" 	=> "Viewport",
				"tag" 	=> Moraso_Util::buildTag(array('name' => 'viewport', 'content' => $viewport))
			);
		}
	}
}