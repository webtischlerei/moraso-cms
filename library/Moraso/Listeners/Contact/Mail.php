<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013 - 2014, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Listeners_Contact_Mail implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		try {
			$mailer = new Moraso_Mail('default');
			$mailer->setBodyHtml(self::_createMessage());
			$mailer->send();

			$_POST['success'] = true;
		} catch (Exception $e) {
			$_POST['message'] = $e->getMessage();
		}
	}

	private static function _createMessage()
	{
		$emailmessage = 'Sehr geehrte Damen und Herren,<br />';
		$emailmessage.= '<br />';
		$emailmessage.= $_POST['title'] . ' ' . $_POST['firstname'] . ' ' . $_POST['lastname'] . ' hat soeben folgende Kontaktanfrage gestellt:<br />';
		$emailmessage.= '<br />';
		$emailmessage.= nl2br($_POST['request']);

		return $emailmessage;
	}

}