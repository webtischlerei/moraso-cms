<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Listeners_Cart implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		// JA, dieser Listener wird benötigt
		if (isset($_POST['form_listener']) && $_POST['form_listener'] === 'morasoCartForm' && isset($_POST['form_action'])) {
			$cart = Moraso_Cart::getInstance();

			// JA, es soll ein Produkt in den Warenkorb gelegt werden
			if ($_POST['form_action'] === 'addArticleToCart') {
				$cart->addArticle($_POST['idart'], (!empty($_POST['qty']) ? $_POST['qty'] : 1));
			}

			// JA, es soll ein Produkt aus den Warenkorb entfernt werden
			if ($_POST['form_action'] === 'deleteArticleFromCart') {
				$cart->removeArticle($_POST['idart']);
			}

			// JA, es sollen Formulardaten gespeichert werden
			if ($_POST['form_action'] === 'addFormData') {
				$formDataType = $_POST['form_data_type'];               
				$cart->setProperty($formDataType, $_POST);
			}

			// JA, die Bestellung wird ausgelößt
			if ($_POST['form_action'] === 'doCheckout') {
				$cart = Moraso_Cart::getInstance();

				if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
					header('Content-Type: application/json');
					echo json_encode(array('success' => $cart->doCheckout()));
					exit();
					die();
				}
			}
		}

		if (isset($_GET['confirmPayment'])) {
			if (isset($_POST['order_id']) || isset($_POST['invoice'])) {
				$order_id = isset($_POST['order_id']) ? $_POST['order_id'] : $_POST['invoice'];

				$paymentStrategy = Moraso_Cart::getPaymentStrategy($order_id);

				Moraso_Cart::setPaymentStatus($order_id, $paymentStrategy->doConfirmPayment($_POST));

				$paymentStrategy->actionAfterConfirm($order_id);
			}
		}
	}
}