<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Listeners_Cart_Mail implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		if ($event->billing['title'] === 'Herr') {
			$anrede = 'Sehr geehrter';
		} elseif ($event->billing['title'] === 'Frau') {
			$anrede = 'Sehr geehrte';
		}

		$emailmessage = $anrede . ' ' . $event->billing['title'] . ' ' . $event->billing['name']['last'] . ',<br />';
		$emailmessage.= '<br />';
		$emailmessage.= 'vielen Dank für Ihre Bestellung.<br />';
		$emailmessage.= '<br />';
		$emailmessage.= 'Ihre Bestellnummer lautet ' . $event->cart->order_id . '.<br /><br />';
		$emailmessage.= 'Ihre Bestellung nochmals in der Übersicht:<br />';

		$nf = new NumberFormatter('de_DE', NumberFormatter::CURRENCY);

		$amount_total = 0;
		$amount_total_tax = array();
		$tax_total = 0;

		$cart = Moraso_Cart::getInstance();

		foreach ($event->articles as $idart => $qty) {
			$articleInfo = Aitsu_Persistence_Article::factory($idart)->load();

			$idartlang = Moraso_Util::getIdArtLang($idart);

			$articleProperties = Aitsu_Persistence_ArticleProperty::factory($idartlang)->load();

			$articlePropertyCart = (object) $articleProperties->cart;

			// Standardpreis
			$price = $articlePropertyCart->price->value;
			
			// schauen ob es auch Staffelpreise gibt
			$staffelpreise = $cart->getStaffelpreise($idartlang);
			
			foreach ($staffelpreise as $staffel => $staffelpreis) {
				if ($qty >= $staffel) {
					$price = $staffelpreis;
				}
			}

			$price_total = bcmul($price, $qty, 2);

			$emailmessage.= $qty . 'x ' . $articleInfo->pagetitle . ' (' . $articlePropertyCart->sku->value . ') für ' . $nf->formatCurrency($price_total, 'EUR') . '<br />';

			$amount_total = bcadd($amount_total, $price_total, 2);

			$tax_class = (int) $articlePropertyCart->tax_class->value;

			if (isset($amount_total_tax[$tax_class])) {
				$amount_total_tax[$tax_class] = $amount_total_tax[$tax_class] + ($price_total - ($price_total / ((100 + $tax_class) / 100)));
			} else {
				$amount_total_tax[$tax_class] = $price_total - ($price_total / ((100 + $tax_class) / 100));
			}
		}

		foreach ($amount_total_tax as $tax_class => $tax_value) {
			$amount_total_tax[$tax_class] = $tax_value;
			$tax_total = $tax_total + $tax_value;
		}

		$amount_total_without_tax = $amount_total - $tax_total;

		$shippingCosts = $cart->getShippingCosts($amount_total_without_tax);

		$amount_total = $amount_total + $shippingCosts;

		$shippingCosts_tax = ($shippingCosts / 119) * 19;

		$amount_total_tax[19] = isset($amount_total_tax[19]) ? $amount_total_tax[19] + $shippingCosts_tax : $shippingCosts_tax;

		$tax_total = 0;
		foreach ($amount_total_tax as $tax_class => $tax_value) {
			$amount_total_tax[$tax_class] = $nf->formatCurrency($tax_value, 'EUR');
			$tax_total = $tax_total + $tax_value;
		}

		$amount_total_without_tax = $amount_total - $tax_total;

		$emailmessage.= '1x Versandkosten · ' . $nf->formatCurrency($shippingCosts, 'EUR') . '<br />';

		$emailmessage.= '<br />';
		$emailmessage.= 'zu überweisender Betrag: ' . $nf->formatCurrency($amount_total, 'EUR') . '<br />';
		$emailmessage.= 'enthaltende MwSt.: ' . $nf->formatCurrency($tax_total, 'EUR') . '<br />';

		$emailmessage.= '<br />';

		$emailmessage.= '<strong>Versandadresse</strong><br />';
		if (isset($event->delivery['business']) && !empty($event->delivery['business'])) {
			$emailmessage.= $event->delivery['business'] . '<br />';
		}
		$emailmessage.= $event->delivery['title'] . ' ' . $event->delivery['name']['first'] . ' ' . $event->delivery['name']['last'] . '<br />';
		$emailmessage.= $event->delivery['street'] . ' ' . $event->delivery['housenumber'] . '<br />';
		$emailmessage.= $event->delivery['postal_code'] . ' ' . $event->delivery['city'] . '<br />';
		$emailmessage.= $event->delivery['country'] . '<br />';

		$emailmessage.= '<br />';

		/** Rechnungsadresse **/
		$emailmessage.= '<strong>Rechnungsadresse</strong><br />';
		if (isset($event->billing['same_than_delivery']) && $event->billing['same_than_delivery']) {
			$emailmessage.= 'Entspricht der Versandadresse<br />';
		} else {
			if (isset($event->billing['business']) && !empty($event->billing['business'])) {
				$emailmessage.= $event->billing['business'] . '<br />';
			}
			$emailmessage.= $event->billing['title'] . ' ' . $event->billing['name']['first'] . ' ' . $event->billing['name']['last'] . '<br />';
			$emailmessage.= $event->billing['street'] . ' ' . $event->billing['housenumber'] . '<br />';
			$emailmessage.= $event->billing['postal_code'] . ' ' . $event->billing['city'] . '<br />';
			$emailmessage.= $event->billing['country'] . '<br />';
		}

		/** Zahlungsweise **/
		$emailmessage.= '<br /><strong>Zahlungsweise</strong><br />';

		switch ($event->payment['method']) {
			case 'paypal':
				$payment = 'PayPal';
				break;
			case 'creditcard':
				$payment = 'Kreditkarte';
				break;
			default:
				$payment = 'Vorkasse';
		}

		$emailmessage.= $payment . '<br /><br /><hr /><br /><br />';

		/** Widerrufsbelehrung **/
		$conditions = Moraso_Db_Simple::fetch('value', '_article_content', array(
			'idartlang' => Moraso_Util::getIdArtLang(Moraso_Config::get('shop.structure.article.tac.conditions')),
			'index' 	=> 'Content'
		));

		$emailmessage.= '<strong>Widerrufsbelehrung</strong><br />';
		$emailmessage.= $conditions;

		$emailmessage.= '<br />';
		
		/** Signatur **/
		$signature = Aitsu_Placeholder::get('email.config.signatur');

		$emailmessage.= $signature;
		
		// Versand
		$mail = new Moraso_Mail('shop');

		$mail->setSubject('Ihre Bestellung');

		$mail->addTo(array(
			$event->billing['name']['first'] . ' ' . $event->billing['name']['last'] => $event->billing['email']
		));

		$mail->setBodyHtml($emailmessage);

		$mail->send();
	}
}