<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Listeners
 */ 

class Moraso_Listeners_User implements Aitsu_Event_Listener_Interface
{
	public static function notify(Aitsu_Event_Abstract $event)
	{
		// JA, dieser Listener wird benötigt
		if (isset($_POST['form_listener']) && $_POST['form_listener'] === 'user' && isset($_POST['form_action'])) {
			$userMapper 			= new \Moraso\Model\User\Mapper();
			$userAuthMapper 		= new \Moraso\Model\User\Auth\Mapper();
			$userHasAclRoleMapper 	= new \Moraso\Model\User\Has\Acl\Role\Mapper();

			// JA, es soll ein Benutzer angelegt werden
			if ($_POST['form_action'] === 'registerUser') {
				$userAuthUsername = $_POST['username'];
				$userAuthPassword = $_POST['password'];

				unset($_POST['form_listener']);
				unset($_POST['form_action']);
				unset($_POST['submit']);
				unset($_POST['username']);
				unset($_POST['password']);
				unset($_POST['password_repeat']);

				$coreData 	= array('salutation', 'firstname', 'lastname', 'email');

				$user 		= $userMapper->create();

				$pluginData = array();
				foreach ($_POST as $key => $value) {
					if (in_array($key, $coreData)) {
						$user->$key = $value;
					} else {
						$pluginData[$key] = $value;
					}
				}

				$user->id_eav_entity = NULL;

				if (!empty($pluginData)) {
					$user->id_eav_entity = \Moraso\Eav::set('usermanagement_plugin_data', $pluginData);
				}
				
				// Benutzer anlegen
				$userMapper->save($user);

				// Authentifizierung anlegen
				$userAuth 			= $userAuthMapper->create();
				$userAuth->user_id 	= $user->id;
				$userAuth->status 	= 2;
				$userAuth->username = $userAuthUsername;
				$userAuth->password = $userAuthPassword;

				$userAuthMapper->save($userAuth);

				// Benutzer der Frontend-Nutzer-Rolle zuweisen
				$role 				= $userHasAclRoleMapper->create();
				$role->id_user 		= $user->id;
				$role->id_acl_role 	= 3;

				$userHasAclRoleMapper->save($role);

				$_POST['user_created'] = true;

				// Kategorie für den Benutzer anlegen
				$createInIdCat 		= Moraso_Config::get('account.mydata');

				$idcat 				= Aitsu_Persistence_Category::factory($createInIdCat)->insert(Aitsu_Registry::get()->env->idlang, $userAuth->username);
				
				$category 			= Aitsu_Persistence_Category::factory($idcat)->load();

				$category->public 	= false;
				$category->visible 	= true;

				$category->save();

				// Dem Benutzer die Kategorie ID zuweisen
				if (!is_null($user->id_eav_entity)) {
					\Moraso\Eav::set('usermanagement_plugin_data', array(
						'id' 		=> $user->id_eav_entity,
						'user.cat' 	=> $idcat
					));
				} else {
					Moraso_Db::startTransaction();
					$user->id_eav_entity = \Moraso\Eav::set('usermanagement_plugin_data', array(
						'user_cat' 	=> $idcat
					));
					Moraso_Db::commit();		

					// Benutzer updaten
					$userMapper->save($user);
				}

				// Artikel für den Benutzer anlegen
				$art 			= Aitsu_Persistence_Article::factory();

				$art->title 	= $userAuth->username;
				$art->pagetitle = 'persönlicher Bereich: ' . $userAuth->username;
				$art->online 	= 1;
				$art->idclient 	= 1;
				$art->idcat 	= $idcat;

				$art->setAsIndex();

				$art->save();
			}

			// JA, es soll ein Benutzer geupdatet werden
			if ($_POST['form_action'] === 'changePersonalData') {
				$userAuthUsername = $_POST['username'];
				$userAuthPassword = $_POST['password'];

				unset($_POST['form_listener']);
				unset($_POST['form_action']);
				unset($_POST['submit']);
				unset($_POST['username']);
				unset($_POST['password']);
				unset($_POST['password_repeat']);

				$coreData 	= array('salutation', 'firstname', 'lastname', 'email');

				$user 		= $userMapper->find(\Moraso\User::get('id'));

				$pluginData = array();
				foreach ($_POST as $key => $value) {
					if (in_array($key, $coreData)) {
						$user->$key = $value;
					} else {
						$pluginData[$key] = $value;
					}
				}

				if (!empty($pluginData)) {
					if (!empty($user->id_eav_entity)) {
						$pluginData['id'] = $user->id_eav_entity;
					}

					$user->id_eav_entity = \Moraso\Eav::set('usermanagement_plugin_data', $pluginData);
				}

				if (empty($user->id_eav_entity)) {
					$user->id_eav_entity = NULL;
				}

				// Benutzer ändern
				$userMapper->save($user);

				// Authentifizierung ändern
				if (!empty($userAuthUsername) && !empty($userAuthUsername)) {
					$userAuth 			= $userAuthMapper->find($user->id);
					$updateUserAuthPass = false;
				
					if (!empty($userAuthUsername)) {
						$userAuth->username = $userAuthUsername;
					}

					if (!empty($userAuthPassword)) {
						$userAuth->password = $userAuthPassword;
						$updateUserAuthPass = true;
					}

					$userAuthMapper->save($userAuth, null, $updateUserAuthPass);
				}	

				$_POST['user_updatet'] = true;
			}
		}
	}
}