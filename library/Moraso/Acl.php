<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */

namespace Moraso;

use Zend\Permissions\Acl\Acl as ZendAcl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;

use Moraso\User;
use Moraso\Model;
use Moraso\Registry;
use Moraso\Service\Manager as ServiceManager;

class Acl
{
	/**
	* TODO
	*/
	public static function init()
	{
		$serviceManager = ServiceManager::get();
		$acl 			= new ZendAcl();

		$user_data 		= $serviceManager->get('moraso-user-data');

		// Rollen laden
		$roleSet = \Moraso\Acl\Role::get();

		if (count($roleSet) === 1) {
			$roleSet = array($roleSet);
		}

		// Rollen in die ACL eintragen
		foreach ($roleSet as $role) {
			if ($role->parent) {
				$parent = \Moraso\Acl\Role::get($role->parent);

				$acl->addRole(new Role(strtolower($role->name)), strtolower($parent->name));
			} else {
				$acl->addRole(new Role(strtolower($role->name)));
			}
		}

		// Ressourcen laden
		$resourceSet = \Moraso\Acl\Resource::get();

		if (count($resourceSet) === 1) {
			$resourceSet = array($resourceSet);
		}

		// Ressourcen in die ACL eintragen
		foreach ($resourceSet as $resource) {
			if ($resource->parent) {
				$parent = \Moraso\Acl\Resource::get($resource->parent);

				$acl->addResource(new Resource(strtolower($resource->name)), strtolower($parent->name));
			} else {
				$acl->addResource(new Resource(strtolower($resource->name)));
			}
		}

		// Regeln laden
		$ruleSet = \Moraso\Acl\Rule::get();

		if (count($ruleSet) === 1) {
			$ruleSet = array($ruleSet);
		}

		// Regeln in die ACL eintragen
		foreach ($ruleSet as $rule) {
			$role 		= strtolower(\Moraso\Acl\Role::get($rule->id_acl_role)->name);
			$resource 	= strtolower(\Moraso\Acl\Resource::get($rule->id_acl_resource)->name);
			$privilege 	= strtolower(\Moraso\Acl\Privilege::get($rule->id_acl_privilege)->name);

			if ($rule->access) {
				$acl->allow(strtolower($role), array($resource), $privilege);
			} else {
				$acl->deny(strtolower($role), array($resource), $privilege);
			}
		}

		// Der Administratoren-Rolle alle Rechte gewähren
		$acl->allow('administrator');
		
		// Wenn ein User angemeldet ist dessen Regeln laden	
		if (isset($user_data->id) && $user_data->id) {
			// Benötigte Klassen laden
			$userHasAclRoleMapper = new \Moraso\Model\User\Has\Acl\Role\Mapper();

			// User-Rolle erzeugen
			$userRole = new Role('user_role_' . $user_data->id);

			// Standard Rolle angeben von welcher geerbt wird wenn dem User keine Rolle zugewiesen wurde
			$role = 'guest';

			// Zugewiesene Rolle auslesen
			$userHasAclRoleObject = $userHasAclRoleMapper->find($user_data->id, 'id_user');

			// Prüfen ob dem User eine Rolle zugewiesen wurde, wenn ja Rolle festlegen
			if (!empty($userHasAclRoleObject->id_acl_role)) {
				$role = strtolower(\Moraso\Acl\Role::get($userHasAclRoleObject->id_acl_role)->name);
			}

			// User-Rolle dem ACL zuweisen
			$acl->addRole($userRole, $role);

			$eav_data = Eav::get('usermanagement_plugin_data', $user_data->id_eav_entity);

			// Zugriff auf die User-Kategorie gewähren
			if (!empty($eav_data['user_cat'])) {
				$acl->allow($userRole, 'cat.' . $eav_data['user_cat'], 'view');
			}

			/*
			// User-Ressourcen laden
			$userHasResources = $userHasAclResourceMapper->find($user_data->id);

			// User-Ressourcen der User-Rolle zuweisen
			if (is_array($userHasResources)) {
				foreach ($userHasResources as $userHasResource) {
					$resource 	= strtolower($aclResourceMapper->find($userHasResource->id_acl_resource)->name);
					$privileges = array('view');

					// TODO: Hier müssen Privilegien geladen werden, nicht Ressourcen sind "allow" oder "deny", sondern Privilegien

					if ($userHasResource->allow) {
						$acl->allow($userRole, array($resource), $privileges);
					} elseif ($userHasResource->deny) {
						$acl->deny($userRole, array($resource), $privileges);
					}
				}
			}
			*/
		}

		// ACL dem ServiceManager übergeben
		$serviceManager->setService('moraso-acl', $acl);
	}

	/**
	* TODO
	*/
	public static function isAllowed($resource, $privilege = 'view')
	{
		$serviceManager = Service\Manager::get();

		$acl 			= $serviceManager->get('moraso-acl');
		$user_data 		= $serviceManager->get('moraso-user-data');

		// Standard-Rolle zuweisen
		$role 			= 'guest';

		// Schauen ob der User eingeloggt ist, wenn ja User-Rolle festlegen
		if (isset($user_data->id) && $user_data->id) {
			$role = 'user_role_' . $user_data->id;
		}

		// Workaround
		if (is_array($resource)) {
			$resource = strtolower($resource['area']);
		}

		// 
		$resource 	= strtolower($resource);
		$privilege 	= strtolower($privilege);

		// 
		if (Registry::has($role . '_' . $resource . '_' . $privilege)) {
			return Registry::get($role . '_' . $resource . '_' . $privilege);
		}

		// prüfen ob die Ressource die angefragt wird überhaupt existiert, wenn nicht diese neu anlegen
		if (!$acl->hasResource($resource)) {
			self::createResource($acl, $resource);
		}

		// Prüfen ob der Nutzer Zugriff auf das Privileg der angegebenen Ressource hat
		$isAllowed = $acl->isAllowed($role, $resource, $privilege);

		// 
		Registry::set($role . '_' . $resource . '_' . $privilege, $isAllowed);

		// 
		return $isAllowed;
	}

	/**
	* TODO
	*/
	private static function createResource($acl, $resource)
	{
		$exploded 					= explode('.', $resource);
		$countExploded 				= count($exploded);
		$countExplodedParent 		= $countExploded-1;
		$shortedResource 			= array_slice($exploded, 0, $countExplodedParent);
		$untrimmedParentResource 	= implode('.', $shortedResource);
		$parentResource 			= rtrim($untrimmedParentResource);

		if (empty($parentResource)) {
			if (!$resourceModel = \Moraso\Acl\Resource::get($resource, 'name')) {
				$resourceModel = \Moraso\Acl\Resource::create(array('name' => $resource));
			}

			$acl->addResource(new Resource($resource));

			return $resourceModel;
		} else {
			if (!$parentResourceModel = \Moraso\Acl\Resource::get($parentResource, 'name')) {
				$parentResourceModel = self::createResource($acl, $parentResource);
			}	

			$resourceModel = \Moraso\Acl\Resource::create(array('name' => $resource, 'parent' => $parentResourceModel->id));

			$acl->addResource(new Resource($resourceModel->name), $parentResourceModel->name);

			return $resourceModel;
		}
	}
}