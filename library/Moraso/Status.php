<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Status
{
	public static function version()
	{
		$status_json = file_get_contents(APPLICATION_PATH . '/status.json');

		$status = json_decode($status_json);

		$version = $status->version;

		if ($version->branch === 'development') {
			return 'DEVELOPMENT';
		}

		if ($version->branch === 'master') {
			return $version->major . '.' . $version->minor . '.' . $version->revision . '-' . $version->build;
		} else {
			return $version->major . '.' . $version->minor . '.' . $version->revision . '-' . $version->build . ' (' . $version->branch . ')';
		}
	}

	public static function latestVersion()
	{
		$status_json = @file_get_contents('https://bitbucket.org/webtischlerei/moraso-cms/raw/master/application/status.json');

		$status = json_decode($status_json);

		$version = $status->version;

		return $version->major . '.' . $version->minor . '.' . $version->revision . '-' . $version->build;
	}
}