<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Cache
 */

namespace Moraso\Cache;

use Zend\Stdlib\SplQueue;
use Zend\Json\Json;

class Page
{
	/**
	 * @return boolean
	 */
	public static function save($content, $lifetime = 10)
	{
		$hash = self::createHashOfCurrentRequest();

		$fileName = CACHE_PATH . '/' . $hash . '.json';

		if (!is_writable(CACHE_PATH)) {
			mkdir(CACHE_PATH);
		}

		if (is_writable(CACHE_PATH)) {
			$data = json_encode(array(
				'content' 	=> $content,
				'lifetime' 	=> time() + $lifetime,
				'etag' 		=> sha1($content)
			));

			$fileHandle = fopen($fileName, "w");

			fwrite($fileHandle, $data);

			fclose($fileHandle);
		}
	}

	/**
	 * @return mixed
	 */
	public static function get()
	{
		$hash = self::createHashOfCurrentRequest();

		$fileName = CACHE_PATH . '/' . $hash . '.json';

		$data = new SplQueue();
		if (is_readable($fileName)) {
			$fileHandle 	= fopen($fileName, "r");
			$jsonData 		= fread($fileHandle, filesize($fileName));
			fclose($fileHandle);
			
			$data 			= Json::decode($jsonData, Json::TYPE_OBJECT);

			$data->isValid 	= $data->lifetime > time() ? true : false;

			if (!$data->isValid) {
				unlink($fileName);
			}			
		} else {
			$data->isValid 	= false;
		}

		return $data;
	}

	/**
	 * @return boolean
	 */
	public static function clear()
	{
		// Alle PageCache Dateien löschen!
	}

	/**
	 * @return mixed
	 */
	public static function createHashOfCurrentRequest()
	{
		return sha1(serialize(array_merge($_REQUEST, array($_SERVER['HTTP_HOST']))));
	}
}