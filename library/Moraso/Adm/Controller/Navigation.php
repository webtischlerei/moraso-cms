<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Adm_Controller_Navigation extends Zend_Controller_Plugin_Abstract
{
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
		if (strtolower($request->getParam('controller')) === 'index') {
			try {
				// 
				$pluginArea = array();

				// alle Plugins welche sich nicht in der Aitsu oder Moraso Library befinden
				$plugins = $this->_getPlugins(null, array('Aitsu', 'Moraso'));

				$pluginArea['thirdParty'] = array(
					'label' 		=> 'Erweiterungen',
					'id' 			=> 'plugins',
					'controller' 	=> 'plugins',
					'action' 		=> 'index',
					'route' 		=> 'default',
					'icon' 			=> 'upload-cloud',
					'area' 			=> 'plugins',
					'hidden' 		=> true
				);

				if (!empty($plugins['thirdparty'])) {
					$pluginArea['thirdParty']['pages'] 	= $plugins['thirdparty'];
					$pluginArea['thirdParty']['hidden'] = false;
				}

				// alle Plugins welche sich in der Aitsu oder Moraso Library befinden
				$plugins = $this->_getPlugins(array('Aitsu', 'Moraso'));

				$pluginArea['verwaltung'] = array(
					'label' 		=> 'Verwaltungen',
					'id' 			=> uniqid(),
					'controller' 	=> 'plugins',
					'action' 		=> 'index',
					'route' 		=> 'default',
					'icon' 			=> 'database'
				);

				if (!empty($plugins['verwaltung'])) {
					$pluginArea['verwaltung']['pages'] = $plugins['verwaltung'];
				} else {
					$pluginArea['verwaltung']['ac'] = array(
						'area' => 'keinZutritt'
					);
				}

				$pluginArea['konfiguration'] = array(
					'label' 		=> 'Konfigurationen',
					'id' 			=> uniqid(),
					'controller' 	=> 'plugins',
					'action' 		=> 'index',
					'route' 		=> 'default',
					'icon' 			=> 'wrench'
				);

				if (!empty($plugins['konfiguration'])) {
					$pluginArea['konfiguration']['pages'] = $plugins['konfiguration'];
				} else {
					$pluginArea['konfiguration']['ac'] = array(
						'area' => 'keinZutritt'
					);
				}

				$pluginArea['system'] = array(
					'label' 		=> 'System',
					'id' 			=> uniqid(),
					'controller' 	=> 'plugins',
					'action' 		=> 'index',
					'route' 		=> 'default',
					'icon' 			=> 'widget'
				);

				if (!empty($plugins['system'])) {
					$pluginArea['system']['pages'] = $plugins['system'];
				} else {
					$pluginArea['system']['ac'] = array(
						'area' => 'keinZutritt'
					);
				}

				$nav = array(
					array(
						'label' 		=> 'Dashboard',
						'id' 			=> 'dashboard',
						'controller' 	=> 'index',
						'action' 		=> 'index',
						'route' 		=> 'default',
						'icon' 			=> 'monitor'
					),
					array(
						'label' 		=> 'Seitenstruktur',
						'id' 			=> 'page',
						'controller' 	=> 'data',
						'action' 		=> 'index',
						'route' 		=> 'default',
						'ac' 			=> array(
							'area' => 'article'
						),
						'icon' 			=> 'page-multiple'
					),
					$pluginArea['verwaltung'],
					$pluginArea['thirdParty'],
					$pluginArea['konfiguration'],
					$pluginArea['system']
				);

				Zend_Registry::set('nav', new Zend_Navigation($nav));
			} catch (Exception $e) {
				echo $e->getMessage();
				exit;
			}
		}
	}

	protected function _getPlugins($with = array(), $without = array())
	{
		$plugins 	= array();
		$namespaces = Moraso_Plugins::getNamespaces();
		$pages 		= array();

		if (!is_null($with) && \Moraso\User::isLoggedIn()) {			
			$plugins['konfiguration'][] = array(
									'label' 		=> 'Sprachen',
									'id' 			=> 'client',
									'controller' 	=> 'client',
									'action' 		=> 'index',
									'route' 		=> 'default',
									'ac' 			=> array(
										'area' 	=> 'client'
									),
									'icon' 			=> 'compass'
								);
			
			$plugins['konfiguration'][] = array(
									'label' => 'Konfigurations-Blöcke',
									'id' => 'configs',
									'controller' => 'config',
									'action' => 'index',
									'route' => 'default',
									'ac' => array(
										'area' => 'config'
									),
									'icon' => 'clipboard-notes'
								);
						
			$plugins['konfiguration'][] = array(
									'label' => 'Scripte',
									'id' => 'scripts',
									'controller' => 'script',
									'action' => 'index',
									'route' => 'default',
									'ac' => array(
										'area' => 'script'
									),
									'icon' => 'list-thumbnails'
								);

			$plugins['konfiguration'][] = array(
				'label' 		=> 'Übersetzungen',
				'id' 			=> 'translation',
				'controller' 	=> 'translation',
				'action' 		=> 'index',
				'route' 		=> 'default',
				'ac' 			=> array(
					'area' 		=> 'translation'
				),
				'icon' 			=> 'web'
			);

			$plugins['konfiguration'][] = array(
				'label' 		=> 'Mapping',
				'id' 			=> 'mapping',
				'controller' 	=> 'mapping',
				'action' 		=> 'index',
				'route' 		=> 'default',
				'ac' 			=> array(
					'area' 		=> 'mapping'
				),
				'icon' 			=> 'arrows-out'
			);
		}

		foreach ($namespaces as $namespace) {
			if ((empty($without) && in_array($namespace, $with)) || (empty($with) && !in_array($namespace, $without))) {
				if (!is_array($namespace)) {
					$pluginDir = APPLICATION_LIBPATH . '/' . $namespace . '/Plugin';
				} else {
					$pluginDir = APPLICATION_LIBPATH . '/' . $namespace['namespace'] . '/' . $namespace['subnamespace'] . '/Plugin';

					$subnamespaceInfo = simplexml_load_file($pluginDir . '/plugin.xml');
					
					$pages[$namespace['namespace']][$namespace['subnamespace']] = array(
						'name' => (string) $subnamespaceInfo->name,
						'icon' => (string) $subnamespaceInfo->icon
					);
				}

				$files 		= Aitsu_Util_Dir::scan($pluginDir, 'plugin.xml');
				$baseLength = strlen($pluginDir);

				foreach ($files as $plugin) {
					$pluginXml 		= realpath(dirname($plugin) . '/plugin.xml');
					$pluginInfo 	= simplexml_load_file($pluginXml);
					$pluginPathInfo = explode('/', substr($plugin, $baseLength + 1));

					$pluginName = $pluginPathInfo[0];

					if (isset($pluginPathInfo[1]) && $pluginPathInfo[1] === 'Generic') {
						$aclAreaCheck = $pluginInfo->privileges->privilege->identifier;

						if (\Moraso\Acl::isAllowed($aclAreaCheck)) {
							if (!is_array($namespace)) {
								$area = (string) !isset($pluginInfo->area) || empty($pluginInfo->area) ? 'system' : $pluginInfo->area;

								if (isset($pluginInfo->parent) && !empty($pluginInfo->parent)) {
									$plugin_pages[(string) $pluginInfo->parent][] = array(
										'label' 		=> (string) $pluginInfo->name,
										'id' 			=> uniqid(),
										'controller' 	=> 'plugin',
										'params' 		=> array(
											'namespace' 	=> $namespace,
											'parent_plugin' => (string) $pluginInfo->parent,
											'plugin' 		=> $pluginPathInfo[2],
											'paction' 		=> 'index',
											'area' 			=> 'generic'
										),
										'route' => 'child_plugin',
										'icon' 	=> (string) $pluginInfo->icon
									);
								} else {
									$plugins[strtolower($area)][strtolower($pluginName)] = array(
										'label' 		=> (string) $pluginInfo->name,
										'id' 			=> uniqid(),
										'controller' 	=> 'plugin',
										'params' 		=> array(
											'namespace' => $namespace,
											'plugin' 	=> $pluginName,
											'paction' 	=> 'index',
											'area' 		=> 'generic'
										),
										'route' => 'plugin',
										'icon' 	=> (string) $pluginInfo->icon
									);
								}
							} else {
								$pages[$namespace['namespace']][$namespace['subnamespace']]['pages'][] = array(
									'label' 		=> (string) $pluginInfo->name,
									'id' 			=> uniqid(),
									'controller' 	=> 'plugin',
									'params' 		=> array(
										'namespace' 	=> $namespace['namespace'],
										'subnamespace' 	=> $namespace['subnamespace'],
										'plugin' 		=> $pluginName,
										'paction' 		=> 'index',
										'area' 			=> 'generic'
									),
									'route' => 'plugin',
									'icon' 	=> (string) $pluginInfo->icon
								);
							}
						}
					}
				}

				if (isset($plugin_pages) && !empty($plugin_pages)) {
					foreach ($plugin_pages as $plugin => $plugin_page) {
						$plugins['verwaltung'][$plugin]['pages'] = $plugin_page;
					}
				}

				if (isset($pages) && !empty($pages)) {
					foreach ($pages as $namespace_name => $namespace) {
						foreach ($namespace as $subnamespace_name => $subnamespace) {
							if (isset($subnamespace['pages']) && !empty($subnamespace['pages'])) {
								usort($subnamespace['pages'], array("Moraso_Adm_Controller_Navigation", "cmp"));

								$plugins['thirdparty'][] = array(
									'label' => $subnamespace['name'],
									'id' 	=> uniqid(),
									'pages' => $subnamespace['pages'],
									'route' => 'plugin',
									'icon' 	=> $subnamespace['icon']
								);
							}
						}
					}
				}
			}
		}

		foreach ($plugins as $pluginArea => $pluginSet) {
			uasort($pluginSet, array('self', 'cmp'));
			$plugins[$pluginArea] = $pluginSet;
		}

		return $plugins;
	}

	static function cmp($a, $b)
	{
		return strcmp($a["label"], $b["label"]);
	}
}