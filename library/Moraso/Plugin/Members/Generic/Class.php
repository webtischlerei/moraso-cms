<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */
class Moraso_Plugin_Members_Generic_Controller extends Moraso_Adm_Plugin_Controller
{
	public function storeAction()
	{
		$data = \Moraso\Eav::get('plugin_generic_management_members');

		$this->_helper->json((object) array(
			'data' => $data
		));
	}

	public function editAction()
	{
		$this->_helper->layout->disableLayout();

		$id 			= $this->getRequest()->getParam('id');
		
		$classExplode 	= explode('_', __CLASS__);
		$formPath 		= APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/forms/';
		$form 			= Aitsu_Forms::factory(strtolower($classExplode[2]), $formPath . 'member.ini');
		$form->title 	= Aitsu_Translate::translate('Edit Members');
		$form->url 		= $this->view->url(array('paction' => 'edit'), 'plugin');

		$formConfig 	= $form->getConfig();
		
		$plugins 		= Aitsu_Util_Dir::scan($formPath . 'Plugins', '*.ini');
		foreach ($plugins as $plugin) {
			$formConfig->merge(new Zend_Config_Ini($plugin));
		}

		if (!empty($id)) {
			$data = \Moraso\Eav::get('plugin_generic_management_members', $id);

			$form->setValue('id', $id);
			$form->setValues($data);
		}

		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		header('Content-type: text/html');

		try {
			if ($form->isValid()) {

				$data = $form->getValues();

				\Moraso\Eav::set('plugin_generic_management_members', $data);

				echo json_encode(array('success' => true, 'id' => $data['id']));
				exit();
			} else {
				echo json_encode(array('success' => false, 'errors' => $form->getErrors()));
				exit();
			}
		} catch (Exception $e) {
			echo json_encode(array('success' => false, 'exception' => true, 'message' => $e->getMessage()));
			exit();
		}
	}

	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id');

		try {
			\Moraso\Eav::delete($id);

			$this->_helper->json((object) array(
				'success' => true
			));
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

}