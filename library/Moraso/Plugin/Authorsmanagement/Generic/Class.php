<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Author
 */
class Moraso_Plugin_Authorsmanagement_Generic_Controller extends Moraso_Adm_Plugin_Controller
{
	protected function _preInit()
	{
		$this->_helper->authorMapper = new \Moraso\Model\Author\Mapper();
	}

	public function storeAction()
	{
		$authorSet = $this->_helper->authorMapper->fetchAll();

		$authors = array();
		foreach ($authorSet as $author) {
			$authors[] = (object) array(
				'id' 				=> $author->id,
				'name' 				=> $author->name,
				'google_plus_id' 	=> $author->google_plus_id,
				'initials' 			=> $author->initials,
				'email' 			=> $author->email
			);
		}
		
		$this->_helper->json((object) array(
			'message' 	=> count($authors) . ' Autoren wurden geladen',
			'authors' 	=> $authors,
			'success' 	=> true,
			'total' 	=> count($authors)
		));
	}

	public function editAction()
	{
		$this->_helper->layout->disableLayout();

		$id 			= $this->getRequest()->getParam('id');
		$classExplode 	= explode('_', __CLASS__);

		$form 			= Aitsu_Forms::factory(strtolower($classExplode[2]), APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/forms/author.ini');
		$form->title 	= 'Autoren bearbeiten';
		$form->url 		= $this->view->url(array('paction' => 'edit'));

		if ($id) {
			$author = $this->_helper->authorMapper->find($id);

			$form->setValues(array(
				'id' 				=> $author->id,
				'name' 				=> $author->name,
				'google_plus_id' 	=> $author->google_plus_id,
				'initials' 			=> $author->initials,
				'email' 			=> $author->email
			));
		}

		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {
				$data = $form->getValues();

				$author 					= $this->_helper->authorMapper->create();

				if (!empty($data['id'])) {
					$author->id 			= $data['id'];
				}

				$author->name 				= $data['name'];
				$author->google_plus_id 	= $data['google_plus_id'];
				$author->initials 			= $data['initials'];
				$author->email 				= $data['email'];

				$this->_helper->authorMapper->save($author);

				$this->_helper->json((object) array(
					'success' => true
				));
			} else {
				$this->_helper->json((object) array(
					'success' 	=> false,
					'errors' 	=> $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();

		$id 			= $this->getRequest()->getParam('id');

		$author_mapper 	= new \Moraso\Model\Author\Mapper();

		$success 		= $author_mapper->delete($id);

		$this->_helper->json((object) array(
			'success' => $success
		));
	}

}