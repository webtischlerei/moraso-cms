<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Acl
 */
class Moraso_Plugin_Acl_Generic_Resources_Controller extends Moraso_Adm_Plugin_Controller
{
	public function storeAction()
	{
		$resourcesModels = \Moraso\Acl\Resource::get();

		$resources = array();
		foreach ($resourcesModels as $resource) {
			if ($resource->parent) {
				$resources[] = array(
					'id' 		=> $resource->id,
					'name' 		=> $resource->name,
					'parent' 	=> \Moraso\Acl\Resource::get($resource->parent)->name
				);
			} else {
				$resources[] = array(
					'id' 		=> $resource->id,
					'name' 		=> $resource->name
				);
			}
		}

		usort($resources, function($a, $b) {
			return strnatcmp($a['name'], $b['name']);
		});
		
		$this->_helper->json((object) array(
			'resources' => $resources
		));
	}

	public function editAction()
	{
		$this->_helper->layout->disableLayout();

		$classExplode 	= explode('_', __CLASS__);

		$form 			= Aitsu_Forms::factory(strtolower($classExplode[2]), APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/' . $classExplode[4] . '/forms/resource.ini');
		$form->title 	= 'Ressource';
		$form->url 		= $this->view->url(array('paction' => 'edit'), 'child_plugin');

		$id 			= $this->getRequest()->getParam('id');

		$this->view->id = $id;

		// Ressourcen
		$resourceSet = \Moraso\Acl\Resource::get();

		$resources = array();
		foreach ($resourceSet as $resource) {
			if ($resource->id !== $id) {
				$resources[] = (object) array(
					'value' => $resource->id,
					'name' 	=> $resource->name
				);
			}
		}

		usort($resources, function($a, $b) {
			return strnatcmp($a->name, $b->name);
		});

		$form->setOptions('parent', $resources);

		if (!empty($id)) {
			$resource = \Moraso\Acl\Resource::get($id);

			$form->setValues(array(
				'id' 		=> $resource->id,
				'name' 		=> $resource->name,
				'parent'	=> $resource->parent
			));
		}

		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {
				$data = $form->getValues();

				$set = array(
					'name' 		=> $data['name'],
					'parent' 	=> empty($data['parent']) ? NULL : $data['parent']
				);

				if (empty($data['id'])) {
					$resource = \Moraso\Acl\Resource::create($set);
				} else {
					$resource = \Moraso\Acl\Resource::update($id, $set);
				}
				
				$this->_helper->json((object) array(
					'success' 	=> true,
					'id' 		=> $resource->id,
					'name' 		=> $resource->name
				));
			} else {
				$this->_helper->json((object) array(
					'success' 	=> false,
					'errors' 	=> $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id');

		try {
			\Moraso\Acl\Resource::delete($id);

			$this->_helper->json((object) array(
				'success' => true
			));
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}
}