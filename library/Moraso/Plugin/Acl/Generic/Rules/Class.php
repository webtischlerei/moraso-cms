<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Acl
 */
class Moraso_Plugin_Acl_Generic_Rules_Controller extends Moraso_Adm_Plugin_Controller
{
	/**
	* TODO
	*/
	public function storeAction()
	{
		// alle Regeln auslesen
		$ruleSet = \Moraso\Acl\Rule::get();

		if (count($ruleSet) === 1) {
			$ruleSet = array($ruleSet);
		}

		// Regel für Regel durchgehen und für die spätere Ausgabe in ein gesondertes Array schreiben
		$rules = array();
		foreach ($ruleSet as $rule) {
			$rules[] = array(
				'id' 		=> $rule->id,
				'role' 		=> \Moraso\Acl\Role::get($rule->id_acl_role)->name,
				'resource' 	=> \Moraso\Acl\Resource::get($rule->id_acl_resource)->name,
				'privilege' => \Moraso\Acl\Privilege::get($rule->id_acl_privilege)->name,
				'access' 	=> $rule->access
			);
		}
		
		// Rollen an die View übergeben
		$this->_helper->json((object) array(
			'rules' => $rules
		));
	}

	/**
	* TODO
	*/
	public function editAction()
	{
		$this->_helper->layout->disableLayout();

		$classExplode 	= explode('_', __CLASS__);

		$form 			= Aitsu_Forms::factory(strtolower($classExplode[2]), APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/' . $classExplode[4] . '/forms/rule.ini');
		$form->title 	= 'Regel';
		$form->url 		= $this->view->url(array('paction' => 'edit'), 'child_plugin');

		$id 			= $this->getRequest()->getParam('id');

		$this->view->id = $id;

		// Rollen
		$roleSet = \Moraso\Acl\Role::get();

		$roles = array();
		foreach ($roleSet as $role) {
			$roles[] = (object) array(
				'value' => $role->id,
				'name' 	=> $role->name
			);
		}

		usort($roles, function($a, $b) {
			return strnatcmp($a->name, $b->name);
		});

		$form->setOptions('role', $roles);

		// Ressourcen
		$resourceSet = \Moraso\Acl\Resource::get();

		$resources = array();
		foreach ($resourceSet as $resource) {
			$resources[] = (object) array(
				'value' => $resource->id,
				'name' 	=> $resource->name
			);
		}

		usort($resources, function($a, $b) {
			return strnatcmp($a->name, $b->name);
		});

		$form->setOptions('resource', $resources);

		// Privilegien
		$privilegeSet = \Moraso\Acl\Privilege::get();

		if (count($privilegeSet) === 1) {
			$privilegeSet = array($privilegeSet);
		}

		$privileges = array();
		foreach ($privilegeSet as $privilege) {
			$privileges[] = (object) array(
				'value' => $privilege->id,
				'name' 	=> $privilege->name
			);
		}

		usort($privileges, function($a, $b) {
			return strnatcmp($a->name, $b->name);
		});
		
		$form->setOptions('privilege', $privileges);

		if (!empty($id)) {
			$rule = \Moraso\Acl\Rule::get($id);

			$form->setValues(array(
				'id' 		=> $rule->id,
				'role' 		=> $rule->id_acl_role,
				'resource' 	=> $rule->id_acl_resource,
				'privilege' => $rule->id_acl_privilege,
				'access' 	=> $rule->access
			));
		}

		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {
				$data = $form->getValues();

				$set = array(
					'id_acl_role' 		=> $data['role'],
					'id_acl_resource' 	=> $data['resource'],
					'id_acl_privilege' 	=> $data['privilege'],
					'access' 			=> $data['access']
				);

				if (empty($data['id'])) {
					$rule = \Moraso\Acl\Rule::create($set);
				} else {
					$rule = \Moraso\Acl\Rule::update($id, $set);
				}
				
				$this->_helper->json((object) array(
					'success' 	=> true,
					'id' 		=> $rule->id
				));
			} else {
				$this->_helper->json((object) array(
					'success' 	=> false,
					'errors' 	=> $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	/**
	* TODO
	*/
	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id');

		try {
			\Moraso\Acl\Rule::delete($id);

			$this->_helper->json((object) array(
				'success' => true
			));
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}
}