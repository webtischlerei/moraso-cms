<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Acl
 */
class Moraso_Plugin_Acl_Generic_Roles_Controller extends Moraso_Adm_Plugin_Controller
{
	public function storeAction()
	{
		$rolesModels = \Moraso\Acl\Role::get();

		$roles = array();
		foreach ($rolesModels as $role) {
			if ($role->parent) {
				$roles[] = array(
					'id' 		=> $role->id,
					'name' 		=> $role->name,
					'parent' 	=> \Moraso\Acl\Role::get($role->parent)->name
				);
			} else {
				$roles[] = array(
					'id' 		=> $role->id,
					'name' 		=> $role->name
				);
			}
		}
		
		$this->_helper->json((object) array(
			'roles' => $roles
		));
	}

	public function editAction()
	{
		$this->_helper->layout->disableLayout();

		$classExplode 	= explode('_', __CLASS__);

		$form 			= Aitsu_Forms::factory(strtolower($classExplode[2]), APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/' . $classExplode[4] . '/forms/role.ini');
		$form->title 	= 'Rolle';
		$form->url 		= $this->view->url(array('paction' => 'edit'), 'child_plugin');

		$id 			= $this->getRequest()->getParam('id');

		$this->view->id = $id;

		// Rollen
		$roleSet = \Moraso\Acl\Role::get();

		$roles = array();
		foreach ($roleSet as $role) {
			if ($role->id !== $id) {
				$roles[] = (object) array(
					'value' => $role->id,
					'name' 	=> $role->name
				);
			}
		}

		usort($roles, function($a, $b) {
			return strnatcmp($a->name, $b->name);
		});

		$form->setOptions('parent', $roles);

		if (!empty($id)) {
			$role = \Moraso\Acl\Role::get($id);

			$form->setValues(array(
				'id' 		=> $role->id,
				'name' 		=> $role->name,
				'parent'	=> $role->parent
			));
		}

		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {
				$data = $form->getValues();

				$set = array(
					'name' 		=> $data['name'],
					'parent' 	=> empty($data['parent']) ? NULL : $data['parent']
				);

				if (empty($data['id'])) {
					$role = \Moraso\Acl\Role::create($set);
				} else {
					$role = \Moraso\Acl\Role::update($id, $set);
				}
				
				$this->_helper->json((object) array(
					'success' 	=> true,
					'id' 		=> $role->id,
					'name' 		=> $role->name
				));
			} else {
				$this->_helper->json((object) array(
					'success' 	=> false,
					'errors' 	=> $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id');

		try {
			\Moraso\Acl\Role::delete($id);

			$this->_helper->json((object) array(
				'success' => true
			));
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}
}