<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Acl
 */
class Moraso_Plugin_Acl_Generic_Privileges_Controller extends Moraso_Adm_Plugin_Controller
{
	public function storeAction()
	{
		$privilegesModels = \Moraso\Acl\Privilege::get();

		if (count($privilegesModels) === 1) {
			$privilegesModels = array($privilegesModels);
		}

		$privileges = array();
		foreach ($privilegesModels as $privilege) {
			$privileges[] = array(
				'id' 	=> $privilege->id,
				'name' 	=> $privilege->name
			);
		}
		
		$this->_helper->json((object) array(
			'privileges' => $privileges
		));
	}

	public function editAction()
	{
		$this->_helper->layout->disableLayout();

		$classExplode 	= explode('_', __CLASS__);

		$form 			= Aitsu_Forms::factory(strtolower($classExplode[2]), APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/' . $classExplode[4] . '/forms/privilege.ini');
		$form->title 	= 'Privileg';
		$form->url 		= $this->view->url(array('paction' => 'edit'), 'child_plugin');

		$id 			= $this->getRequest()->getParam('id');

		$this->view->id = $id;

		if (!empty($id)) {
			$privilege = \Moraso\Acl\Privilege::get($id);

			$form->setValues(array(
				'id' 	=> $privilege->id,
				'name' 	=> $privilege->name
			));
		}

		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {
				$data = $form->getValues();

				$set = array(
					'name' 	=> $data['name']
				);

				if (empty($data['id'])) {
					$privilege = \Moraso\Acl\Privilege::create($set);
				} else {
					$privilege = \Moraso\Acl\Privilege::update($id, $set);
				}
				
				$this->_helper->json((object) array(
					'success' 	=> true,
					'id' 		=> $privilege->id,
					'name' 		=> $privilege->name
				));
			} else {
				$this->_helper->json((object) array(
					'success' 	=> false,
					'errors' 	=> $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id');

		try {
			\Moraso\Acl\Privilege::delete($id);

			$this->_helper->json((object) array(
				'success' => true
			));
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}
}