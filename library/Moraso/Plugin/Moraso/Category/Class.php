<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2012 - 2014, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Plugin_Moraso_Category_Controller extends Moraso_Adm_Plugin_Controller
{
	const ID = '50a213b9-6568-4869-9fdd-56007f000001';

	public function init()
	{
		$this->_helper->layout->disableLayout();
		header("Content-type: text/javascript");
	}

	public static function register($idcat)
	{
		$enabled = !empty($idcat) ? 1 : 0;

		return (object) array(
			'name' 		=> 'moraso',
			'tabname' 	=> 'Übersicht',
			'enabled' 	=> $enabled,
			'position' 	=> -2,
			'id' 		=> self::ID
		);
	}

	public function indexAction() 
	{
		$idcat 				= $this->getRequest()->getParam('idcat');
		$cat 				= Aitsu_Persistence_Category::factory($idcat)->load();
		$idlang 			= \Moraso\Session::get('currentLanguage');
		$clipboardArticles 	= \Moraso\Session::get('clipboardArticles');

		$this->view->usePublishing 			= Moraso_Config::get('sys.usePublishing');
		$this->view->allowArticleDuplicate 	= Moraso_Config::get('sys.article.allow.duplicate');
		$this->view->idcat 					= $idcat;
		$this->view->categoryname 			= $cat->name;
		$this->view->isInFavories 			= Aitsu_Persistence_CatFavorite::factory($idcat)->load()->isInFavorites();
		$this->view->isClipboardEmpty 		= empty($clipboardArticles) || count($clipboardArticles) == 0;
		$this->view->allowEdit 				= (\Moraso\Acl::isAllowed('language.' . $idlang) && \Moraso\Acl::isAllowed('article', 'update') && \Moraso\Acl::isAllowed('cat.' . $idcat));
		$this->view->allowNew 				= (\Moraso\Acl::isAllowed('language.' . $idlang) && \Moraso\Acl::isAllowed('article', 'insert') && \Moraso\Acl::isAllowed('cat.' . $idcat));
		$this->view->allowPublishing 		= (\Moraso\Acl::isAllowed('language.' . $idlang) && \Moraso\Acl::isAllowed('article', 'publish') && \Moraso\Acl::isAllowed('cat.' . $idcat));
	}

	public function articlesAction()
	{
		$idcat 	= $this->getRequest()->getParam('idcat');
		$idlang = \Moraso\Session::get('currentLanguage');

		if (isset($_POST['xaction']) && $_POST['xaction'] == 'update') {
			$data 	= json_decode($_POST['data']);

			$idcat 	= Aitsu_Db::fetchOne('' .
							'select ' .
							'   idcat ' .
							'from ' .
							'   _cat_art ' .
							'where ' .
							'   idart = :idart', array(
						':idart' => $data->id
			));

			$arts 	= Aitsu_Db::fetchAll('' .
							'select ' .
							'	artlang.idart, ' .
							'	artlang.idartlang ' .
							'from ' .
							'   _art_lang as artlang ' .
							'left join ' .
							'   _cat_art as catart on artlang.idart = catart.idart ' .
							'where ' .
							'	catart.idcat = :idcat ' .
							'and ' .
							'   artlang.idlang = :idlang ' .
							'order by ' .
							'	artlang.artsort asc', array(
						':idcat' => $idcat,
						':idlang' => $idlang
			));

			$pos = 0;
			for ($i = 0; $i < count($arts); $i++) {
				$idart 		= $arts[$i]['idart'];
				$idartlang 	= $arts[$i]['idartlang'];

				if ($pos == $data->artsort) {
					$pos++;
				}
				$artsort = $idart == $data->id ? $data->artsort : $pos++;

				Aitsu_Db::put('_art_lang', 'idartlang', array(
					'idartlang' => $idartlang,
					'artsort' 	=> $artsort
				));

				Aitsu_Db::query('' .
						'update ' .
						'   _pub_art_lang ' .
						'set ' .
						'   artsort =:artsort ' .
						'where ' .
						'   idartlang =:idartlang ' .
						'and ' .
						'   status =:status', array(
					':artsort' 		=> $artsort,
					':idartlang' 	=> $idartlang,
					':status' 		=> 1
				));
			}
		}

		$data = array();

		$arts = Aitsu_Persistence_View_Articles::full($idcat, null);

		if ($arts) {
			foreach ($arts as $art) {
				$data[] = (object) array(
					'id' 		=> $art['idart'],
					'title' 	=> $art['title'],
					'pagetitle' => $art['pagetitle'],
					'urlname' 	=> $art['urlname'],
					'online' 	=> $art['online'],
					'published' => $art['published'],
					'isstart' 	=> $art['isstart'],
					'artsort' 	=> $art['artsort']
				);
			}
		}

		$this->_helper->json((object) array(
			'data' => $data
		));
	}
}