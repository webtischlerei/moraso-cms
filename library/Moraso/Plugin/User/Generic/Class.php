<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/User
 */
class Moraso_Plugin_User_Generic_Controller extends Moraso_Adm_Plugin_Controller
{
	protected function _preInit()
	{
		$this->_helper->userMapper 					= new \Moraso\Model\User\Mapper();
		$this->_helper->userAuthMapper 				= new \Moraso\Model\User\Auth\Mapper();
		
		$this->_helper->aclRoleMapper 				= new \Moraso\Model\Acl\Role\Mapper();
		$this->_helper->aclResourceMapper 			= new \Moraso\Model\Acl\Resource\Mapper();

		$this->_helper->userHasAclRoleMapper 		= new \Moraso\Model\User\Has\Acl\Role\Mapper();
	}
	
	public function storeAction()
	{
		$userSet = $this->_helper->userMapper->fetchAll();

		$users = array();
		foreach ($userSet as $user) {
			$addUser = array(
				'id' 			=> $user->id,
				'salutation' 	=> $user->salutation,
				'firstname' 	=> $user->firstname,
				'lastname' 		=> $user->lastname,
				'email' 		=> $user->email
			);

			$userAuth = $this->_helper->userAuthMapper->find($user->id);

			if (!is_null($userAuth)) {
				$addUser['username'] = $userAuth->username;
			}

			$userHasAclRoleObject = $this->_helper->userHasAclRoleMapper->find($user->id, 'id_user');

			if (!empty($userHasAclRoleObject->id_acl_role)) {
				$aclRoleObject = $this->_helper->aclRoleMapper->find($userHasAclRoleObject->id_acl_role);
				$addUser['role'] = $aclRoleObject->name;
			} else {
				$addUser['role'] = 'Keiner Rolle zugewiesen';
			}

			$users[] = $addUser;
		}

		$this->_helper->json((object) array(
			'message' 	=> count($users) . ' Einträge wurden geladen',
			'users' 	=> $users,
			'success' 	=> true,
			'total' 	=> count($users)
		));
	}

	public function editAction()
	{
		$this->_helper->layout->disableLayout();

		$userAuthMapper = $this->_helper->userAuthMapper;

		$id 			= $this->getRequest()->getParam('id');

		$classExplode 	= explode('_', __CLASS__);
		$formPath 		= APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/forms/';
		$form 			= Aitsu_Forms::factory(strtolower($classExplode[2]), $formPath . 'user.ini');
		$form->title 	= 'Benutzer bearbeiten';
		$form->url 		= $this->view->url(array('paction' => 'edit'));

		$formConfig 	= $form->getConfig();
		
		$plugins 		= Aitsu_Util_Dir::scan($formPath . 'Plugins', '*.ini');
		foreach ($plugins as $plugin) {
			$formConfig->merge(new Zend_Config_Ini($plugin));
		}
		
		/* Anrede */
		$salutations = array();
		foreach (array('Herr', 'Frau') as $salutation) {
			$salutations[] = (object) array(
				'value' => $salutation,
				'name' 	=> $salutation
			);
		}
		$form->setOptions('salutation', $salutations);

		/* Status */
		$status = array('AKTIV' => 1, 'INAKTIV' => 2, 'GESPERRT' => 3);
		$options = array();
		foreach ($status as $key => $value) {
			$options[] = (object) array(
				'name' => $key,
				'value' => $value
			);
		}
		$form->setOptions('status', $options);

		/* Sicherheitsstufe */
		$securityLevels = array('normale Authentifizierung' => 1, 'zusätzliche Authentifizierung' => 2);
		$options = array();
		foreach ($securityLevels as $key => $value) {
			$options[] = (object) array(
				'name' => $key,
				'value' => $value
			);
		}
		$form->setOptions('security_level', $options);

		/* ACL - Role */
		$aclRoleSet = $this->_helper->aclRoleMapper->fetchAll();

		$aclRoles = array();
		foreach ($aclRoleSet as $aclRoleObject) {
			$aclRoles[] = (object) array(
				'value' => $aclRoleObject->id,
				'name' 	=> $aclRoleObject->name
			);
		}
		$form->setOptions('role', $aclRoles);

		if ($id) {
			$user = $this->_helper->userMapper->find($id);

			if ($user) {
				$form->setValues(array(
					'id' 			=> $user->id,
					'salutation' 	=> $user->salutation,
					'firstname' 	=> $user->firstname,
					'lastname' 		=> $user->lastname,
					'email' 		=> $user->email
				));
			}

			// Plugin Data
			$pluginData = \Moraso\Eav::get('usermanagement_plugin_data', $user->id_eav_entity);

			if (is_array($pluginData) && !empty($pluginData)) {
				foreach ($pluginData as $key => $value) {
					if (!empty($value)) {
						$form->setValue('pluginData_' . $key, $value);
					}
				}
			}

			// AUTH
			$userAuth = $userAuthMapper->find($user->id);

			if ($userAuth) {
				$form->setValues(array(
					'status' 		=> $userAuth->status,
					'username' 		=> $userAuth->username,
					'access_from' 	=> $userAuth->access_from 	=== '0000-00-00 00:00:00' ? null : $userAuth->access_from,
					'access_until' 	=> $userAuth->access_until 	=== '0000-00-00 00:00:00' ? null : $userAuth->access_until
				));
			}

			// ACL (Role)
			$userHasAclRole = $this->_helper->userHasAclRoleMapper->find($id, 'id_user');

			if (isset($userHasAclRole->id_acl_role) && !empty($userHasAclRole->id_acl_role)) {
				$form->setValues(array(
					'role' => $userHasAclRole->id_acl_role
				));
			}
		}
		
		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {
				$data = $form->getValues();

				// USER
				$updateUser = false;

				if (!empty($data['id'])) {
					$user 		= $this->_helper->userMapper->find($data['id']);
				} else {
					$updateUser = true;
					$user 		= $this->_helper->userMapper->create();
				}

				if ($user->salutation !== $data['salutation']) {
					$updateUser 		= true;
					$user->salutation 	= $data['salutation'];
				}

				if ($user->firstname !== $data['firstname']) {
					$updateUser 		= true;
					$user->firstname 	= $data['firstname'];
				}

				if ($user->lastname !== $data['lastname']) {
					$updateUser 	= true;
					$user->lastname = $data['lastname'];
				}

				if ($user->email !== $data['email']) {
					$updateUser 	= true;
					$user->email 	= $data['email'];
				}

				// Daten durch Plugins
				$pluginData = array();
				foreach ($data as $key => $value) {
					$expKey = explode('_', $key);

					if ($expKey[0] === 'pluginData'){
						 $pluginData[$expKey[1]] = $value;
					}
				}

				$user->id_eav_entity = NULL;

				if (!empty($pluginData)) {
					$updateUser 	= true;
					if (!is_null($user->id_eav_entity)) {
						$pluginData['id'] = $user->id_eav_entity;
					}

					$user->id_eav_entity = \Moraso\Eav::set('usermanagement_plugin_data', $pluginData);
				}

				if ($updateUser) {
					$this->_helper->userMapper->save($user);
				}

				// AUTH
				$updateUserAuth 	= false;
				$updateUserAuthPass = false;

				if (empty($data['username'])) {
					$userAuthMapper->delete($user->id);
				} else {
					$salt = false;

					$userAuth = $userAuthMapper->find($user->id);

					if (!$userAuth->id) {
						$userAuth 			= $userAuthMapper->create();
						$userAuth->user_id 	= $user->id;
					}

					if (!empty($data['status'])) {
						if ($userAuth->status !== $data['status']) {
							$updateUserAuth 	= true;
							$userAuth->status 	= (int) $data['status'];
						}
					}

					if (!empty($data['username'])) {
						if ($userAuth->username !== $data['username']) {
							$updateUserAuth 	= true;
							$userAuth->username = $data['username'];
						}
					}

					if (!empty($data['password'])) {
						$updateUserAuth 	= true;
						$updateUserAuthPass = true;
						$userAuth->password = $data['password'];
						$salt 				= $userAuthMapper->getSalt($data['security_level']);
					}

					if (!empty($data['access_from'])) {
						if ($userAuth->access_from !== $data['access_from']) {
							$updateUserAuth 		= true;
							$userAuth->access_from 	= $data['access_from'];
						}
					}

					if (!empty($data['access_until'])) {
						if ($userAuth->access_until !== $data['access_until']) {
							$updateUserAuth 		= true;
							$userAuth->access_until = $data['access_until'];
						}
					}

					if ($updateUserAuth) {
						$userAuthMapper->save($userAuth, $salt, $updateUserAuthPass);
					}
				}

				// ACL (Role)
				if (!empty($data['role'])) {
					$updateUserHasAclRole 	= false;
					$userHasAclRole 		= $this->_helper->userHasAclRoleMapper->find($user->id, 'id_user');

					if (!$userHasAclRole) {
						$updateUserHasAclRole 		= true;
						$userHasAclRole 			= $this->_helper->userHasAclRoleMapper->create();
						$userHasAclRole->id_user 	= $user->id;
					}

					if ($userHasAclRole->id_acl_role !== (int) $data['role']) {
						$updateUserHasAclRole 			= true;
						$userHasAclRole->id_acl_role 	= $data['role'];
					}

					if ($updateUserHasAclRole) {
						$this->_helper->userHasAclRoleMapper->save($userHasAclRole);
					}
				}

				if ($user->id) {
					$this->_helper->json((object) array(
						'success' 	=> true,
						'user' 		=> (object) array(
							'id' 			=> $user->id,
							'salutation' 	=> $user->salutation,
							'firstname' 	=> $user->firstname,
							'lastname' 		=> $user->lastname,
							'auth' 			=> (object) array(
								'updatePass' 	=> $updateUserAuthPass,
								'generated' 	=> isset($salt) ? ($salt == Moraso_Config::get('sys.salt') || empty($salt) ? false : true) : false,
								'salt' 			=> isset($salt) ? $salt : null
							)
						)
					));
				} else {
					$this->_helper->json((object) array(
						'success' => false
					));
				}
			} else {
				$this->_helper->json((object) array(
					'success' => false,
					'errors' => $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' => false,
				'exception' => true,
				'message' => $e->getMessage()
			));
		}
	}

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();

		$this->_helper->json((object) array(
			'success' => $this->_helper->userMapper->delete($this->getRequest()->getParam('id'))
		));
	}
}