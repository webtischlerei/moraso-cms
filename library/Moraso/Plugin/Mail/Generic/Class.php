<?php

/**
 * Plugin zur Verwaltung verschiedener E-Mail-Konfigurationen
 *
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Plugin\Mail\Generic
 */
class Moraso_Plugin_Mail_Generic_Controller extends Moraso_Adm_Plugin_Controller
{
	public function init()
	{
		$this->_helper->layout->disableLayout();
	}

	public function indexAction()
	{
		header("Content-type: text/javascript");
	}

	public function storeAction()
	{
		$data = Moraso_Db::fetchAll('SELECT identifier, value FROM _moraso_config WHERE identifier LIKE :like', array(
			':like' => 'mail.%'
		));

		$configs_raw = array();
		foreach ($data as $row) {
			$explode = explode('.', $row['identifier']);

			$alias = $explode[1];

			unset($explode[0]);
			unset($explode[1]);

			$configs_raw[$alias][implode('_', $explode)] = $row['value'];
			$configs_raw[$alias]['alias'] = $alias;
		}

		$configs = array();
		foreach ($configs_raw as $alias => $config) {
			$configs[] = $config;
		}

		$this->_helper->json((object) array(
			'configs' => $configs,
			'success' => true
		));
	}

	public function editAction()
	{
		$alias = $this->getRequest()->getParam('alias');

		$this->_helper->layout->disableLayout();

		$classExplode = explode('_', __CLASS__);

		$form = Aitsu_Forms::factory(strtolower($classExplode[2]), APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/forms/set.ini');
		$form->title = 'E-Mail-Konfiguration';
		$form->url = $this->view->url(array('paction' => 'edit'));

		$data = Moraso_Db::fetchAll('SELECT identifier, value FROM _moraso_config WHERE identifier LIKE :like', array(
			':like' => 'mail.' .$alias . '.%'
		));

		$configs = array();
		foreach ($data as $row) {
			$explode = explode('.', $row['identifier']);

			$alias = $explode[1];

			unset($explode[0]);
			unset($explode[1]);

			$configs[implode('_', $explode)] = $row['value'];
			$configs['alias'] = $alias;
		}

		if (empty($configs['transport_smtp_auth'])) {
			$configs['transport_smtp_auth'] = 'none';
		}

		if (empty($configs['transport_smtp_ssl'])) {
			$configs['transport_smtp_ssl'] = 'none';
		}

		if ($configs) {
			$form->setValues($configs);
		}
		
		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {
				$data = $form->getValues();

				foreach ($data as $key => $value) {
					Moraso_Db::query('DELETE FROM _moraso_config WHERE identifier =:identifier', array(
						':identifier' => 'mail.' . $alias . '.' . str_replace('_', '.', $key)
					));

					Moraso_Db::put('_moraso_config', 'id', array(
						'config' => 'default',
						'env' => 'default',
						'identifier' => 'mail.' . $alias . '.' . str_replace('_', '.', $key),
						'value' => $value
					));
				}

				$this->_helper->json((object) array(
					'success' => true,
					'data' => (object) $data
				));
			} else {
				$this->_helper->json((object) array(
					'success' => false,
					'errors' => $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' => false,
				'exception' => true,
				'message' => $e->getMessage()
			));
		}
	}

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();

		Moraso_Db::query('DELETE FROM _moraso_config WHERE identifier LIKE :like', array(
			':like' => 'mail.' . $this->getRequest()->getParam('alias') . '.%'
		));

		$this->_helper->json((object) array(
			'success' => true,
		));
	}
}