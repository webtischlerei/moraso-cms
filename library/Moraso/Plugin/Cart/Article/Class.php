<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Plugin_Cart_Article_Controller extends Moraso_Adm_Plugin_Controller
{
	const ID = '51d413d2-d9b0-4818-851e-065cc0a8b230';

	public function init()
	{
		header("Content-type: text/javascript");
		$this->_helper->layout->disableLayout();
	}

	public static function register($idart)
	{
		return (object) array(
			'name' 		=> 'cart',
			'tabname' 	=> 'Warenkorb',
			'enabled' 	=> self::getPosition($idart, 'cart'),
			'position' 	=> self::getPosition($idart, 'cart'),
			'id' 		=> self::ID
		);
	}

	public function indexAction()
	{
		$idart      = $this->getRequest()->getParam('idart');
		$idlang     = \Moraso\Session::get('currentLanguage');
		$idartlang  = Moraso_Util::getIdArtLang($idart, $idlang);

		$classExplode = explode('_', __CLASS__);

		$form 			= Aitsu_Forms::factory(strtolower($classExplode[2]), APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/forms/cart.ini');
		$form->title 	= 'Shopsystem';
		$form->url 		= $this->view->url(array('namespace' => 'moraso', 'plugin' => 'cart', 'area' => 'article', 'paction' => 'index'), 'plugin');

		/* set Options */
		$tax_classes = array(
			'Regelsatz - DE - 19 %' 	=> 19,
			'Ermäßigter Steuersatz - DE - 7 %' 	=> 7,
			'Normalsatz - CH - 8 %' 	=> 8,
			'Sondersatz für Beherbergungsleistungen - CH - 3,8 %' => 3.8,
			'Reduzierte Satz - CH - 2,5 %' => 2.5
		);

		$options = array();
		foreach ($tax_classes as $key => $value) {
			$options[] = (object) array(
				'name' => $key,
				'value' => $value
			);
		}
		$form->setOptions('tax_class', $options);

		/* set Values */
		$article = Aitsu_Persistence_ArticleProperty::factory($idartlang);  
		$article->load();

		if ($this->getRequest()->getParam('loader')) {
			$data = array(
				'idart' => $idart
			);

			if (isset($article->cart)) {
				$cart = (object) $article->cart;

				$data['sku'] 		= $cart->sku->value;
				$data['price'] 		= $cart->price->value;
				$data['tax_class'] 	= $cart->tax_class->value;
			}
			
			if (!isset($data['sku']) || empty($data['sku'])) {
				$data['sku'] = 'SKU' . (10000 + $idart);
			} 

			$form->setValues($data);

			$this->view->form = $form;

			$this->view->api = new stdClass();
			$this->view->api->staffelpreise = new stdClass();
			$this->view->api->staffelpreise->read 		= $this->view->url(array('namespace' => 'moraso', 'plugin' => 'cart', 'area' => 'article', 'paction' => 'getStaffelpreise', 'idart' => $idart), 'plugin');
			$this->view->api->staffelpreise->create 	= $this->view->url(array('namespace' => 'moraso', 'plugin' => 'cart', 'area' => 'article', 'paction' => 'setStaffelpreise', 'idart' => $idart), 'plugin');
			$this->view->api->staffelpreise->update 	= $this->view->url(array('namespace' => 'moraso', 'plugin' => 'cart', 'area' => 'article', 'paction' => 'updateStaffelpreise', 'idart' => $idart), 'plugin');
			$this->view->api->staffelpreise->destroy 	= $this->view->url(array('namespace' => 'moraso', 'plugin' => 'cart', 'area' => 'article', 'paction' => 'deleteStaffelpreise', 'idart' => $idart), 'plugin');
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {
				$data = $form->getValues();

				$article->setValue('cart', 'sku', $data['sku']);
				$article->setValue('cart', 'price', $data['price'], 'float');
				$article->setValue('cart', 'tax_class', $data['tax_class']);

				$article->save();

				$this->_helper->json((object) array(
					'success' 	=> true,
					'data' 		=> (object) $data
				));
			} else {
				$this->_helper->json((object) array(
					'success' 	=> false,
					'errors' 	=> $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function getstaffelpreiseAction()
	{
		$idart 		= $this->getRequest()->getParam('idart');
		$idlang 	= \Moraso\Session::get('currentLanguage');
		$idartlang 	= Moraso_Util::getIdArtLang($idart, $idlang);

		$staffelpreise = Moraso_Db::fetchAll("SELECT * FROM `_aitsu_property` WHERE `identifier` LIKE 'cart:price_staffel_%';");

		$returnStaffelpreise = array(
			'data' => array()
		);

		foreach ($staffelpreise as $staffelpreis) {
			$price = Moraso_Db_Simple::fetch('floatvalue', '_aitsu_article_property', array(
					'propertyid' 	=> $staffelpreis['propertyid'],
					'idartlang' 	=> $idartlang
				));

			if ($price) {
				$returnStaffelpreise['data'][] = array(
					'price' 	=> $price,
					'quantity' 	=> str_replace('cart:price_staffel_', '', $staffelpreis['identifier'])
				);
			}
		}

		usort($returnStaffelpreise['data'], function($a, $b) {
    		return $a['quantity'] - $b['quantity'];
		});

		$returnStaffelpreise['total'] 	= count($returnStaffelpreise['data']);
		$returnStaffelpreise['success'] = true;

		$this->_helper->json((object) $returnStaffelpreise);
	}

	public function setstaffelpreiseAction()
	{
		$idart 		= $this->getRequest()->getParam('idart');
		$idlang 	= \Moraso\Session::get('currentLanguage');
		$idartlang 	= Moraso_Util::getIdArtLang($idart, $idlang);

		$article = Aitsu_Persistence_ArticleProperty::factory($idartlang);  
		$article->load();

		$body 			= $this->getRequest()->getRawBody();
		$body_decode 	= json_decode($body);
		$staffelpreise 	= $body_decode->data;

		foreach ($staffelpreise as $staffelpreis) {
			$article->setValue('cart', 'price_staffel_' . $staffelpreis->quantity, $staffelpreis->price, 'float');
		}

		$article->save();

		$this->_helper->json((object) array('success' => true));
	}

	public function updatestaffelpreiseAction()
	{
		$idart 		= $this->getRequest()->getParam('idart');
		$idlang 	= \Moraso\Session::get('currentLanguage');
		$idartlang 	= Moraso_Util::getIdArtLang($idart, $idlang);

		$article = Aitsu_Persistence_ArticleProperty::factory($idartlang);  
		$article->load();

		$body 			= $this->getRequest()->getRawBody();
		$body_decode 	= json_decode($body);
		$staffelpreise 	= $body_decode->data;

		foreach ($staffelpreise as $staffelpreis) {
			$article->setValue('cart', 'price_staffel_' . $staffelpreis->quantity, $staffelpreis->price, 'float');
		}

		$article->save();

		$this->_helper->json((object) array('success' => true));
	}

	public function deletestaffelpreiseAction()
	{
		$idart 		= $this->getRequest()->getParam('idart');
		$idlang 	= \Moraso\Session::get('currentLanguage');
		$idartlang 	= Moraso_Util::getIdArtLang($idart, $idlang);

		$article = Aitsu_Persistence_ArticleProperty::factory($idartlang);  
		$article->load();

		$body 			= $this->getRequest()->getRawBody();
		$body_decode 	= json_decode($body);
		$staffelpreise 	= $body_decode->data;

		foreach ($staffelpreise as $staffelpreis) {
			$article->setValue('cart', 'price_staffel_' . $staffelpreis, 0, 'float');
			$article->unsetValue('cart', 'price_staffel_' . $staffelpreis);
		}

		$article->save();

		$this->_helper->json((object) array('success' => true));
	}
}