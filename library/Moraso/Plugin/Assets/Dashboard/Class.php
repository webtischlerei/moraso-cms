<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Asset
 */
class Moraso_Plugin_Assets_Dashboard_Controller extends Moraso_Adm_Plugin_Controller
{
	const ID = '4fcf0b8c-b71c-465b-aa70-1a977f000001';

	public static function register()
	{
		return (object) array(
			'name' 		=> 'assets',
			'tabname' 	=> 'Assets',
			'enabled' 	=> true,
			'id' 		=> self :: ID
		);
	}

	public function storeAction()
	{
		$assetSet 	= \Moraso\Asset::get();

		if (count($assetSet) === 1) {
			$assetSet = array($assetSet);
		}

		$assets 	= array();
		foreach ($assetSet as $asset) {
			$assets[] = array(
				'id' 			=> $asset->id,
				'headline' 		=> $asset->headline,
				'subheadline' 	=> $asset->subheadline,
				'text_1' 		=> $asset->text_1
			);
		}

		$this->_helper->json((object) array(
			'assets' => $assets
		));
	}
}