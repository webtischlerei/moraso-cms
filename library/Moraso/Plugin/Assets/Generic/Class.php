<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Asset
 */

class Moraso_Plugin_Assets_Generic_Controller extends Moraso_Adm_Plugin_Controller
{
	protected function _preInit()
	{
		$this->_helper->mediaMapper 			= new \Moraso\Model\Media\Mapper();
		$this->_helper->mediaDescriptionMapper 	= new \Moraso\Model\Media\Description\Mapper();
	}
	
	public function storeAction()
	{
		$assetSet = \Moraso\Asset::get();

		if (count($assetSet) === 1) {
			$assetSet = array($assetSet);
		}

		$assets = array();
		foreach ($assetSet as $asset) {
			$assets[] = array(
				'id' 			=> $asset->id,
				'headline' 		=> $asset->headline,
				'active' 		=> $asset->active,
				'created' 		=> $asset->created,
				'lastmodified' 	=> $asset->lastmodified
			);
		}	

		$this->_helper->json((object) array(
			'message' 	=> count($assets) . ' Einträge wurden geladen',
			'assets' 	=> $assets,
			'success' 	=> true,
			'total' 	=> count($assets)
		));
	}

	public function configAction()
	{
		$this->_helper->layout->disableLayout();

		$classExplode 	= explode('_', __CLASS__);

		$form 			= Aitsu_Forms::factory(strtolower($classExplode[2]), APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/forms/config.ini');
		$form->title 	= 'Asset-Plugin konfigurieren';
		$form->url 		= $this->view->url(array('paction' => 'config'), 'plugin');

		$form->setValue('assets_media_source', Moraso_Config::get('assets.media.source'));

		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {
				$data 			= $form->getValues();
				$currentClient 	= \Moraso\Session::get('currentClient');

				$dataset = array(
					'id' 			=> Moraso_Db::fetchOne('select id from _moraso_config where identifier =:identifier', array(':identifier' => 'assets.media.source')),
					'config' 		=> Aitsu_Persistence_Clients::factory($currentClient)->load()->config,
					'env' 			=> 'default',
					'identifier' 	=> 'assets.media.source',
					'value' 		=> str_replace('idart ', '', $data['assets_media_source'])
				);

				if (empty($dataset['id'])) {
					unset($dataset['id']);
				}

				Moraso_Db::put('_moraso_config', 'id', $dataset);

				$this->_helper->json((object) array(
					'success' 	=> true,
					'data' 		=> (object) $dataset
				));
			} else {
				$this->_helper->json((object) array(
					'success' 	=> false,
					'errors' 	=> $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function editAction()
	{
		$id 				= $this->getRequest()->getParam('id');
		$currentLanguage 	= \Moraso\Registry::get('currentLanguage');

		$this->_helper->layout->disableLayout();

		$classExplode = explode('_', __CLASS__);

		$form 			= Aitsu_Forms::factory(strtolower($classExplode[2]), APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/forms/edit.ini');
		$form->title 	= 'Asset bearbeiten';
		$form->url 		= $this->view->url(array('paction' => 'edit'), 'plugin');

		/* media */
		$medias = $this->_helper->mediaMapper->find(Moraso_Config::get('assets.media.source'), 'idart', true);

		$mediaCollection = array();
		$mediaCollection[] = (object) array(
			'name' 	=> '--- no media selected ---',
			'value' => 0
		);

		if (!empty($medias)) {
			foreach ($medias as $media) {
				$mediaDescription = $this->_helper->mediaDescriptionMapper->find(array(
					$media->mediaid,
					$currentLanguage
				), array(
					'mediaid',
					'idlang'
				));

				$mediaCollection[] = (object) array(
					'name' 	=> isset($mediaDescription->name) ? $mediaDescription->name . ' &mdash; ' . $media->filename : $media->filename,
					'value' => $media->mediaid
				);
			}
		}

		$form->setOptions('media_1', $mediaCollection);
		$form->setOptions('media_2', $mediaCollection);
		$form->setOptions('media_3', $mediaCollection);

		/* active / inactive */
		$activeCollection = array();
		$activeCollection[] = (object) array(
			'name' 	=> 'inactive',
			'value' => 0
		);
		$activeCollection[] = (object) array(
			'name' 	=> 'active',
			'value' => 1
		);
		$form->setOptions('active', $activeCollection);

		/* get Values */
		if (!empty($id)) {
			$asset = \Moraso\Asset::get($id);

			$data = array(
				'id' 			=> $asset->id,
				'headline' 		=> $asset->headline,
				'subheadline' 	=> $asset->subheadline,
				'text_1' 		=> $asset->text_1,
				'text_2' 		=> $asset->text_2,
				'active' 		=> $asset->active
			);

			$data['media_1'] = Aitsu_Db::fetchOne('' .
							'select ' .
							'   mediaid ' .
							'from ' .
							'   _assets_have_media ' .
							'where ' .
							'   idasset =:id ' .
							'order by ' .
							'   mediaid asc ' .
							'limit ' .
							'   0, 1', array(
						':id' => $id
			));

			$data['media_2'] = Aitsu_Db::fetchOne('' .
							'select ' .
							'   mediaid ' .
							'from ' .
							'   _assets_have_media ' .
							'where ' .
							'   idasset =:id ' .
							'order by ' .
							'   mediaid asc ' .
							'limit ' .
							'   1, 1', array(
						':id' => $id
			));

			$data['media_3'] = Aitsu_Db::fetchOne('' .
							'select ' .
							'   mediaid ' .
							'from ' .
							'   _assets_have_media ' .
							'where ' .
							'   idasset =:id ' .
							'order by ' .
							'   mediaid asc ' .
							'limit ' .
							'   2, 1', array(
						':id' => $id
			));

			$form->setValues($data);
		}

		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {

				$data = $form->getValues();

				$now = date('Y-m-d H:i:s');

				if (empty($data['id'])) {
					unset($data['id']);
				}

				$asset = \Moraso\Asset::create($data);

				/* set media */
				Aitsu_Db::query('delete from _assets_have_media where idasset =:idasset', array(
					':idasset' => $asset->id
				));

				$mediaData = array('idasset' => $asset->id);

				if (!empty($data['media_1'])) {
					$mediaData['mediaid'] = $data['media_1'];

					Aitsu_Db::put('_assets_have_media', 'id', $mediaData);
				}

				if (!empty($data['media_2'])) {
					$mediaData['mediaid'] = $data['media_2'];

					Aitsu_Db::put('_assets_have_media', 'id', $mediaData);
				}

				if (!empty($data['media_3'])) {
					$mediaData['mediaid'] = $data['media_3'];

					Aitsu_Db::put('_assets_have_media', 'id', $mediaData);
				}

				$this->_helper->json((object) array(
					'success' 	=> true,
					'data' 		=> (object) $data
				));
			} else {
				$this->_helper->json((object) array(
					'success' 	=> false,
					'errors' 	=> $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function deleteAction()
	{
		$id = $this->getRequest()->getParam('id');

		try {
			\Moraso\Asset::delete($id);

			$this->_helper->json((object) array(
				'success' => true
			));
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function activateAction()
	{
		$id = $this->getRequest()->getParam('id');

		try {
			\Moraso\Asset::setActive($id, 1);

			$this->_helper->json((object) array(
				'success' => true
			));
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function deactivateAction()
	{
		$id = $this->getRequest()->getParam('id');

		try {
			\Moraso\Asset::setActive($id, 0);

			$this->_helper->json((object) array(
			'success' => true
			));
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}
}