<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Plugin\Cache\Generic
 */
class Moraso_Plugin_Cache_Generic_Controller extends Moraso_Adm_Plugin_Controller
{
	public function storeAction()
	{
		$cache = Moraso_Cache::init();

		$cacheIds = $cache->getIds();

		$entries = array();
		foreach ($cacheIds as $id) {
			$metaData = $cache->getMetadatas($id);

			$entries[] = array(
				'id' 		=> $id,
				'expire' 	=> date('d.m.Y H:i:s', $metaData['expire']),
				'mtime' 	=> date('d.m.Y H:i:s', $metaData['mtime']),
				'tags' 		=> implode(', ', $metaData['tags'])
			);
		}

		$this->_helper->json((object) array(
			'entries' => $entries,
			'success' => true
		));
	}

	public function configAction()
	{
		$this->_helper->layout->disableLayout();

		$classExplode = explode('_', __CLASS__);

		$form = Aitsu_Forms::factory(strtolower($classExplode[2]), APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/forms/config.ini');
		$form->title = Aitsu_Translate::translate('Konfigurationen');
		$form->url = $this->view->url(array('paction' => 'config'), 'plugin');

		/* Verfahren */
		$types = array();
		$types['Dateisystem'] = 'file';

		if (extension_loaded('apc') && ini_get('apc.enabled')) {
			$types['APC'] = 'apc';
		}

		$typeOptions = array();
		foreach ($types as $key => $value) {
			$typeOptions[] = (object) array(
				'name' => $key,
				'value' => $value
			);
		}

		/* Status */
		$status = array('AKTIV' => 'aktiv', 'INAKTIV' => 'inaktiv');
		$statusOptions = array();
		foreach ($status as $key => $value) {
			$statusOptions[] = (object) array(
				'name' => $key,
				'value' => $value
			);
		}

		/* Internal */
		$form->setOptions('cache_internal_enable', $statusOptions);
		if (!is_bool(Moraso_Config::get('cache.internal.enable'))) {
			$form->setValue('cache_internal_enable', Moraso_Config::get('cache.internal.enable') == '1' ? 'aktiv' : 'inaktiv');
		}

		if (!is_bool(Moraso_Config::get('cache.internal.lifetime'))) {
			$form->setValue('cache_internal_lifetime', Moraso_Config::get('cache.internal.lifetime'));
		}

		if (!is_bool(Moraso_Config::get('cache.internal.prefix'))) {
			$form->setValue('cache_internal_prefix', Moraso_Config::get('cache.internal.prefix'));
		}

		if (!is_bool(Moraso_Config::get('cache.internal.lifetime'))) {
			$form->setValue('cache_internal_lifetime', Moraso_Config::get('cache.internal.lifetime'));
		}

		$form->setOptions('cache_internal_logging', $statusOptions);
		if (!is_bool(Moraso_Config::get('cache.internal.logging'))) {
			$form->setValue('cache_internal_logging', Moraso_Config::get('cache.internal.logging') == '1' ? 'aktiv' : 'inaktiv');
		}
		
		$form->setOptions('cache_internal_write_control', $statusOptions);
		if (!is_bool(Moraso_Config::get('cache.internal.write.control'))) {
			$form->setValue('cache_internal_write_control', Moraso_Config::get('cache.internal.write.control') == '1' ? 'aktiv' : 'inaktiv');
		}

		$form->setOptions('cache_internal_automatic_serialization', $statusOptions);
		if (!is_bool(Moraso_Config::get('cache.internal.automatic.serialization'))) {
			$form->setValue('cache_internal_automatic_serialization', Moraso_Config::get('cache.internal.automatic.serialization') == '1' ? 'aktiv' : 'inaktiv');
		}

		if (!is_bool(Moraso_Config::get('cache.internal.automatic.cleaning.factor'))) {
			$form->setValue('cache_internal_automatic_cleaning_factor', Moraso_Config::get('cache.internal.automatic.cleaning.factor'));
		}

		$form->setOptions('cache_internal_ignore_user_abort', $statusOptions);
		if (!is_bool(Moraso_Config::get('cache.internal.ignore.user.abort'))) {
			$form->setValue('cache_internal_ignore_user_abort', Moraso_Config::get('cache.internal.ignore.user.abort') == '1' ? 'aktiv' : 'inaktiv');
		}

		$form->setOptions('cache_internal_type', $typeOptions);
		if (!is_bool(Moraso_Config::get('cache.internal.type'))) {
			$form->setValue('cache_internal_type', Moraso_Config::get('cache.internal.type'));
		}

		if (!is_bool(Moraso_Config::get('cache.clear.key'))) {
			$form->setValue('cache_clear_key', Moraso_Config::get('cache.clear.key'));
		}

		/* Page */
		$form->setOptions('cache_page_enable', $statusOptions);
		if (!is_bool(Moraso_Config::get('cache.page.enable'))) {
			$form->setValue('cache_page_enable', Moraso_Config::get('cache.page.enable') == '1' ? 'aktiv' : 'inaktiv');
		}

		if (!is_bool(Moraso_Config::get('cache.page.lifetime'))) {
			$form->setValue('cache_page_lifetime', Moraso_Config::get('cache.page.lifetime'));
		}

		/* Browser */
		$form->setOptions('cache_browser_enable', $statusOptions);
		if (!is_bool(Moraso_Config::get('cache.browser.enable'))) {
			$form->setValue('cache_browser_enable', Moraso_Config::get('cache.browser.enable') == '1' ? 'aktiv' : 'inaktiv');
		}

		if (!is_bool(Moraso_Config::get('cache.browser.lifetime'))) {
			$form->setValue('cache_browser_expireTime', Moraso_Config::get('cache.browser.expireTime'));		
		}	

		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {
				$data = $form->getValues();

				foreach ($data as $key => $value) {
					$value 			= $value === 'aktiv' ? true : ($value === 'inaktiv' ? false : $value);
					$currentClient 	= \Moraso\Session::get('currentClient');

					$dataset = array(
						'id' 			=> Moraso_Db::fetchOne('select id from _moraso_config where identifier =:identifier', array(':identifier' => str_replace('_', '.', $key))),
						'config' 		=> Aitsu_Persistence_Clients::factory($currentClient)->load()->config,
						'env' 			=> 'default',
						'identifier' 	=> str_replace('_', '.', $key),
						'value' 		=> $value
					);

					if (empty($dataset['id'])) {
						unset($dataset['id']);
					}

					Moraso_Db::put('_moraso_config', 'id', $dataset);
				}

				$this->_helper->json((object) array(
					'success' => true,
					'data' => (object) $dataset
				));
			} else {
				$this->_helper->json((object) array(
					'success' => false,
					'errors' => $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' => false,
				'exception' => true,
				'message' => $e->getMessage()
			));
		}
	}

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();

		Moraso_Cache::remove($this->getRequest()->getParam('id'));

		$this->_helper->json((object) array(
			'success' => true
		));
	}

	public function clearcacheAction()
	{
		$this->_helper->layout->disableLayout();

		Moraso_Cache::clean();

		$this->_helper->json((object) array());
	}
}