<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Events
 */

class Moraso_Plugin_Events_Generic_Controller extends Moraso_Adm_Plugin_Controller
{
	public function storeAction()
	{
		$this->_helper->json((object) array(
			'data' => \Moraso\Events::getEvents()
		));
	}

	public function configAction()
	{
		$this->_helper->layout->disableLayout();

		$classExplode 	= explode('_', __CLASS__);

		$form 			= Aitsu_Forms::factory(strtolower($classExplode[2]), APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/forms/config.ini');
		$form->title 	= Aitsu_Translate::translate('Config Events-Plugin');
		$form->url 		= $this->view->url(array('paction' => 'config'));

		$form->setValue('events_media_source', Moraso_Config::get('events.media.source'));

		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {
				$data 			= $form->getValues();
				$currentClient 	= \Moraso\Session::get('currentClient');

				$dataset = array(
					'id' 			=> Moraso_Db::fetchOne('select id from _moraso_config where identifier =:identifier', array(':identifier' => 'events.media.source')),
					'config' 		=> Aitsu_Persistence_Clients::factory($currentClient)->load()->config,
					'env' 			=> 'default',
					'identifier' 	=> 'events.media.source',
					'value' 		=> str_replace('idart ', '', $data['events_media_source'])
				);

				if (empty($dataset['id'])) {
					unset($dataset['id']);
				}

				Moraso_Db::put('_moraso_config', 'id', $dataset);

				$this->_helper->json((object) array(
					'success' 	=> true,
					'data' 		=> (object) $dataset
				));
			} else {
				$this->_helper->json((object) array(
					'success' 	=> false,
					'errors' 	=> $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}
	
	public function editAction()
	{
		$this->_helper->layout->disableLayout();

		$idevent 		= $this->getRequest()->getParam('idevent');

		$classExplode 	= explode('_', __CLASS__);

		$form 			= Aitsu_Forms::factory(strtolower($classExplode[2]), APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/forms/edit.ini');
		$form->title 	= Aitsu_Translate::translate('Edit event');
		$form->url 		= $this->view->url(array('paction' => 'edit'));

		$organizer 		= \Moraso\Events::getAllOrganizer();

		$organizerCollection = array();
		foreach ($organizer as $row) {
			$organizerCollection[] = (object) array(
				'name' 	=> $row['name'],
				'value' => $row['idorganizer']
			);
		}
		$form->setOptions('idorganizer', $organizerCollection);

		$categories = \Moraso\Events::getCategories();

		$categoryCollection = array();
		foreach ($categories as $category) {
			$categoryCollection[] = (object) array(
				'name' 	=> $category['name'],
				'value' => $category['idcategory']
			);
		}
		$form->setOptions('idcategory', $categoryCollection);

		$activeCollection = array();
		$activeCollection[] = (object) array(
			'name' 	=> 'inactive',
			'value' => 0
		);
		$activeCollection[] = (object) array(
			'name' 	=> 'active',
			'value' => 1
		);
		$form->setOptions('active', $activeCollection);

		$medias = \Moraso\Events::getMedia();

		$mediaCollection = array();
		$mediaCollection[] = (object) array(
			'name' 	=> '--- no media selected ---',
			'value' => 0
		);
		foreach ($medias as $media) {
			$mediaCollection[] = (object) array(
				'name' 	=> $media['name'],
				'value' => $media['mediaid']
			);
		}
		$form->setOptions('media_1', $mediaCollection);
		$form->setOptions('media_2', $mediaCollection);
		$form->setOptions('media_3', $mediaCollection);

		if (!empty($idevent)) {
			$data = \Moraso\Events::getEvent($idevent);

			$form->setValues($data);

			$organizerInfo = \Moraso\Events::getOrganizerInfo($data['idorganizer']);

			$form->setValue('organizer_name', $organizerInfo['name']);
			$form->setValue('organizer_phone', $organizerInfo['phone']);
			$form->setValue('organizer_email', $organizerInfo['email']);
			$form->setValue('organizer_homepage', $organizerInfo['homepage']);
		}

		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {

				$data 	= $form->getValues();
				$now 	= date('Y-m-d H:i:s');

				if (empty($data['idevent'])) {
					unset($data['idevent']);

					$data['created'] 		= $now;
					$data['lastmodified'] 	= $now;
					$data['idclient'] 		= \Moraso\Session::get('currentClient');
				}

				//$data['idorganizer'] = \Moraso\Events::getOrganizer($data);

				//if (empty($data['idorganizer'])) {
					$data['idorganizer'] = \Moraso\Events::setOrganizer($data);
				//}

				if (empty($data['endtime'])) {
					$data['endtime'] = $data['starttime'];
				}

				\Moraso\Events::setEvent($data);

				$this->_helper->json((object) array(
					'success' 	=> true,
					'data' 		=> (object) $data
				));
			} else {
				$this->_helper->json((object) array(
					'success' 	=> false,
					'errors' 	=> $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function deleteAction()
	{
		$idevent = $this->getRequest()->getParam('idevent');

		try {
			\Moraso\Events::deleteEvent($idevent);

			$this->_helper->json((object) array(
				'success' => true
			));
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function activateAction()
	{
		$idevent = $this->getRequest()->getParam('idevent');

		try {
			\Moraso\Events::setStatus($idevent, 1);

			$this->_helper->json((object) array(
				'success' => true
			));
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function deactivateAction()
	{
		$idevent = $this->getRequest()->getParam('idevent');

		try {
			\Moraso\Events::setStatus($idevent, 0);

			$this->_helper->json((object) array(
				'success' => true
			));
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}
}