<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/User
 */

class Moraso_Plugin_ListUniversal_Generic_Controller extends Moraso_Adm_Plugin_Controller
{
	public function storeAction()
	{
		$listSet = \Moraso\ListUniversal::getLists();

		$lists = array();
		foreach ($listSet as $list) {
			$lists[] = array(
				'id' 	=> $list->id,
				'name' 	=> $list->name
			);
		}

		$this->_helper->json((object) array(
			'message' 	=> count($lists) . ' Listen wurden geladen',
			'lists' 	=> $lists,
			'success' 	=> true,
			'total' 	=> count($lists)
		));
	}

	public function columnvaluesstoreAction()
	{
		$id 		= $this->getRequest()->getParam('id');

		$valueSet 	= \Moraso\ListUniversal::getColumnValues($id);

		$values = array();
		foreach ($valueSet as $value) {
			$values[] = array(
				'id' 	=> $value->id,
				'value' => $value->value
			);
		}

		$this->_helper->json((object) array(
			'message' 	=> count($values) . ' Werte wurden geladen',
			'values' 	=> $values,
			'success' 	=> true,
			'total' 	=> count($values)
		));
	}

	public function combostoreAction()
	{
		$id 	= $this->getRequest()->getParam('id');
		$query 	= $this->getRequest()->getParam('query');

		$cells 	= \Moraso\ListUniversal::getCellsByColumnId($id);

		$values = array();
		foreach ($cells as $cell) {
			$values[] = $cell->value_varchar ?: $cell->value_int ?: $cell->value_float ?: $cell->value_text ?: $cell->value_date ?: $cell->value_datetime ?: $cell->value_time ?: $cell->value_bool;
		}

		$results = array();
		foreach ($values as $value) {
			if (!$query || strpos($value, $query) !== false) {
				$results[] = array('value' => $value);
			}
		}

		$this->_helper->json((object) array(
			'message' 	=> count($results) . ' Werte wurden geladen',
			'values' 	=> $results,
			'success' 	=> true,
			'total' 	=> count($resultsresults),
			'query' 	=> $query
		));
	}

	public function rowstoreAction()
	{
		// 
		$id_list 	= $this->getRequest()->getParam('id_list');

		// 
		$rowSet 	= \Moraso\ListUniversal::getRows($id_list);

		// 
		$rows = array();
		foreach ($rowSet as $row) {
			// 
			$addRow = array(
				'id' 		=> $row->id,
				'id_list' 	=> $row->id_list
			);

			// 
			$cells = \Moraso\ListUniversal::getRowCells($row->id);

			// 
			foreach ($cells as $cell) {
				$addRow[$cell->id_column] = $cell->value_varchar ?: $cell->value_int ?: $cell->value_float ?: $cell->value_text ?: $cell->value_date ?: $cell->value_datetime ?: $cell->value_time ?: $cell->value_bool;

				$column = \Moraso\ListUniversal::getColumn($cell->id_column);

				if ($column->type === 'combo' || $column->type === 'radiogroup') {
					if ($addRow[$cell->id_column]) {
						$columnValue = \Moraso\ListUniversal::getColumnValue($addRow[$cell->id_column]);

						if (!empty($columnValue)) {
							$addRow[$cell->id_column] = $columnValue->value;
						}
					}
				}

				if ($column->type === 'fileuploadfield') {
					if ($addRow[$cell->id_column]) {
						$file = Aitsu_Core_File::factory(\Moraso\Session::get('currentLanguage'), $addRow[$cell->id_column]);
						
						if (!empty($file)) {
							$addRow[$cell->id_column] = $file->medianame ? $file->medianame : $file->filename;
						}
					}
				}
			}

			// 
			$rows[] = $addRow;
		}

		// 
		$this->_helper->json((object) array(
			'message' 	=> count($rows) . ' Reihen wurden geladen',
			'rows' 		=> $rows,
			'success' 	=> true,
			'total' 	=> count($rows)
		));
	}

	public function structurestoreAction()
	{
		$id 		= $this->getRequest()->getParam('id');

		$columnSet 	= \Moraso\ListUniversal::getListColumns($id);

		$columns = array();
		foreach ($columnSet as $column) {
			$columns[] = array(
				'id' 					=> $column->id,
				'id_list' 				=> $column->id_list,
				'name' 					=> $column->name,
				'type' 					=> $column->type,
				'pos' 					=> $column->pos,
				'displayInBeOverview' 	=> $column->displayInBeOverview
			);
		}

		$this->_helper->json((object) array(
			'message' 	=> count($columns) . ' Spalten wurden geladen',
			'columns' 	=> $columns,
			'success' 	=> true,
			'total' 	=> count($columns)
		));
	}

	public function listAction()
	{
		$this->_helper->layout->disableLayout();

		$id 					= $this->getRequest()->getParam('id');

		$this->view->id_list 	= $id;
		$this->view->name 		= \Moraso\ListUniversal::getListName($id);

		header("Content-type: text/javascript");
		return;
	}

	public function liststructureAction()
	{
		$this->_helper->layout->disableLayout();

		$id 							= $this->getRequest()->getParam('id');

		$this->view->universalListId 	= $id;

		header("Content-type: text/javascript");
		return;
	}

	public function listdataAction()
	{
		$this->_helper->layout->disableLayout();

		$id 							= $this->getRequest()->getParam('id');

		$this->view->universalListId 	= $id;
		$this->view->columns 			= \Moraso\ListUniversal::getListColumns($id);

		header("Content-type: text/javascript");
		return;
	}

	public function editlistAction()
	{
		$this->_helper->layout->disableLayout();

		$id 			= $this->getRequest()->getParam('id');

		$listName 		= $id ? \Moraso\ListUniversal::getListName($id) : 'neue Liste';

		$classExplode 	= explode('_', __CLASS__);

		$form 			= Aitsu_Forms::factory(strtolower($classExplode[2]), APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/forms/list.ini');
		$form->title 	= $listName . ' - Konfiguration';
		$form->url 		= $this->view->url(array('paction' => 'editList'), 'plugin');

		if ($id) {
			$form->setValue('id', 	$id);
			$form->setValue('name', $listName);
		}		

		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {
				$data = $form->getValues();
				
				if ($data['id']) {
					\Moraso\ListUniversal::setListName($data['id'], $data['name']);
				} else {
					$listModel = \Moraso\ListUniversal::createList($data['name']);

					$data['id'] =  $listModel->id;
				}

				$this->_helper->json((object) array(
					'success' 	=> true,
					'data' 		=> (object) $data
				));
			} else {
				$this->_helper->json((object) array(
					'success' 	=> false,
					'errors' 	=> $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function editcolumnAction()
	{
		$this->_helper->layout->disableLayout();

		$id 			= $this->getRequest()->getParam('id');
		$id_list 		= $this->getRequest()->getParam('id_list');

		if ($id) {
			$column = \Moraso\ListUniversal\Column::get($id);
		}

		$classExplode 	= explode('_', __CLASS__);

		$form 			= Aitsu_Forms::factory(strtolower($classExplode[2]), APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/forms/column.ini');
		$form->title 	= isset($column) ? $column->name : 'neue Spalte' . ' - Konfiguration';
		$form->url 		= $this->view->url(array('paction' => 'editColumn'), 'plugin');

		// 
		if ($id_list) {
			$form->setValue('id_list', 	$id_list);

			if (isset($column)) {
				$form->setValue('id', 					$column->id);
				$form->setValue('name', 				$column->name);
				$form->setValue('type', 				$column->type);
				$form->setValue('pos', 					$column->pos);
				$form->setValue('displayInBeOverview', 	$column->displayInBeOverview);
			}
		}

		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {
				// 
				$data = $form->getValues();
				
				// 
				$column = \Moraso\ListUniversal\Column::set($data);

				// Struktur hat sich verändert, die VIEW muss neu gebaut werden
				\Moraso\ListUniversal::createView($data['id_list']);

				$this->_helper->json((object) array(
					'success' 	=> true,
					'name' 		=> $column->name,
					'id_column' => $column->id,
					'id_list' 	=> $column->id_list
				));
			} else {
				$this->_helper->json((object) array(
					'success' 	=> false,
					'errors' 	=> $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function editcolumnvaluesAction()
	{
		$this->_helper->layout->disableLayout();

		$id 				= $this->getRequest()->getParam('id');

		$this->view->id 	= $id;
		$this->view->name 	= \Moraso\ListUniversal::getColumnName($id) ;

		if ($this->getRequest()->getParam('loader')) {
			header("Content-type: text/javascript");
			return;
		}
	}

	public function editrowAction()
	{
		$this->_helper->layout->disableLayout();

		$id 			= $this->getRequest()->getParam('id');
		$id_list 		= $this->getRequest()->getParam('id_list');

		$classExplode 	= explode('_', __CLASS__);

		$form 			= Aitsu_Forms::factory(strtolower($classExplode[2]), APPLICATION_LIBPATH . '/' . $classExplode[0] . '/' . $classExplode[1] . '/' . $classExplode[2] . '/' . $classExplode[3] . '/forms/row.ini');
		
		if ($id) {
			$form->title = 'Datensatz (' . $id . ')';
		} else {
			$form->title = 'neuer Datensatz';
		}

		// 
		$form->url = $this->view->url(array('paction' => 'editRow'), 'plugin');

		// 
		$formGroups = $form->getGroups();

		// 
		$formGroupOneFields 		= new Zend_Config(array(), true);
		$formGroupOneFields->field 	= new Zend_Config(array(), true);

		// 
		$trueFalse 		= array();
		$trueFalse[] 	= array('name' => '0', 'value' => '0');
		$trueFalse[] 	= array('name' => '1', 'value' => '1');

		// 
		$columns = \Moraso\ListUniversal::getListColumns($id_list);

		// 
		$optionSet = array();

		// 
		foreach ($columns as $column) {
			// 
			$newField 				= new Zend_Config(array(), true);
			$newField->extjs 		= new Zend_Config(array(), true);
			$newField->label 		= $column->name;

			// 
			$columnValueSet = \Moraso\ListUniversal::getColumnValues($column->id);

			// 
			if ($columnValueSet) {
				// 
				$columnValueOptions = array();
				
				// 
				foreach ($columnValueSet as $columnValue) {
					$columnValueOptions[] = array(
						'value' => $columnValue->id,
						'name' 	=> $columnValue->value
					);
				}

				// 
				$optionSet[$column->id] = $columnValueOptions;
			}			

			// 
			$newField->type = $column->type;

			// 
			switch ($newField->type) {
				case 'textfield_autocomplete':
					$newField->type 				= 'combo';
					$newField->extjs->typeAhead 	= true;
					$newField->extjs->triggerAction = 'all';
					$newField->extjs->lazyRender 	= true;
					$newField->extjs->minChars 		= 1;
					$newField->extjs->emptyText 	= 'bitte wählen';
					$newField->extjs->loadingText 	= 'Suchen...';
					$newField->extjs->store 		= "new Ext.data.JsonStore({
															autoDestroy: 	true,
															url: 			'" . $this->view->url(array('paction' => 'comboStore', 'id' => $column->id), 'plugin') ."',
															storeId: 		'comboStore_column" . $column->id . "',
															root: 			'values',
															idProperty: 	'value',
															fields: [
																'value'
															]
														})";
					$newField->extjs->valueField 	= 'value';
					$newField->extjs->displayField 	= 'value';
					break;
				case 'true_false_switch':
					$newField->type = 'radiogroup';

					$optionSet[$column->id] = $trueFalse;
					break;
				case 'textarea':
					$newField->extjs->height = 200;
					break;
				case 'fileuploadfield':
					$newField->extjs->value = '';
					break;
			}

			// 
			$formGroupOneFields->{$column->id} = $newField;
		}

		// 
		$formGroups->{1}->field = $formGroupOneFields;

		// 
		if (!empty($optionSet)) {
			foreach ($optionSet as $id_column => $options) {
				$form->setOptions($id_column, $options);
			}
		}

		// 
		$form->setValue('id_list', $id_list);

		// 
		if ($id) {
			$form->setValue('id', $id);

			// 
			$cells 	= \Moraso\ListUniversal::getRowCells($id);

			// 
			foreach ($cells as $cell) {
				$form->setValue($cell->id_column, $cell->value_varchar ?: $cell->value_int ?: $cell->value_float ?: $cell->value_text ?: $cell->value_date ?: $cell->value_datetime ?: $cell->value_time ?: $cell->value_bool);
			}
		}

		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {
				$data = $form->getValues();

				if (isset($_FILES)) {
					$mediaHolder = Moraso_Config::get('article.mediaHolder');

					foreach ($_FILES as $id_column => $file) {
						if (isset($file['tmp_name']) && !empty($file['tmp_name'])) {
							$media = Aitsu_Core_File::upload($mediaHolder, $_FILES[$id_column]['name'], $_FILES[$id_column]['tmp_name']);

							$data[$id_column] = $media->mediaid;
						}
					}					
				}

				$cells = array();
				foreach ($data as $id_column => $value) {
					if ($id_column !== 'id' && $id_column !== 'id_list') {
						$cells[$id_column] = $value;
					}
				}
				
				if ($data['id']) {
					$row 	= \Moraso\ListUniversal::updateRow($data['id'], $cells);
				} else {
					$row 	= \Moraso\ListUniversal::createRow($data['id_list'], $cells);
				}

				echo json_encode(array('success' => true, 'id' => $row->id, 'id_list' => $row->id_list));
				exit();
			} else {
				echo json_encode(array('success' => false, 'errors' => $form->getErrors()));
				exit();
			}
		} catch (Exception $e) {
			echo json_encode(array('success' => false, 'exception' => true, 'message' => $e->getMessage()));
			exit();
		}
	}

	public function addcolumnvalueAction()
	{
		$id 	= $this->getRequest()->getParam('id');
		$value 	= $this->getRequest()->getParam('value');

		\Moraso\ListUniversal::addColumnValue($id, $value);

		$this->_helper->json((object) array(
			'success' => true
		));
	}

	public function editcolumnvalueAction()
	{
		$id 	= $this->getRequest()->getParam('id');
		$value 	= $this->getRequest()->getParam('value');

		\Moraso\ListUniversal::editColumnValue($id, $value);

		$this->_helper->json((object) array(
			'success' => true
		));
	}

	public function deleterowAction()
	{
		$id = $this->getRequest()->getParam('id');

		try {
			\Moraso\ListUniversal::deleteRow($id);

			$this->_helper->json((object) array(
				'success' => true
			));
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function deletecolumnAction()
	{
		$id = $this->getRequest()->getParam('id');

		try {
			\Moraso\ListUniversal::deleteColumn($id);

			$this->_helper->json((object) array(
				'success' => true
			));
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function deletelistAction()
	{
		$id = $this->getRequest()->getParam('id');

		try {
			\Moraso\ListUniversal::deleteList($id);

			$this->_helper->json((object) array(
				'success' => true
			));
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function deletecolumnvalueAction()
	{
		$id = $this->getRequest()->getParam('id');

		try {
			\Moraso\ListUniversal::deleteColumnValue($id);

			$this->_helper->json((object) array(
				'success' => true
			));
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}
}