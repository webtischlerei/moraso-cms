<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Plugin_Modules_Generic_Controller extends Moraso_Adm_Plugin_Controller
{
	public function init()
	{
		$this->_helper->layout->disableLayout();
	}

	public function indexAction()
	{
		header("Content-type: text/javascript");
	}

	public function storeAction()
	{
		$modules = array();

		$modules_list_application = Aitsu_Util_Dir::scan(APPLICATION_LIBPATH, 'module.json');

		$this->_createModuleEntry($modules, $modules_list_application);
		
		$this->_helper->json((object) array(
			'modules' => $modules
		));
	}

	private function _createModuleEntry(&$modules, $modules_list)
	{
		foreach ($modules_list as $module_json) {
			$module_info = json_decode(file_get_contents($module_json));

			$financiers = array();
			if (isset($module_info->financiers) && !empty($module_info->financiers)) {
				foreach ($module_info->financiers as $financier) {
					$financiers[] = (string) $financier->business;
				}
			}

			$module_exploded = explode('/', substr(str_replace('module.json', '', $module_json), strlen(APPLICATION_LIBPATH)));

			if (!empty($module_exploded[3])) {
				$namespace = $module_exploded[1];
				$subnamespace = $module_exploded[2];

				$modules[] = array(
					'namespace' 	=> $namespace,
					'subnamespace' 	=> $subnamespace !== 'Module' ? $subnamespace : '',
					'name' 			=> (string) $module_info->name,
					'author' 		=> (string) $module_info->author,
					'copyright' 	=> (string) $module_info->copyright,
					'financier' 	=> implode(', ', $financiers),
					'path' 			=> str_replace('module.json', '', $module_json)
				);
			}
		}
	}
}