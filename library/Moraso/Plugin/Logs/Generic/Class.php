<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Plugin_Logs_Generic_Controller extends Moraso_Adm_Plugin_Controller
{
	public function init()
	{
		$this->_helper->layout->disableLayout();
	}

	public function indexAction()
	{
		header("Content-type: text/javascript");
	}

	public function storeAction()
	{
		$mapper = new \Moraso\Model\Log\Mapper();

		$rows 	= $mapper->fetchAll(50, 'id DESC');
		
		$logs = array();
		foreach ($rows as $row) {
			$logs[] = array(
				'date'      	=> date('d.m.Y', strtotime($row->timestamp)),
				'time'     		=> date('H:i:s', strtotime($row->timestamp)),
				'priority'  	=> $row->priority,
				'message' 		=> $row->message,
				'message_long' 	=> '<pre>' . $row->message . '</pre>',
				'file' 			=> $row->file,
				'line' 			=> $row->line,
				'library' 		=> $row->library,
				'skin' 			=> $row->skin,
				'module' 		=> $row->module,
				'plugin' 		=> $row->plugin,
				'plugin_area' 	=> $row->plugin_area
			);
		}

		$this->_helper->json((object) array(
			'logs' => $logs
		));
	}
}