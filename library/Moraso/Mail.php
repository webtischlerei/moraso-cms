<?php

/**
 * Diese Klasse kümmert sich um einen Reibungslosen Versand von Mails
 *
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Mail
 */
class Moraso_Mail
{
	private $_mail;

	public function __construct($id = 'default')
	{
		$this->_mail = new stdClass();
		
		try {
			$this->_setConfigurations($id);

			$this->_setTransporter();

			$this->_mail->zend_mail = new \Zend\Mail\Message('UTF-8');
			$this->_mail->zend_mail->setFrom($this->_mail->configurations->from->mail, $this->_mail->configurations->from->name);
			$this->_mail->zend_mail->setSubject($this->_mail->configurations->subject);
			$this->_mail->zend_mail->addTo($this->_mail->configurations->to->mail, $this->_mail->configurations->to->name);
		} catch (Exception $e) {
			trigger_error($e->getMessage());
			echo $e->getMessage();
		}
	}

	/**
	* Diese Methode ließt alle Mail-spezifischen Konfigurationen aus der Datenbank aus
	*/
	private function _setConfigurations($id)
	{
		$username = Moraso_Config::get('mail.' . $id . '.transport.smtp.username');

		if (empty($username)) {
			throw new Exception('Es wurde kein SMTP User für den Mail-Versand angegeben!');
		}

		$this->_mail->configurations = new stdClass();

		$this->_mail->configurations->from 					= new stdClass();
		$this->_mail->configurations->from->mail 			= Moraso_Config::get('mail.' . $id . '.from.mail');
		$this->_mail->configurations->from->name 			= Moraso_Config::get('mail.' . $id . '.from.name');

		$this->_mail->configurations->subject 				= Moraso_Config::get('mail.' . $id . '.subject');

		$this->_mail->configurations->to 					= new stdClass();
		$this->_mail->configurations->to->mail 				= Moraso_Config::get('mail.' . $id . '.to.mail');
		$this->_mail->configurations->to->name 				= Moraso_Config::get('mail.' . $id . '.to.name');

		$this->_mail->configurations->transport 			= new stdClass();
		$this->_mail->configurations->transport->host 		= Moraso_Config::get('mail.' . $id . '.transport.smtp.host');
		$this->_mail->configurations->transport->auth 		= Moraso_Config::get('mail.' . $id . '.transport.smtp.auth');
		$this->_mail->configurations->transport->username 	= Moraso_Config::get('mail.' . $id . '.transport.smtp.username');
		$this->_mail->configurations->transport->password 	= Moraso_Config::get('mail.' . $id . '.transport.smtp.password');
		$this->_mail->configurations->transport->ssl 		= Moraso_Config::get('mail.' . $id . '.transport.smtp.ssl');
		$this->_mail->configurations->transport->port 		= Moraso_Config::get('mail.' . $id . '.transport.smtp.port');
	}

	/**
	* Diese Methode stellt den SMTP Transporter passend ein
	*/
	private function _setTransporter()
	{
		$transporter 	= new \Zend\Mail\Transport\Smtp();

		$smtpOptions = array(
			'connection_config' => array(
				'username' 	=> $this->_mail->configurations->transport->username,
				'password' 	=> $this->_mail->configurations->transport->password
			)
		);

		if ($this->_mail->configurations->transport->host) {
			$smtpOptions['host'] = $this->_mail->configurations->transport->host;
		};

		if ($this->_mail->configurations->transport->port) {
			$smtpOptions['port'] = $this->_mail->configurations->transport->port;
		};

		if ($this->_mail->configurations->transport->auth && $this->_mail->configurations->transport->auth !== 'none') {
			$smtpOptions['connection_class'] = $this->_mail->configurations->transport->auth;
		};

		if ($this->_mail->configurations->transport->ssl && $this->_mail->configurations->transport->ssl !== 'none') {
			$smtpOptions['connection_config']['ssl'] = $this->_mail->configurations->transport->ssl;
		};

		$options = new \Zend\Mail\Transport\SmtpOptions($smtpOptions);

		$transporter->setOptions($options);

		$this->_mail->transporter = $transporter;
	}

	/**
	* Mit dieser Methode kann man den Betreff beeinflussen
	*/
	public function setSubject($subject)
	{
		$this->_mail->zend_mail->setSubject($subject);
	}

	/**
	* Mit dieser Methode kann man Empfänger festlegen
	*/
	public function addTo(array $receiver)
	{
		foreach ($receiver as $name => $mail) {
			$this->_mail->zend_mail->addTo($mail, $name);
		}
	}

	/**
	* Mit dieser Methode kann man Kopie-Empfänger festlegen
	*/
	public function addCc(array $receiver_cc)
	{
		foreach ($receiver_cc as $name => $mail) {
			$this->_mail->zend_mail->addCc($mail, $name);
		}
	}

	/**
	* Mit dieser Methode kann man "unsichtbare" Kopie-Empfänger festlegen
	*/
	public function addBcc(array $receiver_bcc)
	{
		foreach ($receiver_bcc as $name => $mail) {
			$this->_mail->zend_mail->addBcc($mail, $name);
		}
	}

	/**
	* Legt die Antwortadresse fest
	*/
	public function setReplyTo($name, $mail)
	{
		$this->_mail->zend_mail->clearReplyTo();
		$this->_mail->zend_mail->setReplyTo($mail, $name);
	}

	/**
	* E-Mail Inhalt setzen (Text)
	*/
	public function setBodyText($text)
	{
		$text 		= new \Zend\Mime\Part($text);
		$text->type = "text/plain";

		$body 		= new \Zend\Mime\Message();
		$body->setParts(array($text));
		
		$this->_mail->zend_mail->setBody($body);
	}

	/**
	* E-Mail Inhalt setzen (HTML)
	*/
	public function setBodyHtml($html)
	{
		$html 		= new \Zend\Mime\Part($html);
		$html->type = "text/html";

		$body 		= new \Zend\Mime\Message();
		$body->setParts(array($html));

		$this->_mail->zend_mail->setBody($body);
	}

	/**
	* Diese Methode verschickt die Mail an den gewünschten Empfänger
	*/
	public function send()
	{
		try {
			$this->_mail->transporter->send($this->_mail->zend_mail);
		} catch (Exception $e) {
			trigger_error($e->getMessage());

			throw $e;
		}
	}	
}