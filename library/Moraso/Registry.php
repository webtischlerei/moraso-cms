<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */

namespace Moraso;

use Moraso\Service\Manager as ServiceManager;

use Zend\Stdlib\SplQueue;

class Registry
{
	/**
	* TODO
	*/
	public static function init()
	{
		$serviceManager = ServiceManager::get();

		$serviceManager->setService('registry', new SplQueue());
	}

	/**
	* TODO
	*/
	public static function set($item, $value)
	{
		$registry = self::getRegistry();

		$registry->$item = $value;
	}

	/**
	* TODO
	*/
	public static function get($item)
	{
		$registry = self::getRegistry();

		return $registry->$item;
	}

	/**
	* TODO
	*/
	public static function has($item)
	{
		$registry = self::getRegistry();

		return isset($registry->$item);
	}

	/**
	* TODO
	*/
	public static function isEdit($set = null)
	{
		if (is_null($set)) {
			return self::get('isEdit');
		}

		self::set('isEdit', $set);
	}

	/**
	* TODO
	*/
	private static function getRegistry()
	{
		$serviceManager = ServiceManager::get();

		return $serviceManager->get('registry');
	}
}