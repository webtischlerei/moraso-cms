<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */

namespace Moraso;

use Zend\Cache\StorageFactory;

class MorasoCache
{
	public static function init()
	{
		if (!is_dir(APPLICATION_PATH . '/data/moraso_cache/')) {
			mkdir(APPLICATION_PATH . '/data/moraso_cache/', 0777, true);
		}

		$cache = StorageFactory::factory(array(
			'adapter' => array(
				'name'    => 'filesystem',
				'options' => array(
					'namespace' => 'moraso',
					'readable' 	=> (bool) \Moraso_Config::get('cache.internal.enable'),
					'writable' 	=> (bool) \Moraso_Config::get('cache.internal.enable'),
					'cache_dir' => APPLICATION_PATH . '/data/moraso_cache/'
				)
			),
			'plugins' => array(
				'exception_handler' => array(
					'throw_exceptions' => true
				),
				'Serializer'
			)
		));

		$serviceManager = Service\Manager::get();
		$serviceManager->setService('moraso-cache', $cache);
		$serviceManager->setService('moraso-cache-performance', new \stdClass());
	}

	public static function setItem($key, $value, $lifetime = 0, array $tags = array())
	{
		if (\Aitsu_Application_Status::isEdit() || \Aitsu_Application_Status::getEnv() === 'backend') {
			return false;
		}

		if (!empty($lifetime)) {
			$serviceManager = Service\Manager::get();

			/* Performance Cache */
			$performanceCache = $serviceManager->get('moraso-cache-performance');

			$performanceCache->$key = $value;

			/* Cache */
			$cache = $serviceManager->get('moraso-cache');

			$cache->getOptions()->setTtl($lifetime === 'eternal' ? 31536000 : $lifetime);

			$cache->setTags($key, $tags);
			$cache->setItem($key, $value);
		}		
	}

	public static function getItem($key)
	{
		if (\Aitsu_Application_Status::isEdit() || \Aitsu_Application_Status::getEnv() === 'backend') {
			return false;
		}

		$serviceManager = Service\Manager::get();

		/* Performance Cache */
		$performanceCache = $serviceManager->get('moraso-cache-performance');

		if (isset($performanceCache->key) && !empty($performanceCache->key)) {
			return $performanceCache->key;
		}

		/* Cache */
		$cache = $serviceManager->get('moraso-cache');

		if (!$cache->hasItem($key)) {
			return false;
		}

		$result = $cache->getItem($key, $success);

		if ($success) {
			return $result;
		}

		return false;
	}

	public static function removeItem($key)
	{
		$serviceManager = Service\Manager::get();

		/* Performance Cache */
		$performanceCache = $serviceManager->get('moraso-cache-performance');

		unset($performanceCache->$key);

		/* Cache */
		$cache = $serviceManager->get('moraso-cache');

		$cache->removeItem($key);
	}

	public static function flush()
	{
		$serviceManager = Service\Manager::get();

		/* Performance Cache */
		if ($serviceManager->has('moraso-cache-performance')) {
			$performanceCache = $serviceManager->get('moraso-cache-performance');

			$performanceCache = null;
		}

		/* Cache */
		if ($serviceManager->has('moraso-cache')) {
			$cache = $serviceManager->get('moraso-cache');

			$cache->flush();
		}
	}
}