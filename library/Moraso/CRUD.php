<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */

namespace Moraso;

use \ReflectionClass;
use \ReflectionProperty;

use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;

abstract class CRUD implements EventManagerAwareInterface
{
	protected $eventManager;

	/**
	* TODO
	*/
	public static function create(array $set)
	{
		$mapper = self::getMapper();
		$model 	= self::getModel();

		foreach ($set as $row => $value) {
			$model->$row = $value;
		}

		$mapper->save($model);

		return $model;
	}

	/**
	* TODO
	*/
	public static function read($id = null, $col = null, $limit = null, $order = null, $forceArray = false)
	{
		$mapper = self::getMapper();

		return $mapper->find($id, $col, $forceArray, $limit, $order);
	}

	/**
	* TODO
	*/
	public static function update($id, array $set)
	{
		$mapper = self::getMapper();
		$model 	= self::getModel($id);

		foreach ($set as $row => $value) {
			$model->$row = $value;
		}
		
		$mapper->save($model);

		return $model;
	}

	/**
	* TODO
	*/
	public static function delete($id, $col = null)
	{
		$mapper = self::getMapper();

		return $mapper->delete($id, $col);
	}

	/**
	* TODO
	*/
	public static function get($id = null, $col = null, $limit = null, $order = null, $forceArray = false)
	{
		return self::read($id, $col, $limit, $order, $forceArray);
	}

	/**
	* TODO
	*/
	private static function getMapper()
	{
		$calledClass 	= get_called_class();
		
		$mapper 		= static::$mapper;

		return new $mapper;
	}

	/**
	* TODO
	*/
	private static function getModel($id = null)
	{
		$mapper = self::getMapper();

		if (is_null($id)) {
			return $mapper->create();
		} else {
			return $mapper->find($id);
		}
	}

	/**
	* TODO
	*/
	public function setEventManager(EventManagerInterface $eventManager)
	{
		$eventManager->addIdentifiers(array(
			get_called_class()
		));

		$this->eventManager = $eventManager;
	}

	/**
	* TODO
	*/
	public function getEventManager()
	{
		if (null === $this->eventManager) {
			$this->setEventManager(new EventManager());
		}

		return $this->eventManager;
	}
}