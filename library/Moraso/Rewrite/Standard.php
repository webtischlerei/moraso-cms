<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Rewrite
 */
class Moraso_Rewrite_Standard extends Moraso_Rewrite_Abstract
{
	private static $_instance = null;

	public static function getInstance()
	{
		if (self::$_instance === null) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	final private function __construct() {}

	final private function __clone() {}

	public function register()
	{
		return empty(Aitsu_Registry::get()->env->idlang);
	}

	public function registerParams()
	{
		if (isset($_GET['url']) && substr($_GET['url'], -5) === '.html') {
			$pathInfo 	= pathinfo($_GET['url']);
			$url 		= $pathInfo['dirname'] . '/';
			$urlname 	= $pathInfo['filename'];
		} else {
			$url 		= isset($_GET['url']) ? $_GET['url'] : '';
			$urlname 	= null;
		}

		$idcat 		= Moraso_Config::get('sys.startcat');
		$idclient 	= Moraso_Config::get('sys.client');

		if (Moraso_Config::get('rewrite.uselang') && !empty($url)) {
			$idlang = Moraso_Db_Simple::fetch('idlang', '_lang', array('name' => strtok($url, '/')), 1);
		} else {
			$idlang = Moraso_Config::get('sys.language');
		}

		if (empty($url)) {
			$result = Moraso_Db::fetchRowC('eternal', '' .
							'SELECT ' .
							'   artlang.idart, ' .
							'   artlang.idlang, ' .
							'   artlang.idartlang, ' .
							'   catlang.idcat, ' .
							'	catlang.idcatlang, ' .
							'   cat.idclient ' .
							'FROM ' .
							'   _art_lang AS artlang ' .
							'LEFT JOIN ' .
							'   _cat_lang AS catlang ON artlang.idartlang = catlang.startidartlang ' .
							'LEFT JOIN ' .
							'   _cat AS cat ON catlang.idcat = cat.idcat ' .
							'WHERE ' .
							'   catlang.idcat =:idcat ' .
							'AND' .
							'   artlang.idlang =:idlang', array(
						':idcat' 	=> $idcat,
						':idlang' 	=> $idlang
			));
		} elseif (Moraso_Config::get('rewrite.uselang') && preg_match('@^\\w*/?$@', $url)) {
			if (empty($urlname)) {
				$result = Moraso_Db::fetchRowC('eternal', '' .
							'SELECT ' .
							'   artlang.idart, ' .
							'   artlang.idlang, ' .
							'   artlang.idartlang, ' .
							'   catlang.idcat, ' .
							'	catlang.idcatlang, ' .
							'   lang.idclient ' .
							'FROM ' .
							'   _art_lang AS artlang ' .
							'LEFT JOIN ' .
							'   _cat_lang AS catlang ON artlang.idartlang = catlang.startidartlang ' .
							'LEFT JOIN ' .
							'   _lang AS lang ON catlang.idlang = lang.idlang ' .
							'WHERE ' .
							'   catlang.idcat =:idcat ' .
							'AND ' .
							'   lang.name =:langname ' .
							'AND ' .
							'   lang.idclient =:client ', array(
						':idcat' 	=> $idcat,
						':langname' => str_replace('/', '', $url),
						':client' 	=> $idclient
				));
			} else {
				$result = Moraso_Db::fetchRowC('eternal', '' .
					'SELECT ' .
					'	artlang.idart, ' .
					'   artlang.idlang, ' .
					'   artlang.idartlang, ' .
					'   catlang.idcat, ' .
					'	catlang.idcatlang, ' .
					'   cat.idclient ' .
					'FROM ' .
					'   _art_lang AS artlang ' .
					'LEFT JOIN ' .
					'   _cat_art AS catart ON artlang.idart = catart.idart ' .
					'LEFT JOIN ' .
					'   _cat_lang AS catlang ON catart.idcat = catlang.idcat AND artlang.idlang = catlang.idlang ' .
					'LEFT JOIN ' .
					'   _cat AS cat ON catlang.idcat = cat.idcat ' .
					'WHERE ' .
					'   artlang.urlname =:urlname ' .
					'AND ' .
					'   catlang.idlang =:idlang', array(
						':urlname' 	=> $urlname,
						':idlang' 	=> $idlang
				));
			}
		} else {
			if (!empty($urlname)) {
				$result = Moraso_Db::fetchRowC('eternal', '' .
					'SELECT ' .
					'   artlang.idart, ' .
					'   artlang.idlang, ' .
					'   artlang.idartlang, ' .
					'   catlang.idcat, ' .
					'	catlang.idcatlang, ' .
					'   cat.idclient ' .
					'FROM ' .
					'   _art_lang AS artlang ' .
					'LEFT JOIN ' .
					'   _cat_art AS catart ON artlang.idart = catart.idart ' .
					'LEFT JOIN ' .
					'   _cat_lang AS catlang ON catart.idcat = catlang.idcat AND artlang.idlang = catlang.idlang ' .
					'LEFT JOIN ' .
					'   _cat AS cat ON catlang.idcat = cat.idcat ' .
					'WHERE ' .
					'   artlang.urlname =:urlname ' .
					'AND ' .
					'   catlang.url =:url ' .
					'AND ' .
					'   cat.idclient =:client ', array(
						':urlname' 	=> $urlname,
						':url' 		=> $url,
						':client' 	=> $idclient
				));
			} else {
				$result = Moraso_Db::fetchRowC('eternal', '' .
					'SELECT ' .
					'   artlang.idart, ' .
					'   artlang.idlang, ' .
					'   artlang.idartlang, ' .
					'   catlang.idcat, ' .
					'	catlang.idcatlang, ' .
					'   cat.idclient ' .
					'FROM ' .
					'   _art_lang AS artlang ' .
					'LEFT JOIN ' .
					'   _cat_lang AS catlang ON artlang.idartlang = catlang.startidartlang ' .
					'LEFT JOIN ' .
					'   _cat AS cat ON catlang.idcat = cat.idcat ' .
					'WHERE ' .
					'   catlang.url =:url ' .
					'AND ' .
					'   cat.idclient =:client ', array(
						':url' 		=> $url,
						':client' 	=> $idclient
				));
			}
		}

		if ($result) {
			Aitsu_Registry::get()->env->idart = $result['idart'];
			Aitsu_Registry::get()->env->idcat = $result['idcat'];
			Aitsu_Registry::get()->env->idlang = $result['idlang'];
			Aitsu_Registry::get()->env->idartlang = $result['idartlang'];
			Aitsu_Registry::get()->env->idcatlang = $result['idcatlang'];
			Aitsu_Registry::get()->env->idclient = $result['idclient'];
		} else {
			Aitsu_Registry::get()->env->idlang = $idlang;
		}
	}

	public function getParamsByUrl($url)
	{
		$url = str_replace(Moraso_Config::get('sys.webpath'), '', $url);

		if (substr($url, -5) === '.html') {
			$pathInfo = pathinfo($url);
			$url = $pathInfo['dirname'];
			$urlname = $pathInfo['filename'];
		} else {
			$url = substr($url, -1) == '/' ? substr($url, 0, -1) : $url;
			$urlname = null;
		}

		$idcat = Moraso_Config::get('sys.startcat');
		$idlang = Moraso_Config::get('sys.language');
		$idclient = Moraso_Config::get('sys.client');

		if (Moraso_Config::get('rewrite.uselang') && preg_match('@^\\w*/?$@', $url)) {
			$result = Moraso_Db::fetchRowC('eternal', '' .
							'SELECT ' .
							'   artlang.idart, ' .
							'   artlang.idlang, ' .
							'   artlang.idartlang, ' .
							'   catlang.idcat, ' .
							'   lang.idclient ' .
							'FROM ' .
							'   _art_lang AS artlang ' .
							'LEFT JOIN ' .
							'   _cat_lang AS catlang ON artlang.idartlang = catlang.startidartlang ' .
							'LEFT JOIN ' .
							'   _lang AS lang ON catlang.idlang = lang.idlang ' .
							'WHERE ' .
							'   catlang.idcat =:idcat ' .
							'AND ' .
							'   lang.name =:langname ' .
							'AND ' .
							'   lang.idclient =:client ', array(
						':idcat' 	=> $idcat,
						':langname' => $url,
						':client' 	=> $idclient
			));
		} else {
			if ($urlname == null) {
				$result = Moraso_Db::fetchRowC('eternal', '' .
								'SELECT ' .
								'   artlang.idart, ' .
								'   artlang.idlang, ' .
								'   artlang.idartlang, ' .
								'   catlang.idcat, ' .
								'   cat.idclient ' .
								'FROM ' .
								'   _art_lang AS artlang ' .
								'LEFT JOIN ' .
								'   _cat_lang AS catlang ON artlang.idartlang = catlang.startidartlang ' .
								'LEFT JOIN ' .
								'   _cat AS cat ON catlang.idcat = cat.idcat ' .
								'where ' .
								'   catlang.url =:url ' .
								'AND ' .
								'   cat.idclient =:client ', array(
							':url' 		=> $url,
							':client' 	=> $idclient
				));
			} else {
				$result = Moraso_Db::fetchRowC('eternal', '' .
								'SELECT ' .
								'   artlang.idart, ' .
								'   artlang.idlang, ' .
								'   artlang.idartlang, ' .
								'   catlang.idcat, ' .
								'   cat.idclient ' .
								'FROM ' .
								'   _art_lang AS artlang ' .
								'LEFT JOIN ' .
								'   _cat_art AS catart ON artlang.idart = catart.idart ' .
								'LEFT JOIN ' .
								'   _cat_lang AS catlang ON catart.idcat = catlang.idcat AND artlang.idlang = catlang.idlang ' .
								'LEFT JOIN ' .
								'   _cat AS cat ON catlang.idcat = cat.idcat ' .
								'where ' .
								'   artlang.urlname =:urlname ' .
								'AND ' .
								'   catlang.url =:url ' .
								'AND ' .
								'   cat.idclient =:client ', array(
							':urlname' 	=> $urlname,
							':url' 		=> $url,
							':client' 	=> $idclient
				));
			}
		}

		return $result;
	}

	public function rewriteOutput($html)
	{
		$this->_populateMissingUrls(Aitsu_Registry::get()->env->idlang);

		$matches = array();
		if (preg_match_all('/\\{ref:(idcat|idart)\\-(\\d+)\\}/s', $html, $matches) == 0) {
			return $html;
		}

		$matches[0] = array_unique($matches[0], SORT_REGULAR);
		$baseUrl 	= Moraso_Config::get('sys.webpath');

		$idarts = array();
		$idcats = array();
		foreach (array_keys($matches[0]) AS $key) {
			if ($matches[1][$key] == 'idart') {
				$idarts[$matches[2][$key]] = $matches[0][$key];
			} elseif ($matches[1][$key] == 'idcat') {
				$idcats[$matches[2][$key]] = $matches[0][$key];
			}
		}

		if (empty($idarts) && empty($idcats)) {
			return $html;
		}

		$rewriteSearch 	= array();
		$rewriteReplace = array();

		if (!empty($idarts)) {
			$results = Moraso_Db::fetchAll('' .
				'SELECT ' .
				'	artlang.idart AS idart, ' .
				'	CONCAT(catlang.url, artlang.urlname, \'.html\') AS url, ' .
				'	artlang.idlang AS idlang ' .
				'FROM ' .
				'	_art_lang AS artlang ' .
				'INNER JOIN ' .
				'	_cat_art AS catart ON artlang.idart = catart.idart ' .
				'INNER JOIN ' .
				'	_cat_lang AS catlang ON (artlang.idlang = catlang.idlang AND catart.idcat = catlang.idcat)' .
				'WHERE ' .
				'	artlang.idart IN (' . implode(',', array_keys($idarts)) . ') ' .
				'AND ' .
				'	artlang.idlang =:idlang', array(
					':idlang' => Aitsu_Registry::get()->env->idlang
				)
			);

			if ($results) {
				foreach ($results AS $article) {
					if (($url = Moraso_Cache::load('rewriting_idlang_' . $article['idlang'] . '_idart_' . $article['idart'])) === false) {
						$url = $baseUrl . $article['url'];

						Moraso_Cache::save('rewriting_idlang_' . $article['idlang'] . '_idart_' . $article['idart'], $url, Aitsu_Util_Date::secondsUntilEndOf('year'), array('rewriting'));
					}

					$rewriteSearch[] = $idarts[$article['idart']];
					$rewriteReplace[] = $url;
				}
			}
		}

		if (!empty($idcats)) {
			$results = Moraso_Db::fetchAll('' .
				'SELECT ' .
				'	catlang.idcat AS idcat, ' .
				'	catlang.url AS url, ' .
				'	catlang.idlang AS idlang ' .
				'FROM ' .
				'	_cat_lang AS catlang ' .
				'WHERE ' .
				'	catlang.idcat IN (' . implode(',', array_keys($idcats)) . ') ' .
				'AND ' .
				'	catlang.idlang =:idlang ', array(
					':idlang' => Aitsu_Registry::get()->env->idlang
				)
			);

			if ($results) {
				foreach ($results AS $category) {
					if (($url = Moraso_Cache::load('rewriting_idlang_' . $category['idlang'] . '_idcat_' . $category['idcat'])) === false) {
						$url = $baseUrl . $category['url'];

						Moraso_Cache::save('rewriting_idlang_' . $category['idlang'] . '_idcat_' . $category['idcat'], $url, Aitsu_Util_Date::secondsUntilEndOf('year'), array('rewriting'));
					}

					$rewriteSearch[] = $idcats[$category['idcat']];
					$rewriteReplace[] = $url;
				}
			}
		}

		return preg_replace('/\\{ref:(idcat|idart)\\-(\\d+)\\}/s', '/', str_replace($rewriteSearch, $rewriteReplace, $html));
	}

	protected function _populateMissingUrls($idlang)
	{
		$categoriesWithoutUrl = Moraso_Db::fetchOne('' .
						'SELECT ' .
						'   COUNT(idcat) ' .
						'FROM ' .
						'   _cat_lang ' .
						'WHERE ' .
						'   url IS NULL ' .
						'AND ' .
						'   idlang =:idlang', array(
					':idlang' => $idlang
		));

		if (empty($categoriesWithoutUrl)) {
			return;
		}

		Moraso_Db::startTransaction();

		try {
			Moraso_Db::query('' .
					'UPDATE ' .
					'   _cat_lang AS catlang, ' .
					'   ( ' .
					'       SELECT ' .
					'           child.idcat AS idcat, ' .
					'       	CONCAT(GROUP_CONCAT(catlang.urlname ORDER BY parent.lft ASC SEPARATOR \'/\'), \'/\') AS url ' .
					'       FROM ' .
					'           _cat AS child ' .
					'       LEFT JOIN ' .
					'           _cat AS parent ON child.lft BETWEEN parent.lft AND parent.rgt ' .
					'       LEFT JOIN ' .
					'           _cat_lang AS catlang ON parent.idcat = catlang.idcat AND parent.parentid > 0 ' .
					'       WHERE ' .
					'           catlang.idlang =:idlang ' .
					'       GROUP BY ' .
					'           child.idcat ' .
					'   ) AS url ' .
					'SET ' .
					'   catlang.url = url.url ' .
					'WHERE ' .
					'   catlang.idcat = url.idcat ' .
					'AND ' .
					'	catlang.url IS NULL ' .
					'AND ' .
					'   catlang.idlang =:idlang', array(
				':idlang' => $idlang
			));

			Moraso_Db::query('UPDATE _cat_lang SET url = \'\' WHERE url IS NULL');

			if (Moraso_Config::get('rewrite.uselang')) {
				Moraso_Db::query('' .
						'UPDATE ' .
						'   _cat_lang AS catlang, ' .
						'   _lang AS lang ' .
						'SET ' .
						'   catlang.url = concat(lang.name, \'/\', catlang.url) ' .
						'WHERE ' .
						'   lang.idlang = catlang.idlang ' .
						'AND ' .
						'	catlang.url  NOT LIKE concat(lang.name, \'/%\') ' .
						'AND ' .
						'   catlang.idlang =:idlang', array(
					':idlang' => $idlang
				));
			}

			Moraso_Db::commit();
		} catch (Exception $e) {
			Moraso_Db::rollback();
			trigger_error('Exception in ' . __FILE__ . ' on line ' . __LINE__);
			trigger_error('Message: ' . $e->getMessage());
			trigger_error($e->getTraceAsString());
		}
	}
}
