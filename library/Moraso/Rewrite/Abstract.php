<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
abstract class Moraso_Rewrite_Abstract implements Moraso_Rewrite_Interface
{
	public function register()
	{
		return false;
	}

	public function rewriteOutput($html)
	{
		return $html;
	}
}
