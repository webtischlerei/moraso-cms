<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
interface Moraso_Rewrite_Interface
{
	public static function getInstance();
	
	public function register();

	public function registerParams();

	public function rewriteOutput($html);
}
