<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */

namespace Moraso\Content\Config;

class Select extends AbstractConfig
{
	public function getTemplate()
	{
		return 'select.phtml';
	}
	
	public static function set($index, $name, $label, $keyValuePairs, $fieldset)
	{
		$config = new self($index, $name);
		
		$config->facts['fieldset'] 			= $fieldset;
		$config->facts['label'] 			= $label;
		$config->params['keyValuePairs'] 	= $keyValuePairs;
	
		return $config->currentValue();
	}
}