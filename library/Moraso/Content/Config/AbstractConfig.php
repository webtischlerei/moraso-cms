<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */

namespace Moraso\Content\Config;

abstract class AbstractConfig
{
	protected $facts 	= array();
	protected $params 	= array();

	final protected function __construct($index, $name, $idartlang = null, $suppressRegistration = false)
	{
		if (strlen($name) > 127) {
			throw new \Exception('The name may consist of not more than 127 characters.');
		}
		
		$this->facts['index'] 		= $index;
		$this->facts['name'] 		= str_replace('.', '_', $name);
		$this->facts['type'] 		= 'text';
		$this->facts['idartlang'] 	= $idartlang == null ? \Aitsu_Registry::get()->env->idartlang : $idartlang;
		
		if (!$suppressRegistration) {
			\Aitsu_Content_Edit::registerConfig($this);
		}
	}
	
	final public function __set($key, $value)
	{
		$this->facts[$key] = $value;
	}
	
	final public function __get($key)
	{
		if (!isset($this->facts[$key]) && !isset($this->params[$key])) {
			return null;
		}
		
		if (isset($this->params[$key])) {
			return $this->params[$key];
		}
		
		return $this->facts[$key];
	}
	
	final public function __isset($key)
	{
		return isset($this->facts[$key]) || isset($this->params[$key]);
	}
	
	final public function currentValue()
	{
		if (\Aitsu_Core_Article_Property::factory($this->facts['idartlang'])->getValue('ModuleConfig_' . $this->facts['index'], $this->facts['name']) == null) {
			return null;
		}
		
		$value = \Aitsu_Core_Article_Property::factory($this->facts['idartlang'])->getValue('ModuleConfig_' . $this->facts['index'], $this->facts['name'])->value;
	
		if ($this->facts['type'] == 'date' && $value == '0000-00-00 00:00:00') {
			$value = '';
		} 
	
		return $value;
	}

	abstract public function getTemplate();
}