<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */

namespace Moraso\Content\Config;

use ReflectionClass;
use ReflectionProperty;

class ListUniversal extends AbstractConfig
{
	public function getTemplate()
	{
		return 'listuniversal.phtml';
	}
	
	public static function set($index, $name, $label, $id_list)
	{
		$rowSet		 = \Moraso\ListUniversal::getRows($id_list);

		$listColumns = \Moraso\ListUniversal::getListColumns($id_list);

		$column_1 = $listColumns[0]->id;
		$column_2 = $listColumns[1]->id;
		$column_3 = $listColumns[2]->id;

		$rows = array();
		foreach ($rowSet as $key => $row) {	
			$rows[$key]['id'] = $row->id;
			$rows[$key]['column_1'] = '';
			$rows[$key]['column_2'] = '';
			$rows[$key]['column_3'] = '';

			$cells = \Moraso\ListUniversal::getRowCells($row->id);

			foreach ($cells as $cell) {
				$value 	= $cell->value_varchar ?: $cell->value_int ?: $cell->value_float ?: $cell->value_text ?: $cell->value_date ?: $cell->value_datetime ?: $cell->value_time ?: $cell->value_bool;
				$column = \Moraso\ListUniversal::getColumn($cell->id_column);

				if ($column->type === 'combo' || $column->type === 'radiogroup') {
					$columnValue = \Moraso\ListUniversal::getColumnValue($value);

					if (!empty($columnValue)) {
						$value = $columnValue->value;
					}
				}

				if ($column->type === 'fileuploadfield') {
					$file = \Aitsu_Core_File::factory(\Moraso\Session::get('currentLanguage'), $value);
						
					if (!empty($file)) {
						$value = $file->medianame ? $file->medianame : $file->filename;
					}
				}

				if ($cell->id_column == $column_1) {
					$rows[$key]['column_1'] = str_replace("\n\r", "<br />", str_replace("\n", "<br />", str_replace("\r", "<br />", substr($value, 0, 25))));
				} elseif ($cell->id_column == $column_2) {
					$rows[$key]['column_2'] = str_replace("\n\r", "<br />", str_replace("\n", "<br />", str_replace("\r", "<br />", substr($value, 0, 25))));
				} elseif ($cell->id_column == $column_3) {
					$rows[$key]['column_3'] = str_replace("\n\r", "<br />", str_replace("\n", "<br />", str_replace("\r", "<br />", substr($value, 0, 25))));
				}
			}
		}
		
		$config 				= new self($index, $name);
		$config->facts['tab'] 	= true;
		$config->facts['label'] = $label;
		$config->facts['type'] 	= 'serialized';
		$config->params['rows'] = $rows;
	
		return $config->currentValue();
	}
}