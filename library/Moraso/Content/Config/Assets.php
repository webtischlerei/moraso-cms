<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Content_Config_Assets extends Aitsu_Content_Config_Abstract
{
	public function getTemplate()
	{
		return 'assets.phtml';
	}

	public static function set($index, $name, $label)
	{
		$instance = new self($index, $name);

		$instance->facts['tab'] = true;
		$instance->facts['label'] = $label;
		$instance->facts['type'] = 'serialized';

		$assets = \Moraso\Asset::get(null, null, null, null, true);

		$data = array();
		foreach ($assets as $key => $asset) {		
			if ($asset->active) {
				$data[$key]['id'] 			= $asset->id;
				$data[$key]['headline'] 	= $asset->headline;
				$data[$key]['subheadline'] 	= $asset->subheadline;
				$data[$key]['mediaid'] 		= Moraso_Db::simpleFetch('mediaid', '_assets_have_media', array('idasset' => $asset->id), 1, 0, array('id' => 'ASC'));

				if ($data[$key]['mediaid']) {
					$media 						= Moraso_Db::simpleFetch(array('filename', 'extension'), '_media', array('mediaid' => $data[$key]['mediaid']), 999);
						
					$data[$key]['extension'] 	= $media[0]['extension'];
					$data[$key]['filename'] 	= $media[0]['filename'];
				} else {
					$data[$key]['extension'] 	= '';
					$data[$key]['filename'] 	= '';
				}
			}
		}
		
		$instance->params['assets'] = $data;

		return $instance->currentValue();
	}
}