<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/User
 */

namespace Moraso;

use \Moraso\Model\User\Auth\Table 				as UserAuthTable;
use \Moraso\Database\Connection 				as DatabaseConnection;
use \Moraso\Service\Manager 					as ServiceManager;

use Zend\Authentication\Adapter\DbTable 		as AuthenticationAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session 		as SessionStorage;

class User
{
	private $auth, $userAuthTable;

	/**
	* TODO
	*/
	public static function init()
	{
		$user = new self;

		$serviceManager = ServiceManager::get();

		$serviceManager->setService('moraso-user', $user);

		$user_data = $user->id ? $user->get() : array();

		$user_data_service = $serviceManager->setService('moraso-user-data', $user_data);

		$user_data_service->setAllowOverride(true);
	}

	public function __construct()
	{
		$this->userAuthTable = new UserAuthTable();

		$adapter = new AuthenticationAdapter(DatabaseConnection::getAdapter());
		$adapter
			->setTableName($this->userAuthTable->getTable())
			->setIdentityColumn('username')
			->setCredentialColumn('password');

		$select = $adapter->getDbSelect();
		$select->where(array(
			'status' => 1,
			'(
				(access_until IS NULL AND access_from IS NULL)
				 OR 
				(CURDATE() >= access_from AND access_until IS NULL)
				 OR 
				(CURDATE() <= access_until AND access_from IS NULL)
				 OR 
				(CURDATE() BETWEEN access_from AND access_until)				
			)'
		));

		$this->auth = new AuthenticationService(null, $adapter);

		$this->auth->setStorage(new SessionStorage('morasoLogin'));
	}

	/**
	 * @return boolean
	 */
	public static function login($username, $password, $salt = null)
	{
		$context = !(isset($this) && get_class($this) == __CLASS__) ? new self() : $this;

		if (is_null($salt)) {
			$salt = \Moraso_Config::get('sys.salt');
		}

		$context->auth->getAdapter()
			->setIdentity($username)
			->setCredential(new \Zend\Db\Sql\Expression("SHA1(CONCAT('" . $salt . "', '" . $password . "', '" . $salt . "'))"));

		$result = $context->auth->authenticate();

		if ($result->isValid()) {
			$user = $context->auth->getAdapter()->getResultRowObject(array('user_id'));

			$storage = $context->auth->getStorage();
			$storage->write($user);

			$context->userAuthTable->update(array(
				'last_modified' => new \Zend\Db\Sql\Expression('last_modified'),
				'last_login' 	=> new \Zend\Db\Sql\Expression('NOW()')
			), array(
				'user_id =?' => $user->user_id
			));

			return true;
		}

		return false;
	}

	/**
	 * @return boolean
	 */
	public static function isLoggedIn()
	{
		$context = !(isset($this) && get_class($this) == __CLASS__) ? new self() : $this;

		return $context->auth->getIdentity();
	}

	/**
	 * @return void
	 */
	public static function logout()
	{
		$context = !(isset($this) && get_class($this) == __CLASS__) ? new self() : $this;

		$serviceManager = ServiceManager::get();

		$user = $serviceManager->get('moraso-user');
		$user = null;

		$context->auth->clearIdentity();
	}

	/**
	 * @return mixed
	 */
	public static function get($name = null)
	{
		$context = !(isset($this) && get_class($this) == __CLASS__) ? new self() : $this;

		$identity = $context->auth->getIdentity();

		if (isset($identity->user_id) && $identity->user_id) {
			$userMapper = new \Moraso\Model\User\Mapper();

			$user = $userMapper->find($identity->user_id);

			return is_null($name) ? $user : $user->$name;
		}

		return null;
	}

	/**
	 * @return mixed
	 */
	public function __get($name)
	{
		return $this->get($name);
	}
}