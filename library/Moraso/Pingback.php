<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Pingback extends Zend_XmlRpc_Server
{
    public function ping($sourceUrl, $targetUrl)
    {
        try {
            $targetUri = Zend_Uri::factory($targetUrl[0]);
            $sourceUri = Zend_Uri::factory($sourceUrl[0]);

            $targetUrlParams = Moraso_Rewrite_Standard::getInstance()->getParamsByUrl($targetUrl[0]);

            $sourceClient = new Zend_Http_Client($sourceUrl[0]);
            $sourceResponse = $sourceClient->request(Zend_Http_Client::GET);

            if (!$sourceResponse->isSuccessful()) {
                return 0x0010;
            }
            
            $sourceBody = $sourceResponse->getBody();

            if ($targetUri->getHost() != $sourceUri->getHost() && (strpos($sourceBody, $targetUrl)) === false) {
                return 0x0011;
            }  elseif ($targetUri->getHost() == $sourceUri->getHost() && (strpos($sourceBody, $targetUri->getPath())) === false) {
                return 0x0011;
            }

            $targetClient = new Zend_Http_Client($targetUrl[0]);
            $targetResponse = $targetClient->request(Zend_Http_Client::GET);
            
            if (!$targetResponse->isSuccessful()) {
                return 0x0020;
            }

            $sourceTitles = array();
            preg_match_all('/<title>(.*?)<\\/title>/is', $sourceBody, $sourceTitles);

            $sourceMetaTagDescription = array();
            preg_match_all('/<meta name="description" content="(.*?)" \/>/is', $sourceBody, $sourceMetaTagDescription);

            if (!empty($sourceMetaTagDescription[1][0])) {
                $comment = $sourceMetaTagDescription[1][0];
            } else {
                $comment = 'Es wurde kein Description Eintrag gefunden';
            }  

            $properties = Aitsu_Persistence_ArticleProperty::factory(Moraso_Util::getIdartLang($targetUrlParams['idart']));

            $commentsInfo = (object) $properties->comments;
            $comments = Moraso_Comments::getComments($commentsInfo->node_id->value);

            foreach ($comments as $comment_lvl_2) {
                if (!empty($comment_lvl_2['comment']['pingback_url'])) {
                    if ($comment_lvl_2['comment']['pingback_url'] === $sourceUrl[0]) {
                        return 0x0030;
                    }
                }
            }

            Moraso_Comments::create($commentsInfo->node_id->value, array(
                'author' => $sourceTitles[1][0],
                'comment' => $comment,
                'pingback_url' => $sourceUrl[0]
            ), true, true);
        } catch (Exception $e) {
            trigger_error($e->getMessage());
            return false;
        }
        
        return true;
    }
}
