<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Plugins
{
	public static function installPrivileges($namespace, $plugin, $type, $subnamespace = null)
	{
		if (empty($subnamespace)) {
			$pluginInfo = simplexml_load_file(APPLICATION_LIBPATH . '/' . ucfirst($namespace) . '/Plugin/' . ucfirst($plugin) . '/' . ucfirst($type) . '/plugin.xml');
		} else {
			$pluginInfo = simplexml_load_file(APPLICATION_LIBPATH . '/' . ucfirst($namespace) . '/' . ucfirst($subnamespace) . '/Plugin/' . ucfirst($plugin) . '/' . ucfirst($type) . '/plugin.xml');
		}

		foreach ($pluginInfo->privileges->privilege as $privilege) {
			$privilegeid = Moraso_Db::fetchOne('' .
				'SELECT ' .
				'   privilegeid ' .
				'FROM ' .
				'   _acl_privilege ' .
				'WHERE ' .
				'   identifier =:privilege', array(
					':privilege' => (string) $privilege->identifier
					));

			if (empty($privilegeid)) {
				$privilegeid = Moraso_Db::put('_acl_privilege', 'privilegeid', array(
					'identifier' => (string) $privilege->identifier
					));

				Moraso_Db::put('_acl_privileges', null, array(
					'roleid' => 18,
					'privilegeid' => $privilegeid
					));
			}
		}
	}

	public static function installDatabase($namespace, $plugin, $type)
	{ 
	}

	public static function getNamespaces()
	{
		$plugins 	= Aitsu_Util_Dir::scan(APPLICATION_LIBPATH, 'plugin.xml');
		$baseLength = strlen(APPLICATION_LIBPATH);
		$namespaces = array();

		foreach ($plugins as $plugin) {
			$pluginPathInfo = explode('/', substr($plugin, $baseLength + 1));

			if ($pluginPathInfo[2] === 'Plugin' && (isset($pluginPathInfo[5]) && $pluginPathInfo[5] === 'plugin.xml')) {
				$namespaces[] = array(
					'namespace' 	=> $pluginPathInfo[0],
					'subnamespace' 	=> $pluginPathInfo[1]
				);
			} else {
				$namespaces[] = $pluginPathInfo[0];
			}
		}

		$return = array();
		foreach ($namespaces as $namespace) {            
			if (!in_array($namespace, $return)) {
				$return[] = $namespace;
			}
		}

		return $return;
	}

	public static function getAllPlugins($area = 'article', $idart = 0)
	{
		$pluginCollection = array();

		$namespaces = self::getNamespaces();

		foreach ($namespaces as $namespace) {
			if (!is_array($namespace)) {
				$pluginDir = APPLICATION_LIBPATH . '/' . $namespace . '/Plugin';
			} else {
				$pluginDir = APPLICATION_LIBPATH . '/' . $namespace['namespace'] . '/' . $namespace['subnamespace'] . '/Plugin';
			}

			$plugins = Aitsu_Util_Dir::scan($pluginDir, 'Class.php');
			$baseLength = strlen($pluginDir);

			foreach ($plugins as $plugin) {
				$pluginPathInfo = explode('/', substr($plugin, $baseLength + 1));
				
				$pluginName = $pluginPathInfo[0];

				if (strtolower($pluginPathInfo[1]) === $area) {
					if (\Moraso\Acl::isAllowed('plugin.' . strtolower($pluginName) . '.' . $area)) {
						include_once ($plugin);

						if ($area === 'article') {
							$registry = call_user_func(array(
								$namespace . '_Plugin_' . ucfirst($pluginName) . '_' . ucfirst($area) . '_Controller',
								'register'
								), $idart);
						} elseif ($area === 'dashboard') {
							$registry = call_user_func(array(
								$namespace . '_Plugin_' . ucfirst($pluginName) . '_' . ucfirst($area) . '_Controller',
								'register'
								));
						}
						
						if ($registry->enabled) {
							if (!is_array($namespace)) {
								$pluginCollection[] = (object) array(
									'namespace' => $namespace,
									'name' => $pluginName,
									'position' => !empty($registry->position) ? $registry->position : 0
								);
							} else {
								$pluginCollection[] = (object) array(
									'namespace' => $namespace['namespace'],
									'subnamespace' => $namespace['subnamespace'],
									'name' => $pluginName,
									'position' => !empty($registry->position) ? $registry->position : 0
								);
							}
						}
					}
				}
			}
		}

		return $pluginCollection;
	}
}