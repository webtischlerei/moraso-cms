<?php

/**
 * @author Christian Kehres <christian.kehres@gmai.com>
 * @copyright 2013 - 2014 Christian Kehres
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Module
 */
class Moraso_Module_Cart_Cart_Class extends Moraso_Module_Abstract
{
	protected function _init()
	{
		Aitsu_Registry::setExpireTime(0);
	}

	protected function _main()
	{
		$nf 				= new NumberFormatter($this->_defaults['numberFormatter'], NumberFormatter::CURRENCY);

		$cart 				= Moraso_Cart::getInstance();

		$cartArticles 		= $cart->getArticles();

		$amount_total 		= 0;
		$amount_total_tax 	= array();
		$tax_total 			= 0;

		$articles = array();
		foreach ($cartArticles as $idart => $qty) {
			$articleInfo 			= Aitsu_Persistence_Article::factory($idart)->load();
			$idartlang 				= Moraso_Util::getIdArtLang($idart);
			$articleProperties 		= Aitsu_Persistence_ArticleProperty::factory($idartlang)->load();
			$articlePropertyCart 	= (object) $articleProperties->cart;
			
			// Standardpreis
			$price 					= $articlePropertyCart->price->value;
			
			// schauen ob es auch Staffelpreise gibt
			$staffelpreise = $cart->getStaffelpreise($idartlang);
			
			foreach ($staffelpreise as $staffel => $staffelpreis) {
				if ($qty >= $staffel) {
					$price = $staffelpreis;
				}
			}

			$price_total 			= bcmul($price, $qty, 2);
			$sku 					= $articlePropertyCart->sku->value;
			$tax_class 				= $articlePropertyCart->tax_class->value;

			$articles[$idart] 		= (object) array(
				'qty' 			=> $qty,
				'pagetitle' 	=> $articleInfo->pagetitle,
				'teasertitle'   => $articleInfo->teasertitle,
				'summary' 		=> $articleInfo->summary,
				'price' 		=> $nf->formatCurrency($price, $this->_defaults['currency']),
				'sku' 			=> $sku,
				'tax_class' 	=> $tax_class,
				'mainimage' 	=> \Moraso\Html\Helper\Image::getPath($idart, $articleInfo->mainimage, 100, 100, 2),
				'price_total' 	=> $nf->formatCurrency($price_total, $this->_defaults['currency'])
			);

			$amount_total = bcadd($amount_total, $price_total, 2);

			if (isset($amount_total_tax[$tax_class])) {
				$amount_total_tax[$tax_class] = $amount_total_tax[$tax_class] + ($price_total - ($price_total / ((100 + $tax_class) / 100)));
			} else {
				$amount_total_tax[$tax_class] = $price_total - ($price_total / ((100 + $tax_class) / 100));
			}
		}

		foreach ($amount_total_tax as $tax_class => $tax_value) {
			$amount_total_tax[$tax_class] 	= $nf->formatCurrency($tax_value, $this->_defaults['currency']);
			$tax_total 						= $tax_total + $tax_value;
		}

		$this->_view->articles 					= (object) $articles;
		$this->_view->amount_total 				= $nf->formatCurrency($amount_total, $this->_defaults['currency']);
		$this->_view->amount_total_tax 			= $amount_total_tax;
		$this->_view->amount_total_without_tax 	= $nf->formatCurrency($amount_total - $tax_total, $this->_defaults['currency']);
	}
}