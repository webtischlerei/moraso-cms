<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Module\Cart
 */
class Moraso_Module_Cart_Product_Add_Class extends Moraso_Module_Abstract
{
	protected function _main()
	{
		$this->_view->idart = Aitsu_Registry::get()->env->idart;
	}
}