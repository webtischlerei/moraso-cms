<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Module\Cart
 */
class Moraso_Module_Cart_Product_Info_Class extends Moraso_Module_Abstract
{
	protected function _main()
	{
		$nf 		= new NumberFormatter($this->_defaults['numberFormatter'], NumberFormatter::CURRENCY);

		$article 	= Aitsu_Persistence_ArticleProperty::factory($this->_defaults['idartlang'])->load();

		$cart 		= (object) $article->cart;

		$price 		= $cart->price->value;
		$tax_class 	= $cart->tax_class->value;
		$tax 		= ($price / (100 + $tax_class)) * $tax_class;
		$price_net 	= $price - $tax;

		$this->_view->cart = (object) array(
			'sku' 			=> $cart->sku->value,
			'price' 		=> $nf->formatCurrency($price, $this->_defaults['currency']),
			'tax_class' 	=> $cart->tax_class->value,
			'tax' 			=> $nf->formatCurrency($tax, $this->_defaults['currency']),
			'price_net' 	=> $nf->formatCurrency($price_net, $this->_defaults['currency']),
			'staffelpreise' => $this->getStaffelPreise($nf)
		);
	}

	private function getStaffelPreise($nf)
	{
		$idartlang = Aitsu_Registry::get()->env->idartlang;

		$staffelpreise = Moraso_Db::fetchAll("SELECT * FROM `_aitsu_property` WHERE `identifier` LIKE 'cart:price_staffel_%';");

		$returnStaffelpreise = array();

		foreach ($staffelpreise as $staffelpreis) {
			$price = Moraso_Db_Simple::fetch('floatvalue', '_aitsu_article_property', array(
					'propertyid' 	=> $staffelpreis['propertyid'],
					'idartlang' 	=> $idartlang
				));

			if ($price) {
				$returnStaffelpreise[] = array(
					'price' 	=> $nf->formatCurrency($price, $this->_defaults['currency']),
					'quantity' 	=> str_replace('cart:price_staffel_', '', $staffelpreis['identifier'])
				);
			}
		}

		usort($returnStaffelpreise, function($a, $b) {
    		return $a['quantity'] - $b['quantity'];
		});

		return $returnStaffelpreise;
	}
}