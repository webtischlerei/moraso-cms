<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */ 

namespace Moraso\Module\Cart\Checkout\Overview;

use Moraso\Module\AbstractModule;

class Overview extends AbstractModule
{
	protected function init()
	{
		$nf = new \NumberFormatter($this->moduleConfig->parameters['numberFormatter'], \NumberFormatter::CURRENCY);

		/* get Data */
		$cart 				= \Moraso_Cart::getInstance();
		$properties 		= $cart->getProperties();
		$cartArticles 		= $cart->getArticles();
		$amount_total 		= 0;
		$amount_total_tax 	= array();
		$tax_total 			= 0;
		$articles 			= array();

		foreach ($cartArticles as $idart => $qty) {
			$articleInfo 			= \Aitsu_Persistence_Article::factory($idart)->load();

			$idartlang 				= \Moraso_Util::getIdArtLang($idart);

			$articleProperties 		= \Aitsu_Persistence_ArticleProperty::factory($idartlang)->load();

			$articlePropertyCart 	= (object) $articleProperties->cart;

			// Standardpreis
			$price = $articlePropertyCart->price->value;
			
			// schauen ob es auch Staffelpreise gibt
			$staffelpreise = $cart->getStaffelpreise($idartlang);
			
			foreach ($staffelpreise as $staffel => $staffelpreis) {
				if ($qty >= $staffel) {
					$price = $staffelpreis;
				}
			}

			$price_total 			= bcmul($price, $qty, 2);
			$sku 					= $articlePropertyCart->sku->value;

			$articles[$idart] = (object) array(
				'qty' 			=> $qty,
				'pagetitle' 	=> $articleInfo->pagetitle,
				'price' 		=> $nf->formatCurrency($price, $this->moduleConfig->parameters['currency']),
				'sku' 			=> $sku,
				'price_total' 	=> $nf->formatCurrency($price_total, $this->moduleConfig->parameters['currency'])
			);

			$amount_total 	= bcadd($amount_total, $price_total, 2);

			$tax_class 		= (int) $articlePropertyCart->tax_class->value;

			if (isset($amount_total_tax[$tax_class])) {
				$amount_total_tax[$tax_class] = $amount_total_tax[$tax_class] + ($price_total - ($price_total / ((100 + $tax_class) / 100)));
			} else {
				$amount_total_tax[$tax_class] = $price_total - ($price_total / ((100 + $tax_class) / 100));
			}
		}

		$cart->createOrder();

		$paymentStrategy 	= \Moraso_Cart::getPaymentStrategy(null, $properties['payment']['method']);

		$payment 			= new \Moraso_Cart_Payment($paymentStrategy);

		$checkoutUrl 		= $payment->getCheckoutUrl();
		$hiddenFields 		= $payment->getHiddenFormFields();

		foreach ($amount_total_tax as $tax_class => $tax_value) {
			$tax_total = $tax_total + $tax_value;
		}

		$amount_total_without_tax 	= $amount_total - $tax_total;

		$shippingCosts 				= $cart->getShippingCosts($amount_total_without_tax);

		$amount_total 				= $amount_total + $shippingCosts;

		$shippingCosts_taxPercent 	= \Moraso_Config::get('shop.shipping.tax_percent');

		if (empty($shippingCosts_taxPercent)) {
			$shippingCosts_taxPercent = 19;
		}

		$shippingCosts_tax 			= ($shippingCosts / (100+$shippingCosts_taxPercent)) * $shippingCosts_taxPercent;

		$amount_total_tax[$shippingCosts_taxPercent] = isset($amount_total_tax[$shippingCosts_taxPercent]) ? $amount_total_tax[$shippingCosts_taxPercent] + $shippingCosts_tax : $shippingCosts_tax;

		$tax_total = 0;
		foreach ($amount_total_tax as $tax_class => $tax_value) {
			$amount_total_tax[$tax_class] 	= $nf->formatCurrency($tax_value, $this->moduleConfig->parameters['currency']);
			$tax_total 						= $tax_total + $tax_value;
		}

		$amount_total_without_tax 					= $amount_total - $tax_total;

		$this->renderer->checkoutUrl 				= $checkoutUrl;
		$this->renderer->hiddenFields 				= $hiddenFields;
		$this->renderer->properties 				= $properties;
		$this->renderer->articles 					= $articles;
		$this->renderer->amount_total 				= $nf->formatCurrency($amount_total, $this->moduleConfig->parameters['currency']);
		$this->renderer->amount_total_tax 			= $amount_total_tax;
		$this->renderer->amount_total_without_tax 	= $nf->formatCurrency($amount_total_without_tax, $this->moduleConfig->parameters['currency']);
		$this->renderer->shippingCosts 				= $nf->formatCurrency($shippingCosts, $this->moduleConfig->parameters['currency']);

		\Aitsu_Util_Javascript::addReference('/skin/js/vendor/moraso.cart.js');
	}
}