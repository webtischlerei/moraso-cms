<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */ 

namespace Moraso\Module\Cart\Checkout\Delivery;

use Moraso\Module\AbstractModule;

class Delivery extends AbstractModule
{
	protected function main()
	{
		$cart = \Moraso_Cart::getInstance();

		$data = $cart->getProperty('delivery');
		
		$this->renderer->data = $data;
	}
}