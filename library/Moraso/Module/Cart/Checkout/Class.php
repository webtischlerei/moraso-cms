<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Module\Cart
 */
class Moraso_Module_Cart_Checkout_Class extends Moraso_Module_Abstract
{
	protected $_allowEdit = false;

	protected function _init()
	{
		Aitsu_Registry::setExpireTime(0);
	}

	protected function _main()
	{
		$cart = Moraso_Cart::getInstance();

		$articles = $cart->getArticles();

		$this->_view->articles = (object) $articles;
	}
}