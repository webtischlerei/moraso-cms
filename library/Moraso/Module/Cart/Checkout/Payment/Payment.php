<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */ 

namespace Moraso\Module\Cart\Checkout\Payment;

use Moraso\Module\AbstractModule;

class Payment extends AbstractModule
{
	protected function main()
	{
		$cart = \Moraso_Cart::getInstance();

		$data = $cart->getProperty('payment');
		
		$this->renderer->data = $data;
	}
}