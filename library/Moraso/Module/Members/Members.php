<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */ 

namespace Moraso\Module\Members;

use Moraso\Module\AbstractModule;
use Moraso\Eav;

class Members extends AbstractModule
{
	protected function main()
	{
		$members = Eav::get('plugin_generic_management_members');

		usort($members, array($this, 'compare'));

		$this->renderer->members = $members;
	}

	private function compare($a, $b)
	{
		return $this->_cmp(0, $a, $b);
	}

	private function _cmp($run, $a, $b)
	{
		$sorting = $this->moduleConfig->parameters['sorting'];

		if ($a[$sorting[$run]['key']] == $b[$sorting[$run]['key']] || ($a[$sorting[$run]['key']] == $b[$sorting[$run]['key']] && isset($sorting[$run+1]['key']))) {
			return $this->_cmp($run+1, $a, $b);
		} else {
			return $sorting[$run]['order'] === 'asc' ? strcmp($a[$sorting[$run]['key']], $b[$sorting[$run]['key']]) : strcmp($b[$sorting[$run]['key']], $a[$sorting[$run]['key']]);
		}
	}
}