<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */ 

namespace Moraso\Module\Account\LastLogin;

use Moraso\Module\AbstractModule;

use Moraso\User;
use Moraso\Model\User\Auth\Mapper as UserAuthMapper;

class LastLogin extends AbstractModule
{
	protected function main()
	{
		$user 			= new User();
		$userAuthMapper = new UserAuthMapper();

		$userAuth 		= $userAuthMapper->find($user->id);

		$this->renderer->last_login = date('d.m.Y H:i:s', strtotime($userAuth->last_login));
	}
}