<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */ 

namespace Moraso\Module\Account\Form\Change\PersonalData;

use Moraso\Module\AbstractModule;
use Moraso\User;
use Moraso\Eav;

class PersonalData extends AbstractModule
{
	protected function main()
	{
		$user = new User();
		
		$this->renderer->salutation = $user->salutation;
		$this->renderer->firstname 	= $user->firstname;
		$this->renderer->lastname 	= $user->lastname;
		$this->renderer->email 		= $user->email;

		if ($user->id_eav_entity) {
			$eav_data = Eav::get('usermanagement_plugin_data', $user->id_eav_entity);

			foreach ($eav_data as $key => $value) {
				$this->renderer->$key = $value;
			}
		}
	}
}