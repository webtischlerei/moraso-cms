<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Module_Pingback_Class extends Moraso_Module_Abstract
{
    protected $_renderOnlyAllowed = true;
    protected $_withoutView = true;

    protected function _main()
    {
    	if (Moraso_Config::get('pingback.enabled')) {
	        $server = new Zend_XmlRpc_Server();
	        $server->setClass('Moraso_Pingback', 'pingback');

	        echo $server->handle();
	    }
    }
}