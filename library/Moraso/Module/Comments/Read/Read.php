<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */ 

namespace Moraso\Module\Comments\Read;

use Moraso\Module\AbstractModule;

class Read extends AbstractModule
{
	protected function main()
	{
		$parentNodeId 	= $this->getParentNodeId($this->moduleConfig->parameters['idartlang']);

		$comments 		= \Moraso_Comments::getComments($parentNodeId, false, $this->moduleConfig->parameters['startLevel'], $this->moduleConfig->parameters['maxLevel']);

		$this->renderer->comments 		= $comments;
		$this->renderer->canDelete 		= \Moraso\Acl::isAllowed('delete.comment');
	}

	protected function getParentNodeId($idartlang)
	{
		$properties 	= \Aitsu_Persistence_ArticleProperty::factory($idartlang);

		$commentsInfo 	= (object) $properties->comments;

		if (!isset($commentsInfo->node_id->value) || empty($commentsInfo->node_id->value)) {
			return $this->createArticleMainNode($idartlang);
		} else {
			return $commentsInfo->node_id->value;
		}
	}

	protected function createArticleMainNode($idartlang)
	{
		$properties 				= \Aitsu_Persistence_ArticleProperty::factory($idartlang);
		$nodesCommentsMainNodeId 	= \Moraso_Config::get('nodes.comments.mainNodeId');

		if (empty($nodesCommentsMainNodeId)) {
			$nodesCommentsMainNodeId = $this->createMainNode();
		}

		$articleMainNodeId 			= \Moraso_Nodes::insert($nodesCommentsMainNodeId, true, true);

		$properties->setValue('comments', 'node_id', $articleMainNodeId);
		$properties->save();

		return $articleMainNodeId;
	}

	protected function createMainNode()
	{
		$nodesCommentsMainNodeId = \Moraso_Nodes::insert(null, true, true);

		$data = array(
			'config' 		=> 'default',
			'env' 			=> 'default',
			'identifier' 	=> 'nodes.comments.mainNodeId',
			'value' 		=> $nodesCommentsMainNodeId
		);

		\Moraso_Db::put('_moraso_config', 'id', $data);

		return $nodesCommentsMainNodeId;
	}
}