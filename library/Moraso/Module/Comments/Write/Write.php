<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */ 

namespace Moraso\Module\Comments\Write;

use Moraso\Module\AbstractModule;

class Write extends AbstractModule
{
	protected function main()
	{
		if ($this->moduleConfig->configurable['spam_protect_time']) {
			$spam_protect_time = \Aitsu_Content_Config_Text::set($this->index, 'spam_protect_time', 'Spamschutz in Sekunden', 'Konfiguration');
		}

		if (!isset($this->moduleConfig->parameters['parent_node_id']) || empty($this->moduleConfig->parameters['parent_node_id'])) {
			$this->moduleConfig->parameters['parent_node_id'] = $this->getParentNodeId($this->moduleConfig->parameters['idartlang']);
		}

		$this->renderer->spam_protect_time 	= !empty($spam_protect_time) ? $spam_protect_time : $this->moduleConfig->parameters['spam_protect_time'];
		$this->renderer->parent_node_id 	= $this->moduleConfig->parameters['parent_node_id'];
	}

	protected function getParentNodeId($idartlang)
	{
		$properties 	= \Aitsu_Persistence_ArticleProperty::factory($idartlang);

		$commentsInfo 	= (object) $properties->comments;

		if (!isset($commentsInfo->node_id->value) || empty($commentsInfo->node_id->value)) {
			return $this->createArticleMainNode($idartlang);
		} else {
			return $commentsInfo->node_id->value;
		}
	}

	protected function createArticleMainNode($idartlang)
	{
		$properties 				= \Aitsu_Persistence_ArticleProperty::factory($idartlang);
		$nodesCommentsMainNodeId 	= \Moraso_Config::get('nodes.comments.mainNodeId');

		if (empty($nodesCommentsMainNodeId)) {
			$nodesCommentsMainNodeId = $this->createMainNode();
		}

		$articleMainNodeId 			= \Moraso_Nodes::insert($nodesCommentsMainNodeId, true, true);

		$properties->setValue('comments', 'node_id', $articleMainNodeId);
		$properties->save();

		return $articleMainNodeId;
	}

	protected function createMainNode()
	{
		$nodesCommentsMainNodeId = \Moraso_Nodes::insert(null, true, true);

		$data = array(
			'config' 		=> 'default',
			'env' 			=> 'default',
			'identifier' 	=> 'nodes.comments.mainNodeId',
			'value' 		=> $nodesCommentsMainNodeId
		);

		\Moraso_Db::put('_moraso_config', 'id', $data);

		return $nodesCommentsMainNodeId;
	}
}