<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
 
class Moraso_Module_Text_Class extends Moraso_Module_Abstract
{
	protected $_withoutView = true;

    protected function _main()
    {
        $text = htmlentities(Aitsu_Content_Text::get($this->_index, 0), ENT_COMPAT, 'UTF-8');

        $return = (empty($text) && Aitsu_Registry::isEdit()) ? Aitsu_LoremIpsum::get(5) : $text;

        if (isset($this->_defaults['tag']) && !empty($this->_defaults['tag'])) {
			$return = '<' . $this->_defaults['tag'] . '>' . $return . '</' . $this->_defaults['tag'] . '>';
		}

		return $return;
    }
}