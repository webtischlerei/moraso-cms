<?php

/**
 * @author Christian Kehres <christian.kehres@gmail.com>
 * @copyright (c) 2014, Christian Kehres <http://www.christian-kehres.de>
 */
namespace Moraso\Module\Guestbook;

use Moraso\Module\AbstractModule;

/**
 * Class Guestbook
 * @package Moraso\Module\Guestbook
 */
class Guestbook extends AbstractModule
{
    /**
     *
     */
    protected function main()
    {
        if (isset($_POST['create_guestbook_entry'])) {
            $success = self::createEntry($_POST, $this->_params->autoActive);

            if ($success) {
                $redirect = \Moraso_Rewrite_Standard::getInstance()->rewriteOutput(
                    '{ref:idart-' . $this->moduleConfig->parameters['redirect_idart'] . '}'
                );

                header("Location: " . $redirect . "");
            } else {
                $this->renderer->formData = (object)$_POST;
            }
        }

        $this->renderer->entries = self::getEntries();
    }

    /**
     * @param $data
     * @param bool $autoActive
     * @return bool
     */
    private static function createEntry($data, $autoActive = false)
    {
        \Moraso_Db::startTransaction();

        try {
            $data['idclient'] = \Aitsu_Registry::get()->env->idclient;
            $data['created'] = date('Y-m-d H:i:s');

            if ($autoActive) {
                $data['active'] = 1;
            }

            \Moraso_Guestbook::setEntry($data);
        } catch (Exception $e) {
            trigger_error('Gästebuch Eintrag konnte nicht gespeichert werden! ' . $e->getMessage());
            \Moraso_Db::rollback();
            return false;
        }

        \Moraso_Db::commit();

        return true;
    }

    /**
     * @param int $limit
     * @return mixed
     */
    private static function getEntries($limit = 1000)
    {
        $entries = \Moraso_Guestbook::getActiveEntries($limit);

        return $entries;
    }
}
