<?php
/**
 * @author Christian Kehres <christian.kehres@webtischlerei.de>
 * @copyright 2014 Christian Kehres
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */ 

namespace Moraso\Module\Youtube;

use Moraso\Module\AbstractModule;

class Youtube extends AbstractModule
{
	protected function main()
	{
		// Breite
		if (isset($this->moduleConfig->configurable['width']) && $this->moduleConfig->configurable['width']) {
			if (!isset($this->moduleConfig->configurable['width']['selects'])) {
				$width 		= \Aitsu_Content_Config_Text::set($this->index, 'width', 'Breite', 'Videoausgabe');
			} else {
				$width_select 	= \Aitsu_Content_Config_Select::set($this->index, 'width', 'Breite', array_flip($this->moduleConfig->configurable['width']['selects']['names']), 'Videoausgabe');

				if ($width_select) {
					$width = $this->moduleConfig->configurable['width']['selects']['values'][$width_select];
				}
			}
		}

		$this->renderer->width = (int) (!empty($width) ? $width : $this->moduleConfig->parameters['width']);

        // Höhe
        if (isset($this->moduleConfig->configurable['height']) && $this->moduleConfig->configurable['height']) {
            if (!isset($this->moduleConfig->configurable['height']['selects'])) {
                $height 		= \Aitsu_Content_Config_Text::set($this->index, 'height', 'Höhe', 'Videoausgabe');
            } else {
                $height_select 	= \Aitsu_Content_Config_Select::set($this->index, 'height', 'Höhe', array_flip($this->moduleConfig->configurable['height']['selects']['names']), 'Videoausgabe');

                if ($height_select) {
                    $height = $this->moduleConfig->configurable['height']['selects']['values'][$height_select];
                }
            }
        }

        $this->renderer->height = (int) (!empty($height) ? $height : $this->moduleConfig->parameters['height']);

        // Video ID
        if (isset($this->moduleConfig->configurable['videoId']) && $this->moduleConfig->configurable['videoId']) {
            $videoId = \Aitsu_Content_Config_Text::set($this->index, 'videoId', 'Video ID', 'Videoausgabe');
        }

        $this->renderer->videoId = (string) (!empty($videoId) ? $videoId : $this->moduleConfig->parameters['videoId']);
	}
}