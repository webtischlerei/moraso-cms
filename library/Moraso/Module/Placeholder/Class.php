<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Module\Placeholder
 */
class Moraso_Module_Placeholder_Class extends Moraso_Module_Abstract
{
	protected function _main()
	{
		if ($this->_defaults['configurable']['placeholder']) {
			$placeholderSet = Aitsu_Db::fetchAll('SELECT id, identifier FROM _placeholder');

			$placeholderSelect = array();
			foreach ($placeholderSet as $key => $value) {
				$placeholderSelect[$value['identifier']] = $value['id'];
			}

			$placeholder_id = Aitsu_Content_Config_Select::set($this->_index, 'placeholder', 'Platzhalter', $placeholderSelect, $this->_translation['configuration']);

			$this->_view->placeholder = Aitsu_Placeholder::get(Aitsu_Placeholder::get((int) $placeholder_id));
		}

		if (empty($this->_view->placeholder)) {
			if (is_numeric($this->_index)) {
				$this->_view->placeholder = Aitsu_Placeholder::get(Aitsu_Placeholder::get((int) $this->_index));
			} else {
				$this->_view->placeholder = Aitsu_Placeholder::get($this->_index);
			}	
		}

		if (empty($this->_view->placeholder)) {
			$this->_withoutView = true;
			return '';
		}
	}
}