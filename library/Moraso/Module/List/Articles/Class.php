<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Module\List
 */
class Moraso_Module_List_Articles_Class extends Moraso_Module_Abstract
{
	protected function _main()
	{
		$this->_translation = array();
		$this->_translation['configuration'] = Aitsu_Translate::_('Configuration');

		if ($this->_defaults['configurable']['categories']) {
			$categories = Aitsu_Content_Config_Text::set($this->_index, 'categories', Aitsu_Translate::_('Categories'), 'Konfiguration');
		}

		$categories = !empty($categories) ? $categories : $this->_defaults['categories'];

		if ($this->_defaults['configurable']['useOfStartArticle']) {
			$useOfStartArticleSelect = array(
				'show all articles' => 1,
				'do not show start articles' => 2,
				'show only start articles' => 3
			);

			$useOfStartArticle = Aitsu_Content_Config_Select::set($this->_index, 'useOfStartArticle', Aitsu_Translate::_('useOfStartArticle'), $useOfStartArticleSelect, 'Konfiguration');
		}

		$useOfStartArticle = !empty($useOfStartArticle) ? (int) $useOfStartArticle : $this->_defaults['useOfStartArticle'];

		if ($this->_defaults['configurable']['sortCategoryFirst']) {
			$sortCategoryFirstSelect = array(
				'true' => true,
				'false' => false
			);

			$sortCategoryFirst = Aitsu_Content_Config_Select::set($this->_index, 'sortCategoryFirst', Aitsu_Translate::_('sortCategoryFirst'), $sortCategoryFirstSelect, 'Konfiguration');
		}

		$sortCategoryFirst = isset($sortCategoryFirst) && strlen($sortCategoryFirst) > 0 ? filter_var($sortCategoryFirst, FILTER_VALIDATE_BOOLEAN) : $this->_defaults['sortCategoryFirst'];

		if ($this->_defaults['configurable']['orderBy']) {
			$orderBySelect = array(
				'artsort' => 'artsort',
				'created' => 'created',
				'modified' => 'modified',
				'metadate' => 'metadate'
			);

			$orderBy = Aitsu_Content_Config_Select::set($this->_index, 'orderBy', Aitsu_Translate::_('orderBy'), $orderBySelect, 'Konfiguration');
		}

		$orderBy = !empty($orderBy) ? $orderBy : $this->_defaults['orderBy'];

		if ($this->_defaults['configurable']['ascending']) {
			$ascendingSelect = array(
				'true' => true,
				'false' => false
			);

			$ascending = Aitsu_Content_Config_Select::set($this->_index, 'ascending', Aitsu_Translate::_('ascending'), $ascendingSelect, 'Konfiguration');
		}

		$ascending = isset($ascending) && strlen($ascending) > 0 ? filter_var($ascending, FILTER_VALIDATE_BOOLEAN) : $this->_defaults['ascending'];

		if ($this->_defaults['configurable']['offset']) {
			$offset = Aitsu_Content_Config_Text::set($this->_index, 'offset', Aitsu_Translate::_('Offset'), 'Konfiguration');
		}

		$offset = !empty($offset) ? (int) $offset : $this->_defaults['offset'];

		if ($this->_defaults['configurable']['limit']) {
			$limit = Aitsu_Content_Config_Text::set($this->_index, 'limit', Aitsu_Translate::_('Limit'), 'Konfiguration');
		}

		$limit = !empty($limit) ? (int) $limit : $this->_defaults['limit'];

		if ($this->_defaults['configurable']['page']) {
			$page = Aitsu_Content_Config_Text::set($this->_index, 'page', Aitsu_Translate::_('Page'), 'Konfiguration');
		}

		$page = !empty($_GET['page']) ? (int) $_GET['page'] : (isset($page) ? (int) $page : $this->_defaults['page']);

		if ($this->_defaults['configurable']['templateRenderingWhenNoArticles']) {
			$templateRenderingWhenNoArticlesSelect = array(
				'true' => true,
				'false' => false
			);

			$templateRenderingWhenNoArticles = Aitsu_Content_Config_Select::set($this->_index, 'templateRenderingWhenNoArticles', Aitsu_Translate::_('templateRenderingWhenNoArticles'), $templateRenderingWhenNoArticlesSelect, 'Konfiguration');
		}

		$templateRenderingWhenNoArticles = isset($templateRenderingWhenNoArticles) && strlen($templateRenderingWhenNoArticles) > 0 ? filter_var($templateRenderingWhenNoArticles, FILTER_VALIDATE_BOOLEAN) : $this->_defaults['templateRenderingWhenNoArticles'];

		if ($this->_defaults['configurable']['sortListByGivenCategories']) {
			$sortListByGivenCategoriesSelect = array(
				'true' => true,
				'false' => false
			);

			$sortListByGivenCategories = Aitsu_Content_Config_Select::set($this->_index, 'sortListByGivenCategories', Aitsu_Translate::_('sortListByGivenCategories'), $sortListByGivenCategoriesSelect, 'Konfiguration');
		}

		$sortListByGivenCategories = isset($sortListByGivenCategories) && strlen($sortListByGivenCategories) > 0 ? filter_var($sortListByGivenCategories, FILTER_VALIDATE_BOOLEAN) : $this->_defaults['sortListByGivenCategories'];

		if ($page > 1) {
			$offset = ($page - 1) * $limit;
		}

		$aggregation = Moraso_Aggregation_Article::factory();
		$aggregation->useOfStartArticle($useOfStartArticle);
		$aggregation->whereInCategories(array($categories));

		if ($sortCategoryFirst) {
			if ($sortListByGivenCategories) {
				$aggregation->orderBy('FIND_IN_SET(catlang.idcat, "' . $categories . '")');
			} else {
				$aggregation->orderBy('catlang.idcat');
			}
		}

		$aggregation->orderBy($orderBy, $ascending);

		if (isset($this->_params->populateWith)) {
			foreach ($this->_params->populateWith as $alias => $populateWith) {

				$type = $populateWith->index;

				if (isset($populateWith->type)) {
					if ($populateWith->type == 'property' || $populateWith->type == 'files') {
						$type = $populateWith->type . ':' . $type;
					}
				}

				if (isset($populateWith->datatype) && !empty($populateWith->datatype)) {
					$aggregation->populateWith($type, $alias, $populateWith->datatype);
				} else {
					$aggregation->populateWith($type, $alias);
				}
			}
		}

		$aggregationAll = clone $aggregation;

		$articles = $aggregation->fetch($offset, $limit);

		if (count($articles) === 0 && !$templateRenderingWhenNoArticles) {
			$this->_withoutView = true;
			return '';
		}

		$articlesAll = $aggregationAll->fetch(0, 99999);

		$this->_view->articles = $articles;
		$this->_view->pages = ceil(count($articlesAll) / $limit);
		$this->_view->currentPage = $page;
	}
}