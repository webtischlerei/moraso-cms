<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Assets
 */
class Moraso_Module_List_Assets_Class extends Moraso_Module_Abstract
{
	protected function _main()
	{
		$currentLanguage 		= \Moraso\Registry::get('currentLanguage');

		$assetHasMediaMapper 	= new \Moraso\Model\Asset\Has\Media\Mapper();
		$mediaMapper 			= new \Moraso\Model\Media\Mapper();
		$mediaDescriptionMapper = new \Moraso\Model\Media\Description\Mapper();

		if ($this->_defaults['all']) {
			$selectedAssets = \Moraso\Asset::getList(null, $this->_defaults['orderBy'], $this->_defaults['orderType']);
		} else {
			$selectedAssets = Moraso_Content_Config_Assets::set($this->_index, 'assets', 'Assets');
		}

		$assets = array();
		if (is_array($selectedAssets) && !empty($selectedAssets)) {
			foreach ($selectedAssets as $key => $sourceAsset) {
				$id = is_array($sourceAsset) ? $sourceAsset['id'] : $sourceAsset;

				$asset = \Moraso\Asset::get($id);

				if ($asset->active) {
					$assets[$key] 			= $asset;
					$assetHasMediaObjects 	= $assetHasMediaMapper->find($asset->id, 'idasset', true);

					$data = array();
					foreach ($assetHasMediaObjects as $assetHasMediaObject) {
						$mediaObject 			= $mediaMapper->find($assetHasMediaObject->mediaid, 'mediaid');
						$mediaDescription 		= $mediaDescriptionMapper->find(array(
							$assetHasMediaObject->mediaid,
							$currentLanguage
						), array(
							'mediaid',
							'idlang'
						));

						$data[] = array(
							'idart' 	=> $mediaObject->idart,
							'filename' 	=> $mediaObject->filename,
							'name' 		=> $mediaDescription->name
						);
					}

					if (!empty($data)) {
						$assets[$key]->media = $data;
					}
				}
			}
		}

		if (empty($assets)) {
			$this->_withoutView = true;
			return '';
		}

		$this->_view->assets = $assets;
	}
}