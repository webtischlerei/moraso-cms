<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Module\List
 */
class Moraso_Module_List_Download_Class extends Moraso_Module_Abstract
{
	public function _main()
	{
		$files = Moraso_Content_Config_Media::set($this->_index, 'DownloadList', 'Files', $this->_defaults['idart']);

		$this->_view->files = Moraso_Persistence_View_Media::byFileName($this->_defaults['idart'], $files);
	}
}