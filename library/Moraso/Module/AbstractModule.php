<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */

namespace Moraso\Module;

use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver;
use Zend\Config\Reader\Json as JsonReader;
use Zend\Config\Reader\Ini 	as IniReader;
use Zend\Stdlib\Parameters;

use Moraso\MorasoCache 		as Cache;
use Moraso\Acl;
use Moraso\Service\Manager 	as ServiceManager;
use Moraso\Content\Config 	as ContentConfig;
use Moraso\Util\Date 		as MorasoDate;

abstract class AbstractModule
{
	protected 	$output 					= '',
				$index 						= '',
				$moduleConfig,
				$cacheKey 					= '',
				$renderer,
				$resolver,
				$modulePath 				= '',
				$tags 						= array('Module'),
				$acl,
				$aclResource,
				$withoutView 				= false,
				$returnValue 				= '';

	/**
	* TODO
	*/
	final public function __construct($index = null, $parameters = null)
	{
		// 
		$serviceManager = ServiceManager::get();

		// 
		$this->moduleConfig 	= new Parameters();
		$this->modulePath 		= implode('/', array_slice(explode('\\', get_class($this)), 1, -1));
		$this->index 			= $index;

		//
		$this->acl 				= $serviceManager->get('moraso-acl');
		$this->aclResource 		= 'module.' . strtolower(implode('.', array_slice(explode('\\', get_class($this)), 2, -1)));

		// 
		$this->renderer 		= new PhpRenderer();
		$this->resolver 		= new Resolver\AggregateResolver();
		$this->renderer->setResolver($this->resolver);

		//
		if (!Acl::isAllowed($this->aclResource)) {
			$this->setOutput('');
			return;
		}

		// 
		$this->setTemplatePaths();
		
		// 
		$this->setModuleConfigs($parameters);

		// 
		$this->init();

		// 
		$this->setConfigurations();
 
		// 
		$this->createCacheKey();

		// 
		if (($output = Cache::getItem($this->cacheKey)) !== false) {
			$this->setOutput($output);
			return;
		}

		// 
		$this->main();

		// 
		foreach ($this->moduleConfig->parameters as $key => $value) {
			$this->renderer->$key = $value;
		}

        //
        $this->renderer->index = $this->index;

		// 
		if (!$this->withoutView) {
			// 
			if ($this->resolver->resolve($this->moduleConfig->parameters['template'])) {
				// 
				$output = $this->renderer->render($this->moduleConfig->parameters['template']);
			
				// 
				$this->tags[] = 'idart_' . \Aitsu_Registry::get()->env->idart;
				$this->tags[] = 'idcat_' . \Aitsu_Registry::get()->env->idcat;
				$this->tags[] = 'idlang_' . \Aitsu_Registry::get()->env->idlang;

				// 
				Cache::setItem($this->cacheKey, $this->output, $this->moduleConfig->parameters['lifetime'], $this->tags);
			} else {
				// 
				trigger_error('Das Modul "' . implode('/', array_slice(explode('\\', get_class($this)), 2, -1)) . ':' . $this->index . '" kann das Template "' . $this->moduleConfig->parameters['template'] . '.phtml" nicht finden!');

				// 
				$output = '';
			}

			//
			$this->setOutput($output);
		} elseif (!empty($this->returnValue)) {
			//
			$this->setOutput($this->returnValue);
		}

		// 
		\Aitsu_Registry::setExpireTime($this->moduleConfig->parameters['expiretime']);

		// 
		$this->backendAction($parameters);
	}

	/**
	* TODO
	*/
	final public static function setAclResource()
	{
		$resource = 'module.' . strtolower(implode('.', array_slice(explode('\\', get_class($this)), 2, -1)));
	}

	/**
	* TODO
	*/
	final protected function backendAction($parameters)
	{
		if (\Aitsu_Application_Status::isEdit()) {
			$allowEdit 	= $this->moduleConfig->parameters['allowEdit'];
			$isBlock 	= $this->moduleConfig->parameters['isBlock'];

			if ($allowEdit && Acl::isAllowed($this->aclResource, 'edit')) {
				if (rtrim(preg_replace('(<!--.*?-->)', '', $this->getOutput())) == '') {
					if ($isBlock) {
						$this->setOutput('
							<div style="border:1px dashed #CCC; padding:2px 2px 2px 2px;">
								<div style="height:15px; background-color: #CCC; color: white; font-size: 11px; padding:2px 5px 0 5px;">
									<span style="font-weight:bold; float:left;">' . $this->index . '</span> <span style="float:right;">Module <span style="font-weight:bold;">' . $this->moduleConfig->name . '</span></span>
								</div>
							</div>
						');
					} else {
						$this->setOutput($this->moduleConfig->name . '::' . $this->index);
					}
				}

				if (!$isBlock) {
					$this->setOutput('<span style="border:1px dashed #CCC; padding:2px 2px 2px 2px;">', false, true);
					$this->setOutput('</span>', true);
				}

				$this->setOutput('<code class="aitsu_params" style="display:none;">' . $parameters . '</code>', false, true);
			}
		}
	}

	/**
	* TODO
	*/
	final protected function setConfigurations()
	{
		// Template
		if (!$this->withoutView) {
			if ($this->moduleConfig->configurable['template']) {
				$template = ContentConfig\Select::set($this->index, 'template', 'Template', $this->getAvailableTemplates(), 'Konfigurationen');
				
				if (!empty($template)) {
					$this->moduleConfig->parameters['template'] = $template;
				}
			}
		}

		// Artikel
		if ($this->moduleConfig->configurable['idart']) {
			$idart = \Aitsu_Content_Config_Link::set($this->index, 'idart', 'Artikel', 'Quelle');

			if (!empty($idart)) {
				$this->moduleConfig->parameters['idart'] 		= preg_replace('/[^0-9]/', '', $idart);
				$this->moduleConfig->parameters['idartlang'] 	= \Moraso_Util::getIdArtLang($this->moduleConfig->parameters['idart'], $this->moduleConfig->parameters['idlang']);
			}
		}

		// Kategorie
		if ($this->moduleConfig->configurable['idcat']) {
			$idcat = \Aitsu_Content_Config_Link::set($this->index, 'idcat', 'Kategorie', 'Quelle');

			if (!empty($idcat)) {
				$this->moduleConfig->parameters['idcat'] = preg_replace('/[^0-9]/', '', $idcat);
			}
		}

		// "custom Fields"
		if (is_numeric($this->moduleConfig->customFields)) {
			for ($i = 1; $i <= $this->moduleConfig->customFields; $i++) {
				$this->moduleConfig->parameters['customField_' . $i] = \Aitsu_Content_Config_Text::set($this->index, 'customField_' . $i, '#' . $i, 'individuelle Konfigurationen');
			}
		} elseif (is_array($this->moduleConfig->customFields)) {
			foreach ($this->moduleConfig->customFields as $customFieldName => $customFieldValues) {
				if (is_array($customFieldValues)) {
					$this->moduleConfig->parameters[\Moraso_Util_String::slugify($customFieldName)] = \Aitsu_Content_Config_Select::set($this->index, 'customField_' . \Moraso_Util_String::slugify($customFieldName), $customFieldName, $customFieldValues, 'individuelle Konfigurationen');
				} elseif ($customFieldValues == '##link##') {
					$this->moduleConfig->parameters[\Moraso_Util_String::slugify($customFieldName)] = \Aitsu_Content_Config_Link::set($this->index, \Moraso_Util_String::slugify($customFieldName), $customFieldName, 'individuelle Konfigurationen');
				} else {
					$this->moduleConfig->parameters[\Moraso_Util_String::slugify($customFieldName)] = \Aitsu_Content_Config_Text::set($this->index, 'customField_' . \Moraso_Util_String::slugify($customFieldName), $customFieldName, 'individuelle Konfigurationen');
				}
			}
		}
	}

	/**
	* TODO
	*/
	final protected function getAvailableTemplates()
	{
		$return 			= array();
		$resolverIterator 	= clone($this->resolver->getIterator());
		$templatePaths 		= $resolverIterator->extract()->getPaths();
		$templateFiles 		= array ();

		foreach ($templatePaths as $templatePath) {
			$files = glob($templatePath . '*.phtml');

			if (!empty($files)) {
				$templateFiles = array_merge($templateFiles, $files);
			}
		}

		foreach ($templateFiles as $file) {
			$content = file_get_contents($file);

			if (function_exists("mb_detect_encoding")) {
				$contentEncoding = mb_detect_encoding($content);

				if ($contentEncoding !== "UTF-8") {
					$content = mb_convert_encoding($content, "UTF-8", $contentEncoding);
				}
			}

			if (preg_match('/^<\\!\\-{2}\\s*(.*?)\\s*\\-{2}>/', $content, $match)) {
				$return[$match[1]] = substr(basename($file), 0, -6);
			}
		}

		return $return;
	}

	/**
	* TODO
	*/
	final protected function setModuleConfigs($parameters = null)
	{
		$file 		= $this->getModuleConfigsFromFile();
		$script 	= $this->getModuleConfigsFromScript($parameters);

		$parameters = is_array($script) ? array_replace_recursive($file, $script) : $file;

		$parameters['parameters'] = $this->transformParameters($parameters['parameters']);
		
		$this->moduleConfig->fromArray($parameters);
	}

	/**
	* TODO
	*/
	final protected function getModuleConfigsFromFile()
	{
		$jsonReader 	= new JsonReader();

		$modulePaths 	= array();
		$modulePaths[] 	= LIBRARY_PATH . '/Moraso/Module/';
		$modulePaths[] 	= LIBRARY_PATH . '/Moraso/' . $this->modulePath . '/';

		$additionalLibraries = \Moraso_Library_Tree::getJson();

		if (!empty($additionalLibraries)) {
			foreach ($additionalLibraries->libraries as $additionalLibrary) {
				$modulePaths[] = LIBRARY_PATH . '/' . $additionalLibrary . '/' . $this->modulePath . '/';
			}
		}

		$heredity = \Moraso_Skin_Heredity::build();

		foreach (array_reverse($heredity) as $skin) {
			$modulePaths[] = APPLICATION_PATH . "/skins/" . $skin . "/module/" . str_replace('Module/', '', str_replace('Module_', '', $this->modulePath)) . '/';
		}

		$parameters = array();
		foreach ($modulePaths as $key => $modulePath) {
			if (is_readable($modulePath . 'module.json')) {
				$module_config 	= $jsonReader->fromFile($modulePath . 'module.json');
				$parameters 	= array_replace_recursive($parameters, $module_config);
			}
		}

		return $parameters;
	}

	/**
	* TODO
	*/
	final protected function getModuleConfigsFromScript($parameters)
	{
		if (!is_null($parameters)) {
			$iniReader = new IniReader();

			$parameters = $iniReader->fromString($parameters);

			foreach ($parameters as $key => $value) {
				if ($key != 'configurable') {
					$parameters['parameters'][$key] = $value;
					unset($parameters[$key]);
				}
			}
		}

		return $parameters;
	}

	/**
	* TODO
	*/
	final protected function transformParameters($parameters)
	{
		foreach ($parameters as $key => $value) {
			if (is_array($value)) {
				$parameters[$key] = $this->transformParameters($value);
			} else {
				switch($value) {
					case '##this.article.idart##':
						$value = \Aitsu_Registry::get()->env->idart;
						break;
					case '##this.article.idlang##':
						$value = \Aitsu_Registry::get()->env->idlang;
						break;
					case '##this.article.idartlang##':
						$value = \Aitsu_Registry::get()->env->idartlang;
						break;
					case '##this.article.idcat##':
						$value = \Aitsu_Registry::get()->env->idcat;
						break;
					case '##secondsUntilEndOf.minute##':
						$value = MorasoDate::secondsUntilEndOfThis('minute');
						break;
					case '##secondsUntilEndOf.hour##':
						$value = MorasoDate::secondsUntilEndOfThis('hour');
						break;
					case '##secondsUntilEndOf.day##':
						$value = MorasoDate::secondsUntilEndOfThis('day');
						break;
					case '##secondsUntilEndOf.week##':
						$value = MorasoDate::secondsUntilEndOfThis('week');
						break;
					case '##secondsUntilEndOf.month##':
						$value = MorasoDate::secondsUntilEndOfThis('month');
						break;
					case '##secondsUntilEndOf.quarter##':
						$value = MorasoDate::secondsUntilEndOfThis('quarter');
						break;
					case '##secondsUntilEndOf.half-year##':
						$value = MorasoDate::secondsUntilEndOfThis('half-year');
						break;
					case '##secondsUntilEndOf.year##':
						$value = MorasoDate::secondsUntilEndOfThis('year');
						break;
					case '##moraso.config.cache.internal.lifetime##':
						$value = \Moraso_Config::get('cache.internal.lifetime');
						break;
					case '##moraso.config.cache.browser.expireTime##':
						$value = \Moraso_Config::get('cache.browser.expireTime');
						break;
					case '##true##':
						$value = true;
						break;
					case '##false##':
						$value = false;
						break;
				}

				$parameters[$key] = $value;	
			}
		}

		return $parameters;
	}

	/**
	* TODO
	*/
	final protected function createCacheKey()
	{
		$module_parts 	= explode('\\', get_class($this));
		$module_sliced 	= array_slice($module_parts, 1, -1);

		$this->cacheKey = implode('_', $module_sliced) . '_' . $this->index . '_1_' . sha1($this->moduleConfig->toString());
	}

	/**
	* TODO
	*/
	final public function setOutput($output, $append = false, $prepend = false)
	{
		if ($append) {
			$this->output = $this->output . $output;
		} elseif ($prepend) {
			$this->output = $output . $this->output;
		} else {
			$this->output = $output;
		}
	}

	/**
	* TODO
	*/
	final protected function setTemplatePaths()
	{
		$paths 	= array();
		$paths[] = LIBRARY_PATH . '/Moraso/' . $this->modulePath . '/Views/';

		$additionalLibraries = \Moraso_Library_Tree::getJson();

		if (!empty($additionalLibraries)) {
			foreach ($additionalLibraries->libraries as $additionalLibrary) {
				$paths[] = LIBRARY_PATH . '/' . $additionalLibrary . '/' . $this->modulePath . '/Views/';
			}
		}
		
		$heredity = \Moraso_Skin_Heredity::build();

		foreach (array_reverse($heredity) as $skin) {
			$paths[] = APPLICATION_PATH . "/skins/" . $skin . "/module/" . str_replace('Module/', '', str_replace('Module_', '', $this->modulePath)) . '/Views/';
		}

		$stack = new Resolver\TemplatePathStack();

		$stack->setPaths($paths);

		$this->resolver->attach($stack);
	}

	/**
	* TODO
	*/
	protected function init()
	{

	}

	/**
	* TODO
	*/
	protected function main()
	{

	}

	/**
	* TODO
	*/
	final public function getOutput()
	{
		return $this->output;
	}

	/**
	* TODO
	*/
	final public function getModuleConfig()
	{
		return $this->moduleConfig;
	}
}