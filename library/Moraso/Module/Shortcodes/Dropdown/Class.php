<?php

/**
 * @author Christian Kehres, <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Module_Shortcodes_Dropdown_Class extends Moraso_Module_Abstract
{
	protected $_renderOnlyAllowed 	= true;
	protected $_withoutView 		= true;

	protected function _main()
	{
		$allModules = Aitsu_Util_Dir::scan(ROOT_PATH, 'module.json');
		$modules 	= array();

		foreach ($allModules as $module) {
			$search 	= array('module.json', LIBRARY_PATH, APPLICATION_PATH);
			$replace 	= array('', '', '');

			$exploded 	= explode('/', substr(str_replace($search, $replace, $module), 1, -1));

			if ($exploded[0] === 'skins') {
				$module = implode('.', array_slice($exploded, 3, count($exploded)));
			} elseif ($exploded[1] !== 'Module') {
				$module = $exploded[1] . '.' . implode('.', array_slice($exploded, 3, count($exploded)));
			} else {
				$module = implode('.', array_slice($exploded, 2, count($exploded)));
			}

			if (!empty($module)) {
				if (!in_array($module, $modules)) {
					$modules[] 	= $module;
				}
			}			
		}

		$heredity 				= Moraso_Skin_Heredity::build();
		$additionalLibraries 	= Moraso_Library_Tree::getJson();

		$files = array();

		foreach ($modules as $module) {
			$moduleExploded = explode('.', $module);

			if (file_exists(realpath(APPLICATION_PATH . '/../library/') . '/Moraso/Module/' . implode('/', $moduleExploded) . '/module.json')) {
				$files[$module][] = LIBRARY_PATH . '/Moraso/Module/' . implode('/', $moduleExploded) . '/module.json';
			}	

			if (!empty($additionalLibraries)) {
				foreach ($additionalLibraries->libraries as $additionalLibrary) {
					if (file_exists(realpath(APPLICATION_PATH . '/../library/') . '/' . $additionalLibrary . '/Module/' . implode('/', $moduleExploded) . '/module.json')) {
						$files[$module][] = LIBRARY_PATH . '/' . $additionalLibrary . '/Module/' . implode('/', $moduleExploded) . '/module.json';
					}
				}
			}

			foreach ($heredity as $skin) {
				if (file_exists(APPLICATION_PATH . '/skins/' . $skin . '/module/' . implode('/', $moduleExploded) . '/module.json')) {
					$files[$module][] = APPLICATION_PATH . '/skins/' . $skin . '/module/' . implode('/', $moduleExploded) . '/module.json';
				}
			}
		}

		$jsonReader = new \Zend\Config\Reader\Json();

		$shortcodes = array();
		foreach ($files as $module => $files) {
			$parameters = array();
			foreach ($files as $path) {
				$parameters = array_replace_recursive($parameters, $jsonReader->fromFile($path));
			}

			if (isset($parameters['shortcode']) && $parameters['shortcode']) {
				$shortcodes[] = array(
					'label' 	=> $parameters['name'],
					'shortcode' => '[' . $module . ':INDEX]'
				);
			}
		}

		return json_encode($shortcodes);
	}
}