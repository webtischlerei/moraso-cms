<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Module_HTML_Content_Class extends Moraso_Module_Abstract
{
	protected $_withoutView = true;

    protected function _main()
    {
        $startTag = '';
        $endTag = '';

        if (Aitsu_Registry::isEdit()) {
            $startTag = '<div style="padding-top:5px; padding-bottom:5px;">';
            $endTag = '</div>';
        }

        return $startTag . Aitsu_Content_Html::get($this->_index) . $endTag;
    }
}