<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Search
 */
class Moraso_Module_Search_Lucene_Class extends Moraso_Module_Abstract
{
	protected $_allowEdit = false;

	public function _main()
	{
		$searchterm = $_REQUEST['searchterm'];

		$this->_view->searchterm = $searchterm;

		$search_area = Aitsu_Config::get('search.lucene.area');

		$search_array = array ();
		foreach ($search_area as $idcat) {
			$search_array[] = $idcat;
		}

		try {
			$this->_view->results = Aitsu_Lucene_Index::find($searchterm, $search_array);
		} catch (Exception $e) {
			$this->_view->results = array ();
		}
	}
}