<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */ 

namespace Moraso\Module\Image;

use Moraso\Module\AbstractModule;

class Image extends AbstractModule
{
	protected function main()
	{
		// Breite
		if (isset($this->moduleConfig->configurable['width']) && $this->moduleConfig->configurable['width']) {
			if (!isset($this->moduleConfig->configurable['width']['selects'])) {
				$width 		= \Aitsu_Content_Config_Text::set($this->index, 'width', 'Breite', 'Bildausgabe');
			} else {
				$width_select 	= \Aitsu_Content_Config_Select::set($this->index, 'width', 'Breite', array_flip($this->moduleConfig->configurable['width']['selects']['names']), 'Bildausgabe');

				if ($width_select) {
					$width = $this->moduleConfig->configurable['width']['selects']['values'][$width_select];
				}
			}
		}

		$this->renderer->width = (int) (!empty($width) ? $width : $this->moduleConfig->parameters['width']);

		// Höhe
		if (isset($this->moduleConfig->configurable['height']) && $this->moduleConfig->configurable['height']) {
			if (!isset($this->moduleConfig->configurable['height']['selects'])) {
				$height 		= \Aitsu_Content_Config_Text::set($this->index, 'height', 'Höhe', 'Bildausgabe');
			} else {
				$height_select 	= \Aitsu_Content_Config_Select::set($this->index, 'height', 'Höhe', array_flip($this->moduleConfig->configurable['height']['selects']['names']), 'Bildausgabe');
				
				if ($height_select) {
					$height = $this->moduleConfig->configurable['height']['selects']['values'][$height_select];
				}
			}
		}

		$this->renderer->height = (int) (!empty($height) ? $height : $this->moduleConfig->parameters['height']);

		// Bildausgabe
		if (isset($this->moduleConfig->configurable['render']) && $this->moduleConfig->configurable['render']) {
			$renderSelect = array(
				'skalieren' 	=> 0,
				'zuschneiden' 	=> 1,
				'fokussieren' 	=> 2
			);

			$render = \Aitsu_Content_Config_Select::set($this->index, 'render', 'Bildausgabe', $renderSelect, 'Bildausgabe');
		}

		$this->renderer->render = (int) (isset($render) && strlen($render) > 0 ? $render : $this->moduleConfig->parameters['render']);

		// Alle Bilder ausgeben
		if (isset($this->moduleConfig->configurable['all']) && $this->moduleConfig->configurable['all']) {
			$showAllSelect = array(
				'alle Bilder ausgeben' 			=> true,
				'ausgewählte Bilder ausgeben' 	=> false
			);

			$all = \Aitsu_Content_Config_Radio::set($this->index, 'all', 'Selektierung', $showAllSelect, 'Bildausgabe');
		}

		$all = filter_var(isset($all) && strlen($all) > 0 ? $all : $this->moduleConfig->parameters['all'], FILTER_VALIDATE_BOOLEAN);

		// Bilder übergeben welche ausgegeben werden sollen
		if (!$all) {
			$images 						= \Moraso_Content_Config_Media::set($this->index, 'Image.Media', 'Media', $this->moduleConfig->parameters['idart']);
			$this->renderer->selectedImages = \Moraso_Persistence_View_Media::byFileName($this->moduleConfig->parameters['idart'], $images);
		} else {
			$this->renderer->selectedImages = \Moraso_Persistence_View_Media::ofSpecifiedArticle($this->moduleConfig->parameters['idart']);
		}

		// Attribute
		$this->renderer->attributes = new \stdClass;

		// classes
		$class = '';

		if (isset($this->moduleConfig->configurable['class']) && $this->moduleConfig->configurable['class']) {
			$class = \Aitsu_Content_Config_Text::set($this->index, 'class', 'Klasse', 'Attribute');
		}

		$class = !empty($class) ? $class : (isset($this->moduleConfig->parameters['class']) ? $this->moduleConfig->parameters['class'] : '');

		if (!empty($class)) {
			$this->renderer->attributes->class = $class;
		}

		/*
		if (!empty($this->moduleConfig->parameters['attr'])) {
			$attr = new Zend_Config(Moraso_Util::object_to_array($this->moduleConfig->parameters['attr']), array('allowModifications' => true));
		} else {
			$attr = new Zend_Config(array(), array('allowModifications' => true));
		}

		if ($this->moduleConfig->configurable['attr']) {
			$attr_config = Aitsu_Content_Config_Textarea::set($this->index, 'attr', Aitsu_Translate::_('Attributes'), $this->_translation['configuration']);
		}

		if (!empty($attr_config)) {
			$config = new Zend_Config(Moraso_Util::parseSimpleIni($attr_config)->toArray(), array('allowModifications' => true));
			$attr = $attr->merge($config);
		}

		if ($this->moduleConfig->configurable['rel']) {
			$rel = Aitsu_Content_Config_Text::set($this->index, 'rel', Aitsu_Translate::_('rel'), $this->_translation['configuration']);
		}

		$attr->rel = !empty($rel) ? $rel : (isset($this->moduleConfig->parameters['attr']->rel) && !empty($this->moduleConfig->parameters['attr']->rel)) ? $this->moduleConfig->parameters['attr']->rel : $this->moduleConfig->parameters['rel'];

		if (!isset($attr->style)) {
			$attr->style = new stdClass();
		}

		if ($this->moduleConfig->configurable['style']) {
			$attr->style->self = Aitsu_Content_Config_Text::set($this->index, 'style', Aitsu_Translate::_('Style'), $this->_translation['configuration']);
		}

		if ($this->moduleConfig->configurable['float']) {
			$floatSelect = array(
				Aitsu_Translate::_('not specified') => '',
				Aitsu_Translate::_('left') => 'left',
				Aitsu_Translate::_('right') => 'right',
				Aitsu_Translate::_('none') => 'none'
			);

			$float = Aitsu_Content_Config_Select::set($this->index, 'float', Aitsu_Translate::_('Float'), $floatSelect, $this->_translation['configuration']);
		}

		if (!empty($float)) {
			$attr->style->float = $float;
		}

		$this->renderer->attributes = $attr;
		*/

		if (empty($this->renderer->selectedImages)) {
			if (!$this->moduleConfig->parameters['renderWithoutImages']) {
				$this->withoutView = true;
			}
		}
	}
}