<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
 
class Moraso_Module_Login_Class extends Moraso_Module_Abstract
{
	protected $_allowEdit = false;

	protected function _init()
	{
		Aitsu_Registry::setExpireTime(0);

		if (\Moraso\User::isLoggedIn()) {
			$this->_view->template = 'loggedin.phtml';
		}
	}
}