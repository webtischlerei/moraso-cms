<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */ 

namespace Moraso\Module\Login\Status;

use Moraso\Module\AbstractModule;
use Moraso\User;
use Moraso\Eav;

class Status extends AbstractModule
{
	protected function main()
	{
		$user = new User();

		if ($user->isLoggedIn()) {
			$this->renderer->loggedIn 	= true;

			$eav_data = Eav::get('usermanagement_plugin_data', $user->id_eav_entity);

			$this->renderer->salutation = $user->salutation;
			$this->renderer->firstname 	= $user->firstname;
			$this->renderer->lastname 	= $user->lastname;
		}
	}
}