<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Module
 */
class Moraso_Module_Language_Selector_Class extends Moraso_Module_Abstract
{
	protected $_allowEdit = false;

	protected function _main()
	{
		$languages = Moraso_Db::fetchAll('' .
		'select ' .
		'	idlang, ' .
		'	name, ' .
		'	longname ' .
		'from _lang ' .
		'where ' .
		'	idclient = :idclient', array (
			':idclient' => Aitsu_Config::get('sys.client')
		));

		foreach ($languages as $language) {
			$language = (object) $language;
			Aitsu_Core_Navigation_Language::getInstance()->registerLang($language->idlang, $language->name, $language->longname);
		}

		$this->_view->langs = Aitsu_Core_Navigation_Language::getInstance()->getLangs();
	}
}