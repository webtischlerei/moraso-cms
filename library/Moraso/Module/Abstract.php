<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso\Module
 */
abstract class Moraso_Module_Abstract
{
	protected $_renderOnMobile 				= true;
	protected $_renderOnTablet 				= true;
	protected $_moduleConfigDefaults 		= array();
	protected $_withoutView 				= false;
	protected $_allowEdit 					= true;
	protected $_id;
	protected $_type 						= null;
	protected $_view 						= null;
	protected $_context 					= null;
	protected $_index 						= null;
	protected $_params 						= null;
	protected $_moduleName 					= '';
	protected $_isVolatile 					= false;
	protected $_cacheIfLoggedIn 			= false;
	protected $_disableCacheArticleRelation = false;
	protected $_isBlock 					= true;
	protected $_renderOnlyAllowed 			= false;

	protected static function _getInstance($className)
	{
		$instance = new $className ();

		$additionalLibraries = Moraso_Library_Tree::getJson();

		$modules = array();
		$modules[] = 'Skin\\.Module';
		if (!empty($additionalLibraries)) {
			foreach ($additionalLibraries->libraries as $additionalLibrary) {
				$modules[] = $additionalLibrary . '\\.Module';
			}
		}
		$modules[] = 'Moraso\\.Module';

		$className = str_replace('_', '.', $className);
		$className = preg_replace('/^(?:' . implode('|', $modules) . '|Module)\\./', "", $className);
		$className = preg_replace('/\\.Class$/', "", $className);

		if (isset($_GET['renderOnly']) && $className == substr($_GET['renderOnly'], 0, strlen($className)) && !$instance->_renderOnlyAllowed) {
			throw new Exception('"' . $className . '" erlaubt kein RenderOnly!');
		}

		$instance->_moduleName = $className;

		return $instance;
	}

	public static function init($context, $instance = null)
	{
		$instance = is_null($instance) ? self::_getInstance($context['className']) : $instance;

		$isMobile = Aitsu_Registry::get()->env->mobile->detect->isMobile;
		$isTablet = Aitsu_Registry::get()->env->mobile->detect->isTablet;

		if (($isMobile == 'is' && !$instance->_renderOnMobile) || ($isTablet == 'is' && !$instance->_renderOnTablet)) {
			return false;
		}

		if (($isMobile == 'is' && $isTablet == 'isNot') && (!$instance->_renderOnMobile && $instance->_renderOnTablet)) {
			return false;
		}

		if (!$instance->_isBlock) {
			Aitsu_Content_Edit::isBlock(false);
		}

		$instance->_context = $context;

		$instance->_context['rawIndex'] = $instance->_context['index'];
		$instance->_context['index'] = preg_replace('/[^a-zA-Z_0-9]/', '_', $instance->_context['index']);
		$instance->_context['index'] = str_replace('.', '_', $instance->_context['index']);

		$instance->_index = empty($instance->_context['index']) ? 'noindex' : $instance->_context['index'];

		if (!empty($instance->_context['params'])) {
			$instance->_params = Aitsu_Util::parseSimpleIni($instance->_context['params']);
		}

		$instance->_defaults = $instance->_getModulConfigDefaults(str_replace('_', '.', strtolower($instance->_moduleName)));
		$instance->_view = $instance->_getView();
		$instance->_view->index = $instance->_index;

		$instance->_translation = array(
			'configuration' => Aitsu_Translate::_('Configuration'),
			'source' => Aitsu_Translate::_('Source')
		);

		if ((!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') && (isset($instance->defaults['ajax_sleep_before_rendering']) && !empty($instance->defaults['ajax_sleep_before_rendering']))) {
			sleep($instance->defaults['ajax_sleep_before_rendering']);
		}

		$output = $instance->_init();

		$cachedOutput = $instance->_getFromCache($context['className']);

		if ($cachedOutput) {
			if ((!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') && (isset($instance->defaults['ajax_sleep_after_rendering']) && !empty($instance->defaults['ajax_sleep_after_rendering']))) {
				sleep($instance->defaults['ajax_sleep_after_rendering']);
			}

			return $cachedOutput;
		}

		$availableTemplates = $instance->_getTemplates();

		if (isset($instance->_defaults['newRenderingMethode']) && $instance->_defaults['newRenderingMethode']) {
			if (!$instance->_withoutView) {
				if ($instance->_defaults['configurable']['template']) {
					$template = Aitsu_Content_Config_Select::set($instance->_index, 'template', Aitsu_Translate::_('Template'), $availableTemplates, $instance->_translation['configuration']);

					if (!empty($template)) {
						$instance->_view->template = $template . '.phtml';
					}
				}
			}

			if ($instance->_defaults['configurable']['idart']) {
				$idart = Aitsu_Content_Config_Link::set($instance->_index, 'idart', 'idart', $instance->_translation['source']);

				if (!empty($idart)) {
					$instance->_defaults['idart'] = preg_replace('/[^0-9]/', '', $idart);
					$instance->_defaults['idartlang'] = Moraso_Util::getIdArtLang($instance->_defaults['idart'], $instance->_defaults['idlang']);
				}
			}

			if ($instance->_defaults['configurable']['idcat']) {
				$idcat = Aitsu_Content_Config_Link::set($instance->_index, 'idcat', 'idcat', $instance->_translation['source']);

				if (!empty($idcat)) {
					$instance->_defaults['idcat'] = preg_replace('/[^0-9]/', '', $idcat);
				}
			}

			if ($instance->_defaults['configurable']['mail_config_set']) {
				$availableMailConfigs = array();

				$data = Moraso_Db::fetchAll('SELECT identifier, value FROM _moraso_config WHERE identifier LIKE :like', array(
					':like' => 'mail.%'
				));

				$configs_raw = array();
				foreach ($data as $row) {
					$explode = explode('.', $row['identifier']);

					$alias = $explode[1];

					unset($explode[0]);
					unset($explode[1]);

					$configs_raw[$alias][implode('_', $explode)] = $row['value'];
				}

				$availableMailConfigs = array();
				foreach ($configs_raw as $alias => $config) {
					$availableMailConfigs[$config['name']] = $alias;
				}

				$mail_config_set = Aitsu_Content_Config_Select::set($instance->_index, 'mail_config_set', 'E-Mail-Konfiguration', $availableMailConfigs, $instance->_translation['configuration']);

				if (!empty($mail_config_set)) {
					$instance->_defaults['mail_config_set'] = $mail_config_set;
				}
			}

			$instance->_view->customFields = array();
			if (is_numeric($instance->_defaults['customFields'])) {
				for ($i = 1; $i <= $instance->_defaults['customFields']; $i++) {
					$instance->_view->customFields[$i] = Aitsu_Content_Config_Text::set($instance->_index, 'customField_' . $i, '#' . $i, 'individuelle Konfigurationen');
				}
			} elseif (is_object($instance->_defaults['customFields'])) {
				foreach ($instance->_defaults['customFields'] as $customFieldName => $customFieldValues) {
					if (is_object($customFieldValues)) {
						$instance->_view->customFields[Moraso_Util_String::slugify($customFieldName)] = Aitsu_Content_Config_Select::set($instance->_index, 'customField_' . Moraso_Util_String::slugify($customFieldName), $customFieldName, $customFieldValues, 'individuelle Konfigurationen');
					} elseif ($customFieldValues == '###link###') {
						$instance->_view->customFields[Moraso_Util_String::slugify($customFieldName)] = Aitsu_Content_Config_Link::set($instance->_index, Moraso_Util_String::slugify($customFieldName), $customFieldName, 'individuelle Konfigurationen');
					} else {
						$instance->_view->customFields[Moraso_Util_String::slugify($customFieldName)] = Aitsu_Content_Config_Text::set($instance->_index, 'customField_' . Moraso_Util_String::slugify($customFieldName), $customFieldName, 'individuelle Konfigurationen');
					}
				}
			}
		}

		$output .= $instance->_main();

		if ((isset($instance->_defaults['newRenderingMethode']) && $instance->_defaults['newRenderingMethode']) && !$instance->_withoutView) {
			if (!isset($instance->_view->template) || empty($instance->_view->template)) {
				$instance->_view->template = $instance->_defaults['template'] . '.phtml';
			}

			$output .= $instance->_view->render($instance->_view->template);
		}

		$instance->_saveIntoCache($output);

		if (Aitsu_Application_Status::isEdit()) {
			$maxLength = 60;
			$index = strlen($context['index']) > $maxLength ? substr($context['index'], 0, $maxLength) . '...' : $context['index'];

			$match = array();

			if (rtrim(preg_replace('(<!--.*?-->)', '', $output)) == '' && $instance->_allowEdit) {
				$module_config = json_decode(file_get_contents(realpath(APPLICATION_PATH . '/../library/') . '/' . str_replace('_', '/', str_replace('Class', '', $context['className'])) . 'module.json'));

				$moduleName = $module_config->name;

				if ($instance->_isBlock) {
					return '' .
					'<code class="aitsu_params" style="display:none;">' . $context['params'] . '</code>' .
					'<div style="border:1px dashed #CCC; padding:2px 2px 2px 2px;">' .
					'   <div style="height:15px; background-color: #CCC; color: white; font-size: 11px; padding:2px 5px 0 5px;">' .
					'       <span style="font-weight:bold; float:left;">' . $index . '</span><span style="float:right;">Module <span style="font-weight:bold;">' . $moduleName . '</span></span>' .
					'   </div>' .
					'</div>';
				} else {
					return '' .
					'<span style="border:1px dashed #CCC; padding:2px 2px 2px 2px;">' .
					'   ' . $moduleName . '::' . $index .
					'</span>';
				}
			}

			if (!$instance->_isBlock) {
				return '' .
				'<code class="aitsu_params" style="display:none;">' . $context['params'] . '</code>' .
				'<span style="border:1px dashed #CCC; padding:2px 2px 2px 2px;">' . $output . '</span>';
			}

			if (isset($instance->_params->suppressWrapping) && $instance->_params->suppressWrapping) {
				return $output;
			}

			return '' .
			'<code class="aitsu_params" style="display:none;">' . $context['params'] . '</code>' .
			'<div>' . $output . '</div>';
		}

		if ((!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') && (isset($instance->defaults['ajax_sleep_after_rendering']) && !empty($instance->defaults['ajax_sleep_after_rendering']))) {
			sleep($instance->defaults['ajax_sleep_after_rendering']);
		}

		return $output;
	}

	protected function _init()
	{
		return '';
	}

	protected function _main()
	{
		return '';
	}

	protected function _saveIntoCache($data)
	{
		$lifeTime = $this->_cachingPeriod();

		if (empty($lifeTime)) {
			return false;
		}

		if ($data == null) {
			$data = '';
		}

		if (!is_string($data)) {
			throw new Exception('non-string data to be cached in ' . get_class($this));
		}

		$tags = array ();

		if (!empty ($this->_type)) {
			$tags[] = 'type_' . $this->_type;
		}

		if (!$this->_disableCacheArticleRelation) {
			$tags[] = 'cat_' . Aitsu_Registry::get()->env->idcat;
			$tags[] = 'art_' . Aitsu_Registry::get()->env->idart;
		}

		if ($this->_isVolatile) {
			$tags[] = 'volatile';
		}

		Moraso_Cache::save($this->_id, $data, $lifeTime, $tags);
	}

	protected function _getFromCache($id)
	{
		$id = $this->_normalizeIndex($id);

		if (!$this->_disableCacheArticleRelation) {
			$this->_id = $id . '_Index_' . $this->_index . '_idartlang_' . Aitsu_Registry::get()->env->idartlang;
		} else {
			$lang = Aitsu_Application_Status::isEdit() ? \Moraso\Session::get('currentLanguage') : Aitsu_Registry::get()->env->idlang;
			$this->_id = $id . '_Index_' . $this->_index . '_idlang_' . $lang;
		}	

		if (($output = Moraso_Cache::load($this->_id)) !== false) {
			return $output;
		}

		return false;
	}

	protected function _getView($view = null)
	{
		if ($this->_view != null) {
			return $this->_view;
		}

		$view = empty($view) ? new Zend_View() : $view;

		$module_parts = explode('_', get_class($this));
		$module_sliced = array_slice($module_parts, 1, -1);
		
		$modulePath = implode('/', $module_sliced);
		
		$view->addScriptPath(APPLICATION_PATH . '/modules/' . str_replace('Module_', '', $modulePath) . '/');
		$view->addScriptPath(realpath(APPLICATION_PATH . '/../library/') . '/Moraso/' . $modulePath . '/');

		$additionalLibraries = Moraso_Library_Tree::getJson();

		if (!empty($additionalLibraries)) {
			foreach ($additionalLibraries->libraries as $additionalLibrary) {
				$view->addScriptPath(realpath(APPLICATION_PATH . '/../library/') . '/' . $additionalLibrary . '/' . $modulePath . '/');
			}
		}
		
		$heredity = Moraso_Skin_Heredity::build();

		foreach (array_reverse($heredity) as $skin) {
			$view->addScriptPath(APPLICATION_PATH . "/skins/" . $skin . "/module/" . str_replace('Module/', '', str_replace('Module_', '', $modulePath)) . '/');
		}
		
		return $view;
	}

	protected function _getDefaults()
	{
		$module_parts = explode('_', get_class($this));
		$module_sliced = array_slice($module_parts, 1, -1);
		
		$modulePath = implode('/', $module_sliced);

		$modulePaths = array();

		$modulePaths[] = realpath(APPLICATION_PATH . '/../library/') . '/Moraso/Module/'; // für die root "module.json"
		$modulePaths[] = APPLICATION_PATH . '/modules/' . str_replace('Module_', '', $modulePath) . '/';
		$modulePaths[] = realpath(APPLICATION_PATH . '/../library/') . '/Moraso/' . $modulePath . '/';

		$additionalLibraries = Moraso_Library_Tree::getJson();

		if (!empty($additionalLibraries)) {
			foreach ($additionalLibraries->libraries as $additionalLibrary) {
				$modulePaths[] = realpath(APPLICATION_PATH . '/../library/') . '/' . $additionalLibrary . '/' . $modulePath . '/';
			}
		}

		$heredity = Moraso_Skin_Heredity::build();

		foreach (array_reverse($heredity) as $skin) {
			$modulePaths[] = APPLICATION_PATH . "/skins/" . $skin . "/module/" . str_replace('Module/', '', str_replace('Module_', '', $modulePath)) . '/';
		}

		$defaults = array();
		$defaults['newRenderingMethode'] = false;

		foreach ($modulePaths as $key => $modulePath) {
			if (file_exists($modulePath . 'module.json')) {
				if ($key >= 1) {
					$defaults['newRenderingMethode'] = true;
				}

				$module_config = json_decode(file_get_contents($modulePath . 'module.json'));

				if (isset($module_config->defaults) && !empty($module_config->defaults)) {
					foreach ($module_config->defaults as $key => $value) {
						if ($key === 'configurable' && is_object($value)) {
							foreach ($value as $param => $bool) {
								$defaults['configurable'][$param] = $bool;
							}
						} else {
							switch($value) {
								case '##this.article.idart##':
									$value = Aitsu_Registry::get()->env->idart;
									break;
								case '##this.article.idlang##':
									$value = Aitsu_Registry::get()->env->idlang;
									break;
								case '##this.article.idartlang##':
									$value = Aitsu_Registry::get()->env->idartlang;
									break;
								case '##this.article.idcat##':
									$value = Aitsu_Registry::get()->env->idcat;
									break;
								case '##secondsUntilEndOf.day##':
									$value = Aitsu_Util_Date::secondsUntilEndOf('day');
									break;
								case '##secondsUntilEndOf.month##':
									$value = Aitsu_Util_Date::secondsUntilEndOf('month');
									break;
								case '##secondsUntilEndOf.year##':
									$value = Aitsu_Util_Date::secondsUntilEndOf('year');
									break;
							}

							$defaults[$key] = $value;
						}
					}
				}
			}
		}

		return $defaults;
	}

	protected function _getModulConfigDefaults($module)
	{
		$moduleConfig = Moraso_Config::get('module.' . $module);

		if (isset($this->_params->idart) && !empty($this->_params->idart)) {
			if (isset($this->_params->idlang) && !empty($this->_params->idlang)) {
				$this->_params->idartlang = Moraso_Util::getIdArtLang($this->_params->idart, $this->_params->idlang);
			} else {
				$this->_params->idartlang = Moraso_Util::getIdArtLang($this->_params->idart);
			}
		} elseif (isset($this->_params->idlang) && !empty($this->_params->idlang)) {
			$this->_params->idartlang = Moraso_Util::getIdArtLang($this->_params->idart, $this->_params->idlang);
		} elseif (isset($this->_params->idartlang) && !empty($this->_params->idartlang)) {
			$this->_params->idart = Moraso_Util::getIdArt($this->_params->idartlang);
			$this->_params->idlang = Moraso_Util::getIdLangByIdArtLang($this->_params->idartlang);
		}

		$defaults = $this->_getDefaults();

		foreach ($defaults as $key => $value) {
			$type = gettype($value);

			if (isset($moduleConfig->$key->default)) {
				$default = $moduleConfig->$key->default;
				$defaults[$key] = $type == 'integer' ? (int) $default : ($type == 'boolean' ? filter_var($default, FILTER_VALIDATE_BOOLEAN) : $default);
			}

			if (isset($moduleConfig->$key->configurable)) {
				$defaults['configurable'][$key] = filter_var($moduleConfig->$key->configurable, FILTER_VALIDATE_BOOLEAN);

				if (isset($moduleConfig->$key->selects) && $defaults['configurable'][$key]) {
					$selects = $moduleConfig->$key->selects;

					foreach ($selects as $i => $select) {
						if (!is_object($select)) {
							$defaults['selects'][$key]['values'][$i] = $select;
							$defaults['selects'][$key]['names'][$i] = $select;
						} else {
							$defaults['selects'][$key]['values'][$i] = $select->value;
							$defaults['selects'][$key]['names'][$i] = $select->name;
						}
					}
				}
			}

			if (!isset($defaults['configurable'][$key])) {
				$defaults['configurable'][$key] = false;
			}

			if (isset($this->_params->$key)) {
				$default = $this->_params->$key;

				if ($default === 'config') {
					if (isset($this->_params->default->$key)) {
						$default = $this->_params->default->$key;
						$defaults[$key] = $type == 'integer' ? (int) $default : ($type == 'boolean' ? filter_var($default, FILTER_VALIDATE_BOOLEAN) : $default);
					}

					if (isset($this->_params->selects->$key)) {
						$selects = $this->_params->selects->$key;

						foreach ($selects as $i => $select) {
							if (!is_object($select)) {
								$defaults['selects'][$key]['values'][$i] = $select;
								$defaults['selects'][$key]['names'][$i] = $select;
							} else {
								$defaults['selects'][$key]['values'][$i] = $select->value;
								$defaults['selects'][$key]['names'][$i] = $select->name;
							}
						}
					}

					$defaults['configurable'][$key] = true;
				} else {
					$defaults[$key] = $type == 'integer' ? (int) $default : ($type == 'boolean' ? filter_var($default, FILTER_VALIDATE_BOOLEAN) : $default);
				}
			}
		}

		$this->_moduleConfigDefaults = $defaults;

		return $defaults;
	}

	protected function _cachingPeriod()
	{
		if (isset($this->_defaults['caching'])) {
			if ($this->_defaults['caching'] == 'eternal') {
				return 31536000;
			}

			return (int) $this->_defaults['caching'];
		}

		return 0;
	}

	protected function _remove($id = null)
	{
		$id = $this->_normalizeIndex($id);

		if ($id != null) {
			$this->_id = $id . '_' . Aitsu_Registry::get()->env->idartlang;
		}

		Moraso_Cache::remove($this->_id);
	}

	protected function _getTemplates()
	{
		$return = array ();

		$templates = $this->_getView()->getScriptPaths();

		$templateFiles = array ();
		foreach ($templates as $template) {
			$files = glob($template . '*.phtml');
			if (!empty ($files)) {
				$templateFiles = array_merge($templateFiles, $files);
			}
		}

		foreach ($templateFiles as $file) {
			$content = file_get_contents($file);

			if (function_exists("mb_detect_encoding")) {
				$contentEncoding = mb_detect_encoding($content);

				if ($contentEncoding !== "UTF-8") {
					$content = mb_convert_encoding($content, "UTF-8", $contentEncoding);
				}
			}

			if (preg_match('/^<\\!\\-{2}\\s*(.*?)\\s*\\-{2}>/', $content, $match)) {
				$return[$match[1]] = substr(basename($file), 0, -6);
			}
		}

		return $return;
	}

	private function _normalizeIndex($id)
	{
		return preg_replace('/[^a-zA-Z_0-9]/', '', $id);
	}
}