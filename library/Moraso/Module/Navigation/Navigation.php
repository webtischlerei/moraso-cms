<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */ 

namespace Moraso\Module\Navigation;

use Moraso\Module\AbstractModule;
use Moraso\Navigation\Frontend as FrontendNavigation;

class Navigation extends AbstractModule
{
	protected function main()
	{          
		$idcat = !empty($this->moduleConfig->parameters['idcat']) ? $this->moduleConfig->parameters['idcat'] : \Moraso_Config::get('navigation.' . $this->index);

		$level = isset($this->moduleConfig->parameters['level']) && !empty($this->moduleConfig->parameters['level']) ? $this->moduleConfig->parameters['level'] : \Moraso_Config::get('navigation.' . $this->index . '.level');

		$this->renderer->nav = FrontendNavigation::getTree($idcat, $level);
	}
}