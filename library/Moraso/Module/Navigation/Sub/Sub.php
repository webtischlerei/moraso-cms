<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */ 

namespace Moraso\Module\Navigation\Sub;

use Moraso\Module\AbstractModule;
use Moraso\Navigation\Frontend as FrontendNavigation;

class Sub extends AbstractModule
{
	protected function main()
	{
		$bc 		= \Aitsu_Persistence_View_Category::breadCrumb($this->moduleConfig->parameters['idcat']);
		$firstLevel = (int) $this->moduleConfig->parameters['firstLevel'];

		if (!isset($bc[$firstLevel])) {
			return '';
		}

		$nav = FrontendNavigation::getTree($bc[$firstLevel]['idcat'], $firstLevel);

		if (empty($nav) || (!isset($nav[0]['hasChildren']) || empty($nav[0]['hasChildren']))) {
			$this->withoutView = true;
			return;
		}

		$this->renderer->nav = $nav[0]['children'];
	}
}