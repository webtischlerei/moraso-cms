<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Author
 */
class Moraso_Module_Article_Author_Class extends Moraso_Module_Abstract
{
	protected function _main()
	{
		$id = Moraso_Db_Simple::fetch('author', '_art_meta', array('idartlang' => $this->_defaults['idartlang']), 1, 'eternal');

		$author = new Moraso\Model\Author\Mapper();

		$this->_view->author = $author->find($id);
	}
}