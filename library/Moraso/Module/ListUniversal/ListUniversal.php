<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */ 

namespace Moraso\Module\ListUniversal;

use Moraso\Module\AbstractModule;
use Moraso\Content\Config 			as ContentConfig;

use Moraso\Model\ListUniversal\View;
use Moraso\ListUniversal 			as LU;

class ListUniversal extends AbstractModule
{
	private $sortByColumns = array();

	protected function main()
	{
		// 
		$listSet = \Moraso\ListUniversal::getLists();

		// 
		$lists = array();
		foreach ($listSet as $list) {
			$lists[$list->name] = $list->id;
		}

		// 
		$id_list = ContentConfig\Select::set($this->index, 'id_list', 'Liste', $lists, 'Konfigurationen');

		// 	
		if (empty($id_list)) {
			if (isset($this->moduleConfig->parameters['id_list'])) {
				$id_list = $this->moduleConfig->parameters['id_list'];
			} else {
				$this->withoutView 	= true;
				return;
			}
		}

		// 
		$operationModes = array(
			'Alle' 				=> 'all',
			'Zufallseintrag' 	=> 'random',
			'Auswahl' 			=> 'select'
		);

		// 
		$operationMode = ContentConfig\Select::set($this->index, 'operation_mode', 'Betriebsmodus', $operationModes, 'Konfigurationen');

		// 
		if (!$operationMode) {
			if (isset($this->moduleConfig->parameters['operationMode'])) {
				$operationMode = $this->moduleConfig->parameters['operationMode'];
			} else {
				$operationMode = 'all';
			}
		}

		// 
		if ($operationMode === 'select') {
			$selectedRows = ContentConfig\ListUniversal::set($this->index, 'rows', 'Datensätze', $id_list);
		}

		// 
		if ($id_list) {
			// 
			$columns = LU::getListColumns($id_list);

			// 
			$sortColumns = array(
				'keine Sortierung' => 'no_sorting'
			);

			// 
			foreach ($columns as $column) {
				$sortColumns[$column->name . ' (Aufsteigend)'] 	= $column->id . '_asc';
				$sortColumns[$column->name . ' (Absteigend)'] 	= $column->id . '_desc';
			}

			// 
			for ($i=1; $i <= count($sortColumns)/2; $i++) { 
				$sorting 	= ContentConfig\Select::set($this->index, 'sort_by_columns_' . $i . '_column', $i . '. Spalte', $sortColumns, 'Sortierung');

				if ($sorting && $sorting !== 'no_sorting') {
					$exploded 	= explode('_', $sorting);
					$column 	= LU::getColumn($exploded[0]);

					$this->sortByColumns[] = array('column' => str_replace('-', '_', \Moraso_Util_String::slugify($column->name)), 'order' => $exploded[1]);
				}
			}
		}
		
		// 
		$listViewMapper = new View\Mapper($id_list);

		// 
		$rows = $listViewMapper->fetchAll();

		// 
		if ($operationMode === 'random') {
			$this->renderer->row = $rows[rand(0, count($rows)-1)];
		} else {
			if ($operationMode === 'select') {
				$sortedRows = array();

				// 
				foreach ($selectedRows as $selectedRow) {
					foreach ($rows as $key => $row) {
						if ($row->id == $selectedRow) {
							$sortedRows[] = $row;
							unset($rows[$key]);
							break;
						}
					}
				}

				// 
				$rows = $sortedRows;
			}

			// 
			if (!empty($this->sortByColumns)) {
				// 
				usort($rows, array($this, 'compare'));
			}

			// 
			$this->renderer->rows = $rows;
		}
	}

	private function compare($a, $b)
	{
		return $this->_cmp(0, $a, $b);
	}

	private function _cmp($run, $a, $b)
	{
		$sorting = $this->sortByColumns;

		$column = $sorting[$run]['column'];
		$order 	= $sorting[$run]['order'];

		if (($a->$column == $b->$column || $a->$column == $b->$column) && (isset($sorting[$run+1]) && !empty($sorting[$run+1]))) {
			return $this->_cmp($run+1, $a, $b);
		} else {
			return $order === 'asc' ? strcmp($a->$column, $b->$column) : strcmp($b->$column, $a->$column);
		}
	}
}