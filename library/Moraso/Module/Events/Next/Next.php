<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */

namespace Moraso\Module\Events\Next;

use Moraso\Module\AbstractModule;

use Moraso\Events as MorasoEvents;

class Next extends AbstractModule
{
	protected function main()
	{
		$events = MorasoEvents::getNextEvents($this->moduleConfig->parameters['limit']);

		foreach ($events as $key => $event) {
			$eventMedia = MorasoEvents::getEventMedia($event['idevent']);

			$data = array();
			foreach ($eventMedia as $id) {
				$data[] = MorasoEvents::getMediaInfo($id);
			}

			if (!empty($data)) {
				$events[$key]['media'] = $data;
			}
		}

		$this->renderer->events = $events;
	}
}