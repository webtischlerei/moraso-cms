<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */

namespace Moraso\Module\Events;

use Moraso\Module\AbstractModule;

use Moraso\Events as MorasoEvents;

class Events extends AbstractModule
{
	protected function main()
	{
		$this->renderer->currentUrl = \Aitsu_Util::getCurrentUrl();
		$this->renderer->categories = MorasoEvents::getCategories();

		if (isset($_POST['filter']) && !empty($_POST['filter'])) {
			$this->renderer->filter = new \stdClass();
			$this->renderer->filter->idcategory = $_POST['filter']['idcategory'];
			$this->renderer->filter->year 		= $_POST['filter']['year'];
			$this->renderer->filter->month 		= $_POST['filter']['month'];
			$this->renderer->filter->day 		= $_POST['filter']['day'];
		}
		
		$nowYear = date('Y');

		$this->renderer->years = array($nowYear, $nowYear + 1, $nowYear + 2);

		$this->renderer->months = array(
			1 	=> \Aitsu_Translate::_('January'),
			2 	=> \Aitsu_Translate::_('February'),
			3 	=> \Aitsu_Translate::_('March'),
			4 	=> \Aitsu_Translate::_('April'),
			5 	=> \Aitsu_Translate::_('Mai'),
			6 	=> \Aitsu_Translate::_('June'),
			7 	=> \Aitsu_Translate::_('July'),
			8 	=> \Aitsu_Translate::_('August'),
			9 	=> \Aitsu_Translate::_('September'),
			10 	=> \Aitsu_Translate::_('October'),
			11 	=> \Aitsu_Translate::_('November'),
			12 	=> \Aitsu_Translate::_('December')
		);

		$this->renderer->days = range(1, 31);

		$whereList = array();
		
		$whereList[] = 'event.active = 1 ';
		
		if (!empty($this->renderer->filter->idcategory)) {
			$whereList[] = 'event.idcategory =' . $this->renderer->filter->idcategory .' ';
		}
		
		if (!empty($this->renderer->filter->year)) {
			$whereList[] = 'year(event.starttime) =' . $this->renderer->filter->year .' ';
		}
		
		if (!empty($this->renderer->filter->month)) {
			$whereList[] = 'month(event.starttime) =' . $this->renderer->filter->month .' ';
		}
		
		if (!empty($this->renderer->filter->day)) {
			$whereList[] = '(day(event.starttime) =' . $this->renderer->filter->day .' or day(event.endtime) =' . $this->renderer->filter->day .') ';
		}

		$events = MorasoEvents::getFilteredEvents($whereList);
		
		foreach ($events as $key => $event) {
			$eventMedia = MorasoEvents::getEventMedia($event['idevent']);

			$data = array();
			foreach ($eventMedia as $id) {
				$data[] = MorasoEvents::getMediaInfo($id);
			}

			if (!empty($data)) {
				$events[$key]['media'] = $data;
			}
		}
		
		$this->renderer->events = $events;
	}
}