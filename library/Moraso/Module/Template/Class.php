<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2014, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Module_Template_Class extends Moraso_Module_Abstract
{
	protected $_allowEdit = false;
	protected $_withoutView = true;

	protected function _init()
	{
		if (isset($_REQUEST['renderOnly'])) {
			return '<script type="application/x-moraso" src="' . $_REQUEST['renderOnly'] . '">' . (isset($_REQUEST['params']) ? $_REQUEST['params'] : '') . '</script>';
		}

		$index 		= str_replace('_', ' ', $this->_index);
		$parameters = $this->_params;
		$params 	= Aitsu_Content_Config_Hidden::set($index, 'Template_params', $parameters);

		$startTag 	= '';
		$endTag 	= '';
		$output 	= '';

		$keyValuePairs = array();

		if (isset($params->template) || isset($params->template_hidden)) {
			$keyValuePairs_visible 	= array();
			$keyValuePairs_hidden 	= array();

			if (!empty($params->template)) {
				foreach ((array) $params->template as $key => $line) {
					$keyValuePairs_visible[$line->name] = $key;
					$keyValuePairs[$line->name] 		= $key;
				}
			}

			if (!empty($params->template_hidden)) {
				foreach ((array) $params->template_hidden as $key => $line) {
					$keyValuePairs_hidden[$line->name] 	= $key;
					$keyValuePairs[$line->name] 		= $key;
				}
			}

			if (!Aitsu_Registry::isFront()) {
				if (\Moraso\Acl::isAllowed('templates', 'selectable')) {
					$groupedTemplates_visible = array();
							
					foreach ($keyValuePairs_visible as $name => $key) {
						if (strpos($name, '.phtml') === false) {
							$explodedName = explode('/', $name);

							if (count($explodedName) > 1) {
								$groupedTemplates_visible[trim($explodedName[0])][trim($explodedName[1])] = $key;
								unset($keyValuePairs_visible[$name]);
								unset($keyValuePairs[$name]);
							}
						}
					}

					if (!empty($keyValuePairs_visible)) {
						$template_visible = Aitsu_Content_Config_Radio::set($index, 'SubTemplate', '', $keyValuePairs_visible, 'Allgemeine Templates');
					}

					if (!empty($groupedTemplates_visible)) {
						foreach ($groupedTemplates_visible as $tempalteGroup => $groupedTemplateGroup) {
							$template_visible_grouped = Aitsu_Content_Config_Radio::set($index, 'SubTemplate', '', $groupedTemplateGroup, $tempalteGroup . ' Templates');
						}
					}

					if (\Moraso\Acl::isAllowed('templates', 'selectable.hidden')) {
						$groupedTemplates_hidden = array();

						foreach ($keyValuePairs_hidden as $name => $key) {
							if (strpos($name, '.phtml') === false) {
								$explodedName = explode('/', $name);

								if (count($explodedName) > 1) {
									$groupedTemplates_hidden[trim($explodedName[0])][trim($explodedName[1])] = $key;
									unset($keyValuePairs_hidden[$name]);
									unset($keyValuePairs[$name]);
								}
							}
						}

						if (!empty($keyValuePairs_hidden)) {
							$template_hidden = Aitsu_Content_Config_Radio::set($index, 'SubTemplate', '', $keyValuePairs_hidden, 'Allgemeine Templates - für den Kunden zur Auswahl nicht sichtbar');
						}

						if (!empty($groupedTemplates_hidden)) {
							foreach ($groupedTemplates_hidden as $tempalteGroup => $groupedTemplateGroup) {
								$template_hidden_grouped = Aitsu_Content_Config_Radio::set($index, 'SubTemplate', '', $groupedTemplateGroup, $tempalteGroup . ' Templates - für den Kunden zur Auswahl nicht sichtbar');
							}
						} 
					}

					$template = !empty($template_hidden_grouped) ? $template_hidden_grouped : (!empty($template_visible_grouped) ? $template_visible_grouped : (!empty($template_hidden) ? $template_hidden : (!empty($template_visible) ? $template_visible : '')));
				}
			} else {
				$template = Aitsu_Content_Config_Hidden::set($index, 'SubTemplate', '', $keyValuePairs, 'Template');
			}
			
			if (Aitsu_Registry::isEdit()) {
				$edit = (isset($params->hoverEdit) && $params->hoverEdit) || !isset($params->hoverEdit) ? ' no-edit' : ' ';

				$startTag 	= '<div id="Template-' . $index . '-' . $this->_defaults['idartlang'] . '" class="aitsu_editable on-demand' . $edit . '"><div class="aitsu_hover">';
				$startTag  .= '<div class="show-on-demand" style="cursor:pointer; background-color:black; color:white; padding:10px; display:none; z-index: 999999; position: relative;">Edit template area <strong>' . $index . '</strong></div>';
				$endTag 	= '</div></div>';
			}

			if (empty($template) && isset($params->defaultTemplate)) {
				$template = self::_getDefaultTemplate($index, $params);
			}
		} else {
			$template = self::_getDefaultTemplate($index, $params);
		}

		$code = '';

		if ((Aitsu_Registry::isEdit() || Aitsu_Registry::get()->env->editAction == '1') && count($keyValuePairs) >= 1) {
			$parameters = str_replace("\n", '\n', str_replace("\r\n", "\n", $this->_context['params']));
			$code 		= '<code class="aitsu_params" style="display:none;">' . $parameters . '</code>';
		}

		try {
			if (!empty($template)) {
				if (isset($params->template_hidden->$template->param)) {
					$this->_view->param = $params->template_hidden->$template->param;
				} elseif (isset($params->template->$template->param)) {
					$this->_view->param = $params->template->$template->param;
				}

				if (isset($params->template_hidden->$template->file)) {
					$template = $params->template_hidden->$template->file;
				} elseif (isset($params->template->$template->file)) {
					$template = $params->template->$template->file;
				}

				$heredity = Moraso_Skin_Heredity::build();

				foreach (array_reverse($heredity) as $skin) {
					$this->_view->addScriptPath(APPLICATION_PATH . '/skins/' . $skin . '/');
				}
				
				$output = $this->_view->render($template);
			}
		} catch (Exception $e) {
			$output = '<strong>' . $e->getMessage() . '</strong><pre>' . $e->getTraceAsString() . '</pre>';
		}

		return $startTag . $code . $output . $endTag;
	}

	protected static function _getDefaultTemplate($index, $params)
	{
		if (isset(Moraso_Article_Config::factory()->module->template->$index->defaultTemplate)) {
			$defaultTemplate = Moraso_Article_Config::factory()->module->template->$index->defaultTemplate;
		}

		if (!isset($defaultTemplate)) {
			return $params->defaultTemplate;
		}

		if (!isset($defaultTemplate->ifindex)) {
			return $defaultTemplate->default;
		}

		if (Aitsu_Persistence_Article::factory(Aitsu_Registry::get()->env->idart, Aitsu_Registry::get()->env->idlang)->isIndex()) {
			return $defaultTemplate->ifindex;
		}

		return $defaultTemplate->default;
	}
}