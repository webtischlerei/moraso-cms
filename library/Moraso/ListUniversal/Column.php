<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/ListUniversal
 */

namespace Moraso\ListUniversal;

use Moraso\Model\ListUniversal\Column as ColumnModel;

class Column
{
	/**
	 * [get description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public static function get($id)
	{
		// 
		$columnMapper = self::getMapper();

		// 
		return $columnMapper->find($id);
	}

	/**
	 * [getByListId description]
	 * @param  [type] $id_list [description]
	 * @return [type]          [description]
	 */
	public static function getByListId($id_list)
	{
		// 
		$columnMapper = self::getMapper();

		// 
		return $columnMapper->find($id_list, 'id_list', true);
	}
	
	/**
	 * [set description]
	 * @param [type] $data [description]
	 */
	public static function set($data)
	{
		// 
		$columnMapper = self::getMapper();

		// 
		$column = self::fillModel($data);

		//
		$columnMapper->save($column);

		// 
		return $column;
	}

	/**
	 * [delete description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public static function delete($id)
	{
		// 
		$columnMapper = self::getMapper();

		// 
		$columnMapper->delete($id);
	}

	/**
	 * [fillModel description]
	 * @param  array  $data [description]
	 * @return [type]       [description]
	 */
	private static function fillModel(array $data)
	{
		// 
		if ($data['id']) {
			$column = self::get($data['id']);

			unset($data['id']);
			unset($data['id_list']);
		} else {
			$column = self::createModel();
		}
		
		// 
		foreach ($data as $key => $value) {
			$column->$key = $value;
		}
		
		// 
		return $column;
	}

	/**
	 * [createModel description]
	 * @return [type] [description]
	 */
	private static function createModel()
	{
		// 
		$columnMapper = self::getMapper();

		// 
		return $columnMapper->create();
	}

	/**
	 * [getMapper description]
	 * @return [type] [description]
	 */
	private static function getMapper()
	{
		// 
		return new ColumnModel\Mapper();
	}
}