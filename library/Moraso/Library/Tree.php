<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Library_Tree
{
	public static function getJson()
	{
		$id = 'libraryTreeJson_Client' . Moraso_Config::get('sys.client');

		if ($cachedLibraryTreeJson = \Moraso_Cache::load($id)) {
            return $cachedLibraryTreeJson;
        }

		if (!file_exists(APPLICATION_PATH . '/configs/libraryTree.json') || !is_readable(APPLICATION_PATH . '/configs/libraryTree.json')) {
			return '';
		}

		$json = new Zend_Config_Json(APPLICATION_PATH . '/configs/libraryTree.json');

		\Moraso_Cache::save($id, $json, 'eternal');

		return $json;
	}
}