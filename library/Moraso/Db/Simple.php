<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */

class Moraso_Db_Simple
{
    public static function fetch($select, $from, array $where = array(), $limit = 1, $caching = 0, array $orderBy = array())
    {
        $cols = PHP_INT_MAX;

        if (is_array($select)) {
            $select = '`' . (count($select) > 1 ? implode('`, `', $select) : $select[0]) . '`';
        } else {
            if ($select !== 'all' && $select !== '*') {
                $cols = 1;
            }

            $select = ($select === 'all' || $select === '*') ? '*' : '`' . $select . '`';
        }
        
        $whereClause = array();
        $whereValues = array();
        foreach ($where as $key => $value) {
            $clause = '=';

            if (is_array($value)) {
                $clause = $value['clause'];
                $value = $value['value'];
            }

            $whereClause[] = '`' . $key . '`' . ' ' . $clause . ':value_' . $key;
            $whereValues[':value_' . $key] = $value;
        }
        $where = empty($whereClause) ? '' : ' WHERE ' . implode(' AND ', $whereClause);

        $orderClause = array();
        if (!empty($orderBy)) {
            foreach ($orderBy as $field => $sort) {
                $orderClause[] = $field . ' ' . $sort;
            }
        } 
        $orderBy = empty($orderClause) ? '' : ' ORDER BY ' . implode(', ', $orderClause);

        $query = 'SELECT ' . $select . ' FROM ' . $from . $where . $orderBy . ' LIMIT 0, ' . $limit;
        
        if ($limit === 1) {
            return $cols > 1 ? Moraso_Db::fetchRowC($caching, $query, $whereValues) : Moraso_Db::fetchOneC($caching, $query, $whereValues);     
        } else {
            return $cols > 1 ? Moraso_Db::fetchAllC($caching, $query, $whereValues) : Moraso_Db::fetchColC($caching, $query, $whereValues);
        }
    }

    public static function update($table, array $updates, array $where, $limit = 0)
    {
        $set = '';
    	foreach ($updates as $field => $value) {
            if ($value !== 'NOW()') {
                $value = Aitsu_Registry::get()->db->quote($value);
            }

    		$set[] = '`' . $field . '` = ' . $value;
    	}
        
        $whereClause = array();
        $whereValues = array();
        foreach ($where as $key => $value) {
            $whereClause[] = $key . ':value_' . trim(str_replace(array('=>', '>=', '=', '<=', '=<', '!='), array('', '', '', '', '', ''), $key));
            $whereValues[':value_' . trim(str_replace(array('=>', '>=', '=', '<=', '=<', '!='), array('', '', '', '', '', ''), $key))] = $value;
        }

        $orderClause = array();
        if (!empty($orderBy)) {
            foreach ($orderBy as $field => $sort) {
                $orderClause[] = $field . ' ' . $sort;
            }
        } 
        $orderBy = empty($orderClause) ? '' : ' ORDER BY ' . implode(', ', $orderClause);

        if (empty($limit)) {
        	$query = 'UPDATE `' . $table . '` SET ' . implode(', ', $set) . ' WHERE ' . implode(' AND ', $whereClause);
        } else {
        	$query = 'UPDATE `' . $table . '` SET ' . implode(', ', $set) . ' WHERE ' . implode(' AND ', $whereClause) . ' LIMIT ' . $limit;
        }
        
        return Moraso_Db::query($query, $whereValues);
    }

    public static function insert($table, array $inserts)
    {
        $fields = array();
        $values = array();
    	foreach ($inserts as $field => $value) {
    		$fields[] = '`' . $field . '`';

            if ($value !== 'NOW()') {
    		    $values[] = Aitsu_Registry::get()->db->quote($value);
            } else {
                $values[] = $value;
            }
    	}
                
        $query = 'INSERT INTO ' . $table . ' (' . implode(', ', $fields) . ') VALUES (' . implode(', ', $values) . ')';

        return Moraso_Db::query($query)->getLastInsertId();
    }

    public static function delete($from, array $where)
    {
        $whereClause = array();
        $whereValues = array();

        foreach ($where as $key => $value) {
            $clause = '=';

            if (is_array($value)) {
                $clause = $value['clause'];
                $value = $value['value'];
            }

            $whereClause[] = $key . ' ' . $clause . ':value_' . $key;
            $whereValues[':value_' . $key] = $value;
        }

        Moraso_Db::query('DELETE FROM ' . $from . ' WHERE ' . implode(' AND ', $whereClause) . '', $whereValues);
    }
}