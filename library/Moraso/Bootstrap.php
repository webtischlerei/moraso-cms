<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */

namespace Moraso;

set_include_path(realpath(dirname(__FILE__) . '/..') . PATH_SEPARATOR . get_include_path());
set_include_path(realpath(dirname(__FILE__) . '/../..') . PATH_SEPARATOR . get_include_path());

use Moraso\Cache\Page as PageCache;
use Moraso\Session as Session;
use Moraso\User;

class Bootstrap
{
	public 		$pageContent 	= null;
	public 		$serviceManager = null;

	protected function _InitDefaultTimezone()
	{
		date_default_timezone_set('Europe/Berlin');
	}

	protected function _InitServiceManger()
	{
		Service\Manager::init();

		$this->serviceManager = Service\Manager::get();
	}

	protected function _InitRegistry()
	{
		Registry::init();
	}

	protected function _InitDatabaseConnection()
	{
		Database\Connection::init();
	}
	
	protected function _GetPageCacheFromFileSystem()
	{
		if ((isset($_GET['edit']) && $_GET['edit'] == 1) || isset($_GET['clearcache']) || isset($_GET['wbc'])) {
			return;
		}

		$pageCache = PageCache::get();

		if ($pageCache->isValid) {
			header("ETag: " . $pageCache->etag);

			if (!$this->checkIfETagIsValid($pageCache->etag)) {
				echo $pageCache->content;
			}

			exit(0);
		}
	}

	protected function _ReadConfiguration()
	{
		\Moraso_Config::init();
	}

	protected function _InitCache()
	{
		MorasoCache::init();
	}
	
	protected function _ExecuteConfiguredPreInits()
	{
		\Aitsu_Event::raise('frontend.preInit', null);
	}

	protected function _InitializeSession()
	{
		Session::init();
	}

	protected function _SetMorasoErrorHandler()
	{
		set_error_handler(array('Moraso_Log', 'errorHandler'), E_ALL);
	}

	protected function _CleanCache()
	{
		if (!isset($_GET['clearcache']) || empty($_GET['clearcache'])) {
			return;
		}

		PageCache::clear();

		if ($_GET['clearcache'] == 'all') {
			\Aitsu_Util_Dir::rm(APPLICATION_PATH . '/data/cachetransparent/data');
			\Moraso_Cache::clean();

			MorasoCache::flush();
			return;
		}

		\Moraso_Cache::clean(\Zend_Cache::CLEANING_MODE_MATCHING_TAG, array($_GET['clearcache']));
	}

	protected function _InitUser()
	{
		User::init();
	}

	protected function _UserAuth()
	{
		$user = $this->serviceManager->get('moraso-user');

		if (!$user->isLoggedIn()) {
			if (isset($_REQUEST['username']) && isset($_REQUEST['password'])) {
				if ($user->login($_REQUEST['username'], $_REQUEST['password'])) {
					$this->serviceManager->setService('moraso-user-data', $user->get());
				}
			}
		} else {
			if (isset($_REQUEST['logout'])) {
				$user->logout();
				header('Location: ' . strtok($_SERVER['REQUEST_URI'], '?'));
				exit(0);
			}
		}
	}

	protected function _InitAcl()
	{
		Acl::init();
	}

	protected function _AuthenticateUser()
	{
		if (\Moraso\User::isLoggedIn()) {
			// TODO: Session verlängern

			if ((isset($_GET['edit']) || isset($_GET['preview'])) && !\Moraso\Acl::isAllowed('article', 'update')) {
				header('HTTP/1.1 401 Access Denied');
				echo 'Access denied';
				exit();
			}	
		}

		Registry::isEdit(isset($_GET['edit']));

		\Aitsu_Registry::isEdit(isset($_GET['edit']));
		\Aitsu_Registry::isFront(!isset($_GET['edit']));
		\Aitsu_Application_Status::isEdit(isset($_GET['edit']));
		\Aitsu_Application_Status::isPreview(isset($_GET['preview']));
		\Aitsu_Application_Status::setEnv('frontend');
	}

	protected function _ExecuteConfiguredInits()
	{
		\Aitsu_Event::raise('frontend.init', null);
	}

	protected function _EvaluateRequest()
	{
		if (!isset($_GET['id'])) {
			// Frontend
			\Moraso_Bootstrap_EvalRequest::run();
		} else {
			// Backend
			$art_lang_mapper 	= new \Moraso\Model\Art\Lang\Mapper();
			$art_lang 			= $art_lang_mapper->find($_GET['id']);

			$cat_art_mapper 	= new \Moraso\Model\Cat\Art\Mapper();
			$cat_art 			= $cat_art_mapper->find(array(NULL, $art_lang->idart));

			$lang_mapper 		= new \Moraso\Model\Lang\Mapper();
			$lang 				= $lang_mapper->find($art_lang->idlang);

			$cat_lang_mapper 	= new \Moraso\Model\Cat\Lang\Mapper();
			$cat_lang 			= $cat_lang_mapper->find(array($cat_art->idcat, $art_lang->idlang), array('idcat', 'idlang'));
			
			\Aitsu_Registry::get()->env->idlang 	= $lang->idlang;
			\Aitsu_Registry::get()->env->lang 		= $lang->idlang;
			\Aitsu_Registry::get()->env->idclient 	= $lang->idclient;
			\Aitsu_Registry::get()->env->client 	= $lang->idclient;
			\Aitsu_Registry::get()->env->idart 		= $art_lang->idart;
			\Aitsu_Registry::get()->env->idartlang 	= $art_lang->idartlang;
			\Aitsu_Registry::get()->env->idcat 		= $cat_art->idcat;
			\Aitsu_Registry::get()->env->idcatlang 	= $cat_lang->idcatlang;
			\Aitsu_Registry::get()->env->ispublic 	= $cat_lang->public;
		}		

		if (\Aitsu_Application_Status::isEdit() && !\Moraso\Acl::isAllowed('article', 'update') && !\Moraso\Acl::isAllowed('cat.' . \Aitsu_Registry::get()->env->idcat)) {
			return;
		}

		if (isset(\Aitsu_Registry::get()->env->ispublic) && \Aitsu_Registry::get()->env->ispublic == 1) {
			return;
		}

		if (!Acl::isAllowed('cat.' . \Aitsu_Registry::get()->env->idcat)) {
			\Aitsu_Registry::get()->env->idart = \Moraso_Config::get('sys.loginpage');
			\Moraso_Bootstrap_EvalRequest::setIdartlang(\Moraso_Config::get('sys.loginpage'));
		}	
	}

	protected function _LockApplicationStatus()
	{
		\Aitsu_Application_Status::setEnv('front');
		\Aitsu_Application_Status::lock();
	}

	protected function _RenderOutput()
	{
		$this->pageContent = '<script type="application/x-moraso" src="Template:Root">' .
				'suppressWrapping = true' .
				'</script>';
	}

	protected function _ExecuteConfiguredTransformations()
	{
		\Aitsu_Event::raise('frontend.dispatch', array('bootstrap' => $this));
	}

	protected function _ExecuteConfiguredUrlRewriting()
	{
		$obj = call_user_func(array(\Aitsu_Registry::get()->config->rewrite->controller, 'getInstance'));

		$this->pageContent = $obj->rewriteOutput($this->pageContent);
	}

	protected function _CacheIntoTheFileSystem()
	{
		$expire = \Aitsu_Registry::getExpireTime();

		if ((isset(\Aitsu_Registry::get()->config->cache->page->enable) && !\Aitsu_Registry::get()->config->cache->page->enable) || \Moraso\User::isLoggedIn() || empty($expire)) {
			return;
		}

		PageCache::save($this->pageContent);
	}

	protected function _TriggerIndexing()
	{
		\Aitsu_Event::raise('frontend.indexing', array('bootstrap' => $this));
	}

	protected function _TriggerEnd()
	{
		\Aitsu_Event::raise('frontend.end', null);
	}

	protected function checkIfETagIsValid($etag)
	{
		if (isset($_SERVER['HTTP_IF_NONE_MATCH']) && $_SERVER['HTTP_IF_NONE_MATCH'] == $etag) {
			header("HTTP/1.1 304 Not Modified");
			header("Connection: Close");

			return true;
		}

		return false;
	}

	public static function run()
	{
		libxml_use_internal_errors(true);

		define('APPLICATION_PATH', 	ROOT_PATH . '/application');
		define('LIBRARY_PATH', 		ROOT_PATH . '/library');
		define('CACHE_PATH', 		APPLICATION_PATH . '/data/pagecache');
		
		$instance = new self();

		try {
			foreach (get_class_methods($instance) as $phase) {
				if (substr($phase, 0, strlen('_')) == '_') {
					call_user_func(array(
						$instance,
						$phase
					));
				}
			}
		} catch (Exception $e) {
			trigger_error('Exception in ' . __FILE__ . ' on line ' . __LINE__ . ': ' . $e->getMessage());
			trigger_error("Stack trace: \n" . $e->getTraceAsString());
			exit();
		}

		$expire = \Aitsu_Registry::getExpireTime();
		$etag 	= sha1($instance->pageContent);

		header("Date: " . gmdate('D, d M Y H:i:s', time()));

		if (empty($expire) || \Aitsu_Application_Status::isEdit() || isset($_REQUEST['logout'])) {
			\Moraso_Cache::disableBrowserCache();
		} else {
			header("Cache-Control: public, max-age=" . $expire . ", s-maxage=" . $expire . ", stale-if-error=60, stale-while-revalidate=2");
			header("Expires: " . gmdate('D, d M Y H:i:s', time() + $expire) . " GMT");
			header("ETag: " . $etag);
		}

		if (\Moraso_Config::get('pingback.enabled')) {
			header("x-pingback", \Moraso_Config::get('sys.webpath') . '?renderOnly=Pingback');
		}

		if (!isset($_GET['wbc']) || !$instance->checkIfETagIsValid($etag) || empty($expire)) {
			echo $instance->pageContent;
		}

		return;
	}
}