<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */
class Moraso_Cache
{
	public static function init()
	{
		if (isset(Aitsu_Registry::get()->cache->zendCacheObject) && !empty(Aitsu_Registry::get()->cache->zendCacheObject)) {
			$zendCacheObject = Aitsu_Registry::get()->cache->zendCacheObject;
		} else {
			$frontendOptions = array();

			$defaults = array(
				'caching' => array(
					'id' 		=> 'cache.internal.enable',
					'default' 	=> false
				),
				'cache_id_prefix' => array(
					'id' 		=> 'cache.internal.prefix',
					'default' 	=> ''
				),
				'lifetime' => array(
					'id' 		=> 'cache.internal.lifetime',
					'default' 	=> 0
				),
				'logging' => array(
					'id' 		=> 'cache.internal.logging',
					'default' 	=> false
				),
				'write_control' => array(
					'id' 		=> 'cache.internal.write.control',
					'default' 	=> true
				),
				'automatic_serialization' => array(
					'id' 		=> 'cache.internal.automatic.serialization',
					'default' 	=> true
				),
				'automatic_cleaning_factor' => array(
					'id' 		=> 'cache.internal.automatic.cleaning.factor',
					'default' 	=> 10
				),
				'ignore_user_abort' => array(
					'id' 		=> 'cache.internal.ignore.user.abort',
					'default' 	=> false
				),
				'type' => array(
					'id' 		=> 'cache.internal.type',
					'default' 	=> 'file'
				)
			);

			foreach ($defaults as $option => $config) {
				if (is_bool(Moraso_Config::get($config['id']))) {
					$frontendOptions[$option] 		= $config['default'];
				} else {
					if (is_bool($config['default'])) {
						$frontendOptions[$option] 	= (bool) Moraso_Config::get('cache.internal.enable');
					} elseif (is_int($config['default'])) {
						$frontendOptions[$option] 	= (int) Moraso_Config::get('cache.internal.enable');
					} else {
						$frontendOptions[$option] 	= (string) Moraso_Config::get('cache.internal.enable');
					}
				}
			}

			switch ($frontendOptions['type']) {
				case 'memcached':
					$backendOptions = array(
						'servers' => array(
							array(
								'host' 				=> 'localhost',
								'port' 				=> 11211,
								'persistent' 		=> true,
								'weight' 			=> 1,
								'timeout' 			=> 5,
								'retry_interval' 	=> 15,
								'status' 			=> true,
								'failure_callback' 	=> ''
							)
						)
					);

					$zendCacheObject = Zend_Cache::factory('Core', 'Memcached', $frontendOptions, $backendOptions);
					break;
				case 'apc':
					$zendCacheObject = Zend_Cache::factory('Core', 'Apc', $frontendOptions);
					break;
				default:
					$backendOptions = array(
						'cache_dir' => APPLICATION_PATH . '/data/cache/'
					);

					if (!is_dir($backendOptions['cache_dir'])) {
						mkdir($backendOptions['cache_dir'], 0777, true);
					}

					if (!is_writable($backendOptions['cache_dir'])) {
						$frontendOptions['caching'] = false;
					}
			 
					$zendCacheObject = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
			}

			Aitsu_Registry::get()->cache->zendCacheObject = $zendCacheObject;
		}

		return $zendCacheObject;
	}

	public static function disableBrowserCache()
	{
		header("Cache-Control: private, no-cache, max=age=0, must-revalidate");
		header("Expires: Mon, 16 Mar 1987 14:35:00 GMT");
		header("Pragma: no-cache");
	}

	public static function save($id, $data, $lifetime = 0, array $tags = array())
	{
		if (Aitsu_Application_Status::isEdit()) {
			return false;
		}

		if (Aitsu_Application_Status::getEnv() === 'backend') {
			return false;
		}

		if (strpos($id, '_Module_Template_Class_Index_Root_') !== false) {
			return false;
		}

		Aitsu_Registry::get()->cache->$id = $data;

		$cache = self::init();

		if ($lifetime === 'eternal') {
			$lifetime = 31536000;
		}

		$cache->save($data, $id, $tags, $lifetime);
	}

	public static function remove($id)
	{
		$cache = self::init();

		$cache->remove($id);
	}

	public static function load($id)
	{
		if (Aitsu_Application_Status::isEdit()) {
			return false;
		}

		if (Aitsu_Application_Status::getEnv() === 'backend') {
			return false;
		}

		if (isset(Aitsu_Registry::get()->cache->$id) && !empty(Aitsu_Registry::get()->cache->$id)) {
			return Aitsu_Registry::get()->cache->$id;
		}

		$cache = self::init();

		return $cache->load($id);
	}

	public static function clean($mode = Zend_Cache::CLEANING_MODE_ALL, $tags = array())
	{
		if (isset(Aitsu_Registry::get()->cache) && !empty(Aitsu_Registry::get()->cache)) {
			unset(Aitsu_Registry::get()->cache);
		}

		if (Aitsu_Application_Status::getEnv() === 'backend') {
			return false;
		}

		$cache = self::init();

		$cache->clean($mode, $tags);
	}
}