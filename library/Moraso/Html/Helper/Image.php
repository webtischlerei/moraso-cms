<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */

namespace Moraso\Html\Helper;

class Image
{
	private $domDocument, $imageElement;

	/**
	 * [getHtml description]
	 * @param  [type] $idart      [description]
	 * @param  [type] $filename   [description]
	 * @param  [type] $width      [description]
	 * @param  [type] $height     [description]
	 * @param  [type] $render     [description]
	 * @param  [type] $attributes [description]
	 * @return [type]             [description]
	 */
	public static function getHtml($idart, $filename, $width, $height, $render, $attributes)
	{
		// 
		$self = new self();

		// HTML erzeugen
		$html = $self->createHtml($idart, $filename, $width, $height, $render, $attributes);

		// erzeugtes HTML zurückgeben
		return $html;
	}

	/**
	 * [createHtml description]
	 * @param  [type] $idart      [description]
	 * @param  [type] $filename   [description]
	 * @param  [type] $width      [description]
	 * @param  [type] $height     [description]
	 * @param  [type] $render     [description]
	 * @param  [type] $attributes [description]
	 * @return [type]             [description]
	 */
	private function createHtml($idart, $filename, $width, $height, $render, $attributes)
	{
		// DOM Element erzeugen
		$this->domDocument = new \DOMDocument();

		// Image Element erzeugen
		$this->imageElement = $this->createElement('img', $this->domDocument);

		// Image-Src erzeugen
		$this->createAttribute($this->imageElement, 'src', self::getPath($idart, $filename, $width, $height, $render));

		// Attribute zuweisen
		foreach ($attributes as $attribute_key => $attribute_value) {
			$this->createAttribute($this->imageElement, $attribute_key, $attribute_value);
		}
		
		// Image Element zurückgeben
		return $this->domDocument->saveHTML();
	}

	/**
	 * [createElement description]
	 * @param  [type] $name     [description]
	 * @param  [type] $appendTo [description]
	 * @return [type]           [description]
	 */
	private function createElement($name, $appendTo)
	{
		// 
		$element = $this->domDocument->createElement($name);

		// 
		$appendTo->appendChild($element);

		// 
		return $element;
	}

	/**
	 * [createAttribute description]
	 * @param  [type] $element   [description]
	 * @param  [type] $attribute [description]
	 * @param  [type] $value     [description]
	 * @return [type]            [description]
	 */
	private function createAttribute($element, $attribute, $value)
	{
		// 
		$attribute 			= $this->domDocument->createAttribute($attribute);
		$attribute->value 	= $value;

		// 
		$element->appendChild($attribute);
	}

	/**
	 * [getPath description]
	 * @param  [type] $idart    [description]
	 * @param  [type] $filename [description]
	 * @param  [type] $width    [description]
	 * @param  [type] $height   [description]
	 * @param  [type] $render   [description]
	 * @return [type]           [description]
	 */
	public static function getPath($idart, $filename, $width, $height, $render) 
	{
		// 
		$path = \Moraso_Config::get('sys.webpath') . 'image/' . $width . '/' . $height . '/' . $render . '/' . $idart . '/' . $filename;

		// 
		return $path;
	}
}
