<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso/Module
 */

namespace Moraso\Html\Helper\Navigation;

/**
* TODO
*/
class Select
{
	/**
	* TODO
	*/
	private $domDocument, $selectElement, $firstEntryName;

	/**
	* TODO
	*/
	public static function getHtml(array $nav, $firstEntryName, $id = null, $name = null)
	{
		// 
		$self = new self();

		// 
		$self->firstEntryName = $firstEntryName;

		// erzeugtes HTML zurückgeben
		return $self->createHtml($nav, $id, $name);
	}

	/**
	* TODO
	*/
	private function createHtml($nav, $id, $name)
	{
		// DOM Element erzeugen
		$this->domDocument = new \DOMDocument();

		// Select erzeugen
		$this->selectElement = $this->createElement('select', $this->domDocument);

		// Select "ID" zuweisen
		if (!empty($id)) {
			$this->createAttribute($this->selectElement, 'id', $id);
		}

		// Select "name" zuweisen
		if (!empty($name)) {
			$this->createAttribute($this->selectElement, 'name', $name);
		}
		
		// "options" erzeugen
		self::createChild(array('name' => $this->firstEntryName));

		foreach ($nav as $row) {
			self::createChild($row);
		}

		// Select Element zurückgeben
		return $this->domDocument->saveHTML();
	}

	/**
	* TODO
	*/
	private function createElement($name, $appendTo, $value = null)
	{
		$element = $this->domDocument->createElement($name, $value);

		$appendTo->appendChild($element);

		return $element;
	}

	/**
	* TODO
	*/
	private function createAttribute($element, $attribute, $value)
	{
		$attribute = $this->domDocument->createAttribute($attribute);
		$attribute->value = $value;

		$element->appendChild($attribute);
	}

	/**
	* TODO
	*/
	private function createChild($row, $prefix = null)
	{
		// "option" erzeugen
		$optionElement = $this->createElement('option', $this->selectElement, $prefix . $row['name']);

		// Prüfen ob es sich um eine Kategorie handelt
		if (isset($row['idcat']) && $row['idcat']) {
			// "value" zuweisen
			$this->createAttribute($optionElement, 'value', '{ref:idcat-' . $row['idcat'] . '}');

			// "selected" zuweisen
			if ($row['isCurrent']) {
				$this->createAttribute($optionElement, 'selected', 'selected');
			}

			// Kinder auslesen
			if ($row['hasChildren']) {
				foreach ($row['children'] as $row) {
					self::createChild($row, '- ');
				}
			}
		}
	}
}