<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013 - 2014, webtischlerei <http://www.webtischlerei.de>
 */
class Moraso_Config extends Aitsu_Config
{
	/**
	* TODO
	*/
	public static function init()
	{
		if (isset($_GET['edit']) || isset($_GET['preview'])) {
			Aitsu_Registry::get()->config = Moraso_Config_Json::getInstance();

			$ini = Moraso_Db::fetchOne('' .
							'select ' .
							'   client.config ' .
							'from ' .
							'   _art_lang as artlang ' .
							'left join ' .
							'   _lang as lang on artlang.idlang = lang.idlang ' .
							'left join ' .
							'   _clients as client on lang.idclient = client.idclient ' .
							'where ' .
							'   artlang.idartlang = :idartlang', array(
						':idartlang' => $_GET['id']
			));

			if (empty($ini)) {
				$ini = 'default';
			}

			self::initConfig($ini);

			return;
		}

		self::initConfig();

		if (!empty($_SERVER['HTTPS'])) {
			Aitsu_Registry::get()->config->sys->webpath 		= str_replace('http://', 'https://', self::get('sys.webpath'));
			Aitsu_Registry::get()->config->sys->canonicalpath 	= str_replace('http://', 'https://', self::get('sys.canonicalpath'));

			self::get('sys.webpath', true);
			self::get('sys.canonicalpath', true);
		}
	}
	
	public static function initConfig($ini = null, $env = null)
	{
		if (empty($ini)) {
			$ini = Aitsu_Mapping::getIni();
		}

		if (empty($env)) {
			$env = Moraso_Util::getEnv();
		}
		
		Aitsu_Registry::get()->config = Moraso_Config_Json::getInstance($env);
		
		Moraso_Config_Db::setConfigFromDatabase($ini, false, $env);
	}
}