<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */
class Moraso_Backend_Controller_Plugin_Accesscontrol extends Zend_Controller_Plugin_Abstract
{
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
		$login 		= $request->getParam('login');
		$password 	= $request->getParam('password');
		
		if (!empty($login) && !empty($password)) {
			try {
				$success = \Moraso\User::login($login, $password);

				// Login erfolgte per Ajax
				if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
					header('Content-Type: application/json');
					echo json_encode(array('success' => $success));
					exit;
				}
			} catch (Exception $e) {
				header('Location: ./');
				exit;
			}
		}

		if (\Moraso\User::isLoggedIn()) {
			// TODO: Session verlängern
		} elseif ($request->getActionName() != 'login' || $request->getControllerName() != 'acl') {
			$request->setControllerName('acl')->setActionName('login')->setDispatched(false);
			return;
		}
	}
}