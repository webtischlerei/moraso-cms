<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */

namespace Moraso\Backend;

class RegisterPlugins
{
	/**
	* TODO
	*/
	public static function init()
	{
		$frontController = \Zend_Controller_Front::getInstance();

		$frontController->registerPlugin(new \Moraso_Backend_Controller_Plugin_Accesscontrol());
		$frontController->registerPlugin(new \Aitsu_Adm_Controller_Plugin_BackendLocale());
		$frontController->registerPlugin(new \Aitsu_Adm_Controller_Plugin_Clientlang());
		$frontController->registerPlugin(new \Moraso_Adm_Controller_Navigation());
		$frontController->registerPlugin(new \Aitsu_Adm_Controller_Plugin_Listeners());
	}
}