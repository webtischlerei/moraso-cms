<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */

namespace Moraso\Backend;

class Router
{
	/**
	* TODO
	*/
	public static function init()
	{
		$router = \Zend_Controller_Front::getInstance()->getRouter();

		$router->addRoute('plugin', new \Zend_Controller_Router_Route('plugin/:namespace/:plugin/:area/:paction/*', array(
			'controller' 	=> 'plugin',
			'action' 		=> 'index',
			'paction' 		=> 'index'
		)));

		$router->addRoute('child_plugin', new \Zend_Controller_Router_Route('plugin/:namespace/:parent_plugin/:area/child/:plugin/:paction/*', array(
			'controller' 	=> 'plugin',
			'action' 		=> 'index',
			'paction' 		=> 'index'
		)));
	}
}