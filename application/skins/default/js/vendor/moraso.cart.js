$('#doCheckoutButton').on('click', function(event) {
	event.preventDefault();

	$(this).attr("disabled", true);

	$('#checkoutProzessing').fadeIn('slow');	

	$.post('#', {
		form_listener: 	'morasoCartForm',
		form_action: 	'doCheckout',
		timestamp: 		new Date().getTime()
	}, function(data) {
		if (data.success) {
			$('#doCheckout').submit();
		} else {
			alert(data);
		}
	}, 'json');
});