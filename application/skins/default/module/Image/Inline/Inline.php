<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Skin/Module
 */ 

namespace Skin\Module\Image\Inline;

use Moraso\Module\Image\Image;

class Inline extends Image
{
	
}