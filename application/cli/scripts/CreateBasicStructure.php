<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2013 - 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */
class Cli_CreateBasicStructure extends Aitsu_Cli_Script_Abstract
{
	protected function _main()
	{
		Aitsu_Registry::get()->env->idlang 	= 1;
		$domain 							= $this->_options['d'];
		$createShopStructure 				= (bool) $this->_options['z'];

		$configSets = array(
			'myAccountDashboard' 	=> array(
				'identifier' 			=> 'mein Konto (Dashboard)',
				'config' 				=> 'module.template.ContentArticle.defaultTemplate.default = templates/content/article/account/dashboard.phtml'
			),
			'myAccountMyData' 		=> array(
				'identifier' 			=> 'mein Konto (meine Daten)',
				'config' 				=> 'module.template.ContentArticle.defaultTemplate.default = templates/content/article/account/personal_data.phtml'
			),
			'myAccountRegister' 	=> array(
				'identifier' 			=> 'mein Konto (Registrierung)',
				'config' 				=> 'module.template.ContentArticle.defaultTemplate.default = templates/content/article/account/register.phtml'
			)
		);

		if ($createShopStructure) {
			$configSets['shopProducts'] = array(
				'identifier' 	=> 'Shop - Produkte',
				'config' 		=> '
					module.template.ContentArticle.defaultTemplate.ifindex = templates/content/article/shop/products.phtml
					module.template.ContentArticle.defaultTemplate.default = templates/content/article/shop/products/product.phtml

					plugin.cart.article.position.default = 650
					plugin.cart.article.position.ifindex = 0
				'
			);
			
			$configSets['shopCart'] = array(
				'identifier' 	=> 'Shop - Warenkorb',
				'config' 		=> 'module.template.ContentArticle.defaultTemplate.default = templates/content/article/shop/cart.phtml'
			);
			
			$configSets['shopCheckout'] = array(
				'identifier' 	=> 'Shop - Kasse',
				'config' 		=> 'module.template.ContentArticle.defaultTemplate.default = templates/content/article/shop/checkout.phtml'
			);
			
			$configSets['shopCheckoutDelivery'] = array(
				'identifier' 	=> 'Shop - Kasse (Lieferung)',
				'config' 		=> 'module.template.ContentArticle.defaultTemplate.default = templates/content/article/shop/checkout/delivery.phtml'
			);
			
			$configSets['shopCheckoutBilling'] = array(
				'identifier' 	=> 'Shop - Kasse (Rechnung)',
				'config' 		=> 'module.template.ContentArticle.defaultTemplate.default = templates/content/article/shop/checkout/billing.phtml'
			);
			
			$configSets['shopCheckoutPayment'] = array(
				'identifier' 	=> 'Shop - Kasse (Zahlung)',
				'config' 		=> 'module.template.ContentArticle.defaultTemplate.default = templates/content/article/shop/checkout/payment.phtml'
			);
			
			$configSets['shopCheckoutOverview'] = array(
				'identifier' 	=> 'Shop - Kasse (Übersicht)',
				'config' 		=> 'module.template.ContentArticle.defaultTemplate.default = templates/content/article/shop/checkout/overview.phtml'
			);		
		}

		$configSetMapper = new \Moraso\Model\Configset\Mapper();

		$configSetIDs = array();
		foreach ($configSets as $alias => $configSetData) {
			$configSet 				= $configSetMapper->create();
			$configSet->identifier 	= $configSetData['identifier'];
			$configSet->config 		= $configSetData['config'];

			$configSetMapper->save($configSet);

			$configSetIDs[$alias] 	= $configSet->id;
		}

		/* erste Ebene anlegen */
		$mainIdCat 		= $this->_createCategory('main');
		$metaIdCat 		= $this->_createCategory('meta');
		$systemIdCat 	= $this->_createCategory('system');

		/* zweite Ebene anlegen */
		$imprintIdCat 		= $this->_createCategory('Impressum', $metaIdCat);
		$contactIdCat 		= $this->_createCategory('Kontakt', $metaIdCat);
		$myAccountIdCat 	= $this->_createCategory('mein Konto', $metaIdCat);
		$loginIdCat 		= $this->_createCategory('Login', $systemIdCat);
		$errorIdCat 		= $this->_createCategory('Error', $systemIdCat);

		/* dritte Ebene anlegen */
		$myAccountDataIdCat = $this->_createCategory('meine Daten', $myAccountIdCat, null, false);

		/* Shop Struktur (Kategorien) anlegen */
		if ($createShopStructure) {
			$productsIdCat 	= $this->_createCategory('Produkte', $mainIdCat, $configSetIDs['shopProducts']);
			$cartIdCat 		= $this->_createCategory('Warenkorb', $mainIdCat);
			$checkoutIdCat 	= $this->_createCategory('Kasse', $mainIdCat);
			$tacIdCat 		= $this->_createCategory('AGB', $metaIdCat);
		}

		// Kategorien fertig angelegt, nun die Artikel anlegen
		
		/* Artikel anlegen */
		$startIdArt 		= $this->_createArticle('index', 'Herzlich willkommen', $mainIdCat);
		$errorIdArt 		= $this->_createArticle('index', 'Seite nicht gefunden', $errorIdCat);
		$loginIdArt 		= $this->_createArticle('index', 'Login', $loginIdCat);
		$imprintIdArt 		= $this->_createArticle('index', 'Impressum', $imprintIdCat);
		$contactIdArt 		= $this->_createArticle('index', 'Kontakt', $contactIdCat);
		$mediaHolderIdArt 	= $this->_createArticle('mediaHolder', 'Datenträger für globale Medien', $systemIdCat, false);

		/* Login-Artikel mit Inhalt befüllen */
		$this->createArticleContent($loginIdArt, '<p>Sie betreten einen geschützten Bereich!</p> _[Login:Login]');

		/* Artikel "mein Konto" anlegen */
		$myAccountIdArt 		= $this->_createArticle('index', 'mein Konto', $myAccountIdCat, true, $configSetIDs['myAccountDashboard']);
		$myAccountDataIdArt 	= $this->_createArticle('index', 'meine Daten', $myAccountDataIdCat, true, $configSetIDs['myAccountMyData']);
		$myAccountRegisterIdArt = $this->_createArticle('Registrierung', 'Registrierung', $myAccountIdCat, false, $configSetIDs['myAccountRegister']);

		/* Shop Struktur (Artikel) anlegen */
		if ($createShopStructure) {
			/* Produkte */
			$productsIdArt 			= $this->_createArticle('index', 'Produkte', $productsIdCat);
			$productAIdArt 			= $this->_createArticle('Produkt A', 'Produkt A', $productsIdCat, false);
			$productBIdArt 			= $this->_createArticle('Produkt B', 'Produkt B', $productsIdCat, false);

			/* Warenkorb */
			$cartIdArt 				= $this->_createArticle('index', 'Warenkorb', $cartIdCat, true, $configSetIDs['shopCart']);

			/* Kasse */
			$checkoutIndexIdArt 	= $this->_createArticle('index', 'Kasse', $checkoutIdCat, true, $configSetIDs['shopCheckout']);
			$checkoutDeliveryIdArt 	= $this->_createArticle('Lieferadresse', 'Lieferadresse', $checkoutIdCat, false, $configSetIDs['shopCheckoutDelivery']);
			$checkoutBillingIdart 	= $this->_createArticle('Rechnungsadresse', 'Rechnungsadresse', $checkoutIdCat, false, $configSetIDs['shopCheckoutBilling']);
			$checkoutPaymentIdart 	= $this->_createArticle('Zahlungsmittel', 'Zahlungsmittel', $checkoutIdCat, false, $configSetIDs['shopCheckoutPayment']);
			$checkoutOverviewIdart 	= $this->_createArticle('Übersicht', 'Übersicht', $checkoutIdCat, false, $configSetIDs['shopCheckoutOverview']);
			$checkoutSuccessIdArt 	= $this->_createArticle('Bestätigung', 'Bestätigung', $checkoutIdCat, false);
			$checkoutCancelIdArt 	= $this->_createArticle('Abbruch', 'Abbruch', $checkoutIdCat, false);
			$checkoutErrorIdArt 	= $this->_createArticle('Fehler', 'Fehler', $checkoutIdCat, false);
			$checkoutPendingIdArt 	= $this->_createArticle('In Bearbeitung', 'In Bearbeitung', $checkoutIdCat, false);
			$checkoutServiceIdArt 	= $this->_createArticle('Service', 'Service', $checkoutIdCat, false);

			/* AGB */
			$tacIdArt 				= $this->_createArticle('index', 'AGB', $tacIdCat);
			$conditionsIdArt 		= $this->_createArticle('Widerrufsbelehrung', 'Widerrufsbelehrung', $tacIdCat, false);

			/* Dummyinhalte erstellen */

			/* Bestellbestätigung */
			$this->createArticleContent($checkoutSuccessIdArt, '<p>Vielen Dank für Ihre Bestellung</p>');

			/* Widerrufsbelehrung */
			$this->createArticleContent($conditionsIdArt, '<p>Ich bin die Widerrufsbelehrung</p>');

			/* Produkte mit Daten befüllen */
			$article = Aitsu_Persistence_ArticleProperty::factory(Moraso_Util::getIdArtLang($productAIdArt));
			$article->load();
			$article->setValue('cart', 'sku', 'TstProA');
			$article->setValue('cart', 'price', '19.95', 'float');
			$article->setValue('cart', 'tax_class', 19);
			$article->save();

			$article = Aitsu_Persistence_ArticleProperty::factory(Moraso_Util::getIdArtLang($productBIdArt));
			$article->load();
			$article->setValue('cart', 'sku', 'TstProB');
			$article->setValue('cart', 'price', '39.95', 'float');
			$article->setValue('cart', 'tax_class', 7);
			$article->save();

			/* Shop E-Mail Konfiguration anlegen */
			Moraso_Db::put('_moraso_config', 'id', array(
				'config' 		=> 'default',
				'env' 			=> 'default',
				'identifier' 	=> 'mail.shop.alias',
				'value' 		=> 'shop'
			));

			Moraso_Db::put('_moraso_config', 'id', array(
				'config' 		=> 'default',
				'env' 			=> 'default',
				'identifier' 	=> 'mail.shop.name',
				'value' 		=> 'Onlineshop'
			));

			Moraso_Db::put('_moraso_config', 'id', array(
				'config' 		=> 'default',
				'env' 			=> 'default',
				'identifier' 	=> 'mail.shop.subject',
				'value' 		=> 'Bestellung im Onlineshop'
			));
		}

		$client = 'default';
		$env 	= 'default';

		$config = Moraso_Persistence_Config::factory();

		$config->setValue($client, $env, 'category.main', $mainIdCat);
		$config->setValue($client, $env, 'category.meta', $metaIdCat);
		$config->setValue($client, $env, 'category.system', $systemIdCat);

		$config->setValue($client, $env, 'article.mediaHolder', $mediaHolderIdArt);

		$config->setValue($client, $env, 'sys.errorpage', $errorIdArt);
		$config->setValue($client, $env, 'sys.startcat', $mainIdCat);
		$config->setValue($client, $env, 'sys.loginpage', $loginIdArt);

		$config->setValue($client, $env, 'navigation.main', $mainIdCat);
		$config->setValue($client, $env, 'navigation.meta', $metaIdCat);

		$config->setValue($client, $env, 'account.register', $myAccountRegisterIdArt);
		$config->setValue($client, $env, 'account.mydata', $myAccountDataIdCat);

		if ($createShopStructure) {
			/* Produkte */
			$config->setValue($client, $env, 'shop.structure.category.products', $productsIdCat);
			$config->setValue($client, $env, 'shop.structure.article.products.index', $productsIdArt);

			/* Warenkorb */
			$config->setValue($client, $env, 'shop.structure.category.cart', $cartIdCat);
			$config->setValue($client, $env, 'shop.structure.article.cart.index', $cartIdArt);

			/* Kasse */
			$config->setValue($client, $env, 'shop.structure.category.checkout', $checkoutIdCat);
			$config->setValue($client, $env, 'shop.structure.article.checkout.index', $checkoutIndexIdArt);
			$config->setValue($client, $env, 'shop.structure.article.checkout.delivery', $checkoutDeliveryIdArt);
			$config->setValue($client, $env, 'shop.structure.article.checkout.billing', $checkoutBillingIdart);
			$config->setValue($client, $env, 'shop.structure.article.checkout.payment', $checkoutPaymentIdart);
			$config->setValue($client, $env, 'shop.structure.article.checkout.overview', $checkoutOverviewIdart);
			$config->setValue($client, $env, 'shop.structure.article.checkout.success', $checkoutSuccessIdArt);
			$config->setValue($client, $env, 'shop.structure.article.checkout.cancel', $checkoutCancelIdArt);
			$config->setValue($client, $env, 'shop.structure.article.checkout.error', $checkoutErrorIdArt);
			$config->setValue($client, $env, 'shop.structure.article.checkout.pending', $checkoutPendingIdArt);
			$config->setValue($client, $env, 'shop.structure.article.checkout.service', $checkoutServiceIdArt);

			/* AGB */
			$config->setValue($client, $env, 'shop.structure.category.tac', $tacIdCat);
			$config->setValue($client, $env, 'shop.structure.article.tac.index', $tacIdArt);
			$config->setValue($client, $env, 'shop.structure.article.tac.conditions', $conditionsIdArt);
		}

		$config->setValue($client, $env, 'sys.webpath', 'http://' . $domain . '/');

		$config->save();
	}

	public function _createCategory($name, $createInIdCat = 0, $configsetid = null, $public = true)
	{
		$idcat 				= Aitsu_Persistence_Category::factory($createInIdCat)->insert(Aitsu_Registry::get()->env->idlang, $name);
		$category 			= Aitsu_Persistence_Category::factory($idcat);

		$category->load();

		$category->public 	= $public;
		$category->visible 	= true;

		if (!is_null($configsetid)) {
			$category->configsetid = $configsetid;
		}

		$category->save();

		return $idcat;
	}

	public function _createArticle($title, $pagetitle, $idcat, $index = true, $configsetid = null)
	{
		$art 			= Aitsu_Persistence_Article::factory();

		$art->title 	= $title;
		$art->pagetitle = $pagetitle;
		$art->online 	= 1;
		$art->idclient 	= 1;
		$art->idcat 	= $idcat;

		if (!is_null($configsetid)) {
			$art->configsetid = $configsetid;
		}

		$art->save();

		if ($index) {
			$art->setAsIndex();
		}

		return $art->idart;
	}

	public function createArticleContent($idart, $content)
	{
		Moraso_Db::put('_article_content', null, array(
			'idartlang' => Moraso_Util::getIdArtLang($idart),
			'index' 	=> 'Content',
			'value' 	=> $content
		));
	}
}