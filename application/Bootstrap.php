<?php

/**
 * @author Andreas Kummer, w3concepts AG
 * @copyright Copyright &copy; 2010, w3concepts AG
 */

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013 - 2014, webtischlerei <http://www.webtischlerei.de>
 */

set_include_path(realpath(dirname(__FILE__)) . PATH_SEPARATOR . get_include_path());
set_include_path(realpath(dirname(__FILE__) . '/..') . PATH_SEPARATOR . get_include_path());

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initDefaultTimezone()
	{
		date_default_timezone_set('Europe/Berlin');
	}

	protected function _initDoctype()
	{
		$this->bootstrap('view');
		$view = $this->getResource('view');
		$view->doctype('XHTML1_STRICT');
	}

	protected function _initServiceManger()
	{
		\Moraso\Service\Manager::init();
	}

	protected function _initRegistry()
	{
		\Moraso\Registry::init();
	}

	protected function _initDatabaseConnection()
	{
		\Moraso\Database\Connection::init();
	}

	protected function _initSetLogging()
	{
		set_error_handler(array('Moraso_Log', 'errorHandler'), E_ALL);
	}

	protected function _initBackendConfig()
	{
		\Moraso_Config::init();
	}

	protected function _initDatabase()
	{
		\Moraso\Database\Connection::init();
	}

	protected function _initSession()
	{
		\Moraso\Session::init();
	}

	protected function _initBackendPreInit()
	{
		Aitsu_Event::raise('backend.preInit', null);
	}

	protected function _initUser()
	{
		\Moraso\User::init();
	}

	protected function _initAcl()
	{
		\Moraso\Acl::init();
	}

	protected function _initRegisterPlugins()
	{
		\Moraso\Backend\RegisterPlugins::init();
	}

	protected function _initRouter()
	{
		\Moraso\Backend\Router::init();
	}

	protected function _initAppStatus()
	{
		Aitsu_Application_Status::isEdit(true);
		Aitsu_Application_Status::isPreview(true);
		Aitsu_Application_Status::setEnv('backend');
		Aitsu_Application_Status::lock();
	}

	protected function _initDisableCaching()
	{
		Moraso_Cache::disableBrowserCache();
	}
}