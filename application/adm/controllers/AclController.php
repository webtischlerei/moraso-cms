<?php
/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright 2014 webtischlerei
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @package Moraso
 */

class AclController extends Zend_Controller_Action
{
	public function loginAction()
	{
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$this->render('login');
	}

	public function logoutAction()
	{
		\Moraso\Session::destroy();

		$this->_redirect('/');
	}
}