<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */
class PluginController extends Zend_Controller_Action
{
	public function indexAction()
	{
		$request 		= $this->getRequest();

		$namespace 		= ucfirst($request->getParam('namespace'));
		$subnamespace   = ucfirst($request->getParam('subnamespace'));
		$plugin 		= ucfirst($request->getParam('plugin'));
		$parent_plugin 	= ucfirst($request->getParam('parent_plugin'));
		$area 			= ucfirst($request->getParam('area'));
		$paction 		= $request->getParam('paction');

		if (!empty($parent_plugin)) {
			$controller = $namespace . '_Plugin_' . $parent_plugin . '_' . $area . '_' . $plugin . '_';

			$pluginPath = APPLICATION_LIBPATH . '/' . $namespace . '/Plugin/' . $parent_plugin . '/' . $area . '/' . $plugin . '/';
		} elseif (empty($subnamespace)) {
			$controller = $namespace . '_Plugin_' . $plugin . '_' . $area . '_';

			$pluginPath = APPLICATION_LIBPATH . '/' . $namespace . '/Plugin/' . $plugin . '/' . $area . '/';
		} else {
			$controller = $namespace . '_' . $subnamespace . '_Plugin_' . $plugin . '_' . $area . '_';

			$pluginPath = APPLICATION_LIBPATH . '/' . $namespace . '/' . $subnamespace . '/Plugin/' . $plugin . '/' . $area . '/';
		}

		$this->_helper->viewRenderer->setNoRender();

		include_once ($pluginPath . 'Class.php');

		$this->view->setScriptPath(array($pluginPath . 'views/'));

		$request->setControllerName(ucfirst($controller));
		$request->setActionName($paction);
		$request->setDispatched(false);
	}
}