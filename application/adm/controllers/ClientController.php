<?php


/**
 * @author Andreas Kummer, w3concepts AG
 * @copyright Copyright &copy; 2010, w3concepts AG
 */

class ClientController extends Zend_Controller_Action {

	public function init() {

		if (!\Moraso\Acl::isAllowed('client', $this->getRequest()->getActionName())) {
			throw new Exception('Access denied');
		}
	}

	public function indexAction() {

		header("Content-type: text/javascript");
		$this->_helper->layout->disableLayout();
	}

	/**
	 * @since 2.1.0.0 - 23.12.2010
	 */
	public function deletelanguageAction() {

		$this->_helper->layout->disableLayout();

		Aitsu_Persistence_Language :: factory($this->getRequest()->getParam('idlang'))->remove();

		$this->_helper->json((object) array (
			'success' => true
		));
	}

	/**
	 * @since 2.1.0.0 - 23.12.2010
	 */
	public function editlanguageAction() {

		$this->_helper->layout->disableLayout();

		$id = $this->getRequest()->getParam('idlang');

		$form = Aitsu_Forms :: factory('editlanguage', APPLICATION_PATH . '/adm/forms/client/language.ini');
		$form->title = Aitsu_Translate :: translate('Edit language');
		$form->url = $this->view->url();

		if (!empty ($id)) {
			$data = Aitsu_Persistence_Language :: factory($id)->load()->toArray();
			$form->setValues($data);
		}

		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {
				$values = $form->getValues();

				$values['idclient'] = 1;

				/*
				 * Persist the data.
				 */
				if (empty ($id)) {
					/*
					 * New language.
					 */
					unset ($values['idlang']);
					Aitsu_Persistence_Language :: factory()->setValues($values)->save();
				} else {
					/*
					 * Update language.
					 */
					Aitsu_Persistence_Language :: factory($id)->load()->setValues($values)->save();
				}

				$this->_helper->json((object) array (
					'success' => true
				));
			} else {
				$this->_helper->json((object) array (
					'success' => false,
					'errors' => $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array (
				'success' => false,
				'exception' => true,
				'message' => $e->getMessage()
			));
		}
	}
}