<?php

/**
 * @author Andreas Kummer <a.kummer@wdrei.ch>
 * @copyright (c) 2013, Andreas Kummer
 *
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2014, Christian Kehres
 */
class TranslationController extends Zend_Controller_Action
{
	public function init()
	{
		if (
			!\Moraso\Acl::isAllowed('client.' . \Moraso\Session::get('currentClient'))
			 || 
			!\Moraso\Acl::isAllowed('language.' . \Moraso\Session::get('currentLanguage'))
			 || 
			!\Moraso\Acl::isAllowed('translation')
		) {
			throw new Exception('Access denied');
		}

		$this->_helper->layout->disableLayout();

		$this->_filter = Aitsu_Util_ExtJs::encodeFilters($this->getRequest()->getParam('filter'));
	}

	public function indexAction()
	{
		
	}

	public function storeAction()
	{
		$refresh = $this->getRequest()->getParam('refresh');

		if ($refresh == 1) {
			$currentLanguage = \Moraso\Session::get('currentLanguage');
			Aitsu_Translate::populate($currentLanguage);
		}

		$this->_helper->json((object) array(
			'data' => Aitsu_Persistence_Translate::getStore(10000, 0, $this->_filter)
		));
	}

	public function editAction()
	{
		$this->_helper->layout->disableLayout();

		$id 			= $this->getRequest()->getParam('translationid');

		$form 			= Aitsu_Forms::factory('edittranslation', APPLICATION_PATH . '/adm/forms/translation/translation.ini');
		$form->title 	= Aitsu_Translate::translate('Edit translation');
		$form->url 		= $this->view->url();

		$data = Aitsu_Persistence_Translate::factory($id)->load()->toArray();
		$form->setValues($data);

		if ($this->getRequest()->getParam('loader')) {
			$this->view->form = $form;
			header("Content-type: text/javascript");
			return;
		}

		try {
			if ($form->isValid()) {
				$values = $form->getValues();

				Aitsu_Persistence_Translate::factory($id)->load()->setValues($values)->save();

				$this->_helper->json((object) array(
					'success' => true
				));
			} else {
				$this->_helper->json((object) array(
					'success' 	=> false,
					'errors' 	=> $form->getErrors()
				));
			}
		} catch (Exception $e) {
			$this->_helper->json((object) array(
				'success' 	=> false,
				'exception' => true,
				'message' 	=> $e->getMessage()
			));
		}
	}

	public function deleteAction()
	{
		$this->_helper->layout->disableLayout();

		Aitsu_Persistence_Translate::factory($this->getRequest()->getParam('translationid'))->remove();

		$this->_helper->json((object) array(
			'success' => true
		));
	}
}