<?php


/**
 * @author Andreas Kummer, w3concepts AG
 * @copyright Copyright &copy; 2011, w3concepts AG
 */

class Adm_Script_Synchronize_Database_Structure extends Aitsu_Adm_Script_Abstract {

	protected $_methodMap = array ();
	protected $_xml = null;

	protected $_tableRestoreOffset = null;
	protected $_constraintRestoreOffset = null;

	public static function getName()
	{
		return 'System-Update durchführen';
	}

	protected function _init() {

		$this->_methodMap = array('_beforeStart');

		$this->_checkForOtherUpdatesBeforeDbUpdate();

		$this->_methodMap[] = '_restoreFunctions';
		$this->_methodMap[] = '_removeConstraints';
		$this->_methodMap[] = '_removeIndexes';
		$this->_methodMap[] = '_removeViews';
		$this->_methodMap[] = '_removeEmptyTables';

		$this->_xml = new DOMDocument();
		$this->_xml->loadXML('<database></database>');
		$xmls = Aitsu_Util_Dir::scan(APPLICATION_LIBPATH, 'database.xml');
		foreach ($xmls as $xml) {
			$dom = new DOMDocument();
			$dom->load($xml);
			foreach ($dom->childNodes as $node) {
				$node = $this->_xml->importNode($node, true);
				$this->_xml->documentElement->appendChild($node);
			}
		}

		$tables = $this->_xml->getElementsByTagName('table');

		$this->_tableRestoreOffset = count($this->_methodMap);
		for ($i = 0; $i < $tables->length; $i++) {
			$this->_methodMap[] = '_restoreTables';
		}

		$this->_constraintRestoreOffset = $this->_tableRestoreOffset + $tables->length;
		for ($i = 0; $i < $tables->length; $i++) {
			$this->_methodMap[] = '_restoreConstraints';
		}

		$this->_methodMap[] = '_restoreViews';
		$this->_methodMap[] = '_restoreTriggers';

		$this->_checkForOtherUpdatesAfterDbUpdate();

		$this->_methodMap[] = '_setVersionInfo';
	}

	protected function _beforeStart() {

		return 'The process may take quite a while. Please be patient.';
	}

	protected function _beforeRemoveIndexes() {

		return 'Removing indexes. This may take quite a while. Please be patient to allow to remove the indexes.';
	}

	protected function _hasNext()
	{
		if ($this->_currentStep < count($this->_methodMap)) {
			return true;
		}

		return false;
	}

	protected function _next() {

		return 'Next line to be executed.';
	}

	protected function _executeStep() {

		$method = $this->_methodMap[$this->_currentStep];
		$response = @call_user_func_array(array (
			$this,
			$method
		), array ());

		if (is_object($response)) {
			return Aitsu_Adm_Script_Response::factory($response->message, 'warning');
		}

		return Aitsu_Adm_Script_Response::factory($response);
	}

	protected function _removeConstraints() {

		$constraints = Moraso_Db::fetchAll('' .
		'select * from information_schema.table_constraints ' .
		'where ' .
		'	table_schema = :schema ' .
		'	and table_name like :prefix ' .
		'	and constraint_type = \'FOREIGN KEY\' ', array (
			':schema' => Moraso_Config::get('database.params.dbname'),
			':prefix' => Moraso_Config::get('database.params.tblprefix') . '%'
		));

		$startTime = time();
		foreach ($constraints as $constraint) {
			Moraso_Db::query('' .
			'alter table `' . $constraint['TABLE_NAME'] . '` drop foreign key `' . $constraint['CONSTRAINT_NAME'] . '`');

			if (time() - $startTime > 15) {
				throw new Aitsu_Adm_Script_Resume_Exception('Removement takes very long. The current step needs to be resumed.');
			}
		}

		return 'Constraints have been removed.';
	}

	protected function _removeIndexes() {

		$indexes = Moraso_Db::fetchAll('' .
		'select distinct ' .
		'	table_name, ' .
		'	index_name ' .
		'from information_schema.statistics ' .
		'where ' .
		'	table_schema = :schema ' .
		'	and table_name like :prefix ' .
		'	and index_name != \'PRIMARY\' ', array (
			':schema' => Moraso_Config::get('database.params.dbname'),
			':prefix' => Moraso_Config::get('database.params.tblprefix') . '%'
		));

		$startTime = time();
		foreach ($indexes as $index) {
			try {
				Moraso_Db::query('' .
				'alter table `' . $index['table_name'] . '` drop index `' . $index['index_name'] . '`');
			} catch (Exception $e) {
				/*
				 * Do nothing. Exceptions may occur if the table, the index belongs to, does
				 * no longer exist.
				 */
			}

			if (time() - $startTime > 15) {
				throw new Aitsu_Adm_Script_Resume_Exception('Removement takes very long. The current step needs to be resumed.');
			}
		}

		return 'Indexes have been removed.';
	}

	protected function _removeViews() {

		$views = Moraso_Db::fetchAll('' .
		'select * from information_schema.views ' .
		'where ' .
		'	table_schema = :schema ' .
		'	and table_name like :prefix ', array (
			':schema' => Moraso_Config::get('database.params.dbname'),
			':prefix' => Moraso_Config::get('database.params.tblprefix') . '%'
		));

		try {
			foreach ($views as $view) {
				Moraso_Db::query('' .
				'drop view `' . $view['TABLE_NAME'] . '`');
			}
		} catch (Exception $e) {
			return (object) array (
				'message' => 'Views have not been dropped due to insufficient privileges. If they are unchanged, they still might work properly. However, it is recommended to drop and recreate them using mysql command line interface.'
			);
		}

		return 'Views have been removed.';
	}

	protected function _removeEmptyTables() {

		$tables = Moraso_Db::fetchAll('' .
		'select * from information_schema.tables ' .
		'where ' .
		'	table_schema = :schema ' .
		'	and table_name like :prefix ', array (
			':schema' => Moraso_Config::get('database.params.dbname'),
			':prefix' => Moraso_Config::get('database.params.tblprefix') . '%'
		));

		foreach ($tables as $table) {
			if (Moraso_Db::fetchOne('' .
				'select count(*) from ' . $table['TABLE_NAME']) == 0) {
				Moraso_Db::query('' .
				'drop table `' . $table['TABLE_NAME'] . '`');
			}
		}

		return 'Empty tables have been removed.';
	}

	protected function _restoreTables() {

		$currentIndex = $this->_currentStep - $this->_tableRestoreOffset;
		$table = $this->_xml->getElementsByTagName('table')->item($currentIndex);

		if (Moraso_Db::fetchOne('' .
			'select count(*) from information_schema.tables ' .
			'where ' .
			'	table_schema = :schema ' .
			'	and table_name = :tablename', array (
				':schema' => Moraso_Config::get('database.params.dbname'),
				':tablename' => Moraso_Config::get('database.params.tblprefix') . $table->attributes->getNamedItem('name')->nodeValue
			)) == 0) {
			$this->_createTable($table);
		} else {
			$this->_reconstructTable($table);
		}

		$this->_restoreIndexes($table);

		$this->_restoreData($table);

		return sprintf('Table %s has been restored.', $table->attributes->getNamedItem('name')->nodeValue);
	}

	protected function _restoreConstraints() {

		$currentIndex = $this->_currentStep - $this->_constraintRestoreOffset;
		$table = $this->_xml->getElementsByTagName('table')->item($currentIndex);

		$prefix = Moraso_Config::get('database.params.tblprefix');

		$tableName = $prefix . $table->getAttribute('name');
		foreach ($table->getElementsByTagName('field') as $field) {
			$columnName = $field->getAttribute('name');
			foreach ($field->getElementsByTagName('constraint') as $constraint) {
				$refTable = $prefix . $constraint->getAttribute('table');
				$refColumn = $constraint->getAttribute('column');
				$onUpdate = $constraint->hasAttribute('onupdate') ? $constraint->getAttribute('onupdate') : 'no action';
				$onDelete = $constraint->hasAttribute('ondelete') ? $constraint->getAttribute('ondelete') : 'no action';
				$indexName = 'fk_' . $columnName . '_' . $refColumn;

				/*
				 * Add an index for the specified column.
				 */
				Moraso_Db::query("alter table $tableName add index `$indexName` (`$columnName`)");

				if ($constraint->hasAttribute('ondelete') && $constraint->getAttribute('ondelete') == 'set null') {
					/*
					 * Set values to null that would case a referential integrity violation.
					 */
					Moraso_Db::query("" .
					"update `$tableName` src " .
					"set src.$columnName = null " .
					"where src.$columnName not in (" .
					"	select tgt.$refColumn from `$refTable` tgt" .
					")");
				} else {
					/*
					 * Remove entries that would cause a referential integrity violation.
					 */
					Moraso_Db::query("" .
					"delete src.* from `$tableName` src " .
					"where src.$columnName not in (" .
					"	select tgt.$refColumn from `$refTable` tgt" .
					")");
				}

				/*
				 * Add the constraint.
				 */
				Moraso_Db::query("" .
				"alter table `$tableName` add foreign key (`$columnName`) " .
				"references `$refTable` (`$refColumn`) " .
				"on delete $onDelete " .
				"on update $onUpdate");
			}
		}

		return sprintf('Constraints on %s have been restored.', $tableName);
	}

	protected function _restoreViews() {

		$failures = false;

		$prefix = Moraso_Config::get('database.params.tblprefix');

		foreach ($this->_xml->getElementsByTagName('view') as $view) {
			$viewName = $prefix . $view->getAttribute('name');
			$viewSelect = $view->nodeValue;
			try {
				Moraso_Db::query("create view `$viewName` as $viewSelect");
			} catch (Exception $e) {
				$failures = true;
			}
		}

		if ($failures) {
			return (object) array (
				'message' => 'Views have not been completely restored due to insufficient privilegies or missing function. You have to add them using the mysql command line interface.'
			);
		}

		return 'Views have been restored.';
	}

	protected function _restoreFunctions() {

		try {
			foreach ($this->_xml->getElementsByTagName('function') as $function) {
				Moraso_Db::getDb()->getConnection()->query('drop function if exists ' . $function->getAttribute('name'));
				Moraso_Db::getDb()->getConnection()->query('drop procedure if exists ' . $function->getAttribute('name'));
				Moraso_Db::getDb()->getConnection()->query($function->nodeValue);
			}
		} catch (Exception $e) {
			return (object) array (
				'message' => 'Functions have not been restored due to insufficient privilegies. You have to add them using the mysql command line interface.'
			);
		}

		return 'Functions have been restored.';
	}

	protected function _restoreTriggers() {

		try {
			foreach ($this->_xml->getElementsByTagName('trigger') as $trigger) {
				try {
					Moraso_Db::getDb()->getConnection()->query('drop trigger ' . $trigger->getAttribute('trigger'));
				} catch (Exception $e) {
					/*
					 * Do nothing. If the trigger does not exist, an exception is thrown and
					 * catched.
					 */
				}
				Moraso_Db::getDb()->getConnection()->query(Moraso_Db::prefix($trigger->nodeValue));
			}
		} catch (Exception $e) {
			trigger_error($e->getMessage());
			return (object) array (
				'message' => 'Triggers have not been restored due to insufficient privilegies. You have to add them using the mysql command line interface.'
			);
		}

		return 'Triggers have been restored.';
	}

	protected function _createTable($node) {

		$statement = 'CREATE TABLE `' . Moraso_Config::get('database.params.tblprefix') . $node->attributes->getNamedItem('name')->nodeValue . '` (';

		$defaultExpressions = array (
			'current_timestamp'
		);

		$primaryKeys = array ();
		$fields = array ();
		foreach ($node->getElementsByTagName('field') as $field) {
			$name = $field->getAttribute('name');
			$type = $field->getAttribute('type');

			if ($field->hasAttribute('nullable')) {
				$null = $field->getAttribute('nullable') == 'true' ? 'null' : 'not null';
			} else {
				$null = 'null';
			}

			if (in_array($field->getAttribute('default'), $defaultExpressions)) {
				$default = "default " . $field->getAttribute('default');
			} else {
				$default = $field->getAttribute('default') == 'null' || $field->getAttribute('type') == 'text' ? '' : "default '" . $field->getAttribute('default') . "'";
			}
			$autoincrement = $field->hasAttribute('autoincrement') && $field->getAttribute('autoincrement') == 'true' ? 'auto_increment' : '';
			$comment = $field->hasAttribute('comment') ? "comment '" . str_replace("'", "''", $field->getAttribute('comment')) . "'" : '';

			$tmp = "`$name` $type $null $default $autoincrement $comment";

			$tmp = str_replace("'CURRENT_TIMESTAMP'", 'current_timestamp', $tmp);

			$fields[] = $tmp;

			if ($field->hasAttribute('primary') && $field->getAttribute('primary') == 'true') {
				$primaryKeys[] = $name;
			}
		}

		$statement .= implode(',', $fields);

		if (count($primaryKeys) > 0) {
			$statement .= ', PRIMARY KEY (`' . implode('`,`', $primaryKeys) . '`)';
		}

		$statement .= ') ENGINE=' . $node->attributes->getNamedItem('engine')->nodeValue;

		$statement .= ' COMMENT=\'Since ' . $node->getAttribute('since') . '\'';

		Moraso_Db::query($statement);
	}

	protected function _restoreIndexes($table) {

		$tableName = Moraso_Config::get('database.params.tblprefix') . $table->getAttribute('name');

		foreach ($table->getElementsByTagName('index') as $index) {
			$type = $index->hasAttribute('type') ? $index->getAttribute('type') : 'index';
			$name = $index->hasAttribute('name') ? '`' . $index->getAttribute('name') . '`' : '';
			$columns = $index->getAttribute('columns');
			Moraso_Db::query("alter table $tableName add $type $name ($columns)");
		}
	}

	protected function _restoreData($table) {

		$tableName = Moraso_Config::get('database.params.tblprefix') . $table->getAttribute('name');

		foreach ($table->getElementsByTagName('dataset') as $dataset) {
			$use = $dataset->getAttribute('use');
			if ($use == 'ifempty') {
				/*
				 * We have to quit if the table is not empty.
				 */
				if (Moraso_Db::fetchOne('select * from ' . $tableName) > 0) {
					return;
				}
			}
			elseif ($use == 'replace') {
				/*
				 * We have to delete the content of the table.
				 */
				Moraso_Db::query('delete from ' . $tableName);
			}
			foreach ($dataset->getElementsByTagName('record') as $record) {
				$fields = array ();
				$valRef = array ();
				$values = array ();
				foreach ($record->getElementsByTagName('value') as $value) {
					$field = $value->getAttribute('attribute');
					$fields[] = $field;
					$valRef[] = ':' . $field;
					$values[':' . $field] = $value->nodeValue;
				}
				Moraso_Db::query('' .
				'replace into ' . $tableName . ' (' . implode(', ', $fields) . ') values (' . implode(', ', $valRef) . ')', $values);
			}
		}
	}

	protected function _reconstructTable($table)
	{
		$schema 		= Moraso_Config::get('database.params.dbname');
		$backupTable 	= Moraso_Config::get('database.params.tblprefix') . 'backup_table';
		$tableName 		= Moraso_Config::get('database.params.tblprefix') . $table->getAttribute('name');

		Moraso_Db::query('drop table if exists ' . $backupTable);
		Moraso_Db::query('rename table ' . $tableName . ' to ' . $backupTable);
		$this->_createTable($table);

		/*
		 * Identify the field intersection to be used to move data to the new structure.
		 */
		$intersection = Moraso_Db::fetchCol('' .
		'select ' .
		'	concat(\'`\', newtable.column_name, \'`\') ' .
		'from information_schema.columns newtable, information_schema.columns oldtable ' .
		'where ' .
		'	newtable.table_schema = oldtable.table_schema ' .
		'	and newtable.column_name = oldtable.column_name ' .
		'	and newtable.table_schema = :schema ' .
		'	and newtable.table_name = :newtable ' .
		'	and oldtable.table_name = :oldtable ', array (
			':schema' => $schema,
			':newtable' => $tableName,
			':oldtable' => $backupTable
		));

		/*
		 * Move data from backup table to the new structure.
		 */
		Moraso_Db::query("" .
		"insert into $tableName (" . implode(', ', $intersection) . ") " .
		"select " . implode(', ', $intersection) . " from $backupTable ");

		Moraso_Db::query('drop table ' . $backupTable);
	}

	protected function _checkForOtherUpdatesBeforeDbUpdate()
	{
		$versionBeforeUpdate 	= $this->getVersionInfo();
		$thisVersion 			= Moraso_Status::version();

		if ($thisVersion !== 'DEVELOPMENT') {
			$updates = array();

			// Die letzte Version bevor dieses Verfahren eingeführt wurde
			if (empty($versionBeforeUpdate)) {
				$versionBeforeUpdate = '1.51.8-302';
			}

			$updates[] = array(
				'before' 	=> '1.52',
				'this' 		=> '1.52',
				'script' 	=> 'DeleteOldAclTables'
			);

			foreach ($updates as $update) {
				if (
					version_compare($thisVersion, $update['this']) >= 0
					&&
					version_compare($versionBeforeUpdate, $update['before']) < 0
				) {
					$this->_methodMap[] = '_updateScript' . $update['script'];
				}
			}
		}
	}

	// kleiner 1.52 auf 1.52
	protected function _updateScriptDeleteOldAclTables()
	{
		$tables = array('clients', 'languages', 'privilege', 'privilege', 'resource', 'resources', 'role', 'roles', 'user');

		Moraso_Db::query('SET foreign_key_checks = 0');
		foreach ($tables as $table) {
			Moraso_Db::query('DROP TABLE IF EXISTS `_acl_' . $table . '`');
		}
		Moraso_Db::query('SET foreign_key_checks = 1');

		return 'Die alten ACL-Tabellen wurden entfernt!';
	}

	protected function _checkForOtherUpdatesAfterDbUpdate()
	{
		$versionBeforeUpdate 	= $this->getVersionInfo();
		$thisVersion 			= Moraso_Status::version();

		if ($thisVersion !== 'DEVELOPMENT') {
			$updates = array();

			$updates[] = array(
				'before' 	=> '1.52',
				'this' 		=> '1.52',
				'script' 	=> 'CreateNewAdmin'
			);

			if (!empty($versionBeforeUpdate)) {
				$updates[] = array(
					'before' 		=> '1.53.12',
					'this' 			=> '1.53.12',
					'script' 		=> 'CreateSystemMediaHolderArticle'
				);
			}

			// Die letzte Version bevor dieses Verfahren eingeführt wurde
			if (empty($versionBeforeUpdate)) {
				$versionBeforeUpdate = '1.51.8-302';
			}

			foreach ($updates as $update) {
				if (
					version_compare($thisVersion, $update['this']) >= 0
					&&
					version_compare($versionBeforeUpdate, $update['before']) < 0
				) {
					$this->_methodMap[] = '_updateScript' . $update['script'];
				}
			}
		}
	}

	// kleiner 1.53.12 auf 1.53.12 oder höher
	protected function _updateScriptCreateSystemMediaHolderArticle()
	{
		$idcat = Moraso_Config::get('category.system');

		if (empty($idcat)) {
			$idcat = 3;
		}

		if (empty(Aitsu_Registry::get()->env->idlang)) {
			Aitsu_Registry::get()->env->idlang = 1;
		}

		$art 			= Aitsu_Persistence_Article::factory();
		$art->title 	= 'mediaHolder';
		$art->pagetitle = 'Datenträger für globale Medien';
		$art->online 	= 1;
		$art->idclient 	= 1;
		$art->idcat 	= $idcat;
		$art->save();

		$config = Moraso_Persistence_Config::factory();
		$config->setValue('default', 'default', 'article.mediaHolder', $art->idart);
		$config->save();

		return 'Datenträger für globale Medien wurde angelegt!';
	}

	// kleiner 1.52 auf 1.52 oder höher
	protected function _updateScriptCreateNewAdmin()
	{
		$userMapper 			= new \Moraso\Model\User\Mapper();
		$userAuthMapper 		= new \Moraso\Model\User\Auth\Mapper();
		$userHasAclRoleMapper 	= new \Moraso\Model\User\Has\Acl\Role\Mapper();

		// Benutzer anlegen
		$user 				= $userMapper->create();
		$user->salutation 	= 'Herr';
		$user->firstname 	= 'Admin';
		$user->lastname 	= 'Admin';
		$user->email 		= 'admin@admin.de';

		// Benutzer speichern
		$userMapper->save($user);

		// Authentifizierung anlegen
		$auth 			= $userAuthMapper->create();
		$auth->user_id 	= $user->id;
		$auth->status 	= 1;
		$auth->username = 'admin';
		$auth->password = 'admin';

		// Authentifizierung speichern
		$userAuthMapper->save($auth, null, true);

		// Benutzer der Administratoren-Rolle zuweisen
		$role 				= $userHasAclRoleMapper->create();
		$role->id_user 		= $user->id;
		$role->id_acl_role 	= 1;

		$userHasAclRoleMapper->save($role);

		return 'Der Administrator wurde angelegt! Benutzername und Kennwort lautet "admin"!';
	}

	protected function _setVersionInfo()
	{
		$versionConfig = array(
			'config' 		=> 'default',
			'env' 			=> 'default',
			'identifier' 	=> 'system.version'
		);

		$versionConfig['id'] 	= Moraso_Db::simpleFetch('id', '_moraso_config', $versionConfig);
		$versionConfig['value'] = Moraso_Status::version();

		Moraso_Db::put('_moraso_config', 'id', $versionConfig);

		return 'Aktuelle Versions-Info auf "' . $versionConfig['value'] . '" gesetzt.';
	}

	protected function getVersionInfo()
	{
		$tableExists = Moraso_Db::fetchOne("
			SELECT count(*) AS count
			FROM information_schema.TABLES
			WHERE (TABLE_SCHEMA = '" . Aitsu_Registry::get()->config->database->params->dbname . "') AND (TABLE_NAME = '_moraso_config')
		");

		if ($tableExists) {
			$version = Moraso_Db::simpleFetch('value', '_moraso_config', array(
				'config' 		=> 'default',
				'env' 			=> 'default',
				'identifier' 	=> 'system.version'
			));
		} else {
			$version = null;
		}

		return $version;
	}
}