<?php

/**
 * @author Christian Kehres <c.kehres@webtischlerei.de>
 * @copyright (c) 2013, webtischlerei <http://www.webtischlerei.de>
 */

namespace Moraso;

define('REQUEST_START', microtime(true));
define('ROOT_PATH', dirname(__FILE__));

require 'vendor/autoload.php';

Bootstrap::run();