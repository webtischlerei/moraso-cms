<?php

/**
 * @author Andreas Kummer, w3concepts AG
 * @copyright Copyright &copy; 2010, w3concepts AG
 */
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
define('APPLICATION_ENV', 'development');

define('ROOT_PATH', realpath(APPLICATION_PATH . '/../'));
define('LIBRARY_PATH', ROOT_PATH . '/library');

// OLDSCHOOL
define('APPLICATION_LIBPATH', LIBRARY_PATH);

set_include_path(implode(PATH_SEPARATOR, array(ROOT_PATH, get_include_path())));
set_include_path(implode(PATH_SEPARATOR, array(LIBRARY_PATH, get_include_path())));

require 'vendor/autoload.php';

$compatible = true;

if (
	ini_get('magic_quotes_gpc')
	 || 
	ini_get('magic_quotes_runtime')
	 || 
	ini_get('magic_quotes_sybase')
	 || 
	version_compare(PHP_VERSION, '5.3.23') < 0
) {
    $compatible = false;
}

if (!$compatible) {
	echo '<h1>Fehler</h1>';

	echo '<table>';
		echo '<thead>';
			echo '<tr>';
				echo '<th>Wert</th>';
				echo '<th>Voraussetzung</th>';
				echo '<th>Dieser Server</th>';
			echo '</tr>';
		echo '</thead>';
		echo '<tbody>';
			echo '<tr>';
				echo '<td>MagicQuotes (gpc)</td>';
				echo '<td>deaktiviert</td>';
				echo '<td>' . (ini_get('magic_quotes_gpc') ? 'aktiviert' : 'deaktiviert') . '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>MagicQuotes (runtime)</td>';
				echo '<td>deaktiviert</td>';
				echo '<td>' . (ini_get('magic_quotes_runtime') ? 'aktiviert' : 'deaktiviert') . '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>MagicQuotes (sybase)</td>';
				echo '<td>deaktiviert</td>';
				echo '<td>' . (ini_get('magic_quotes_sybase') ? 'aktiviert' : 'deaktiviert') . '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>PHP-Version</td>';
				echo '<td>5.3.23</td>';
				echo '<td>' . PHP_VERSION . '</td>';
			echo '</tr>';
		echo '</tbody>';
	echo '</table>';
} else {
	$application = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/admin.ini');
	$application->bootstrap()->run();
}