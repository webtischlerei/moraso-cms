var modalAnimationSpeed 	= 500;

var loading 				= $('#loading');
var loginModal 				= $('#loginModal');
var loginForm 				= $('#login_form');
var loginFormSubmitButton 	= $('#login_form_submit_button');
var extraSecurity 			= $('#extra_security');

$(loading).hide();

Foundation.set_namespace = function() {};
$(document).foundation({
	reveal: {
		close_on_background_click: false,
		animation_speed: modalAnimationSpeed
	}
});

var opts = {
  lines: 20, // The number of lines to draw
  length: 0, // The length of each line
  width: 8, // The line thickness
  radius: 125, // The radius of the inner circle
  corners: 1, // Corner roundness (0..1)
  rotate: 90, // The rotation offset
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: '#ecf0f1', // #rgb or #rrggbb or array of colors
  speed: 0.5, // Rounds per second
  trail: 25, // Afterglow percentage
  shadow: false, // Whether to render a shadow
  hwaccel: true, // Whether to use hardware acceleration
  className: 'spinner', // The CSS class to assign to the spinner
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  top: 'auto', // Top position relative to parent in px
  left: 'auto' // Left position relative to parent in px
};

$(loading).spin(opts);

$(loginModal).foundation('reveal', 'open');

$(loginFormSubmitButton).click(function(e) {
	e.preventDefault();

	$(loading).fadeIn(750);

	$(loginModal).foundation('reveal', 'close');

	setTimeout(function() {
		$.post(window.location.pathname, $(loginForm).serialize(), function(data) {
			setTimeout(function() {
				$(loading).fadeOut(750);

				if (data.success) {
					$(loginForm).submit();
				} else {
					setTimeout(function() {
						$(extraSecurity).show();
						$(loginModal).foundation('reveal', 'open');
					}, 250);
				}
			}, 500);
		});
	}, modalAnimationSpeed+50);
});