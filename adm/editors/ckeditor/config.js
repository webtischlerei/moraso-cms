CKEDITOR.editorConfig = function(config) {
	config.extraPlugins = 'shortcodes,fontawesome';

	config.allowedContent = true;

	config.contentsCss = '/adm/css/font-awesome.css';
};

CKEDITOR.dtd.$removeEmpty['i'] = false;