CKEDITOR.plugins.add('shortcodes',
{   
	requires : ['richcombo'],
	
	init : function(editor)
	{
		var shortcodes = [];       
		$.post('../?renderOnly=Shortcodes.Dropdown', function(data) {
			$.each(data, function(index, shortcode) {
				shortcodes.push([shortcode.shortcode, shortcode.label]);
			});
		}, 'json');

		editor.ui.addRichCombo('shortcodes',
		{
			label : 'Module',
			title : 'Module',
			voiceLabel : 'Module',
			className : 'cke_format',
			multiSelect : false,

			panel :
			{
				css: [editor.config.contentsCss, CKEDITOR.skin.getPath('editor')],
				voiceLabel: editor.lang.panelVoiceLabel
			},

			init : function()
			{
				this.startGroup("verfügbare Module");

				var length = shortcodes.length;
				for (var i = 0; i < length; i++) {
					shortcode = shortcodes[i];
					this.add(shortcode[0], shortcode[1], shortcode[1]);
				}
			},

			onClick : function(value)
			{   
				var random = Math.round(10000+99999*Math.random()*10);
				
				editor.focus();
				editor.fire('saveSnapshot');
				
				value = value.replace(/INDEX/g, random);
				value = value.replace(/\[/g, '_[');
				
				editor.insertHtml(value);
				editor.fire('saveSnapshot');
			}
		});
	}
});
