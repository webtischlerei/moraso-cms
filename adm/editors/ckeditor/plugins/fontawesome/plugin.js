CKEDITOR.plugins.add('fontawesome', {
	icons: 'fontawesome',
	init: function(editor) {
		editor.addCommand('fontawesomeDialog', new CKEDITOR.dialogCommand('fontawesomeDialog'));

		editor.ui.addButton('fontawesome', {
			label: 'Insert Font Awesome Icon',
			command: 'fontawesomeDialog'
		});

		CKEDITOR.dialog.add('fontawesomeDialog', this.path + 'dialogs/fontawesome.js');
	}
});