CKEDITOR.dialog.add('fontawesomeDialog', function (editor) {
	var dialog,
		columns = 5;

	var onChoice = function(iconWrapper) {
		var target 	= new CKEDITOR.dom.element(iconWrapper);

		var value 	= target.$.firstChild.className;

		dialog.hide();

		editor.insertHtml('<i class="' + value + '"></i>');
	};

	var onClick = CKEDITOR.tools.addFunction(onChoice);

	$.getJSON('/adm/editors/ckeditor/plugins/fontawesome/fonts.json', function(icons) {
		categories = icons;
	});

	return {
		title: 'Font Awesome Icons',
		minWidth: 800,
		minHeight: 200,
		buttons: [CKEDITOR.dialog.cancelButton],

		onLoad: function(event) {
			container = this;

			$.each(categories, function(categoryName, icons) {
				html = [];
				i = 0;

				html.push('<table role="listbox" style="width: 100%; max-height: 200px; border-collapse: separate;" align="center" cellspacing="2" cellpadding="2" border="0">');
				html.push('<tbody style="display: block; height: 200px; overflow-y: scroll">');

				$.each(icons, function(index, icon) {
					if (i % columns === 0) {
						html.push('<tr>');
					}

					html.push('<td style="padding: 5px; width: 25%">');
					html.push('<a href="javascript: void(0);" onclick="CKEDITOR.tools.callFunction(' + onClick + ', this); return false;">');
					html.push('<i class="fa fa-' + icon.id + '"></i> <span>' + icon.name + '</span>');
					html.push('</a>');
					html.push('</td>');

					if (i % columns == columns - 1) {
						html.push( '</tr>' );
					}

					i++;
				});

				html.push('</tbody>');
				html.push('</table>');

				container.getContentElement(categoryName, 'iconContainer').getElement().setHtml(html.join(''));
			});
		},

		contents: [
			{
				id: 'Web Application Icons',
				label: 'Web Application',
				elements: [
					{
						type: 'html',
						id: 'iconContainer',
						html: '',
						onLoad: function( event ) {
							dialog = event.sender;
						}
					}
				]
			},
			{
				id: 'File Type Icons',
				label: 'File Type',
				elements: [
					{
						type: 'html',
						id: 'iconContainer',
						html: '',
						onLoad: function( event ) {
							dialog = event.sender;
						}
					}
				]
			},
			{
				id: 'Spinner Icons',
				label: 'Spinner',
				elements: [
					{
						type: 'html',
						id: 'iconContainer',
						html: '',
						onLoad: function( event ) {
							dialog = event.sender;
						}
					}
				]
			},
			{
				id: 'Form Control Icons',
				label: 'Form Control',
				elements: [
					{
						type: 'html',
						id: 'iconContainer',
						html: '',
						onLoad: function( event ) {
							dialog = event.sender;
						}
					}
				]
			},
			{
				id: 'Currency Icons',
				label: 'Currency',
				elements: [
					{
						type: 'html',
						id: 'iconContainer',
						html: '',
						onLoad: function( event ) {
							dialog = event.sender;
						}
					}
				]
			},
			{
				id: 'Text Editor Icons',
				label: 'Text Editor',
				elements: [
					{
						type: 'html',
						id: 'iconContainer',
						html: '',
						onLoad: function( event ) {
							dialog = event.sender;
						}
					}
				]
			},
			{
				id: 'Directional Icons',
				label: 'Directional',
				elements: [
					{
						type: 'html',
						id: 'iconContainer',
						html: '',
						onLoad: function( event ) {
							dialog = event.sender;
						}
					}
				]
			},
			{
				id: 'Video Player Icons',
				label: 'Video Player',
				elements: [
					{
						type: 'html',
						id: 'iconContainer',
						html: '',
						onLoad: function( event ) {
							dialog = event.sender;
						}
					}
				]
			},
			{
				id: 'Brand Icons',
				label: 'Brand',
				elements: [
					{
						type: 'html',
						id: 'iconContainer',
						html: '',
						onLoad: function( event ) {
							dialog = event.sender;
						}
					}
				]
			},
			{
				id: 'Medical Icons',
				label: 'Medical',
				elements: [
					{
						type: 'html',
						id: 'iconContainer',
						html: '',
						onLoad: function( event ) {
							dialog = event.sender;
						}
					}
				]
			}
		]
	};
});