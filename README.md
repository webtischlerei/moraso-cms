# moraso cms (basierend auf dem aitsu CMSF)

moraso: **v1.57.0**

Dieses CMS basiert auf dem aitsu CMSF **v2.7.1**!

- - -
** Ein händisches pull dieses Repositorys ist zur Installation des moraso cms nicht nötig! **
** Zum Download und Setup wird ein automatisiertes Script genutzt! **
- - -

## Installation
Die Installation **muss** über die **Konsole** gestartet werden! Alle hier genannten Befehle müssen in die **Konsole** eingegeben werden!

Wir empfehlen während der Installation das Versionsverwaltung Git zu wählen. Updates sind so auf dem einfachsten Wege einzuspielen.
Sollte sich, wie empfohlen, für Git entschieden worden sein so muss Git bereits installiert und konfiguriert vorliegen!

Vor der Installation werden einige Fragen bzgl. Datenbank- und Admin-Zugang gestellt, diese mit den passenden Werten beantworten,
danach läuft die Installation selbständig durch.

### Folgende Werte sollten sich bereit gelegt werden:
+ Datenbank - Host
+ Datenbank - Name
+ Datenbank - User
+ Datenbank - Passwort
+ Domain

### Installation starten
+ wget http://installer.moraso.de -O install; php install

### Installation abgeschlossen
Sobald die Installation abgeschlossen ist kann man sich unter **http:/www.example.de/adm/** in das moraso cms einloggen und direkt anfangen die gewünschte bzw. benötigte Seitenstruktur anzulegen.

PS: Die Installationsdatei wird nach dem Installationsvorgang automatisch entfernt!

# Problembehebungen

## Ubuntu 12.04 - PHP 5.3.10 (ohne Plesk)
Wenn man einen Server mit Ubuntu 12.04 LTS laufen hat, hat man das Problem das dort PHP nur in Version 5.3.10 vorliegt, wir benötigen aber zum Betrieb von moraso mindestens die Version 5.3.23. Hier eine Anleitung wie man per Konsole eine neuere PHP Version installiert.

+ apt-get install python-software-properties
+ add-apt-repository ppa:ondrej/php5-oldstable
+ apt-get update
+ apt-get install php5

Dann noch den Server neustarten und es sollte wieder alles Problemlos laufen

+ service apache2 restart

Mit **php -v** sieht man dann das alles wunderbar geklappt haben sollte.

## Ubuntu 12.04 - PHP 5.3.10 (mit Plesk)
Hat man auf seinem Server auch noch Plesk laufen wird das oben beschriebene Verfahren wohl Probleme bereiten.

Aber hier gibt es die Möglichkeit mehrere PHP-Versionen parallel zu installieren.

Erst einmal müssen wir die Sachen installieren die uns Standardmäßig fehlen um PHP zu installieren.

+ apt-get install build-essential libxml2-dev libcurl4-openssl-dev pkg-config libbz2-dev libxml2-dev libjpeg-dev libpng-dev freetype* libgmp-dev libc-client-dev libxpm-dev libmcrypt-dev

Noch kurz einen SymLink setzen

+ ln -s /usr/lib/libc-client.a /usr/lib/x86_64-linux-gnu/libc-client.a

Nun besorgen wir uns eine neue PHP-Version, in diesem Fall die 5.5.10.

+ cd /usr/local/src
+ mkdir php
+ cd php
+ wget http://www.php.net/get/php-5.5.10.tar.gz/from/at2.php.net/mirror -O php-5.5.10.tar.gz
+ tar xzvf php-5.5.10.tar.gz
+ rm php-5.5.10.tar.gz
+ cd php-5.5.10

Nun geht es ans kompilieren!

	./configure \
	--prefix=/usr/local/php-5.5.10 \
	--with-libdir=/lib/x86_64-linux-gnu \
	--with-zlib-dir \
	--with-freetype-dir \
	--with-libxml-dir=/usr \
	--with-curl \
	--with-mcrypt \
	--with-zlib \
	--with-gd \
	--with-bz2 \
	--with-zlib \
	--with-mhash \
	--with-pcre-regex \
	--with-mysql \
	--with-pdo-mysql \
	--with-mysqli \
	--with-jpeg-dir=/usr \
	--with-png-dir=/usr \
	--with-openssl \
	--with-imap \
	--with-imap-ssl \
	--with-kerberos \
	--with-gettext \
	--disable-rpath \
	--enable-zip \
	--enable-mbstring \
	--enable-soap \
	--enable-calendar \
	--enable-inline-optimization \
	--enable-sockets \
	--enable-sysvsem \
	--enable-sysvshm \
	--enable-pcntl \
	--enable-mbregex \
	--enable-ftp \
	--enable-gd-native-ttf

+ make
+ make install
+ cp php.ini-development /usr/local/lib/php-5.5.10.ini

In der .ini setzen wir nun noch den include_path

+ vi /usr/local/lib/php-5.5.10.ini

	include_path = ".:/php/includes:/usr/local/php-5.5.10/lib/php"

Nun müssen wir Plesk nur noch mitteilen das eine neue PHP-Version zur Verfügung steht.

+ /usr/local/psa/bin/php_handler --add -displayname 5.5.10 -path /usr/local/php-5.5.10/bin/php-cgi -phpini /usr/local/lib/php-5.5.10.ini -type fastcgi -id 5.5.10

# The MIT License (MIT)

## Copyright 2013 - 2014 webtischlerei

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.